﻿CREATE TABLE [themeshop].[ThemeReplyComments] (
    [CommentReplyId] BIGINT         IDENTITY (1, 1) NOT NULL,
    [CommentId]      BIGINT         NOT NULL,
    [ThemeId]        INT            NOT NULL,
    [UserId]         INT            NOT NULL,
    [Comment]        NVARCHAR (800) NOT NULL,
    [CreateDate]     DATETIME       NOT NULL,
    [IsSellerReply]  BIT            NOT NULL,
    [IsDelete]       BIT            NOT NULL,
    [IsRead]         BIT            NOT NULL,
    CONSTRAINT [PK_ThemeReplyComments] PRIMARY KEY CLUSTERED ([CommentReplyId] ASC),
    CONSTRAINT [FK_ThemeReplyComments_ThemeComments] FOREIGN KEY ([CommentId]) REFERENCES [themeshop].[ThemeComments] ([CommentId]),
    CONSTRAINT [FK_ThemeReplyComments_Themes] FOREIGN KEY ([ThemeId]) REFERENCES [themeshop].[Themes] ([ThemeId])
);

