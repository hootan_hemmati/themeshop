﻿CREATE TABLE [themeshop].[DiscountLogs] (
    [DiscountLogId]       INT            IDENTITY (1, 1) NOT NULL,
    [LogTime]             DATETIME       NOT NULL,
    [IsUsed]              BIT            NOT NULL,
    [UserId]              INT            NOT NULL,
    [Text]                NVARCHAR (MAX) NULL,
    [Discount_DiscountId] INT            NULL,
    CONSTRAINT [PK_dbo.DiscountLogs] PRIMARY KEY CLUSTERED ([DiscountLogId] ASC),
    CONSTRAINT [FK_dbo.DiscountLogs_dbo.Discounts_Discount_DiscountId] FOREIGN KEY ([Discount_DiscountId]) REFERENCES [themeshop].[Discounts] ([DiscountId])
);

