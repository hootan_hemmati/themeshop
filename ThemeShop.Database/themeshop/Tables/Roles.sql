﻿CREATE TABLE [themeshop].[Roles] (
    [RoleId]        INT            IDENTITY (1, 1) NOT NULL,
    [RoleTitle]     NVARCHAR (MAX) NULL,
    [IsDefualtRole] BIT            NOT NULL,
    CONSTRAINT [PK_dbo.Roles] PRIMARY KEY CLUSTERED ([RoleId] ASC)
);

