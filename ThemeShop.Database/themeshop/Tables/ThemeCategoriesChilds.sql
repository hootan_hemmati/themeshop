﻿CREATE TABLE [themeshop].[ThemeCategoriesChilds] (
    [CategoryChildId]       INT IDENTITY (1, 1) NOT NULL,
    [ThemeCategoryId]       INT NOT NULL,
    [ThemeCategoryParentId] INT NOT NULL,
    CONSTRAINT [PK_ThemeCategoriesChilds] PRIMARY KEY CLUSTERED ([CategoryChildId] ASC),
    CONSTRAINT [FK_ThemeCategoriesChilds_ThemeCategories] FOREIGN KEY ([ThemeCategoryId]) REFERENCES [themeshop].[ThemeCategories] ([ThemeCategoryId]),
    CONSTRAINT [FK_ThemeCategoriesChilds_ThemeCategories1] FOREIGN KEY ([ThemeCategoryParentId]) REFERENCES [themeshop].[ThemeCategories] ([ThemeCategoryId])
);

