﻿CREATE TABLE [themeshop].[ThemeGalleries] (
    [GalleryId]        INT            IDENTITY (1, 1) NOT NULL,
    [ThemeId]          INT            NOT NULL,
    [GalleryImageName] NVARCHAR (200) NOT NULL,
    [ImageTitle]       NVARCHAR (200) NOT NULL,
    CONSTRAINT [PK_ThemeGalleries] PRIMARY KEY CLUSTERED ([GalleryId] ASC),
    CONSTRAINT [FK_ThemeGalleries_Themes] FOREIGN KEY ([ThemeId]) REFERENCES [themeshop].[Themes] ([ThemeId])
);

