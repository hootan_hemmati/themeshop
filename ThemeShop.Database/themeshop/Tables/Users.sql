﻿CREATE TABLE [themeshop].[Users] (
    [UserId]                INT            IDENTITY (1, 1) NOT NULL,
    [UserName]              NVARCHAR (100) NULL,
    [Email]                 NVARCHAR (50)  NULL,
    [Mobile]                NVARCHAR (13)  NULL,
    [Password]              NVARCHAR (10)  NULL,
    [ActiveCode]            NVARCHAR (100) NULL,
    [UserDescription]       NVARCHAR (MAX) NULL,
    [IsActive]              BIT            NOT NULL,
    [RegisterDate]          DATETIME       NOT NULL,
    [Avatar]                NVARCHAR (50)  NULL,
    [IsDelete]              BIT            NOT NULL,
    [Name]                  NVARCHAR (250) NULL,
    [Family]                NVARCHAR (250) NULL,
    [ThemeSellerPercentage] INT            NOT NULL,
    CONSTRAINT [PK_dbo.Users] PRIMARY KEY CLUSTERED ([UserId] ASC),
    CONSTRAINT [FK_Users_Wallets] FOREIGN KEY ([UserId]) REFERENCES [themeshop].[Wallets] ([WalletId])
);

