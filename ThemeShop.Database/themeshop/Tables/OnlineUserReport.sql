﻿CREATE TABLE [themeshop].[OnlineUserReport] (
    [OnlineUserReportId] INT      IDENTITY (1, 1) NOT NULL,
    [OnlineUserCount]    BIGINT   NULL,
    [RepostDate]         DATETIME NOT NULL,
    CONSTRAINT [PK_OnlineUserReport] PRIMARY KEY CLUSTERED ([OnlineUserReportId] ASC)
);

