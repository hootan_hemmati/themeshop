﻿CREATE TABLE [themeshop].[Themes] (
    [ThemeId]          INT            IDENTITY (1, 1) NOT NULL,
    [SellerId]         INT            NOT NULL,
    [ThemeTitle]       NVARCHAR (400) NOT NULL,
    [ShortDescription] NVARCHAR (500) NOT NULL,
    [Description]      NVARCHAR (MAX) NOT NULL,
    [Featurs]          NVARCHAR (MAX) NOT NULL,
    [ThemeImageName]   NVARCHAR (200) NULL,
    [Price]            INT            NOT NULL,
    [IsActive]         BIT            NOT NULL,
    [CanInsertComment] BIT            NOT NULL,
    [CreateDate]       DATETIME       NOT NULL,
    [IsDelete]         BIT            NOT NULL,
    [IsRejected]       BIT            NOT NULL,
    CONSTRAINT [PK_Themes] PRIMARY KEY CLUSTERED ([ThemeId] ASC)
);

