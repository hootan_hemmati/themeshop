﻿CREATE TABLE [themeshop].[UserLogs] (
    [LogId]          INT            IDENTITY (1, 1) NOT NULL,
    [UserId]         INT            NOT NULL,
    [LogDescribtion] NVARCHAR (250) NULL,
    [LogCategoryId]  INT            NOT NULL,
    [LogTime]        DATETIME       NOT NULL,
    [LogPrice]       BIGINT         NOT NULL,
    CONSTRAINT [PK_dbo.UserLogs] PRIMARY KEY CLUSTERED ([LogId] ASC),
    CONSTRAINT [FK_dbo.UserLogs_dbo.LogCategories_LogCategoryId] FOREIGN KEY ([LogCategoryId]) REFERENCES [themeshop].[LogCategories] ([LogCategoryId]) ON DELETE CASCADE,
    CONSTRAINT [FK_dbo.UserLogs_dbo.Users_UserId] FOREIGN KEY ([UserId]) REFERENCES [themeshop].[Users] ([UserId]) ON DELETE CASCADE
);

