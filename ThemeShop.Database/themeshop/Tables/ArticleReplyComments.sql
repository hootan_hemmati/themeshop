﻿CREATE TABLE [themeshop].[ArticleReplyComments] (
    [ReplyCommentId] BIGINT         IDENTITY (1, 1) NOT NULL,
    [CommentId]      BIGINT         NOT NULL,
    [ArticleId]      INT            NOT NULL,
    [UserId]         INT            NOT NULL,
    [Comment]        NVARCHAR (800) NOT NULL,
    [CreateDate]     DATETIME       NOT NULL,
    [IsDelete]       BIT            NOT NULL,
    [IsRead]         BIT            NOT NULL,
    CONSTRAINT [PK_ArticleReplyComments] PRIMARY KEY CLUSTERED ([ReplyCommentId] ASC),
    CONSTRAINT [FK_ArticleReplyComments_ArticleComments] FOREIGN KEY ([CommentId]) REFERENCES [themeshop].[ArticleComments] ([CommentId]),
    CONSTRAINT [FK_ArticleReplyComments_Articles] FOREIGN KEY ([ArticleId]) REFERENCES [themeshop].[Articles] ([ArticleId])
);

