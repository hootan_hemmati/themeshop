﻿CREATE TABLE [themeshop].[OrderDetails] (
    [OrderDetailId] INT IDENTITY (1, 1) NOT NULL,
    [OrderId]       INT NOT NULL,
    [ThemeId]       INT NOT NULL,
    [Price]         INT NOT NULL,
    [Dicount]       INT NOT NULL,
    CONSTRAINT [PK_OrderDetails] PRIMARY KEY CLUSTERED ([OrderDetailId] ASC),
    CONSTRAINT [FK_OrderDetails_Orders] FOREIGN KEY ([OrderId]) REFERENCES [themeshop].[Orders] ([OrderId]),
    CONSTRAINT [FK_OrderDetails_Themes] FOREIGN KEY ([ThemeId]) REFERENCES [themeshop].[Themes] ([ThemeId])
);

