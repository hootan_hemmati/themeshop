﻿CREATE TABLE [themeshop].[TemplateInfoes] (
    [TemplateInfoId] INT      IDENTITY (1, 1) NOT NULL,
    [CreateDate]     DATETIME NOT NULL,
    [Price]          INT      NOT NULL,
    [UserId]         INT      NOT NULL,
    [ThemeId]        INT      NOT NULL,
    [SellerId]       INT      NOT NULL,
    CONSTRAINT [PK_TemplateInfoes] PRIMARY KEY CLUSTERED ([TemplateInfoId] ASC),
    CONSTRAINT [FK_dbo.TemplateInfoes_dbo.Themes_ThemeId] FOREIGN KEY ([ThemeId]) REFERENCES [themeshop].[Themes] ([ThemeId]) ON DELETE CASCADE,
    CONSTRAINT [FK_dbo.TemplateInfoes_dbo.Users_SellerId] FOREIGN KEY ([SellerId]) REFERENCES [themeshop].[Users] ([UserId]),
    CONSTRAINT [FK_dbo.TemplateInfoes_dbo.Users_UserId] FOREIGN KEY ([UserId]) REFERENCES [themeshop].[Users] ([UserId])
);

