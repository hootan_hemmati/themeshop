﻿CREATE TABLE [themeshop].[ThemeSelectedCategories] (
    [ThemeSelectedId] INT IDENTITY (1, 1) NOT NULL,
    [ThemeId]         INT NOT NULL,
    [ThemeCategoryId] INT NOT NULL,
    CONSTRAINT [PK_ThemeSelectedCategories] PRIMARY KEY CLUSTERED ([ThemeSelectedId] ASC),
    CONSTRAINT [FK_ThemeSelectedCategories_ThemeCategories] FOREIGN KEY ([ThemeCategoryId]) REFERENCES [themeshop].[ThemeCategories] ([ThemeCategoryId]),
    CONSTRAINT [FK_ThemeSelectedCategories_Themes] FOREIGN KEY ([ThemeId]) REFERENCES [themeshop].[Themes] ([ThemeId])
);

