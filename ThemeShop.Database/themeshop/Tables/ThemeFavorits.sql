﻿CREATE TABLE [themeshop].[ThemeFavorits] (
    [FavoritId] INT      IDENTITY (1, 1) NOT NULL,
    [ThemeId]   INT      NOT NULL,
    [UserId]    INT      NOT NULL,
    [CreatDate] DATETIME NOT NULL,
    CONSTRAINT [PK_ThemeFavorits] PRIMARY KEY CLUSTERED ([FavoritId] ASC),
    CONSTRAINT [FK_ThemeFavorits_Themes] FOREIGN KEY ([ThemeId]) REFERENCES [themeshop].[Themes] ([ThemeId])
);

