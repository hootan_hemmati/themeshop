﻿CREATE TABLE [themeshop].[ArticleRelateds] (
    [RelatedId]      INT IDENTITY (1, 1) NOT NULL,
    [ArticleId]      INT NOT NULL,
    [MainCategoryId] INT NOT NULL,
    [CategoryId]     INT NOT NULL,
    CONSTRAINT [PK_ArticleRelateds] PRIMARY KEY CLUSTERED ([RelatedId] ASC),
    CONSTRAINT [FK_ArticleRelateds_ArticleCategory] FOREIGN KEY ([CategoryId]) REFERENCES [themeshop].[ArticleCategories] ([CategoryId]),
    CONSTRAINT [FK_ArticleRelateds_Articles] FOREIGN KEY ([ArticleId]) REFERENCES [themeshop].[Articles] ([ArticleId]),
    CONSTRAINT [FK_ArticleRelateds_MainArticleCategories] FOREIGN KEY ([MainCategoryId]) REFERENCES [themeshop].[MainArticleCategories] ([MainCatagoryId])
);

