﻿CREATE TABLE [themeshop].[ReportAbuse] (
    [ReportAbuseId] INT            IDENTITY (1, 1) NOT NULL,
    [CreateDate]    DATETIME       NOT NULL,
    [Description]   NVARCHAR (MAX) NOT NULL,
    [IP]            NVARCHAR (100) NULL,
    [IsSeen]        BIT            NOT NULL,
    [ThemeId]       INT            NOT NULL,
    CONSTRAINT [PK_ReportAbuse] PRIMARY KEY CLUSTERED ([ReportAbuseId] ASC),
    CONSTRAINT [FK_ReportAbuse_Themes] FOREIGN KEY ([ThemeId]) REFERENCES [themeshop].[Themes] ([ThemeId])
);

