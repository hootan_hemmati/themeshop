﻿CREATE TABLE [themeshop].[Wallets] (
    [WalletId] INT    IDENTITY (1, 1) NOT NULL,
    [Charge]   BIGINT NOT NULL,
    [IsAdmin]  BIT    NOT NULL,
    CONSTRAINT [PK_dbo.Wallets] PRIMARY KEY CLUSTERED ([WalletId] ASC)
);

