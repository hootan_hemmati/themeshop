﻿CREATE TABLE [themeshop].[ArticleCategory] (
    [ArticleCategoryId] INT IDENTITY (1, 1) NOT NULL,
    [ArticleId]         INT NOT NULL,
    [CategoryId]        INT NOT NULL,
    [MainCategoryId]    INT NOT NULL,
    CONSTRAINT [PK_ArticleCategory] PRIMARY KEY CLUSTERED ([ArticleCategoryId] ASC),
    CONSTRAINT [FK_ArticleCategory_ArticleCategories] FOREIGN KEY ([CategoryId]) REFERENCES [themeshop].[ArticleCategories] ([CategoryId]),
    CONSTRAINT [FK_ArticleCategory_Articles] FOREIGN KEY ([ArticleId]) REFERENCES [themeshop].[Articles] ([ArticleId]),
    CONSTRAINT [FK_ArticleCategory_MainArticleCategories] FOREIGN KEY ([MainCategoryId]) REFERENCES [themeshop].[MainArticleCategories] ([MainCatagoryId])
);

