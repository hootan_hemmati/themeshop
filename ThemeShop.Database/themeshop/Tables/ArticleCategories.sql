﻿CREATE TABLE [themeshop].[ArticleCategories] (
    [CategoryId]    INT            IDENTITY (1, 1) NOT NULL,
    [MainArticleId] INT            NOT NULL,
    [Title]         NVARCHAR (260) NOT NULL,
    [NameInUrl]     NVARCHAR (200) NOT NULL,
    [IsActive]      BIT            NOT NULL,
    [IsDelete]      BIT            NOT NULL,
    CONSTRAINT [PK_ArticleCategories] PRIMARY KEY CLUSTERED ([CategoryId] ASC),
    CONSTRAINT [FK_ArticleCategories_MainArticleCategories] FOREIGN KEY ([MainArticleId]) REFERENCES [themeshop].[MainArticleCategories] ([MainCatagoryId])
);

