﻿CREATE TABLE [themeshop].[ComplaintThemes] (
    [ComplaintQuestionId] INT            IDENTITY (1, 1) NOT NULL,
    [ComplaintId]         INT            NOT NULL,
    [ThemeId]             INT            NOT NULL,
    [Date]                DATETIME       NOT NULL,
    [IsSeen]              BIT            NOT NULL,
    [Description]         NVARCHAR (500) NULL,
    CONSTRAINT [PK_ComplaintThemes] PRIMARY KEY CLUSTERED ([ComplaintQuestionId] ASC),
    CONSTRAINT [FK_ComplaintThemes_Complaints] FOREIGN KEY ([ComplaintId]) REFERENCES [themeshop].[Complaints] ([ComplaintId]),
    CONSTRAINT [FK_ComplaintThemes_Themes] FOREIGN KEY ([ThemeId]) REFERENCES [themeshop].[Themes] ([ThemeId])
);

