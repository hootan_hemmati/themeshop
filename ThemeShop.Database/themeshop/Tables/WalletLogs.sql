﻿CREATE TABLE [themeshop].[WalletLogs] (
    [WalletLogId] INT            IDENTITY (1, 1) NOT NULL,
    [LogTime]     DATETIME       NOT NULL,
    [Text]        NVARCHAR (MAX) NULL,
    [IsCharge]    BIT            NOT NULL,
    [WalletId]    INT            NOT NULL,
    CONSTRAINT [PK_dbo.WalletLogs] PRIMARY KEY CLUSTERED ([WalletLogId] ASC),
    CONSTRAINT [FK_dbo.WalletLogs_dbo.Wallets_WalletId] FOREIGN KEY ([WalletId]) REFERENCES [themeshop].[Wallets] ([WalletId]) ON DELETE CASCADE
);

