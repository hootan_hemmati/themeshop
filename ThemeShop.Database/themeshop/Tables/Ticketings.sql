﻿CREATE TABLE [themeshop].[Ticketings] (
    [TicketId]  INT            IDENTITY (1, 1) NOT NULL,
    [ThemeId]   INT            NOT NULL,
    [UserId]    INT            NULL,
    [Title]     NVARCHAR (300) NOT NULL,
    [CreatedOn] DATETIME       NOT NULL,
    [IsClose]   BIT            NOT NULL,
    [IsDelete]  BIT            NOT NULL,
    [Sender]    INT            NOT NULL,
    [Reciver]   INT            NOT NULL,
    CONSTRAINT [PK_Ticketings] PRIMARY KEY CLUSTERED ([TicketId] ASC),
    CONSTRAINT [FK_Ticketings_Themes] FOREIGN KEY ([ThemeId]) REFERENCES [themeshop].[Themes] ([ThemeId])
);

