﻿CREATE TABLE [themeshop].[ThemePreviews] (
    [PreviewId]     INT            IDENTITY (1, 1) NOT NULL,
    [ThemeId]       INT            NOT NULL,
    [RootFolder]    NVARCHAR (400) NOT NULL,
    [PageTitle]     NVARCHAR (400) NOT NULL,
    [PageName]      NVARCHAR (400) NOT NULL,
    [IsDefaultPage] BIT            NOT NULL,
    [IsActive]      BIT            NOT NULL,
    CONSTRAINT [PK_ThemePreviews] PRIMARY KEY CLUSTERED ([PreviewId] ASC),
    CONSTRAINT [FK_ThemePreviews_Themes] FOREIGN KEY ([ThemeId]) REFERENCES [themeshop].[Themes] ([ThemeId])
);

