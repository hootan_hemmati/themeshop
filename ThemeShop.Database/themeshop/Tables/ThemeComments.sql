﻿CREATE TABLE [themeshop].[ThemeComments] (
    [CommentId]  BIGINT         IDENTITY (1, 1) NOT NULL,
    [ThemeId]    INT            NOT NULL,
    [UserId]     INT            NOT NULL,
    [Comment]    NVARCHAR (800) NOT NULL,
    [CreateDate] DATETIME       NOT NULL,
    [IsDelete]   BIT            NOT NULL,
    [IsRead]     BIT            NOT NULL,
    CONSTRAINT [PK_ThemeComments] PRIMARY KEY CLUSTERED ([CommentId] ASC),
    CONSTRAINT [FK_ThemeComments_Themes] FOREIGN KEY ([ThemeId]) REFERENCES [themeshop].[Themes] ([ThemeId])
);

