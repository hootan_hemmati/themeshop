﻿CREATE TABLE [themeshop].[LogCategories] (
    [LogCategoryId]          INT            IDENTITY (1, 1) NOT NULL,
    [LogCategoryTitle]       NVARCHAR (20)  NULL,
    [LogCategoryDescribtion] NVARCHAR (250) NULL,
    CONSTRAINT [PK_dbo.LogCategories] PRIMARY KEY CLUSTERED ([LogCategoryId] ASC)
);

