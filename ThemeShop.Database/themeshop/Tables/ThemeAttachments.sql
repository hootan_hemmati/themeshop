﻿CREATE TABLE [themeshop].[ThemeAttachments] (
    [AttachId]      INT            IDENTITY (1, 1) NOT NULL,
    [ThemeId]       INT            NOT NULL,
    [Title]         NVARCHAR (250) NOT NULL,
    [FileName]      NVARCHAR (200) NULL,
    [FileExtension] NVARCHAR (50)  NULL,
    [FileSize]      INT            NOT NULL,
    [IsActive]      BIT            NOT NULL,
    [DownloadCount] INT            NOT NULL,
    [IsFree]        BIT            NOT NULL,
    CONSTRAINT [PK_ThemeAttachments] PRIMARY KEY CLUSTERED ([AttachId] ASC),
    CONSTRAINT [FK_ThemeAttachments_Themes] FOREIGN KEY ([ThemeId]) REFERENCES [themeshop].[Themes] ([ThemeId])
);

