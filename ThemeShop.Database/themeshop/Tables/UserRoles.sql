﻿CREATE TABLE [themeshop].[UserRoles] (
    [UserRoleId] INT IDENTITY (1, 1) NOT NULL,
    [UserId]     INT NOT NULL,
    [Roled]      INT NOT NULL,
    [IsDelete]   BIT NOT NULL,
    CONSTRAINT [PK_dbo.UserRoles] PRIMARY KEY CLUSTERED ([UserRoleId] ASC),
    CONSTRAINT [FK_dbo.UserRoles_dbo.Roles_RoleID] FOREIGN KEY ([Roled]) REFERENCES [themeshop].[Roles] ([RoleId]) ON DELETE CASCADE,
    CONSTRAINT [FK_dbo.UserRoles_dbo.Users_UserID] FOREIGN KEY ([UserId]) REFERENCES [themeshop].[Users] ([UserId]) ON DELETE CASCADE
);

