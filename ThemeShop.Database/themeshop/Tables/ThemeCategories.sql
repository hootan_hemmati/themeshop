﻿CREATE TABLE [themeshop].[ThemeCategories] (
    [ThemeCategoryId] INT            IDENTITY (1, 1) NOT NULL,
    [Title]           NVARCHAR (260) NOT NULL,
    [NameInUrl]       NVARCHAR (200) NOT NULL,
    [IsActive]        BIT            NOT NULL,
    [IsDelete]        BIT            NOT NULL,
    CONSTRAINT [PK_ThemeCategories] PRIMARY KEY CLUSTERED ([ThemeCategoryId] ASC)
);

