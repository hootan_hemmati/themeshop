﻿CREATE TABLE [themeshop].[DownloadTokens] (
    [DownloadTokenId] INT            IDENTITY (1, 1) NOT NULL,
    [ThemeId]         INT            NOT NULL,
    [Token]           NVARCHAR (MAX) NULL,
    [UserId]          INT            NOT NULL,
    [GeneratedOn]     DATETIME       NOT NULL,
    [IsExpired]       BIT            NOT NULL,
    CONSTRAINT [PK_DownloadTokens] PRIMARY KEY CLUSTERED ([DownloadTokenId] ASC),
    CONSTRAINT [FK_DownloadTokens_Themes] FOREIGN KEY ([ThemeId]) REFERENCES [themeshop].[Themes] ([ThemeId])
);

