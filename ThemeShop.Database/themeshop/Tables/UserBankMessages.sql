﻿CREATE TABLE [themeshop].[UserBankMessages] (
    [UserbankMessagesId] INT            IDENTITY (1, 1) NOT NULL,
    [UserBankId]         INT            NOT NULL,
    [AnswerTime]         DATETIME       NOT NULL,
    [Message]            NVARCHAR (MAX) NULL,
    [IsAccept]           BIT            NOT NULL,
    CONSTRAINT [PK_dbo.UserBankMessages] PRIMARY KEY CLUSTERED ([UserbankMessagesId] ASC),
    CONSTRAINT [FK_dbo.UserBankMessages_dbo.UserBanks_UserBankId] FOREIGN KEY ([UserBankId]) REFERENCES [themeshop].[UserBanks] ([UserBankId]) ON DELETE CASCADE
);

