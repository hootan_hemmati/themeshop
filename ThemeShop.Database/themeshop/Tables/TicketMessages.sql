﻿CREATE TABLE [themeshop].[TicketMessages] (
    [TicketMessageId] INT            IDENTITY (1, 1) NOT NULL,
    [UserId]          INT            NULL,
    [TicketId]        INT            NOT NULL,
    [Text]            NVARCHAR (MAX) NULL,
    [IsSeen]          BIT            NOT NULL,
    [CreatedOn]       DATETIME       NOT NULL,
    [OwnerId]         INT            NOT NULL,
    CONSTRAINT [PK_TicketMessages] PRIMARY KEY CLUSTERED ([TicketMessageId] ASC),
    CONSTRAINT [FK_TicketMessages_Ticketings] FOREIGN KEY ([TicketId]) REFERENCES [themeshop].[Ticketings] ([TicketId])
);

