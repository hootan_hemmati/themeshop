﻿CREATE TABLE [themeshop].[ThemeChangeStatusMessages] (
    [StatusMessagesId] BIGINT          IDENTITY (1, 1) NOT NULL,
    [ThemeId]          INT             NOT NULL,
    [Status]           INT             NOT NULL,
    [SentDate]         DATETIME        NOT NULL,
    [Message]          NVARCHAR (1000) NOT NULL,
    CONSTRAINT [PK_ThemeChangeStatusMessages] PRIMARY KEY CLUSTERED ([StatusMessagesId] ASC),
    CONSTRAINT [FK_ThemeChangeStatusMessages_Themes] FOREIGN KEY ([ThemeId]) REFERENCES [themeshop].[Themes] ([ThemeId])
);

