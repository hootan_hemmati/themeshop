﻿CREATE TABLE [themeshop].[ArticleComments] (
    [CommentId]  BIGINT         IDENTITY (1, 1) NOT NULL,
    [ArticleId]  INT            NOT NULL,
    [UserId]     INT            NOT NULL,
    [Comment]    NVARCHAR (800) NOT NULL,
    [CreateDate] DATETIME       NOT NULL,
    [IsDelete]   BIT            NOT NULL,
    [IsRead]     BIT            NOT NULL,
    CONSTRAINT [PK_ArticleComments] PRIMARY KEY CLUSTERED ([CommentId] ASC),
    CONSTRAINT [FK_ArticleComments_Articles] FOREIGN KEY ([ArticleId]) REFERENCES [themeshop].[Articles] ([ArticleId])
);

