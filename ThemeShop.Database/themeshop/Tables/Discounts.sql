﻿CREATE TABLE [themeshop].[Discounts] (
    [DiscountId] INT            IDENTITY (1, 1) NOT NULL,
    [Count]      INT            NOT NULL,
    [UsedCount]  INT            NOT NULL,
    [ExpireDate] DATETIME       NOT NULL,
    [CreateDate] DATETIME       NOT NULL,
    [Percentage] INT            NOT NULL,
    [Code]       NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_dbo.Discounts] PRIMARY KEY CLUSTERED ([DiscountId] ASC)
);

