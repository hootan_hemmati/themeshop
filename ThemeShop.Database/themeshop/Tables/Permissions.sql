﻿CREATE TABLE [themeshop].[Permissions] (
    [PermissionId]    INT            IDENTITY (1, 1) NOT NULL,
    [Title]           NVARCHAR (MAX) NULL,
    [Name]            NVARCHAR (MAX) NULL,
    [DisplayPriority] NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_dbo.Permissions] PRIMARY KEY CLUSTERED ([PermissionId] ASC)
);

