﻿CREATE TABLE [themeshop].[ArticleTags] (
    [ArticleTagId] INT            IDENTITY (1, 1) NOT NULL,
    [ArticleId]    INT            NOT NULL,
    [Tag]          NVARCHAR (200) NOT NULL,
    CONSTRAINT [PK_ArticleTags] PRIMARY KEY CLUSTERED ([ArticleTagId] ASC),
    CONSTRAINT [FK_ArticleTags_Articles] FOREIGN KEY ([ArticleId]) REFERENCES [themeshop].[Articles] ([ArticleId])
);

