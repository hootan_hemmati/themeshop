﻿CREATE TABLE [themeshop].[ThemeLikes] (
    [LikeId]  INT            IDENTITY (1, 1) NOT NULL,
    [UserId]  INT            NULL,
    [ThemeId] INT            NOT NULL,
    [IP]      NVARCHAR (100) NULL,
    CONSTRAINT [PK_ThemeLikes] PRIMARY KEY CLUSTERED ([LikeId] ASC),
    CONSTRAINT [FK_ThemeLikes_Themes] FOREIGN KEY ([ThemeId]) REFERENCES [themeshop].[Themes] ([ThemeId])
);

