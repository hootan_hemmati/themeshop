﻿CREATE TABLE [themeshop].[UserBanks] (
    [UserBankId]          INT            IDENTITY (1, 1) NOT NULL,
    [UserId]              INT            NOT NULL,
    [NationalCode]        NVARCHAR (MAX) NULL,
    [NationalCardImage]   NVARCHAR (MAX) NULL,
    [BankAccountHolder]   NVARCHAR (MAX) NULL,
    [BankName]            NVARCHAR (MAX) NULL,
    [SellerAccountNumber] NVARCHAR (MAX) NULL,
    [SellerCardNumber]    NVARCHAR (MAX) NULL,
    [SellerShabaNumber]   NVARCHAR (MAX) NULL,
    [BankCardImage]       NVARCHAR (MAX) NULL,
    [CreateDate]          DATETIME       NOT NULL,
    [IsActive]            BIT            NOT NULL,
    [IsReject]            BIT            NOT NULL,
    CONSTRAINT [PK_dbo.UserBanks] PRIMARY KEY CLUSTERED ([UserBankId] ASC),
    CONSTRAINT [FK_UserBanks_Users] FOREIGN KEY ([UserId]) REFERENCES [themeshop].[Users] ([UserId])
);

