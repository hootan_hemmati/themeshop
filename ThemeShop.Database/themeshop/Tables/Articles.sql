﻿CREATE TABLE [themeshop].[Articles] (
    [ArticleId]        INT            IDENTITY (1, 1) NOT NULL,
    [AuthorId]         INT            NOT NULL,
    [ArticleTitle]     NVARCHAR (400) NOT NULL,
    [ShortDescription] NVARCHAR (500) NOT NULL,
    [ArticleText]      NVARCHAR (MAX) NOT NULL,
    [ArticleImageName] NVARCHAR (200) NULL,
    [IsActive]         BIT            NOT NULL,
    [IsVip]            BIT            NOT NULL,
    [CanInsertComment] BIT            NOT NULL,
    [CreateDate]       DATETIME       NOT NULL,
    [IsDelete]         BIT            NOT NULL,
    CONSTRAINT [PK_Articles] PRIMARY KEY CLUSTERED ([ArticleId] ASC)
);

