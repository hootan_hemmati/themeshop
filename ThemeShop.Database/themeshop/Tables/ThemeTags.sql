﻿CREATE TABLE [themeshop].[ThemeTags] (
    [ThemeTagId] INT            IDENTITY (1, 1) NOT NULL,
    [ThemeId]    INT            NOT NULL,
    [Tag]        NVARCHAR (200) NOT NULL,
    CONSTRAINT [PK_ThemeTags] PRIMARY KEY CLUSTERED ([ThemeTagId] ASC),
    CONSTRAINT [FK_ThemeTags_Themes] FOREIGN KEY ([ThemeId]) REFERENCES [themeshop].[Themes] ([ThemeId])
);

