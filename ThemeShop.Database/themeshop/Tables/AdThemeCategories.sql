﻿CREATE TABLE [themeshop].[AdThemeCategories] (
    [AdThemeCategoryId] INT IDENTITY (1, 1) NOT NULL,
    [AdId]              INT NOT NULL,
    [ThemeCategoryId]   INT NOT NULL,
    CONSTRAINT [PK_AdThemeCategories] PRIMARY KEY CLUSTERED ([AdThemeCategoryId] ASC),
    CONSTRAINT [FK_AdThemeCategories_Ads] FOREIGN KEY ([AdId]) REFERENCES [themeshop].[Ads] ([AdId]),
    CONSTRAINT [FK_AdThemeCategories_ThemeCategories] FOREIGN KEY ([ThemeCategoryId]) REFERENCES [themeshop].[ThemeCategories] ([ThemeCategoryId])
);

