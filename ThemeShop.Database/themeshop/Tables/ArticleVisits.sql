﻿CREATE TABLE [themeshop].[ArticleVisits] (
    [VisitId]   INT            IDENTITY (1, 1) NOT NULL,
    [ArticleId] INT            NOT NULL,
    [IP]        NVARCHAR (100) NOT NULL,
    CONSTRAINT [PK_ArticleVisits] PRIMARY KEY CLUSTERED ([VisitId] ASC),
    CONSTRAINT [FK_ArticleVisits_Articles] FOREIGN KEY ([ArticleId]) REFERENCES [themeshop].[Articles] ([ArticleId])
);

