﻿CREATE TABLE [themeshop].[ThemeVisits] (
    [VisitId] INT            IDENTITY (1, 1) NOT NULL,
    [ThemeId] INT            NOT NULL,
    [IP]      NVARCHAR (100) NULL,
    CONSTRAINT [PK_ThemeVisits] PRIMARY KEY CLUSTERED ([VisitId] ASC),
    CONSTRAINT [FK_ThemeVisits_Themes] FOREIGN KEY ([ThemeId]) REFERENCES [themeshop].[Themes] ([ThemeId])
);

