﻿CREATE TABLE [themeshop].[ContactUs] (
    [ContactId]  INT            IDENTITY (1, 1) NOT NULL,
    [Name]       NVARCHAR (MAX) NULL,
    [Mobile]     NVARCHAR (MAX) NULL,
    [Email]      NVARCHAR (MAX) NULL,
    [Subject]    NVARCHAR (MAX) NULL,
    [Message]    NVARCHAR (MAX) NULL,
    [Answer]     NVARCHAR (MAX) NULL,
    [IPAddress]  NVARCHAR (MAX) NULL,
    [IsRead]     BIT            NOT NULL,
    [CreateDate] DATETIME       NOT NULL,
    CONSTRAINT [PK_dbo.ContactUs] PRIMARY KEY CLUSTERED ([ContactId] ASC)
);

