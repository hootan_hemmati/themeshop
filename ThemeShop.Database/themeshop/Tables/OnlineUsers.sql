﻿CREATE TABLE [themeshop].[OnlineUsers] (
    [UserConnectionId] INT            IDENTITY (1, 1) NOT NULL,
    [UserName]         NCHAR (100)    NULL,
    [IP]               NVARCHAR (150) NULL,
    [StartTime]        DATETIME       NOT NULL,
    [VisitPath]        NVARCHAR (MAX) NULL,
    [OnlineStatus]     BIT            NOT NULL,
    CONSTRAINT [PK_OnlineUsers] PRIMARY KEY CLUSTERED ([UserConnectionId] ASC)
);

