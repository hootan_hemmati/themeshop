﻿CREATE TABLE [themeshop].[Complaints] (
    [ComplaintId]    INT            IDENTITY (1, 1) NOT NULL,
    [ComplaintTitle] NVARCHAR (100) NULL,
    CONSTRAINT [PK_Complaints] PRIMARY KEY CLUSTERED ([ComplaintId] ASC)
);

