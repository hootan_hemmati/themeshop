﻿CREATE TABLE [themeshop].[ThemeOfferTodays] (
    [OfferId]    INT      IDENTITY (1, 1) NOT NULL,
    [ThemeId]    INT      NOT NULL,
    [StartDate]  DATETIME NOT NULL,
    [EndDate]    DATETIME NOT NULL,
    [VisitCount] INT      NOT NULL,
    [IsActive]   BIT      NOT NULL,
    CONSTRAINT [PK_ThemeOfferTodays] PRIMARY KEY CLUSTERED ([OfferId] ASC),
    CONSTRAINT [FK_ThemeOfferTodays_ThemeOfferTodays] FOREIGN KEY ([ThemeId]) REFERENCES [themeshop].[Themes] ([ThemeId])
);

