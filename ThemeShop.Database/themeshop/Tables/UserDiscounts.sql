﻿CREATE TABLE [themeshop].[UserDiscounts] (
    [UserDiscountId] INT IDENTITY (1, 1) NOT NULL,
    [UserId]         INT NOT NULL,
    [DiscountId]     INT NOT NULL,
    CONSTRAINT [PK_dbo.UserDiscounts] PRIMARY KEY CLUSTERED ([UserDiscountId] ASC),
    CONSTRAINT [FK_dbo.UserDiscounts_dbo.Discounts_DiscountId] FOREIGN KEY ([DiscountId]) REFERENCES [themeshop].[Discounts] ([DiscountId]) ON DELETE CASCADE,
    CONSTRAINT [FK_UserDiscounts_Users] FOREIGN KEY ([UserId]) REFERENCES [themeshop].[Users] ([UserId])
);

