﻿CREATE TABLE [themeshop].[RolePremissions] (
    [RolePermissionsId] INT IDENTITY (1, 1) NOT NULL,
    [RoleId]            INT NOT NULL,
    [PermissionId]      INT NOT NULL,
    CONSTRAINT [PK_dbo.RolePremissions] PRIMARY KEY CLUSTERED ([RolePermissionsId] ASC),
    CONSTRAINT [FK_dbo.RolePremissions_dbo.Permissions_PermissionId] FOREIGN KEY ([PermissionId]) REFERENCES [themeshop].[Permissions] ([PermissionId]) ON DELETE CASCADE,
    CONSTRAINT [FK_dbo.RolePremissions_dbo.Roles_RoleId] FOREIGN KEY ([RoleId]) REFERENCES [themeshop].[Roles] ([RoleId]) ON DELETE CASCADE
);

