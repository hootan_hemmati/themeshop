﻿CREATE TABLE [themeshop].[Orders] (
    [OrderId]      INT      IDENTITY (1, 1) NOT NULL,
    [IsActive]     BIT      NOT NULL,
    [IsPay]        BIT      NOT NULL,
    [UserId]       INT      NOT NULL,
    [CreateTime]   DATETIME NOT NULL,
    [CompleteTime] DATETIME NOT NULL,
    [TotalPrice]   BIGINT   NOT NULL,
    CONSTRAINT [PK_dbo.Orders] PRIMARY KEY CLUSTERED ([OrderId] ASC),
    CONSTRAINT [FK_Orders_Users] FOREIGN KEY ([UserId]) REFERENCES [themeshop].[Users] ([UserId])
);

