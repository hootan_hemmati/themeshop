﻿CREATE TABLE [themeshop].[ThemeRelateds] (
    [RelatedId]      INT IDENTITY (1, 1) NOT NULL,
    [ThemeId]        INT NOT NULL,
    [ThemeRelatedId] INT NOT NULL,
    CONSTRAINT [PK_ThemeRelateds] PRIMARY KEY CLUSTERED ([RelatedId] ASC),
    CONSTRAINT [FK_ThemeRelateds_Themes] FOREIGN KEY ([ThemeId]) REFERENCES [themeshop].[Themes] ([ThemeId])
);

