﻿CREATE TABLE [dbo].[ArticleComments] (
    [CommantID]  INT            IDENTITY (1, 1) NOT NULL,
    [ArticleID]  INT            NOT NULL,
    [UserID]     INT            NOT NULL,
    [Comment]    NVARCHAR (800) NOT NULL,
    [CreateDate] DATETIME       NOT NULL,
    [IsDelete]   BIT            NOT NULL,
    [IsRead]     BIT            NOT NULL,
    [ParentID]   INT            NULL,
    CONSTRAINT [PK_dbo.ArticleComments] PRIMARY KEY CLUSTERED ([CommantID] ASC),
    CONSTRAINT [FK_dbo.ArticleComments_dbo.ArticleComments_ParentID] FOREIGN KEY ([ParentID]) REFERENCES [dbo].[ArticleComments] ([CommantID]),
    CONSTRAINT [FK_dbo.ArticleComments_dbo.Articles_ArticleID] FOREIGN KEY ([ArticleID]) REFERENCES [dbo].[Articles] ([ArticleID]) ON DELETE CASCADE,
    CONSTRAINT [FK_dbo.ArticleComments_dbo.Users_UserID] FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([UserID])
);


GO
CREATE NONCLUSTERED INDEX [IX_UserID]
    ON [dbo].[ArticleComments]([UserID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ParentID]
    ON [dbo].[ArticleComments]([ParentID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ArticleID]
    ON [dbo].[ArticleComments]([ArticleID] ASC);

