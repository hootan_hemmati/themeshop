﻿CREATE TABLE [dbo].[SocialNetworks] (
    [SocialID] INT            IDENTITY (1, 1) NOT NULL,
    [Title]    NVARCHAR (100) NOT NULL,
    [IconName] NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_dbo.SocialNetworks] PRIMARY KEY CLUSTERED ([SocialID] ASC)
);

