﻿CREATE TABLE [dbo].[ThemeOfferTodays] (
    [OfferID]    INT      IDENTITY (1, 1) NOT NULL,
    [ThemeID]    INT      NOT NULL,
    [StartDate]  DATETIME NOT NULL,
    [EndDate]    DATETIME NOT NULL,
    [VisitCount] INT      NOT NULL,
    [IsActive]   BIT      NOT NULL,
    CONSTRAINT [PK_dbo.ThemeOfferTodays] PRIMARY KEY CLUSTERED ([OfferID] ASC),
    CONSTRAINT [FK_dbo.ThemeOfferTodays_dbo.Themes_ThemeID] FOREIGN KEY ([ThemeID]) REFERENCES [dbo].[Themes] ([ThemeID]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_ThemeID]
    ON [dbo].[ThemeOfferTodays]([ThemeID] ASC);

