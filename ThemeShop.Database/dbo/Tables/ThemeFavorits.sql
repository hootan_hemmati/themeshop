﻿CREATE TABLE [dbo].[ThemeFavorits] (
    [FavoritID] INT      IDENTITY (1, 1) NOT NULL,
    [ThemeID]   INT      NOT NULL,
    [UserID]    INT      NOT NULL,
    [CreatDate] DATETIME NOT NULL,
    CONSTRAINT [PK_dbo.ThemeFavorits] PRIMARY KEY CLUSTERED ([FavoritID] ASC),
    CONSTRAINT [FK_dbo.ThemeFavorits_dbo.Themes_ThemeID] FOREIGN KEY ([ThemeID]) REFERENCES [dbo].[Themes] ([ThemeID]) ON DELETE CASCADE,
    CONSTRAINT [FK_dbo.ThemeFavorits_dbo.Users_UserID] FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([UserID])
);


GO
CREATE NONCLUSTERED INDEX [IX_UserID]
    ON [dbo].[ThemeFavorits]([UserID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ThemeID]
    ON [dbo].[ThemeFavorits]([ThemeID] ASC);

