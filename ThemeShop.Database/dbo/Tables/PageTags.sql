﻿CREATE TABLE [dbo].[PageTags] (
    [TagID]    INT            IDENTITY (1, 1) NOT NULL,
    [PageID]   INT            NOT NULL,
    [TagTitle] NVARCHAR (300) NOT NULL,
    CONSTRAINT [PK_dbo.PageTags] PRIMARY KEY CLUSTERED ([TagID] ASC),
    CONSTRAINT [FK_dbo.PageTags_dbo.Pages_PageID] FOREIGN KEY ([PageID]) REFERENCES [dbo].[Pages] ([PageID]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_PageID]
    ON [dbo].[PageTags]([PageID] ASC);

