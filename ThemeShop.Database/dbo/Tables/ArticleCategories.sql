﻿CREATE TABLE [dbo].[ArticleCategories] (
    [CategoryID] INT            IDENTITY (1, 1) NOT NULL,
    [Title]      NVARCHAR (260) NOT NULL,
    [NameInUrl]  NVARCHAR (200) NOT NULL,
    [ParentID]   INT            NULL,
    [IsActive]   BIT            NOT NULL,
    [IsDelete]   BIT            NOT NULL,
    CONSTRAINT [PK_dbo.ArticleCategories] PRIMARY KEY CLUSTERED ([CategoryID] ASC),
    CONSTRAINT [FK_dbo.ArticleCategories_dbo.ArticleCategories_ParentID] FOREIGN KEY ([ParentID]) REFERENCES [dbo].[ArticleCategories] ([CategoryID])
);


GO
CREATE NONCLUSTERED INDEX [IX_ParentID]
    ON [dbo].[ArticleCategories]([ParentID] ASC);

