﻿CREATE TABLE [dbo].[ArticleCategory] (
    [ArticleCategoryID] INT IDENTITY (1, 1) NOT NULL,
    [ArticleID]         INT NOT NULL,
    [CategoryID]        INT NOT NULL,
    CONSTRAINT [PK_ArticleCategory] PRIMARY KEY CLUSTERED ([ArticleCategoryID] ASC),
    CONSTRAINT [FK_dbo.ArticleCategories_dbo.ArticleCategories_CategoryID] FOREIGN KEY ([CategoryID]) REFERENCES [dbo].[ArticleCategories] ([CategoryID]) ON DELETE CASCADE,
    CONSTRAINT [FK_dbo.ArticleCategories_dbo.Articles_ArticleID] FOREIGN KEY ([ArticleID]) REFERENCES [dbo].[Articles] ([ArticleID]) ON DELETE CASCADE
);




GO
CREATE NONCLUSTERED INDEX [IX_CategoryID]
    ON [dbo].[ArticleCategory]([CategoryID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ArticleID]
    ON [dbo].[ArticleCategory]([ArticleID] ASC);

