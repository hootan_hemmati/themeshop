﻿CREATE TABLE [dbo].[ThemeRelateds] (
    [RelatedID]      INT IDENTITY (1, 1) NOT NULL,
    [ThemeID]        INT NOT NULL,
    [themeRelatedID] INT NOT NULL,
    CONSTRAINT [PK_dbo.ThemeRelateds] PRIMARY KEY CLUSTERED ([RelatedID] ASC),
    CONSTRAINT [FK_dbo.ThemeRelateds_dbo.Themes_ThemeID] FOREIGN KEY ([ThemeID]) REFERENCES [dbo].[Themes] ([ThemeID]),
    CONSTRAINT [FK_dbo.ThemeRelateds_dbo.Themes_themeRelatedID] FOREIGN KEY ([themeRelatedID]) REFERENCES [dbo].[Themes] ([ThemeID])
);


GO
CREATE NONCLUSTERED INDEX [IX_themeRelatedID]
    ON [dbo].[ThemeRelateds]([themeRelatedID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ThemeID]
    ON [dbo].[ThemeRelateds]([ThemeID] ASC);

