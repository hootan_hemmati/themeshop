﻿CREATE TABLE [dbo].[TicketMessages] (
    [TicketMessageId] INT            IDENTITY (1, 1) NOT NULL,
    [Text]            NVARCHAR (MAX) NULL,
    [TicketId]        INT            NOT NULL,
    [IsSeen]          BIT            NOT NULL,
    [CreatedOn]       DATETIME       NOT NULL,
    [OwnerId]         INT            NOT NULL,
    [Owner_UserID]    INT            NULL,
    CONSTRAINT [PK_TicketMessages] PRIMARY KEY CLUSTERED ([TicketMessageId] ASC),
    CONSTRAINT [FK_dbo.TicketMessages_dbo.Ticketings_TicketId] FOREIGN KEY ([TicketId]) REFERENCES [dbo].[Ticketings] ([TicketId]),
    CONSTRAINT [FK_dbo.TicketMessages_dbo.Users_Owner_UserID] FOREIGN KEY ([Owner_UserID]) REFERENCES [dbo].[Users] ([UserID])
);


GO
CREATE NONCLUSTERED INDEX [IX_TicketId]
    ON [dbo].[TicketMessages]([TicketId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Owner_UserID]
    ON [dbo].[TicketMessages]([Owner_UserID] ASC);

