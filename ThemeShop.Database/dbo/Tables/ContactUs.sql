﻿CREATE TABLE [dbo].[ContactUs] (
    [ContactID]  INT             IDENTITY (1, 1) NOT NULL,
    [UserId]     INT             NULL,
    [Name]       NVARCHAR (200)  NOT NULL,
    [Email]      NVARCHAR (250)  NOT NULL,
    [Mobile]     NVARCHAR (100)  NULL,
    [Message]    NVARCHAR (2000) NOT NULL,
    [Answer]     NVARCHAR (MAX)  NULL,
    [UserIP]     NVARCHAR (100)  NULL,
    [IsRead]     BIT             NOT NULL,
    [CreateDate] DATETIME        NOT NULL,
    [Subject]    NVARCHAR (500)  DEFAULT ('') NOT NULL,
    CONSTRAINT [PK_dbo.ContactUs] PRIMARY KEY CLUSTERED ([ContactID] ASC),
    CONSTRAINT [FK_dbo.ContactUs_dbo.Users_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[Users] ([UserID])
);


GO
CREATE NONCLUSTERED INDEX [IX_UserId]
    ON [dbo].[ContactUs]([UserId] ASC);

