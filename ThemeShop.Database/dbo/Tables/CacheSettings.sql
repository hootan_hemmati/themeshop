﻿CREATE TABLE [dbo].[CacheSettings] (
    [CacheID]        INT            IDENTITY (1, 1) NOT NULL,
    [Title]          NVARCHAR (300) NOT NULL,
    [Name]           NVARCHAR (200) NOT NULL,
    [ControllerName] NVARCHAR (200) NOT NULL,
    [ActionName]     NVARCHAR (200) NOT NULL,
    [CacheDuration]  INT            NOT NULL,
    CONSTRAINT [PK_dbo.CacheSettings] PRIMARY KEY CLUSTERED ([CacheID] ASC)
);

