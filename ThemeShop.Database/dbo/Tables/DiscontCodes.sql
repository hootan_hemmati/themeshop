﻿CREATE TABLE [dbo].[DiscontCodes] (
    [DisCountCodeId] INT            IDENTITY (1, 1) NOT NULL,
    [Code]           NVARCHAR (MAX) NULL,
    [Value]          INT            NOT NULL,
    [UsageCount]     INT            NOT NULL,
    [ThemeId]        INT            NULL,
    [ExpireTime]     DATETIME       NULL,
    [CreateOn]       DATETIME       NOT NULL,
    CONSTRAINT [PK_dbo.DiscontCodes] PRIMARY KEY CLUSTERED ([DisCountCodeId] ASC),
    CONSTRAINT [FK_dbo.DiscontCodes_dbo.Themes_ThemeId] FOREIGN KEY ([ThemeId]) REFERENCES [dbo].[Themes] ([ThemeID])
);


GO
CREATE NONCLUSTERED INDEX [IX_ThemeId]
    ON [dbo].[DiscontCodes]([ThemeId] ASC);

