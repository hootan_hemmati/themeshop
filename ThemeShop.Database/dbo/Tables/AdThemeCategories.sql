﻿CREATE TABLE [dbo].[AdThemeCategories] (
    [AdThemeCategoryId] INT IDENTITY (1, 1) NOT NULL,
    [AdId]              INT NOT NULL,
    [CategoryId]        INT NOT NULL,
    CONSTRAINT [PK_AdThemeCategories] PRIMARY KEY CLUSTERED ([AdThemeCategoryId] ASC),
    CONSTRAINT [FK_dbo.AdThemeCategories_dbo.Ads_AdId] FOREIGN KEY ([AdId]) REFERENCES [dbo].[Ads] ([AdId]) ON DELETE CASCADE,
    CONSTRAINT [FK_dbo.AdThemeCategories_dbo.ThemeCategories_CategoryId] FOREIGN KEY ([CategoryId]) REFERENCES [dbo].[ThemeCategories] ([CategoryID]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_CategoryId]
    ON [dbo].[AdThemeCategories]([CategoryId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_AdId]
    ON [dbo].[AdThemeCategories]([AdId] ASC);

