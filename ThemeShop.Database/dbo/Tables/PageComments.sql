﻿CREATE TABLE [dbo].[PageComments] (
    [CommantID]  INT            IDENTITY (1, 1) NOT NULL,
    [PageID]     INT            NOT NULL,
    [UserID]     INT            NOT NULL,
    [Comment]    NVARCHAR (800) NOT NULL,
    [CreateDate] DATETIME       NOT NULL,
    [IsDelete]   BIT            NOT NULL,
    [IsRead]     BIT            NOT NULL,
    [ParentID]   INT            NULL,
    CONSTRAINT [PK_dbo.PageComments] PRIMARY KEY CLUSTERED ([CommantID] ASC),
    CONSTRAINT [FK_dbo.PageComments_dbo.PageComments_ParentID] FOREIGN KEY ([ParentID]) REFERENCES [dbo].[PageComments] ([CommantID]),
    CONSTRAINT [FK_dbo.PageComments_dbo.Pages_PageID] FOREIGN KEY ([PageID]) REFERENCES [dbo].[Pages] ([PageID]) ON DELETE CASCADE,
    CONSTRAINT [FK_dbo.PageComments_dbo.Users_UserID] FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([UserID]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_UserID]
    ON [dbo].[PageComments]([UserID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ParentID]
    ON [dbo].[PageComments]([ParentID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_PageID]
    ON [dbo].[PageComments]([PageID] ASC);

