﻿CREATE TABLE [dbo].[Pages] (
    [PageID]           INT            IDENTITY (1, 1) NOT NULL,
    [AuthorID]         INT            NOT NULL,
    [PageTitle]        NVARCHAR (400) NOT NULL,
    [BrowserTitle]     NVARCHAR (300) NULL,
    [PageUrl]          NVARCHAR (500) NOT NULL,
    [ShortDescription] NVARCHAR (500) NOT NULL,
    [PageText]         NVARCHAR (MAX) NOT NULL,
    [PageImage]        NVARCHAR (MAX) NULL,
    [CreateDate]       DATETIME       NOT NULL,
    [IsActive]         BIT            NOT NULL,
    [IsDelete]         BIT            NOT NULL,
    [StartDate]        DATETIME       NULL,
    [EndDate]          DATETIME       NULL,
    [IsCommentActive]  BIT            NOT NULL,
    CONSTRAINT [PK_Pages] PRIMARY KEY CLUSTERED ([PageID] ASC),
    CONSTRAINT [FK_dbo.Pages_dbo.Users_AuthorID] FOREIGN KEY ([AuthorID]) REFERENCES [dbo].[Users] ([UserID])
);


GO
CREATE NONCLUSTERED INDEX [IX_AuthorID]
    ON [dbo].[Pages]([AuthorID] ASC);

