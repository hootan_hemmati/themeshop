﻿CREATE TABLE [dbo].[Ticketings] (
    [TicketId]    INT            IDENTITY (1, 1) NOT NULL,
    [User_UserID] INT            NULL,
    [Title]       NVARCHAR (300) DEFAULT ('') NOT NULL,
    [CreatedOn]   DATETIME       NOT NULL,
    [IsClose]     BIT            NOT NULL,
    [IsDelete]    BIT            NOT NULL,
    [ThemeId]     INT            NULL,
    [Sender]      INT            NOT NULL,
    [Reciver]     INT            NOT NULL,
    CONSTRAINT [PK_dbo.Ticketings] PRIMARY KEY CLUSTERED ([TicketId] ASC),
    CONSTRAINT [FK_dbo.Ticketings_dbo.Themes_ThemeId] FOREIGN KEY ([ThemeId]) REFERENCES [dbo].[Themes] ([ThemeID]),
    CONSTRAINT [FK_dbo.Ticketings_dbo.Users_User_UserID] FOREIGN KEY ([User_UserID]) REFERENCES [dbo].[Users] ([UserID])
);


GO
CREATE NONCLUSTERED INDEX [IX_User_UserID]
    ON [dbo].[Ticketings]([User_UserID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ThemeId]
    ON [dbo].[Ticketings]([ThemeId] ASC);

