﻿CREATE TABLE [dbo].[ComplaintThemes] (
    [ComplaintQuestionId] INT            IDENTITY (1, 1) NOT NULL,
    [ComplaintId]         INT            NOT NULL,
    [ThemeId]             INT            NOT NULL,
    [Date]                DATETIME       NOT NULL,
    [IsSeen]              BIT            NOT NULL,
    [Description]         NVARCHAR (500) NULL,
    CONSTRAINT [PK_dbo.ComplaintThemes] PRIMARY KEY CLUSTERED ([ComplaintQuestionId] ASC),
    CONSTRAINT [FK_dbo.ComplaintThemes_dbo.Complaints_ComplaintId] FOREIGN KEY ([ComplaintId]) REFERENCES [dbo].[Complaints] ([ComplaintId]) ON DELETE CASCADE,
    CONSTRAINT [FK_dbo.ComplaintThemes_dbo.Themes_ThemeId] FOREIGN KEY ([ThemeId]) REFERENCES [dbo].[Themes] ([ThemeID]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_ThemeId]
    ON [dbo].[ComplaintThemes]([ThemeId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ComplaintId]
    ON [dbo].[ComplaintThemes]([ComplaintId] ASC);

