﻿CREATE TABLE [dbo].[Ads] (
    [AdId]        INT            IDENTITY (1, 1) NOT NULL,
    [AdTitle]     NVARCHAR (500) NOT NULL,
    [AdUrl]       NVARCHAR (500) NOT NULL,
    [AdImageName] NVARCHAR (500) NULL,
    [CreateDate]  DATETIME       NOT NULL,
    [StartDate]   DATETIME       NOT NULL,
    [EndDate]     DATETIME       NULL,
    [ShowInChild] BIT            NOT NULL,
    [IsActive]    BIT            NOT NULL,
    [IsDelete]    BIT            NOT NULL,
    CONSTRAINT [PK_Ads] PRIMARY KEY CLUSTERED ([AdId] ASC)
);

