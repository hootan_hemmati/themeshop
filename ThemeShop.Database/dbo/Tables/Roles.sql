﻿CREATE TABLE [dbo].[Roles] (
    [RoleID]        INT            NOT NULL,
    [RoleTitle]     NVARCHAR (100) NOT NULL,
    [IsDefaultRole] BIT            NOT NULL,
    [IsDelete]      BIT            NOT NULL,
    CONSTRAINT [PK_Roles] PRIMARY KEY CLUSTERED ([RoleID] ASC)
);

