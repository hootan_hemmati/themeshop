﻿CREATE TABLE [dbo].[Articles] (
    [ArticleID]        INT            IDENTITY (1, 1) NOT NULL,
    [AuthorID]         INT            NOT NULL,
    [ArticleTitle]     NVARCHAR (400) NOT NULL,
    [ShortDescription] NVARCHAR (500) NOT NULL,
    [ArticleText]      NVARCHAR (MAX) NOT NULL,
    [ArticleImageName] NVARCHAR (200) NULL,
    [IsActive]         BIT            NOT NULL,
    [IsVip]            BIT            NOT NULL,
    [CanInsertComment] BIT            NOT NULL,
    [CreateDate]       DATETIME       NOT NULL,
    [IsDelete]         BIT            NOT NULL,
    CONSTRAINT [PK_Articles] PRIMARY KEY CLUSTERED ([ArticleID] ASC),
    CONSTRAINT [FK_dbo.Articles_dbo.Users_AuthorID] FOREIGN KEY ([AuthorID]) REFERENCES [dbo].[Users] ([UserID]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_AuthorID]
    ON [dbo].[Articles]([AuthorID] ASC);

