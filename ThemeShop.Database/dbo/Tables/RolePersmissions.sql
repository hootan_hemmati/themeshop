﻿CREATE TABLE [dbo].[RolePersmissions] (
    [RolePermissionID] INT IDENTITY (1, 1) NOT NULL,
    [RoleID]           INT NOT NULL,
    [PermissionID]     INT NOT NULL,
    CONSTRAINT [PK_dbo.RolePersmissions] PRIMARY KEY CLUSTERED ([RolePermissionID] ASC),
    CONSTRAINT [FK_dbo.RolePersmissions_dbo.Permissions_PermissionID] FOREIGN KEY ([PermissionID]) REFERENCES [dbo].[Permissions] ([PermissionID]) ON DELETE CASCADE,
    CONSTRAINT [FK_dbo.RolePersmissions_dbo.Roles_RoleID] FOREIGN KEY ([RoleID]) REFERENCES [dbo].[Roles] ([RoleID]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_RoleID]
    ON [dbo].[RolePersmissions]([RoleID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_PermissionID]
    ON [dbo].[RolePersmissions]([PermissionID] ASC);

