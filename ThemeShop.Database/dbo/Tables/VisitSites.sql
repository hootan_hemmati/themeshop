﻿CREATE TABLE [dbo].[VisitSites] (
    [VisitID] INT           IDENTITY (1, 1) NOT NULL,
    [Ip]      NVARCHAR (60) NULL,
    [Date]    DATETIME      NOT NULL,
    CONSTRAINT [PK_dbo.VisitSites] PRIMARY KEY CLUSTERED ([VisitID] ASC)
);

