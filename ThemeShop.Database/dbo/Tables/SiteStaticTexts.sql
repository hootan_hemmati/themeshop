﻿CREATE TABLE [dbo].[SiteStaticTexts] (
    [Name] NVARCHAR (128) NOT NULL,
    [Text] NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_dbo.SiteStaticTexts] PRIMARY KEY CLUSTERED ([Name] ASC)
);

