﻿CREATE TABLE [dbo].[ArticleVisits] (
    [VisitID]   INT            IDENTITY (1, 1) NOT NULL,
    [ArticleID] INT            NOT NULL,
    [IP]        NVARCHAR (100) NOT NULL,
    CONSTRAINT [PK_dbo.ArticleVisits] PRIMARY KEY CLUSTERED ([VisitID] ASC),
    CONSTRAINT [FK_dbo.ArticleVisits_dbo.Articles_ArticleID] FOREIGN KEY ([ArticleID]) REFERENCES [dbo].[Articles] ([ArticleID]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_ArticleID]
    ON [dbo].[ArticleVisits]([ArticleID] ASC);

