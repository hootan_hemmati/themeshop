﻿CREATE TABLE [dbo].[TemplateInfoes] (
    [TemplateInfoId] INT      IDENTITY (1, 1) NOT NULL,
    [CreateBuy]      DATETIME NOT NULL,
    [Price]          INT      NOT NULL,
    [UserId]         INT      NOT NULL,
    [ThemeId]        INT      NOT NULL,
    [SellerId]       INT      NOT NULL,
    CONSTRAINT [PK_TemplateInfoes] PRIMARY KEY CLUSTERED ([TemplateInfoId] ASC),
    CONSTRAINT [FK_dbo.TemplateInfoes_dbo.Themes_ThemeId] FOREIGN KEY ([ThemeId]) REFERENCES [dbo].[Themes] ([ThemeID]) ON DELETE CASCADE,
    CONSTRAINT [FK_dbo.TemplateInfoes_dbo.Users_SellerId] FOREIGN KEY ([SellerId]) REFERENCES [dbo].[Users] ([UserID]),
    CONSTRAINT [FK_dbo.TemplateInfoes_dbo.Users_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[Users] ([UserID])
);


GO
CREATE NONCLUSTERED INDEX [IX_UserId]
    ON [dbo].[TemplateInfoes]([UserId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ThemeId]
    ON [dbo].[TemplateInfoes]([ThemeId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_SellerId]
    ON [dbo].[TemplateInfoes]([SellerId] ASC);

