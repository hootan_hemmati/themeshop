﻿CREATE TABLE [dbo].[ThemeComments] (
    [CommantID]  INT            IDENTITY (1, 1) NOT NULL,
    [ThemeID]    INT            NOT NULL,
    [UserID]     INT            NOT NULL,
    [Comment]    NVARCHAR (800) NOT NULL,
    [CreateDate] DATETIME       NOT NULL,
    [IsDelete]   BIT            NOT NULL,
    [IsRead]     BIT            NOT NULL,
    [ParentID]   INT            NULL,
    CONSTRAINT [PK_dbo.ThemeComments] PRIMARY KEY CLUSTERED ([CommantID] ASC),
    CONSTRAINT [FK_dbo.ThemeComments_dbo.ThemeComments_ParentID] FOREIGN KEY ([ParentID]) REFERENCES [dbo].[ThemeComments] ([CommantID]),
    CONSTRAINT [FK_dbo.ThemeComments_dbo.Themes_ThemeID] FOREIGN KEY ([ThemeID]) REFERENCES [dbo].[Themes] ([ThemeID]) ON DELETE CASCADE,
    CONSTRAINT [FK_dbo.ThemeComments_dbo.Users_UserID] FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([UserID])
);


GO
CREATE NONCLUSTERED INDEX [IX_UserID]
    ON [dbo].[ThemeComments]([UserID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ThemeID]
    ON [dbo].[ThemeComments]([ThemeID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ParentID]
    ON [dbo].[ThemeComments]([ParentID] ASC);

