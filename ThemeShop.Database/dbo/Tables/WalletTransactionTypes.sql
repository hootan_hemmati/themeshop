﻿CREATE TABLE [dbo].[WalletTransactionTypes] (
    [TransactionTypeID] INT            NOT NULL,
    [TransactionTitle]  NVARCHAR (100) NOT NULL,
    CONSTRAINT [PK_dbo.WalletTransactionTypes] PRIMARY KEY CLUSTERED ([TransactionTypeID] ASC)
);

