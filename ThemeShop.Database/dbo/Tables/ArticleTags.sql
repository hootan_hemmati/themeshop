﻿CREATE TABLE [dbo].[ArticleTags] (
    [ArticleTagID] INT            IDENTITY (1, 1) NOT NULL,
    [ArticleID]    INT            NOT NULL,
    [Tag]          NVARCHAR (200) NOT NULL,
    CONSTRAINT [PK_ArticleTags] PRIMARY KEY CLUSTERED ([ArticleTagID] ASC),
    CONSTRAINT [FK_dbo.ArticleTags_dbo.Articles_ArticleID] FOREIGN KEY ([ArticleID]) REFERENCES [dbo].[Articles] ([ArticleID]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_ArticleID]
    ON [dbo].[ArticleTags]([ArticleID] ASC);

