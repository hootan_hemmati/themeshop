﻿CREATE TABLE [dbo].[ThemeAttachments] (
    [AttachID]      INT            IDENTITY (1, 1) NOT NULL,
    [ThemeID]       INT            NOT NULL,
    [Title]         NVARCHAR (250) NOT NULL,
    [FileName]      NVARCHAR (200) NULL,
    [FileExtension] NVARCHAR (50)  NULL,
    [FileSize]      INT            NOT NULL,
    [IsActive]      BIT            NOT NULL,
    [DownloadCount] INT            NOT NULL,
    [IsFree]        BIT            NOT NULL,
    CONSTRAINT [PK_dbo.ThemeAttachments] PRIMARY KEY CLUSTERED ([AttachID] ASC),
    CONSTRAINT [FK_dbo.ThemeAttachments_dbo.Themes_ThemeID] FOREIGN KEY ([ThemeID]) REFERENCES [dbo].[Themes] ([ThemeID]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_ThemeID]
    ON [dbo].[ThemeAttachments]([ThemeID] ASC);

