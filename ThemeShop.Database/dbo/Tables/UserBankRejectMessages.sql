﻿CREATE TABLE [dbo].[UserBankRejectMessages] (
    [Id]         BIGINT          IDENTITY (1, 1) NOT NULL,
    [UserBankId] INT             NOT NULL,
    [SentDate]   DATETIME        NOT NULL,
    [Message]    NVARCHAR (1000) NOT NULL,
    CONSTRAINT [PK_UserBankRejectMessages] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_dbo.UserBankRejectMessages_dbo.UserBanks_UserBankId] FOREIGN KEY ([UserBankId]) REFERENCES [dbo].[UserBanks] ([UserBankId]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_UserBankId]
    ON [dbo].[UserBankRejectMessages]([UserBankId] ASC);

