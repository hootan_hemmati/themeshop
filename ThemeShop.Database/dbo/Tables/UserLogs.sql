﻿CREATE TABLE [dbo].[UserLogs] (
    [LogID]       INT             IDENTITY (1, 1) NOT NULL,
    [UserID]      INT             NOT NULL,
    [Description] NVARCHAR (1000) NOT NULL,
    [DateTime]    DATETIME        NOT NULL,
    CONSTRAINT [PK_dbo.UserLogs] PRIMARY KEY CLUSTERED ([LogID] ASC),
    CONSTRAINT [FK_dbo.UserLogs_dbo.Users_UserID] FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([UserID]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_UserID]
    ON [dbo].[UserLogs]([UserID] ASC);

