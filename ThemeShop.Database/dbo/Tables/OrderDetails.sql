﻿CREATE TABLE [dbo].[OrderDetails] (
    [Id]      INT IDENTITY (1, 1) NOT NULL,
    [OrderId] INT NOT NULL,
    [ThemeId] INT NOT NULL,
    [Price]   INT NOT NULL,
    [Dicount] INT NOT NULL,
    CONSTRAINT [PK_OrderDetails] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_dbo.OrderDetails_dbo.Orders_OrderId] FOREIGN KEY ([OrderId]) REFERENCES [dbo].[Orders] ([OrderID]) ON DELETE CASCADE,
    CONSTRAINT [FK_dbo.OrderDetails_dbo.Themes_ThemeId] FOREIGN KEY ([ThemeId]) REFERENCES [dbo].[Themes] ([ThemeID])
);


GO
CREATE NONCLUSTERED INDEX [IX_ThemeId]
    ON [dbo].[OrderDetails]([ThemeId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_OrderId]
    ON [dbo].[OrderDetails]([OrderId] ASC);

