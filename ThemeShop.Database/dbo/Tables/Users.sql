﻿CREATE TABLE [dbo].[Users] (
    [UserID]                INT            IDENTITY (1, 1) NOT NULL,
    [UserName]              NVARCHAR (100) NOT NULL,
    [Email]                 NVARCHAR (250) NOT NULL,
    [Mobile]                NVARCHAR (50)  NULL,
    [Password]              NVARCHAR (100) NOT NULL,
    [ActiveCode]            NVARCHAR (100) NULL,
    [UserDescription]       NVARCHAR (MAX) NULL,
    [IsActive]              BIT            NOT NULL,
    [RegisterDate]          DATETIME       NOT NULL,
    [Avatar]                NVARCHAR (50)  NULL,
    [RegisterIP]            NVARCHAR (150) NULL,
    [IsDelete]              BIT            NOT NULL,
    [Name]                  NVARCHAR (250) NULL,
    [Family]                NVARCHAR (250) NULL,
    [ThemeSellerPercentage] INT            NOT NULL,
    CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED ([UserID] ASC)
);

