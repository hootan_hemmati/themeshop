﻿CREATE TABLE [dbo].[ThemeVisits] (
    [VisitID] INT            IDENTITY (1, 1) NOT NULL,
    [ThemeID] INT            NOT NULL,
    [IP]      NVARCHAR (100) NOT NULL,
    CONSTRAINT [PK_dbo.ThemeVisits] PRIMARY KEY CLUSTERED ([VisitID] ASC),
    CONSTRAINT [FK_dbo.ThemeVisits_dbo.Themes_ThemeID] FOREIGN KEY ([ThemeID]) REFERENCES [dbo].[Themes] ([ThemeID]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_ThemeID]
    ON [dbo].[ThemeVisits]([ThemeID] ASC);

