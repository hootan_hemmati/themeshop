﻿CREATE TABLE [dbo].[SiteSocialNetworks] (
    [NetworkID] INT            IDENTITY (1, 1) NOT NULL,
    [SocialID]  INT            NOT NULL,
    [Url]       NVARCHAR (360) NOT NULL,
    [IsActive]  BIT            NOT NULL,
    CONSTRAINT [PK_dbo.SiteSocialNetworks] PRIMARY KEY CLUSTERED ([NetworkID] ASC),
    CONSTRAINT [FK_dbo.SiteSocialNetworks_dbo.SocialNetworks_SocialID] FOREIGN KEY ([SocialID]) REFERENCES [dbo].[SocialNetworks] ([SocialID]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_SocialID]
    ON [dbo].[SiteSocialNetworks]([SocialID] ASC);

