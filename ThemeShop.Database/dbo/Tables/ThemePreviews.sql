﻿CREATE TABLE [dbo].[ThemePreviews] (
    [PreviewID]     INT            IDENTITY (1, 1) NOT NULL,
    [ThemeID]       INT            NOT NULL,
    [RootFolder]    NVARCHAR (400) NOT NULL,
    [PageTitle]     NVARCHAR (400) NOT NULL,
    [PageName]      NVARCHAR (400) NOT NULL,
    [IsDefaultPage] BIT            NOT NULL,
    [IsActive]      BIT            NOT NULL,
    CONSTRAINT [PK_dbo.ThemePreviews] PRIMARY KEY CLUSTERED ([PreviewID] ASC),
    CONSTRAINT [FK_dbo.ThemePreviews_dbo.Themes_ThemeID] FOREIGN KEY ([ThemeID]) REFERENCES [dbo].[Themes] ([ThemeID]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_ThemeID]
    ON [dbo].[ThemePreviews]([ThemeID] ASC);

