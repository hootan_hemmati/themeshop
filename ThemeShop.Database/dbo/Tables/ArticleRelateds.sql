﻿CREATE TABLE [dbo].[ArticleRelateds] (
    [RelatedID]        INT IDENTITY (1, 1) NOT NULL,
    [ArticleID]        INT NOT NULL,
    [ArticleRelatedID] INT NOT NULL,
    CONSTRAINT [PK_dbo.ArticleRelateds] PRIMARY KEY CLUSTERED ([RelatedID] ASC),
    CONSTRAINT [FK_dbo.ArticleRelateds_dbo.Articles_ArticleID] FOREIGN KEY ([ArticleID]) REFERENCES [dbo].[Articles] ([ArticleID]),
    CONSTRAINT [FK_dbo.ArticleRelateds_dbo.Articles_ArticleRelatedID] FOREIGN KEY ([ArticleRelatedID]) REFERENCES [dbo].[Articles] ([ArticleID])
);


GO
CREATE NONCLUSTERED INDEX [IX_ArticleRelatedID]
    ON [dbo].[ArticleRelateds]([ArticleRelatedID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ArticleID]
    ON [dbo].[ArticleRelateds]([ArticleID] ASC);

