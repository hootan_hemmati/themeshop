﻿CREATE TABLE [dbo].[SiteAbouUs] (
    [AboutID]          INT            NOT NULL,
    [BrowserTitle]     NVARCHAR (300) NOT NULL,
    [Title]            NVARCHAR (400) NOT NULL,
    [ShortDescription] NVARCHAR (700) NOT NULL,
    [Text]             NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_dbo.SiteAbouUs] PRIMARY KEY CLUSTERED ([AboutID] ASC)
);

