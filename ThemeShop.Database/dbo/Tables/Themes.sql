﻿CREATE TABLE [dbo].[Themes] (
    [ThemeID]          INT            IDENTITY (1, 1) NOT NULL,
    [SellerID]         INT            NOT NULL,
    [ThemeTitle]       NVARCHAR (400) NOT NULL,
    [ShortDescription] NVARCHAR (500) NOT NULL,
    [Description]      NVARCHAR (MAX) NOT NULL,
    [Featurs]          NVARCHAR (MAX) NOT NULL,
    [ThemeImageName]   NVARCHAR (200) NULL,
    [Price]            INT            NOT NULL,
    [IsActive]         BIT            NOT NULL,
    [CanInsertComment] BIT            NOT NULL,
    [CreateDate]       DATETIME       NOT NULL,
    [IsDelete]         BIT            NOT NULL,
    [Theme_ThemeID]    INT            NULL,
    [IsRejected]       BIT            NOT NULL,
    CONSTRAINT [PK_Themes] PRIMARY KEY CLUSTERED ([ThemeID] ASC),
    CONSTRAINT [FK_dbo.Themes_dbo.Themes_Theme_ThemeID] FOREIGN KEY ([Theme_ThemeID]) REFERENCES [dbo].[Themes] ([ThemeID]),
    CONSTRAINT [FK_dbo.Themes_dbo.Users_SellerID] FOREIGN KEY ([SellerID]) REFERENCES [dbo].[Users] ([UserID]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_Theme_ThemeID]
    ON [dbo].[Themes]([Theme_ThemeID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_SellerID]
    ON [dbo].[Themes]([SellerID] ASC);

