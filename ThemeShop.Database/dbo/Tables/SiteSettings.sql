﻿CREATE TABLE [dbo].[SiteSettings] (
    [SettingID]         INT            NOT NULL,
    [SiteName]          NVARCHAR (300) NOT NULL,
    [SiteTitle]         NVARCHAR (360) NOT NULL,
    [SiteDescription]   NVARCHAR (500) NOT NULL,
    [SiteEmail]         NVARCHAR (300) NOT NULL,
    [SiteTell]          NVARCHAR (250) NULL,
    [SiteMobile]        NVARCHAR (200) NULL,
    [Address]           NVARCHAR (MAX) NULL,
    [Map]               NVARCHAR (MAX) NULL,
    [SiteFax]           NVARCHAR (200) NULL,
    [SupportTell]       NVARCHAR (200) NULL,
    [SupportEmail]      NVARCHAR (250) NULL,
    [SiteDomain]        NVARCHAR (300) NULL,
    [LogoName]          NVARCHAR (100) NULL,
    [CopyRight]         NVARCHAR (MAX) NULL,
    [UserWalletOwenrId] INT            NULL,
    CONSTRAINT [PK_dbo.SiteSettings] PRIMARY KEY CLUSTERED ([SettingID] ASC)
);

