﻿CREATE TABLE [dbo].[EmailSettings] (
    [EmailID]        INT            IDENTITY (1, 1) NOT NULL,
    [From]           NVARCHAR (200) NOT NULL,
    [DisplayName]    NVARCHAR (200) NOT NULL,
    [SMTP]           NVARCHAR (200) NOT NULL,
    [Port]           INT            NOT NULL,
    [UserName]       NVARCHAR (200) NOT NULL,
    [Password]       NVARCHAR (200) NOT NULL,
    [EnableSSL]      BIT            NOT NULL,
    [IsDefaultEmail] BIT            NOT NULL,
    CONSTRAINT [PK_dbo.EmailSettings] PRIMARY KEY CLUSTERED ([EmailID] ASC)
);

