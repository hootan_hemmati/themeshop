﻿CREATE TABLE [dbo].[Permissions] (
    [PermissionID]    INT            IDENTITY (1, 1) NOT NULL,
    [Title]           NVARCHAR (300) NOT NULL,
    [Name]            NVARCHAR (300) NOT NULL,
    [ParentID]        INT            NULL,
    [DisplayPriority] INT            NULL,
    CONSTRAINT [PK_Permissions] PRIMARY KEY CLUSTERED ([PermissionID] ASC),
    CONSTRAINT [FK_dbo.Permissions_dbo.Permissions_ParentID] FOREIGN KEY ([ParentID]) REFERENCES [dbo].[Permissions] ([PermissionID])
);


GO
CREATE NONCLUSTERED INDEX [IX_ParentID]
    ON [dbo].[Permissions]([ParentID] ASC);

