﻿CREATE TABLE [dbo].[Orders] (
    [OrderID]    INT      IDENTITY (1, 1) NOT NULL,
    [UserID]     INT      NOT NULL,
    [CreateDate] DATETIME NOT NULL,
    [IsFinaly]   BIT      NOT NULL,
    CONSTRAINT [PK_Orders] PRIMARY KEY CLUSTERED ([OrderID] ASC),
    CONSTRAINT [FK_dbo.Orders_dbo.Users_UserID] FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([UserID]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_UserID]
    ON [dbo].[Orders]([UserID] ASC);

