﻿CREATE TABLE [dbo].[ThemeChangeStatusMessages] (
    [Id]       BIGINT          IDENTITY (1, 1) NOT NULL,
    [ThemeId]  INT             NOT NULL,
    [Status]   INT             NOT NULL,
    [SentDate] DATETIME        NOT NULL,
    [Message]  NVARCHAR (1000) NOT NULL,
    CONSTRAINT [PK_ThemeChangeStatusMessages] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_dbo.ThemeChangeStatusMessages_dbo.Themes_ThemeId] FOREIGN KEY ([ThemeId]) REFERENCES [dbo].[Themes] ([ThemeID]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_ThemeId]
    ON [dbo].[ThemeChangeStatusMessages]([ThemeId] ASC);

