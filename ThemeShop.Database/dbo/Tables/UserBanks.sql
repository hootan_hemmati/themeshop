﻿CREATE TABLE [dbo].[UserBanks] (
    [UserBankId]          INT            IDENTITY (1, 1) NOT NULL,
    [UserId]              INT            NOT NULL,
    [CodeMeli]            NVARCHAR (10)  NOT NULL,
    [ImageKartMeli]       NVARCHAR (250) NOT NULL,
    [SellerAccountHolder] NVARCHAR (250) NOT NULL,
    [BankName]            NVARCHAR (250) NOT NULL,
    [SellerAccountNumber] NVARCHAR (250) NOT NULL,
    [SellerCardNumber]    NVARCHAR (250) NOT NULL,
    [SellerShabaNumber]   NVARCHAR (250) NOT NULL,
    [ImageCardBank]       NVARCHAR (250) NOT NULL,
    [CreateDate]          DATETIME       NOT NULL,
    [IsActive]            BIT            NOT NULL,
    [IsReject]            BIT            NOT NULL,
    CONSTRAINT [PK_UserBanks] PRIMARY KEY CLUSTERED ([UserBankId] ASC),
    CONSTRAINT [FK_dbo.UserBanks_dbo.Users_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[Users] ([UserID]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_UserId]
    ON [dbo].[UserBanks]([UserId] ASC);

