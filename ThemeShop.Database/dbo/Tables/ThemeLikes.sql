﻿CREATE TABLE [dbo].[ThemeLikes] (
    [LikeID]  INT            IDENTITY (1, 1) NOT NULL,
    [UserID]  INT            NULL,
    [ThemeID] INT            NOT NULL,
    [IP]      NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_dbo.ThemeLikes] PRIMARY KEY CLUSTERED ([LikeID] ASC),
    CONSTRAINT [FK_dbo.ThemeLikes_dbo.Themes_ThemeID] FOREIGN KEY ([ThemeID]) REFERENCES [dbo].[Themes] ([ThemeID]) ON DELETE CASCADE,
    CONSTRAINT [FK_dbo.ThemeLikes_dbo.Users_UserID] FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([UserID])
);


GO
CREATE NONCLUSTERED INDEX [IX_UserID]
    ON [dbo].[ThemeLikes]([UserID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ThemeID]
    ON [dbo].[ThemeLikes]([ThemeID] ASC);

