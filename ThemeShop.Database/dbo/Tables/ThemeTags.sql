﻿CREATE TABLE [dbo].[ThemeTags] (
    [ThemeTagID] INT            IDENTITY (1, 1) NOT NULL,
    [ThemeID]    INT            NOT NULL,
    [Tag]        NVARCHAR (200) NOT NULL,
    CONSTRAINT [PK_ThemeTags] PRIMARY KEY CLUSTERED ([ThemeTagID] ASC),
    CONSTRAINT [FK_dbo.ThemeTags_dbo.Themes_ThemeID] FOREIGN KEY ([ThemeID]) REFERENCES [dbo].[Themes] ([ThemeID]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_ThemeID]
    ON [dbo].[ThemeTags]([ThemeID] ASC);

