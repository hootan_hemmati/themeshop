﻿CREATE TABLE [dbo].[WalletTypes] (
    [TypeID]    INT            NOT NULL,
    [TypeTitle] NVARCHAR (100) NOT NULL,
    CONSTRAINT [PK_dbo.WalletTypes] PRIMARY KEY CLUSTERED ([TypeID] ASC)
);

