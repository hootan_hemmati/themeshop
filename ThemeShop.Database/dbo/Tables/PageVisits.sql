﻿CREATE TABLE [dbo].[PageVisits] (
    [VisitID] INT            IDENTITY (1, 1) NOT NULL,
    [PageID]  INT            NOT NULL,
    [IP]      NVARCHAR (100) NOT NULL,
    CONSTRAINT [PK_dbo.PageVisits] PRIMARY KEY CLUSTERED ([VisitID] ASC),
    CONSTRAINT [FK_dbo.PageVisits_dbo.Pages_PageID] FOREIGN KEY ([PageID]) REFERENCES [dbo].[Pages] ([PageID]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_PageID]
    ON [dbo].[PageVisits]([PageID] ASC);

