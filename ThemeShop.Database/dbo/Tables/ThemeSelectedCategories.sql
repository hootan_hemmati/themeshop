﻿CREATE TABLE [dbo].[ThemeSelectedCategories] (
    [SelectedID] INT IDENTITY (1, 1) NOT NULL,
    [ThemeID]    INT NOT NULL,
    [CategoryID] INT NOT NULL,
    CONSTRAINT [PK_dbo.ThemeSelectedCategories] PRIMARY KEY CLUSTERED ([SelectedID] ASC),
    CONSTRAINT [FK_dbo.ThemeSelectedCategories_dbo.ThemeCategories_CategoryID] FOREIGN KEY ([CategoryID]) REFERENCES [dbo].[ThemeCategories] ([CategoryID]) ON DELETE CASCADE,
    CONSTRAINT [FK_dbo.ThemeSelectedCategories_dbo.Themes_ThemeID] FOREIGN KEY ([ThemeID]) REFERENCES [dbo].[Themes] ([ThemeID]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_ThemeID]
    ON [dbo].[ThemeSelectedCategories]([ThemeID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_CategoryID]
    ON [dbo].[ThemeSelectedCategories]([CategoryID] ASC);

