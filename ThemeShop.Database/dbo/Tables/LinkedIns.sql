﻿CREATE TABLE [dbo].[LinkedIns] (
    [LinkID]      INT            IDENTITY (1, 1) NOT NULL,
    [Title]       NVARCHAR (200) NOT NULL,
    [Description] NVARCHAR (200) NULL,
    [target]      NVARCHAR (MAX) NULL,
    [url]         NVARCHAR (500) NOT NULL,
    [StartDate]   DATETIME       NULL,
    [EndDate]     DATETIME       NULL,
    [IsActive]    BIT            NOT NULL,
    CONSTRAINT [PK_dbo.LinkedIns] PRIMARY KEY CLUSTERED ([LinkID] ASC)
);

