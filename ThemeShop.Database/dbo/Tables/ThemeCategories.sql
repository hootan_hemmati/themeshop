﻿CREATE TABLE [dbo].[ThemeCategories] (
    [CategoryID] INT            IDENTITY (1, 1) NOT NULL,
    [Title]      NVARCHAR (260) NOT NULL,
    [NameInUrl]  NVARCHAR (200) NOT NULL,
    [ParentID]   INT            NULL,
    [IsActive]   BIT            NOT NULL,
    [IsDelete]   BIT            NOT NULL,
    CONSTRAINT [PK_dbo.ThemeCategories] PRIMARY KEY CLUSTERED ([CategoryID] ASC),
    CONSTRAINT [FK_dbo.ThemeCategories_dbo.ThemeCategories_ParentID] FOREIGN KEY ([ParentID]) REFERENCES [dbo].[ThemeCategories] ([CategoryID])
);


GO
CREATE NONCLUSTERED INDEX [IX_ParentID]
    ON [dbo].[ThemeCategories]([ParentID] ASC);

