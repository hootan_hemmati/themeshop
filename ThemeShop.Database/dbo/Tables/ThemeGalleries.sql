﻿CREATE TABLE [dbo].[ThemeGalleries] (
    [GalleryID]        INT            IDENTITY (1, 1) NOT NULL,
    [ThemeID]          INT            NOT NULL,
    [GalleryImageName] NVARCHAR (200) NOT NULL,
    [ImageTitle]       NVARCHAR (200) DEFAULT ('') NOT NULL,
    CONSTRAINT [PK_dbo.ThemeGalleries] PRIMARY KEY CLUSTERED ([GalleryID] ASC),
    CONSTRAINT [FK_dbo.ThemeGalleries_dbo.Themes_ThemeID] FOREIGN KEY ([ThemeID]) REFERENCES [dbo].[Themes] ([ThemeID]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_ThemeID]
    ON [dbo].[ThemeGalleries]([ThemeID] ASC);

