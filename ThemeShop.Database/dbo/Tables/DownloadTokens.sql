﻿CREATE TABLE [dbo].[DownloadTokens] (
    [Id]          INT            IDENTITY (1, 1) NOT NULL,
    [Token]       NVARCHAR (MAX) NULL,
    [UserId]      INT            NOT NULL,
    [GeneratedOn] DATETIME       NOT NULL,
    [IsExpired]   BIT            NOT NULL,
    [ThemeId]     INT            NOT NULL,
    CONSTRAINT [PK_DownloadTokens] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_dbo.DownloadTokens_dbo.Themes_ThemeId] FOREIGN KEY ([ThemeId]) REFERENCES [dbo].[Themes] ([ThemeID]),
    CONSTRAINT [FK_dbo.DownloadTokens_dbo.Users_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[Users] ([UserID]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_UserId]
    ON [dbo].[DownloadTokens]([UserId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ThemeId]
    ON [dbo].[DownloadTokens]([ThemeId] ASC);

