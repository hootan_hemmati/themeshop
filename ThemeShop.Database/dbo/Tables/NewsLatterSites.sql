﻿CREATE TABLE [dbo].[NewsLatterSites] (
    [NewsLetterID] INT            IDENTITY (1, 1) NOT NULL,
    [Email]        NVARCHAR (200) NOT NULL,
    CONSTRAINT [PK_dbo.NewsLatterSites] PRIMARY KEY CLUSTERED ([NewsLetterID] ASC)
);

