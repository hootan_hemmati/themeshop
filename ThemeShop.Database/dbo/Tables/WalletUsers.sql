﻿CREATE TABLE [dbo].[WalletUsers] (
    [WalletID]          INT            IDENTITY (1, 1) NOT NULL,
    [TransactionTypeID] INT            NOT NULL,
    [TypeID]            INT            NOT NULL,
    [UserID]            INT            NOT NULL,
    [Price]             INT            NOT NULL,
    [OrderID]           INT            NULL,
    [IsPay]             BIT            NOT NULL,
    [CreateDate]        DATETIME       NOT NULL,
    [Description]       NVARCHAR (500) NULL,
    CONSTRAINT [PK_dbo.WalletUsers] PRIMARY KEY CLUSTERED ([WalletID] ASC),
    CONSTRAINT [FK_dbo.WalletUsers_dbo.Orders_OrderID] FOREIGN KEY ([OrderID]) REFERENCES [dbo].[Orders] ([OrderID]),
    CONSTRAINT [FK_dbo.WalletUsers_dbo.Users_UserID] FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([UserID]) ON DELETE CASCADE,
    CONSTRAINT [FK_dbo.WalletUsers_dbo.WalletTransactionTypes_TransactionTypeID] FOREIGN KEY ([TransactionTypeID]) REFERENCES [dbo].[WalletTransactionTypes] ([TransactionTypeID]) ON DELETE CASCADE,
    CONSTRAINT [FK_dbo.WalletUsers_dbo.WalletTypes_TypeID] FOREIGN KEY ([TypeID]) REFERENCES [dbo].[WalletTypes] ([TypeID]) ON DELETE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_UserID]
    ON [dbo].[WalletUsers]([UserID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_TypeID]
    ON [dbo].[WalletUsers]([TypeID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_TransactionTypeID]
    ON [dbo].[WalletUsers]([TransactionTypeID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_OrderID]
    ON [dbo].[WalletUsers]([OrderID] ASC);

