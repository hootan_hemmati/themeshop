﻿CREATE TABLE [dbo].[DiscountCodeUses] (
    [DiscountCodeUseId] INT IDENTITY (1, 1) NOT NULL,
    [DiscontId]         INT NOT NULL,
    [UserId]            INT NOT NULL,
    [OrderId]           INT NOT NULL,
    CONSTRAINT [PK_DiscountCodeUses] PRIMARY KEY CLUSTERED ([DiscountCodeUseId] ASC),
    CONSTRAINT [FK_dbo.DiscountCodeUses_dbo.DiscontCodes_DiscontId] FOREIGN KEY ([DiscontId]) REFERENCES [dbo].[DiscontCodes] ([DisCountCodeId]),
    CONSTRAINT [FK_dbo.DiscountCodeUses_dbo.Orders_OrderId] FOREIGN KEY ([OrderId]) REFERENCES [dbo].[Orders] ([OrderID]),
    CONSTRAINT [FK_dbo.DiscountCodeUses_dbo.Users_UsedBy_UserID] FOREIGN KEY ([UserId]) REFERENCES [dbo].[Users] ([UserID])
);


GO
CREATE NONCLUSTERED INDEX [IX_UserId]
    ON [dbo].[DiscountCodeUses]([UserId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_OrderId]
    ON [dbo].[DiscountCodeUses]([OrderId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_DiscontId]
    ON [dbo].[DiscountCodeUses]([DiscontId] ASC);

