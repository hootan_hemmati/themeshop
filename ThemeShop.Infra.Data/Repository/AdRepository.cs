﻿using Microsoft.EntityFrameworkCore;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

using ThemeShop.Domain.Interfaces;
using ThemeShop.Domain.Models;
using ThemeShop.Infra.Data.Context;

namespace ThemeShop.Infra.Data.Repository
{
    public class AdRepository : IAdRepository
    {
        private readonly ThemeShop_DBContext _db;

        public AdRepository(ThemeShop_DBContext db)
        {
            _db = db;
        }

        #region ad

        public async Task<int> InsertAdAsync(Ads ad)
        {
            await _db.Ads.AddAsync(ad);
            return ad.AdId;
        }

        public async Task<Ads> GetAdAsync(int adsId)
        {
            return await _db.Ads.FindAsync(adsId);
        }

        public void UpdateAd(Ads ad)
        {
            _db.Update(ad);
        }

        public void RemoveAd(Ads ad)
        {
            _db.Ads.Remove(ad);
        }

        public async Task<IList<Ads>> GetAdsAsync(Expression<Func<Ads, bool>> where)
        {
            return await _db.Ads.Where(where).ToListAsync();
        }

        public Task<int> GetAdsCountAsync()
        {
            return _db.Ads.CountAsync();
        }

        public async Task<IList<Ads>> GetAdsAsync()
        {
            return await _db.Ads.ToListAsync();
        }

        #endregion ad

        #region Ad Theme Categories

        public async Task InsertAdCategoryAsync(AdThemeCategories adThemeCategories)
        {
            await _db.AdThemeCategories.AddAsync(adThemeCategories);
        }

        public async Task<IList<AdThemeCategories>> GetAdCategoriesAsync(Expression<Func<AdThemeCategories, bool>> where)
        {
            return await _db.AdThemeCategories.Where(where).ToListAsync();
        }

        public void RemoveAdCategories(IList<AdThemeCategories> categories)
        {
            _db.AdThemeCategories.RemoveRange(categories);
        }

        #endregion Ad Theme Categories

        public async Task SaveAsync()
        {
            await _db.SaveChangesAsync();
        }

        public void Save()
        {
            _db.SaveChanges();
        }
    }
}