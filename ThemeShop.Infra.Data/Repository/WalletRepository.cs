﻿using Microsoft.EntityFrameworkCore;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

using ThemeShop.Domain.Interfaces;
using ThemeShop.Domain.Models;
using ThemeShop.Infra.Data.Context;

namespace ThemeShop.Infra.Data.Repository
{
    public class WalletRepository : IWalletRepository
    {
        private readonly ThemeShop_DBContext _db;

        public WalletRepository(ThemeShop_DBContext db)
        {
            _db = db;
        }

        #region Wallet

        public async Task InsertWalletAsync(Wallets wallets)
        {
            await _db.Wallets.AddAsync(wallets);
        }

        public async Task<Wallets> GetWalletsAsync(int id)
        {
            return await _db.Wallets.FindAsync(id);
        }

        public void UpdateWallet(Wallets wallets)
        {
            _db.Wallets.Update(wallets);
        }

        #endregion Wallet

        #region WalletLog

        public async Task InsertWalletLogAsync(WalletLogs walletLogs)
        {
            await _db.WalletLogs.AddAsync(walletLogs);
        }

        public async Task<int> GetWalletLogCountAsync(Expression<Func<WalletLogs, bool>> where)
        {
            return await _db.WalletLogs.CountAsync(where);
        }

        public async Task<IList<WalletLogs>> GetWalletLogsAsync(Expression<Func<WalletLogs, bool>> where)
        {
            return await _db.WalletLogs.Where(where).Include(w => w.Wallet.User).ToListAsync();
        }

        public async Task<WalletLogs> GetWalletLogsAsync(int id)
        {
            return await _db.WalletLogs.FindAsync(id);
        }

        public async Task<IList<WalletLogs>> GetAllWalletLogsAsync()
        {
            return await _db.WalletLogs.Include(wl => wl.Wallet.User).ToListAsync();
        }

        public async Task<IList<WalletLogs>> FilterWalletLogsAsync(string userName, string transactionType, long? minimumPrice, long? maximumPrice, DateTime startDate, DateTime endDate, string description, int pageId = 1, byte take = 20)
        {
            IList<WalletLogs> transactions = await GetAllWalletLogsAsync();
            if (!string.IsNullOrWhiteSpace(userName) && !string.IsNullOrEmpty(userName))
            {
                transactions = _db.WalletLogs.Where(x => EF.Functions.Like(x.Wallet.User.UserName, $"%{userName}%")).ToList();
            }

            if (!string.IsNullOrWhiteSpace(description) && !string.IsNullOrEmpty(description))
            {
                transactions = transactions.Where(x => x.Text == description).ToList();
            }

            if (endDate != DateTime.MinValue)
            {
                transactions = transactions.Where(x => x.LogTime < endDate).ToList();
            }

            if (startDate != DateTime.MinValue)
            {
                transactions = transactions.Where(x => x.LogTime > startDate).ToList();
            }

            if (maximumPrice != 0 && maximumPrice != null)
            {
                transactions = transactions.Where(x => x.LogPrice < maximumPrice).ToList();
            }

            if (minimumPrice != null && minimumPrice != 0)
            {
                transactions = transactions.Where(x => x.LogPrice > minimumPrice).ToList();
            }

            if (!string.IsNullOrWhiteSpace(transactionType) && !string.IsNullOrEmpty(transactionType))
            {
                transactions = transactions.Where(x => x.LogDescription == transactionType).ToList();
            }

            return transactions;
        }

        #endregion WalletLog

        #region Wallet Transactions

        /// <summary>
        /// Get Wallet and Calculate Balance
        /// </summary>
        /// <param name="walletId"> User Wallet ID </param>
        /// <returns></returns>
        public async Task<long> CalculateAccouuntBallanceAsync(int walletId)
        {
            IList<long> accountCharges = await CalculateAccouuntChargesAsync(walletId);
            IList<long> accouuntPayments = await CalculateAccouuntPaymentsAsync(walletId);
            return accountCharges.Sum() - accouuntPayments.Sum();
        }

        public async Task<IList<long>> CalculateAccouuntChargesAsync(int walletId)
        {
            return await _db.WalletLogs.Where(w => w.IsCharge && w.WalletId == walletId).Select(c => c.LogPrice).ToListAsync();
        }

        public async Task<IList<long>> CalculateAccouuntPaymentsAsync(int walletId)
        {
            return await _db.WalletLogs.Where(w => !w.IsCharge && w.WalletId == walletId).Select(p => p.LogPrice).ToListAsync();
        }

        #endregion Wallet Transactions

        public async Task SaveAsync()
        {
            await _db.SaveChangesAsync();
        }
    }
}