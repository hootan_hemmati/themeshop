﻿using Microsoft.EntityFrameworkCore;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

using ThemeShop.Domain.Interfaces;
using ThemeShop.Domain.Models;
using ThemeShop.Infra.Data.Context;

namespace ThemeShop.Infra.Data.Repository
{
    public class TicketRepository : ITicketRepository
    {
        private readonly ThemeShop_DBContext _db;

        public TicketRepository(ThemeShop_DBContext db)
        {
            _db = db;
        }

        #region Ticketing

        public async Task AddTicketAsync(Ticketings ticketing)
        {
            await _db.AddAsync(ticketing);
        }

        public async Task<IList<Ticketings>> FilterTicketsAsync(string query)
        {
            return await _db.Ticketings.Where(ticket => EF.Functions.Like(ticket.Title, $"%{query}%")).ToListAsync();
        }

        public async Task<int> GetAllTicketsCountAsync()
        {
            return await _db.Ticketings.CountAsync();
        }

        public async Task<int> FilterTicketsCountAsync(string query)
        {
            return await _db.Ticketings.Where(ticket => EF.Functions.Like(ticket.Title, $"%{query}%")).Distinct().CountAsync();
        }

        public async Task<IList<Ticketings>> GetAllTicketsAsync()
        {
            return await _db.Ticketings.Where(ticket => !ticket.IsDelete).Distinct().OrderByDescending(ticket => ticket.IsAnswered == false || ticket.IsClose == false).ToListAsync();
        }

        public async Task<Ticketings> GetTicketByIdAsync(int ticketId)
        {
            return await _db.Ticketings.FindAsync(ticketId);
        }

        public async Task<IList<Ticketings>> GetTicketsAsync(Expression<Func<Ticketings, bool>> where)
        {
            return await _db.Ticketings.Where(where).ToListAsync();
        }

        public async Task<Ticketings> GetTicketAsync(Expression<Func<Ticketings, bool>> where)
        {
            return await _db.Ticketings.FirstOrDefaultAsync(where);
        }

        public async Task<bool> IsTicketExistAsync(Expression<Func<Ticketings, bool>> where)
        {
            return await _db.Ticketings.AnyAsync(where);
        }

        public void UpdateTicket(Ticketings ticket)
        {
            _db.Update(ticket);
        }

        #endregion Ticketing

        #region TicketMessage

        public async Task<IList<TicketMessages>> GetAllTicketMessagesByTicketIdAsync(int ticketId)
        {
            return await _db.TicketMessages.Where(ticketMessage => ticketMessage.TicketId == ticketId).ToListAsync();
        }

        public async Task AddTicketMessageAsync(TicketMessages ticketMessage)
        {
            await _db.AddAsync(ticketMessage);
        }

        public async Task<IList<TicketMessages>> GetTicketMessagesAsync(Expression<Func<TicketMessages, bool>> where)
        {
            return await _db.TicketMessages.Where(where).ToListAsync();
        }

        public async Task<TicketMessages> GetTicketMessageAsync(Expression<Func<TicketMessages, bool>> where)
        {
            return await _db.TicketMessages.FirstOrDefaultAsync(where);
        }

        public async Task<bool> IsTicketMessageExistAsync(Expression<Func<TicketMessages, bool>> where)
        {
            return await _db.TicketMessages.AnyAsync(where);
        }

        public void UpdateTicketMessage(TicketMessages ticketMessages)
        {
            _db.Update(ticketMessages);
        }

        #endregion TicketMessage

        public async Task SaveAsync()
        {
            await _db.SaveChangesAsync();
        }

        public void Save()
        {
            _db.SaveChanges();
        }
    }
}