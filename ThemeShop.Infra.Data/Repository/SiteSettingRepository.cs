﻿using Microsoft.EntityFrameworkCore;

using System;
using System.Linq;
using System.Threading.Tasks;

using ThemeShop.Domain.Interfaces;
using ThemeShop.Infra.Data.Context;

namespace ThemeShop.Infra.Data.Repository
{
    public class SiteSettingRepository : ISiteSettingRepository
    {
        private readonly ThemeShop_DBContext _db;

        public SiteSettingRepository(ThemeShop_DBContext db)
        {
            _db = db;
        }

        #region GetOrderCount

        /// <summary>
        /// Get order count on day
        /// </summary>
        /// <param name="time">Curent Time</param>
        /// <returns></returns>
        public async Task<int> OrderCountOndayAsync(DateTime time)
        {
            int OrderOnDay = await _db.Orders.Where(o => o.CompleteTime.Year == time.Year
                                                        && o.CompleteTime.Month == time.Month
                                                        && o.CompleteTime.Day == time.Day).CountAsync();
            return OrderOnDay;
        }

        /// <summary>
        /// Get order count on month
        /// </summary>
        /// <param name="time">Current Time</param>
        /// <returns></returns>
        public async Task<int> OrderCountOnMonthAsync(DateTime time)
        {
            int OrderOnMonth = await _db.Orders.Where(o => o.CompleteTime.Year == time.Year
                                            && o.CompleteTime.Month == time.Month).CountAsync();
            return OrderOnMonth;
        }

        /// <summary>
        /// Get order count on year
        /// </summary>
        /// <param name="time">Current Time</param>
        /// <returns></returns>
        public async Task<int> OrderCountOnYearsAsync(DateTime time)
        {
            int OrderOnYear = await _db.Orders.Where(o => o.CompleteTime.Year == time.Year).CountAsync();
            return OrderOnYear;
        }

        #endregion GetOrderCount
    }
}