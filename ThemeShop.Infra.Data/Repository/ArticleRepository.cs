﻿using Microsoft.EntityFrameworkCore;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

using ThemeShop.Domain.Interfaces;
using ThemeShop.Domain.Models;
using ThemeShop.Infra.Data.Context;

namespace ThemeShop.Infra.Data.Repository
{
    public class ArticleRepository : IArticleRepository
    {
        private readonly ThemeShop_DBContext _db;

        public ArticleRepository(ThemeShop_DBContext db)
        {
            _db = db;
        }

        #region Article

        public async Task InsertArticleAsync(Articles articles)
        {
            await _db.Articles.AddAsync(articles);
        }

        public async Task<int> GetArticleCountAsync()
        {
            return await _db.Articles.CountAsync();
        }

        public void RemoveArticle(Articles article)
        {
            _db.Articles.Remove(article);
        }

        public void UpdateArticle(Articles articles)
        {
            _db.Update(articles);
        }

        public async Task<Articles> GetArticleAsync(int id)
        {
            return await _db.Articles.FindAsync(id);
        }

        public async Task<IList<Articles>> GetArticlesAsync()
        {
            return await _db.Articles.ToListAsync();
        }
        #endregion Article

        #region Main Category

        public async Task<IList<MainArticleCategories>> GetMainArticleCategoriesAsync()
        {
            return await _db.MainArticleCategories.Where(ac => ac.IsActive && !ac.IsDelete).ToListAsync();
        }

        public async Task<IList<MainArticleCategories>> GetAllMainArticleCategoriesAsync(int articleId)
        {
            return await _db.ArticleCategories.Where(x => x.MainArticleId == articleId && x.IsActive && !x.IsDelete)
                .Include(x => x.MainCategory)
                .Select(x => x.MainCategory).ToListAsync();
        }

        public async Task<MainArticleCategories> GetMainArticleCategoriesAsync(int categoryId)
        {
            return await _db.MainArticleCategories.FirstOrDefaultAsync(ac => ac.MainCatagoryId == categoryId && ac.IsActive && !ac.IsDelete);
        }

        public async Task InsertMainArticleCategoryAsync(MainArticleCategories mainArticleCategories)
        {
            await _db.MainArticleCategories.AddAsync(mainArticleCategories);
        }

        public void UpdateMainArticleCategories(MainArticleCategories categories)
        {
            _db.MainArticleCategories.Update(categories);
        }

        public async Task<IList<MainArticleCategories>> GetMainArticleCategoriesWithChildAsync()
        {
            return await _db.MainArticleCategories.Where(x => x.IsActive && !x.IsDelete)
                .Include(c => c.ArticleCategories)
                .ToListAsync(); ;
        }

        #endregion Main Category

        #region Article Related
        public async Task InsertArticleRelatedAsync(IList<ArticleRelateds> articleRelateds)
        {
            await _db.ArticleRelateds.AddRangeAsync(articleRelateds);
        }

        public async Task InsertArticleRelatedAsync(ArticleRelateds articleRelated)
        {
            await _db.ArticleRelateds.AddAsync(articleRelated);
        }

        public async Task<ArticleRelateds> GetArticleRelatedAsync(int id)
        {
            return await _db.ArticleRelateds.FindAsync(id);
        }

        public async Task<IList<ArticleRelateds>> GetArticleRelatedsAsync(Expression<Func<ArticleRelateds, bool>> where)
        {
            return await _db.ArticleRelateds.Where(where).Include(x => x.Article.ArticleTitle).ToListAsync();
        }

        public void RemoveArticleRelated(IList<ArticleRelateds> articleRelateds)
        {
            _db.ArticleRelateds.RemoveRange(articleRelateds);
        }

        public void RemoveArticleRelated(ArticleRelateds articleRelated)
        {
            _db.ArticleRelateds.Remove(articleRelated);
        }

        #endregion Article Related

        #region ArticleCategories
        public async Task InsertArticleCategoriesAsync(IList<ArticleCategories> articleCategories)
        {
            await _db.ArticleCategories.AddRangeAsync(articleCategories);
        }

        public async Task InsertArticleCategoryAsync(ArticleCategories articleCategories)
        {
            await _db.ArticleCategories.AddAsync(articleCategories);
        }

        public void RemoveArticleCategories(IList<ArticleCategories> articleCategories)
        {
            _db.ArticleCategories.RemoveRange(articleCategories);
        }

        public async Task<ArticleCategories> GetArticleCategoriesAsync(int id)
        {
            return await _db.ArticleCategories.FindAsync(id);
        }

        public void UpdateArticleCategories(ArticleCategories categories)
        {
            _db.ArticleCategories.Update(categories);
        }
        #endregion

        #region Article Category

        public async Task InsertCategoryArticleAsync(ArticleCategory category)
        {
            await _db.ArticleCategory.AddAsync(category);
        }


        public void RemoveCategoryArticle(ArticleCategory category)
        {
            _db.ArticleCategory.Remove(category);
        }

        public void UpdateCategoryArticle(ArticleCategory category)
        {
            _db.Update(category);
        }

        public async Task<IList<ArticleCategory>> GetArticleCategoryAsync(Expression<Func<ArticleCategory, bool>> where)
        {
            return await _db.ArticleCategory.Where(where).ToListAsync();
        }

        #endregion Article Category

        #region Article Tags

        public async Task InsertArticleTagAsync(ArticleTags tags)
        {
            await _db.ArticleTags.AddAsync(tags);
        }

        public async Task<IList<ArticleTags>> GetArticleTagsAsync(Expression<Func<ArticleTags, bool>> where)
        {
            return await _db.ArticleTags.Where(where).ToListAsync();
        }

        public void RemoveArticleTag(ArticleTags tags)
        {
            _db.ArticleTags.Remove(tags);
        }

        public async Task<ArticleTags> GetArticleTagsAsync(int id)
        {
            return await _db.ArticleTags.FindAsync(id);
        }

        #endregion

        #region Article Comments
        public async Task<IList<ArticleComments>> GetArticleCommentAsync(Expression<Func<ArticleComments, bool>> where)
        {
            return await _db.ArticleComments.Where(where).ToListAsync();
        }

        public async Task<ArticleComments> GetArticleCommentAsync(int id)
        {
            return await _db.ArticleComments.FindAsync(id);
        }

        public void RemoveArticleComments(ArticleComments articleComments)
        {
            _db.ArticleComments.Remove(articleComments);
        }

        public async Task InsertArticleCommentsAsync(ArticleComments articleComments)
        {
            await _db.ArticleComments.AddAsync(articleComments);
        }

        public void UpdateArticleComments(ArticleComments articleComments)
        {
            _db.ArticleComments.Update(articleComments);
        }

        #endregion

        public async Task SaveAsync()
        {
            await _db.SaveChangesAsync();
        }
        public void Save()
        {
            _db.SaveChanges();
        }


    }
}