﻿using Microsoft.EntityFrameworkCore;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

using ThemeShop.Domain.Interfaces;
using ThemeShop.Domain.Models;
using ThemeShop.Infra.Data.Context;

namespace ThemeShop.Infra.Data.Repository
{
    public class ThemeRepository : IThemeRepository
    {
        private readonly ThemeShop_DBContext _db;

        public ThemeRepository(ThemeShop_DBContext db)
        {
            _db = db;
        }

        public async Task AddCategoryAsync(ThemeCategories categories)
        {
            await _db.ThemeCategories.AddAsync(categories);
        }

        public async Task AddCategoryThemeAsync(int themeId, int CategoryId)
        {
            await _db.ThemeSelectedCategories.AddAsync(new ThemeSelectedCategories()
            {
                ThemeId = themeId,
                ThemeCategoryId = CategoryId
            });
        }

        public async Task<ThemeCategories> GetCategoryByIdAsync(int categoryId)
        {
            return await _db.ThemeCategories.FindAsync(categoryId);
        }

        public async Task<IList<ThemeCategories>> GetThemeCategoriesAsync(Expression<Func<ThemeCategories, bool>> where)
        {
            return await _db.ThemeCategories.Where(where).ToListAsync();
        }

        public async Task<IList<ThemeCategories>> GetThemeCategoryChildAsync(int parentId)
        {
            return await _db.ThemeCategoriesChilds.Where(t => t.ThemeCategoryParentId == parentId).Select(c => c.ThemeCategory).ToListAsync();
        }

        public async Task<ThemeCategoriesChilds> GetThemeCategoryParentAsync(int categoryId)
        {
            return await _db.ThemeCategoriesChilds.FirstOrDefaultAsync(x => x.ThemeCategoryId == categoryId);
        }

        public async Task<IList<Themes>> GetThemesAsync()
        {
            return await _db.Themes.ToListAsync();
        }

        public void RemoveCategory(ThemeCategories categories)
        {
            _db.ThemeCategories.Remove(categories);
        }

        public void RemoveCategoryFromTheme(IList<ThemeSelectedCategories> selectedCategories)
        {
            _db.ThemeSelectedCategories.RemoveRange(selectedCategories);
        }

        public async Task SaveAsync()
        {
            await _db.SaveChangesAsync();
        }

        public void UpdateCategory(ThemeCategories categories)
        {
            _db.ThemeCategories.Update(categories);
        }

        public async Task<IList<int>> GetCategoriesIdBythemeIdAsync(int themeId)
        {
            return await _db.ThemeSelectedCategories.Where(t => t.ThemeId == themeId).Select(c => c.ThemeCategoryId).ToListAsync();
        }

        public async Task AddThemeChangeStatusAsync(int themeId, int newStatus, string message)
        {
            await _db.ThemeChangeStatusMessages.AddAsync(new ThemeChangeStatusMessages
            {
                Message = message,
                SentDate = DateTime.Now,
                Status = newStatus,
                ThemeId = themeId
            });
        }

        public async Task<Themes> GetThemeByIdAsync(int themeId)
        {
            return await _db.Themes.FindAsync(themeId);
        }

        public void UpdateTheme(Themes themes)
        {
            _db.Themes.Update(themes);
        }

        public async Task<IList<ThemeChangeStatusMessages>> GetThemeChangeStatusMessagesAsync(int themeId)
        {
            return await _db.ThemeChangeStatusMessages.Where(x => x.ThemeId == themeId).ToListAsync();
        }

        public async Task<IList<ThemeCategories>> GetAllThemeCategoriesAsync()
        {
            return await _db.ThemeCategories.ToListAsync();
        }

        public async Task<IList<Users>> GetSellersAsync()
        {
            return await _db.Users.Select(u => new Users()
            {
                UserId = u.UserId,
                UserName = u.UserName
            }).ToListAsync();
        }

        public async Task RemoveTagsFromThemeAsync(int themeId)
        {
            await _db.ThemeTags.Where(t => t.ThemeId == themeId).ForEachAsync(t => RemoveThemeTag(t));
        }

        public void RemoveThemeTag(ThemeTags tags)
        {
            _db.ThemeTags.Remove(tags);
        }

        public async Task AddTagToThemeAsync(int themeId, string[] tags)
        {
            foreach (string item in tags)
            {
                await _db.ThemeTags.AddAsync(new ThemeTags()
                {
                    Tag = item.Trim(),
                    ThemeId = themeId
                });
            }
        }

        public async Task AddThemeAsync(Themes theme)
        {
            await _db.Themes.AddAsync(theme);
        }

        public async Task<bool> CheckRelatedExist(int themeId, int themeRelatedId)
        {
            return await _db.ThemeRelateds.AnyAsync(r =>
                r.ThemeId == themeId && r.ThemeRelatedId == themeRelatedId);
        }

        public async Task AddRelatedThemeAsync(ThemeRelateds themeRelateds)
        {
            await _db.ThemeRelateds.AddAsync(themeRelateds);
        }

        public async Task<IList<ThemeRelateds>> GetRelatedsAsync(Expression<Func<ThemeRelateds, bool>> where, byte page, byte take)
        {
            if (page == 0 && take == 0)
            {
                await _db.ThemeRelateds.Where(where).ToListAsync();
            }
            return await _db.ThemeRelateds.Where(where).Skip((page - 1) * take).Take(take).ToListAsync();
        }

        public void DeleteRelatedTheme(ThemeRelateds themeRelateds)
        {
            _db.ThemeRelateds.Remove(themeRelateds);
        }

        public async Task<ThemeRelateds> GetThemeRelated(int themeRelatedId)
        {
            return await _db.ThemeRelateds.FindAsync(themeRelatedId);
        }

        public async Task InsertThemeGalleryAsync(ThemeGalleries themeGallery)
        {
            await _db.ThemeGalleries.AddAsync(themeGallery);
        }

        public void UpdateThemeGallery(ThemeGalleries themeGalleriey)
        {
            _db.ThemeGalleries.Update(themeGalleriey);
        }

        public async Task<IList<ThemeGalleries>> GetThemeGalleriesAsync(Expression<Func<ThemeGalleries, bool>> where)
        {
            return await _db.ThemeGalleries.Where(where).ToListAsync();
        }

        public void DeleteThemeGallery(ThemeGalleries themeGalleries)
        {
            _db.ThemeGalleries.Remove(themeGalleries);
        }

        public async Task<IList<ThemeAttachments>> GetThemeAttachmentsAsync(int themeId)
        {
            return await _db.ThemeAttachments.Where(t => t.ThemeId == themeId).ToListAsync();
        }

        public async Task AddThemeAttachmentAsync(ThemeAttachments themeAttachments)
        {
            await _db.ThemeAttachments.AddAsync(themeAttachments);
        }

        public async Task<ThemeAttachments> GetThemeAttachmentByIdAsync(int themeAttachmentsId)
        {
            return await _db.ThemeAttachments.FindAsync(themeAttachmentsId);
        }

        public void DeleteThemeAttachment(ThemeAttachments themeAttachments)
        {
            _db.ThemeAttachments.Remove(themeAttachments);
        }

        public void UpdateThemeAttachment(ThemeAttachments themeAttachments)
        {
            _db.ThemeAttachments.Update(themeAttachments);
        }

        public async Task<IList<ThemeOfferTodays>> GetAllOfferAsync(int page)
        {
            return await _db.ThemeOfferTodays.OrderByDescending(t => t.EndDate)
                                       .Skip((page - 1) * 24)
                                       .Take(24).ToListAsync();
        }

        public async Task<int> GetOfferCountAsync()
        {
            return await _db.ThemeOfferTodays.CountAsync();
        }

        public async Task<bool> IsThemeExistAsync(int themeId)
        {
            return await _db.Themes.AnyAsync(t => t.ThemeId == themeId);
        }

        public async Task AddOfferAsync(ThemeOfferTodays themeOffer)
        {
            await _db.ThemeOfferTodays.AddAsync(themeOffer);
        }

        public void DeleteThemeOffer(ThemeOfferTodays themeOffer)
        {
            _db.ThemeOfferTodays.Remove(themeOffer);
        }

        public async Task<ThemeOfferTodays> GetThemeOfferAsync(int themeOfferId)
        {
            return await _db.ThemeOfferTodays.FindAsync(themeOfferId);
        }

        public void UpdateThemeOffer(ThemeOfferTodays themeOffer)
        {
            _db.ThemeOfferTodays.Update(themeOffer);
        }

        public async Task<IList<ThemePreviews>> GetThemePreviewsAsync(Expression<Func<ThemePreviews, bool>> where, byte page, byte take)
        {
            if (page == 0 && take == 0)
            {
                return await _db.ThemePreviews.Where(where).ToListAsync();
            }
            return await _db.ThemePreviews.Where(where).Skip((page - 1) * take).Take(take).ToListAsync();
        }

        public async Task AddThemePreviewAsync(ThemePreviews previews)
        {
            await _db.ThemePreviews.AddAsync(previews);
            throw new NotImplementedException();
        }

        public async Task<ThemePreviews> GetThemePreviewsAsync(int themePreviewId)
        {
            return await _db.ThemePreviews.FindAsync(themePreviewId);
        }

        public void UpdateThemePreview(ThemePreviews previews)
        {
            _db.ThemePreviews.Update(previews);
        }

        public void DeleteThemePreview(ThemePreviews themePreviews)
        {
            _db.ThemePreviews.Remove(themePreviews);
        }

        public async Task<IList<ThemeComments>> GetUnreadCommentsAsync(int page, byte take)
        {
            return await _db.ThemeComments.Where(c => !c.IsDelete && !c.IsRead).Skip((page - 1) * take).Take(take).ToListAsync();
        }
        public async Task<int> GetUnreadCommentsCountAsync()
        {
            return await _db.ThemeComments.CountAsync(c => !c.IsDelete && !c.IsRead);
        }

        public void UpdateComment(ThemeComments comments)
        {
            _db.ThemeComments.Update(comments);
        }

        public void UpdateComment(IList<ThemeComments> comments)
        {
            _db.ThemeComments.UpdateRange(comments);
        }

        public async Task<ThemeComments> GetThemeCommentsAsync(int themeCommentId)
        {
            return await _db.ThemeComments.FindAsync(themeCommentId);
        }

        public async Task<IList<ThemeComments>> GetCommentsbyThemeIdAsync(int themeId, bool showDeleteComments)
        {
            if (showDeleteComments)
            {
                return await _db.ThemeComments.Where(c => c.ThemeId == themeId).ToListAsync();
            }
            return await _db.ThemeComments.Where(c => c.ThemeId == themeId && c.IsDelete == showDeleteComments).ToListAsync();
        }

        public async Task<IList<ThemeOfferTodays>> GetOffersAsync(Expression<Func<ThemeOfferTodays, bool>> where)
        {
            return await _db.ThemeOfferTodays.Where(where).ToListAsync();
        }

        public async Task<int> GetOfferCountAsync(Expression<Func<ThemeOfferTodays, bool>> where)
        {
            return await _db.ThemeOfferTodays.CountAsync(where);
        }

        public void RemoveThemeSelectedCategories(IList<ThemeSelectedCategories> selectedCategories)
        {
            _db.ThemeSelectedCategories.RemoveRange(selectedCategories);
        }

        public async Task<IList<ThemeSelectedCategories>> GetThemeSelectedCategoriesAsync(Expression<Func<ThemeSelectedCategories, bool>> where)
        {
            return await _db.ThemeSelectedCategories.Where(where).ToListAsync();
        }

        public async Task<int> GetThemePreviewsCountAsync(Expression<Func<ThemePreviews, bool>> where)
        {
            return await _db.ThemePreviews.CountAsync(where);
        }

        public void Save()
        {
            _db.SaveChanges();
        }

        public async Task<ThemeGalleries> GetThemeGalleriesAsync(int id)
        {
            return await _db.ThemeGalleries.FindAsync(id);
        }
    }
}