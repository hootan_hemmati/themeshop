﻿using Microsoft.EntityFrameworkCore;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

using ThemeShop.Domain.Interfaces;
using ThemeShop.Domain.Models;
using ThemeShop.Infra.Data.Context;

namespace ThemeShop.Infra.Data.Repository
{
    public class ContactUsRepository : IContactUsRepository
    {
        private readonly ThemeShop_DBContext _db;

        public ContactUsRepository(ThemeShop_DBContext db)
        {
            _db = db;
        }

        #region get contactus

        public async Task<IList<ContactUs>> GetContactUsAsync()
        {
            return await _db.ContactUs.OrderByDescending(contactUs => contactUs.IsRead == false || contactUs.IsAnswered == false).ToListAsync();
        }

        public async Task<IList<ContactUs>> GetContactUsAsync(Expression<Func<ContactUs, bool>> where)
        {
            return await _db.ContactUs.Where(where).ToListAsync();
        }

        /// <summary>
        /// get contactus by id
        /// </summary>
        /// <param name="id">contactus id</param>
        /// <returns>contactus object</returns>
        public async Task<ContactUs> GetContactUsAsync(int id)
        {
            return await _db.ContactUs.FindAsync(id);
        }

        public async Task<int> GetContactUsCountAsync()
        {
            return await _db.ContactUs.CountAsync();
        }

        public async Task<int> GetContactUsCountAsync(Expression<Func<ContactUs, bool>> where)
        {
            return await _db.ContactUs.CountAsync(where);
        }

        public async Task<IList<ContactUs>> FilterContactUsAsync(string query)
        {
            return await _db.ContactUs.Where(contactUs => EF.Functions.Like(contactUs.Subject, $"%{query}%") || EF.Functions.Like(contactUs.Name, $"%{query}%")).Distinct().ToListAsync();
        }

        public async Task<int> FilterContactUsCountAsync(string query)
        {
            return await _db.ContactUs.Where(contactUs => EF.Functions.Like(contactUs.Subject, $"%{query}%") || EF.Functions.Like(contactUs.Name, $"%{query}%")).Distinct().CountAsync();
        }

        public async Task AddContactUs(ContactUs contactUs)
        {
            await _db.ContactUs.AddAsync(contactUs);
        }

        public void Update(ContactUs contact)
        {
            _db.Update(contact);
        }

        #endregion get contactus

        public async Task SaveAsync()
        {
            await _db.SaveChangesAsync();
        }

    }
}