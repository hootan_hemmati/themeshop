﻿using Microsoft.EntityFrameworkCore;

using System.Collections.Generic;
using System.Threading.Tasks;

using ThemeShop.Domain.Interfaces;
using ThemeShop.Domain.Models;
using ThemeShop.Infra.Data.Context;

namespace ThemeShop.Infra.Data.Repository
{
    public class DownloadTokenRepository : IDownloadTokenRepository
    {
        private readonly ThemeShop_DBContext _db;

        public DownloadTokenRepository(ThemeShop_DBContext db)
        {
            _db = db;
        }

        public async Task<IList<DownloadTokens>> GetDownloadTokensAsync()
        {
            return await _db.DownloadTokens.ToListAsync();
        }

        public async Task<string> GetSingleTokenAsync(int themeId, int userId)
        {
            DownloadTokens token = await _db.DownloadTokens.FirstOrDefaultAsync(e => e.UserId == userId && e.ThemeId == themeId && !e.IsExpired);
            return token?.Token;
        }

        public async Task InsertDownloadTokenAsync(DownloadTokens downloadToken)
        {
            await _db.AddAsync(downloadToken);
        }

        public async Task<bool> IsExpireAsync(int themeId, int userId)
        {
            return await _db.DownloadTokens.AnyAsync(e => e.UserId == userId && e.ThemeId == themeId && !e.IsExpired);
        }

        public void Save()
        {
            _db.SaveChanges();
        }

        public async Task SaveAsync()
        {
            await _db.SaveChangesAsync();
        }
    }
}