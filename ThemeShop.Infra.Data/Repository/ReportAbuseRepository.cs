﻿using Microsoft.EntityFrameworkCore;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

using ThemeShop.Domain.Interfaces;
using ThemeShop.Domain.Models;
using ThemeShop.Infra.Data.Context;

namespace ThemeShop.Infra.Data.Repository
{
    public class ReportAbuseRepository : IReportAbuseRepository
    {
        private readonly ThemeShop_DBContext _db;

        public ReportAbuseRepository(ThemeShop_DBContext db)
        {
            _db = db;
        }

        #region ReportAbuse
        public async Task<IList<ReportAbuse>> GetAllReportAbuseAsync()
        {
            return await _db.ReportAbuse.OrderByDescending(reportAbuse => reportAbuse.IsAnswered == false || reportAbuse.IsSeen == false).ToListAsync();
        }

        public async Task<IList<ReportAbuse>> GetReportAbuseAsync(Expression<Func<ReportAbuse, bool>> where)
        {
            return await _db.ReportAbuse.Where(where).ToListAsync();
        }

        public async Task<ReportAbuse> GetReportAbuseByIdAsync(int id)
        {
            return await _db.ReportAbuse.FindAsync(id);
        }

        public async Task<int> GetReportAbuseCountAsync()
        {
            return await _db.ReportAbuse.CountAsync();
        }

        public async Task<int> GetReportAbuseCountAsync(Expression<Func<ReportAbuse, bool>> where)
        {
            return await _db.ReportAbuse.CountAsync(where);
        }

        public async Task<IList<ReportAbuse>> FilterReportAbuseAsync(string query)
        {
            return await _db.ReportAbuse.Where(ReportAbuse => EF.Functions.Like(ReportAbuse.Description, $"%{query}%") || EF.Functions.Like(ReportAbuse.Themes.ThemeTitle, $"%{query}%")).Distinct().ToListAsync();
        }

        public async Task<int> FilterReportAbuseCountAsync(string query)
        {
            return await _db.ReportAbuse.Where(ReportAbuse => EF.Functions.Like(ReportAbuse.Description, $"%{query}%") || EF.Functions.Like(ReportAbuse.Themes.ThemeTitle, $"%{query}%")).Distinct().CountAsync();
        }


        public void Update(ReportAbuse contact)
        {
            _db.Update(contact);
        }


        public async Task AddReportAbuse(ReportAbuse ReportAbuse)
        {
            await _db.ReportAbuse.AddAsync(ReportAbuse);
        }

        #endregion
        public async Task SaveAsync()
        {
            await _db.SaveChangesAsync();
        }
    }
}
