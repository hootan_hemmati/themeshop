﻿using Microsoft.EntityFrameworkCore;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

using ThemeShop.Domain.Interfaces;
using ThemeShop.Domain.Models;
using ThemeShop.Infra.Data.Context;

namespace ThemeShop.Infra.Data.Repository
{
    public class DiscountRepository : IDiscountRepository
    {
        private readonly ThemeShop_DBContext _db;

        public DiscountRepository(ThemeShop_DBContext db)
        {
            _db = db;
        }

        #region Discount

        public async Task<int> InsertDiscountAsync(Discounts discounts)
        {
            await _db.Discounts.AddAsync(discounts);
            await SaveAsync();
            return discounts.DiscountId;
        }

        public void RemoveDiscount(Discounts discounts)
        {
            _db.Discounts.Remove(discounts);
        }

        public async Task<IList<Discounts>> GetAllDiscountsAsync()
        {
            return await _db.Discounts.ToListAsync();
        }

        public async Task<Discounts> GetDiscountAsync(int discountId)
        {
            return await _db.Discounts.FindAsync(discountId);
        }

        public async Task<IList<Discounts>> GetDiscountsAsync()
        {
            return await _db.Discounts.ToListAsync();
        }

        public void UpdateDiscount(Discounts discounts)
        {
            _db.Update(discounts);
        }

        public async Task<int> GetDicountCountAsync()
        {
            return await _db.Discounts.CountAsync();
        }

        #endregion Discount

        #region DiscountLog

        public async Task InsertDiscountLogAsync(DiscountLogs logs)
        {
            await _db.DiscountLogs.AddAsync(logs);
        }

        public async Task<IList<DiscountLogs>> GetDiscountLogsAsync(Expression<Func<DiscountLogs, bool>> where)
        {
            return await _db.DiscountLogs.Where(where).ToListAsync();
        }

        #endregion DiscountLog

        #region DiscountTheme

        public async Task InsertDiscountSelectedThemeAsync(DiscountSelectedTheme discountTheme)
        {
            await _db.DiscountSelectedTheme.AddAsync(discountTheme);
        }

        public void RemoveDiscountTheme(IList<DiscountSelectedTheme> discountSelectedThemes)
        {
            _db.DiscountSelectedTheme.RemoveRange(discountSelectedThemes);
        }

        public async Task<IList<DiscountSelectedTheme>> GetDiscountSelectedThemesAsync(Expression<Func<DiscountSelectedTheme, bool>> where)
        {
            return await _db.DiscountSelectedTheme.Where(where).ToListAsync();
        }

        #endregion DiscountTheme

        #region UserDiscount

        public async Task InsertUserDiscountAsync(UserDiscounts userDiscounts)
        {
            await _db.UserDiscounts.AddAsync(userDiscounts);
        }

        public void RemoveDiscountUsers(IList<UserDiscounts> userDiscounts)
        {
            _db.RemoveRange(userDiscounts);
        }

        public async Task<IList<UserDiscounts>> GetUserDiscountsAsync(Expression<Func<UserDiscounts, bool>> where)
        {
            return await _db.UserDiscounts.Where(where).ToListAsync();
        }

        #endregion UserDiscount

        public async Task SaveAsync()
        {
            await _db.SaveChangesAsync();
        }
    }
}