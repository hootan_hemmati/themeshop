﻿using Microsoft.EntityFrameworkCore;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

using ThemeShop.Domain.Interfaces;
using ThemeShop.Domain.Models;
using ThemeShop.Infra.Data.Context;

namespace ThemeShop.Infra.Data.Repository
{
    public class RoleRepository : IRoleRepository
    {
        private readonly ThemeShop_DBContext _db;

        public RoleRepository(ThemeShop_DBContext db)
        {
            _db = db;
        }

        #region Roles

        public async Task<IList<Roles>> GetAllRolesAsync()
        {
            return await _db.Roles.ToListAsync();
        }

        public async Task<IList<Roles>> FilterRolesAsync(string filterRoles = "")
        {
            return await _db.Roles.Where(role => EF.Functions.Like(role.RoleTitle, $"%{filterRoles}%")).ToListAsync();
        }

        public async Task<int> FilterRolesCountAsync(string filterRoles = "")
        {
            return await _db.Roles.Where(role => EF.Functions.Like(role.RoleTitle, $"%{filterRoles}%")).Distinct().CountAsync();
        }

        public async Task<Roles> GetDefualtRoleAsync()
        {
            return await _db.Roles.FirstOrDefaultAsync(role => role.IsDefualtRole == true);
        }

        public async Task<Roles> GetRoleByIdAsync(int roleId)
        {
            return await _db.Roles.FindAsync(roleId);
        }

        public async Task<Roles> GetRoleByTitleAsync(string roleTitle)
        {
            return await _db.Roles.FirstOrDefaultAsync(role => role.RoleTitle == roleTitle);
        }

        public async Task InsertRoleAsync(Roles role)
        {
            await _db.AddAsync(role);
        }

        public void RemoveRole(Roles role)
        {
            _db.Remove(role);
        }

        public async Task RemoveRoleAsync(int roleId)
        {
            Roles role = await GetRoleByIdAsync(roleId);
            RemoveRole(role);
        }

        public void UpdateRole(Roles role)
        {
            _db.Update(role);
        }

        public async Task<bool> IsRoleExistByRoleTitleAsync(string roleTitle)
        {
            return await _db.Roles.AnyAsync(role => role.RoleTitle == roleTitle);
        }

        public async Task<bool> IsRoleExistByRoleIdAsync(int roleId)
        {
            return await _db.Roles.AnyAsync(role => role.RoleId == roleId);
        }

        #endregion Roles

        #region UserRoles

        public async Task InsertUserRoleAsync(UserRoles userRole)
        {
            await _db.AddAsync(userRole);
        }

        public void DeleteUserRole(UserRoles userRole)
        {
            _db.Remove(userRole);
        }

        public async Task<UserRoles> GetUserRoleByIdAsync(int userRoleId)
        {
            return await _db.UserRoles.FindAsync(userRoleId);
        }

        public async Task DeleteUserRoleByIdAsync(int userRoleId)
        {
            UserRoles userRole = await GetUserRoleByIdAsync(userRoleId);
            DeleteUserRole(userRole);
        }

        public async Task<IList<UserRoles>> GetUserRolesAsync(Expression<Func<UserRoles, bool>> where)
        {
            return await _db.UserRoles.Where(where).ToListAsync();
        }

        public async Task<UserRoles> GetUserRoleAsync(Expression<Func<UserRoles, bool>> where)
        {
            return await _db.UserRoles.FirstOrDefaultAsync(where);
        }

        public async Task<bool> IsUserRoleExistAsync(Expression<Func<UserRoles, bool>> where)
        {
            return await _db.UserRoles.AnyAsync(where);
        }

        #endregion UserRoles

        #region RolesPermissions

        public async Task<IList<RolePremissions>> GetRolePremissionsAsync(Expression<Func<RolePremissions, bool>> where)
        {
            return await _db.RolePremissions.Where(where).ToListAsync();
        }

        public async Task<RolePremissions> GetRolePremissionAsync(Expression<Func<RolePremissions, bool>> where)
        {
            return await _db.RolePremissions.FirstOrDefaultAsync(where);
        }

        public async Task<bool> IsRolePremissionAsync(Expression<Func<RolePremissions, bool>> where)
        {
            return await _db.RolePremissions.AnyAsync(where);
        }

        public async Task<IList<RolePremissions>> GetAllRolePremissionsAsync()
        {
            return await _db.RolePremissions.ToListAsync();
        }

        public async Task<IList<RolePremissions>> GetAllRolePremissionsByRoleIdAsync(int roleId)
        {
            return await _db.RolePremissions.Where(rolePremission => rolePremission.RoleId == roleId).ToListAsync();
        }

        public async Task InsertRolePremissionAsync(RolePremissions rolePremission)
        {
            await _db.AddAsync(rolePremission);
        }

        public async Task<RolePremissions> GetRolePremissionByIdAsync(int rolePermissionId)
        {
            return await _db.RolePremissions.FindAsync(rolePermissionId);
        }

        public void DeleteRolePremission(RolePremissions rolePremission)
        {
            _db.Remove(rolePremission);
        }

        public async Task DeleteRolePremissionByIdAsync(int rolePermissionId)
        {
            RolePremissions rolePremission = await GetRolePremissionByIdAsync(rolePermissionId);
            DeleteRolePremission(rolePremission);
        }

        #endregion RolesPermissions

        public void Save()
        {
            _db.SaveChanges();
        }

        public async Task SaveAsync()
        {
            await _db.SaveChangesAsync();
        }
    }
}