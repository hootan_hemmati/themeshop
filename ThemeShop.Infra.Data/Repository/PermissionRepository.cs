﻿using Microsoft.EntityFrameworkCore;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

using ThemeShop.Domain.Interfaces;
using ThemeShop.Domain.Models;
using ThemeShop.Infra.Data.Context;

namespace ThemeShop.Infra.Data.Repository
{
    public class PermissionRepository : IPermissionRepository
    {
        private readonly ThemeShop_DBContext _db;

        public PermissionRepository(ThemeShop_DBContext db)
        {
            _db = db;
        }

        #region Permissions

        public async Task<IList<Permissions>> GetAllPermissionsAsync()
        {
            return await _db.Permissions.ToListAsync();
        }

        public async Task<Permissions> GetPermissionAsync(Expression<Func<Permissions, bool>> where)
        {
            return await _db.Permissions.FirstOrDefaultAsync(where);
        }

        public async Task<Permissions> GetPermissionByIdAsync(int permissionId)
        {
            return await _db.Permissions.FindAsync(permissionId);
        }

        public async Task<Permissions> GetPermissionByNameAsync(string permissionName)
        {
            return await _db.Permissions.FirstOrDefaultAsync(permission => permission.Name == permissionName);
        }

        public async Task<IList<Permissions>> GetPermissionsAsync(Expression<Func<Permissions, bool>> where)
        {
            return await _db.Permissions.Where(where).ToListAsync();
        }

        public async Task<bool> IsPermissionExist(Expression<Func<Permissions, bool>> where)
        {
            return await _db.Permissions.AnyAsync(where);
        }

        #endregion Permissions
    }
}