﻿using Microsoft.EntityFrameworkCore;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

using ThemeShop.Domain.Interfaces;
using ThemeShop.Domain.Models;
using ThemeShop.Infra.Data.Context;

namespace ThemeShop.Infra.Data.Repository
{
    public class UserRepository : IUserRepository
    {
        private readonly ThemeShop_DBContext _db;

        public UserRepository(ThemeShop_DBContext db)
        {
            _db = db;
        }

        #region OnlineUser

        public async Task<IList<OnlineUsers>> GetAllOnlineUsersAsync()
        {
            return await _db.OnlineUsers.ToListAsync();
        }

        public async Task<IList<OnlineUsers>> GetAllTodayOnlineUsersAsync()
        {
            return await _db.OnlineUsers.Where(user => user.StartTime.Day == DateTime.Now.Day).ToListAsync();
        }

        public async Task InsertOnlineUserAsync(OnlineUsers onlineUser)
        {
            await _db.AddAsync(onlineUser);
        }

        public void UpdateOnlineUser(OnlineUsers onlineUser)
        {
            _db.Update(onlineUser);
        }

        public async Task<OnlineUsers> FindOnlineUserAsync(int id)
        {
            return await _db.OnlineUsers.FindAsync(id);
        }

        public async Task<bool> IsOnlineUserExistByUserNameAsync(string username)
        {
            return await _db.OnlineUsers.AnyAsync(user => user.UserName == username);
        }

        public async Task<bool> IsOnlineUserExistByIpAsync(string ip)
        {
            return await _db.OnlineUsers.AnyAsync(user => user.Ip == ip);
        }

        public async Task<OnlineUsers> FindOnlineUserByIpAsync(string ip)
        {
            return await _db.OnlineUsers.FirstOrDefaultAsync(user => user.Ip == ip);
        }

        public async Task<OnlineUsers> FindOnlineUserByUserNameAsync(string userName)
        {
            return await _db.OnlineUsers.FirstOrDefaultAsync(user => user.UserName == userName);
        }

        #endregion OnlineUser

        #region Users

        public async Task<IList<Users>> GetAllUsersAsync()
        {
            return await _db.Users.ToListAsync();
        }

        public Task<int> GetAllUsersCountAsync()
        {
            return _db.Users.CountAsync();
        }

        public async Task<IList<Users>> FilterUsersAsync(string query, int userSearchMode = 0, int userStatusEnum = 0)
        {
            switch (userSearchMode)
            {
                case 0:
                    {
                        switch (userStatusEnum)
                        {
                            case 0:
                                {
                                    return await _db.Users.Where(user => EF.Functions.Like(user.UserName, $"%{query}%")).Distinct().ToListAsync();
                                }
                            case 1:
                                {
                                    return await _db.Users.Where(user => EF.Functions.Like(user.UserName, $"%{query}%") && user.IsDelete).Distinct().ToListAsync();
                                }
                            case 2:
                                {
                                    return await _db.Users.Where(user => EF.Functions.Like(user.UserName, $"%{query}%") && !user.IsActive).Distinct().ToListAsync();
                                }
                            case 3:
                                {
                                    return await _db.Users.Where(user => EF.Functions.Like(user.UserName, $"%{query}%") && user.IsActive).Distinct().ToListAsync();
                                }
                            case 4:
                                {
                                    return await _db.Users.Where(user => EF.Functions.Like(user.UserName, $"%{query}%") && user.IsActive && user.IsDelete).Distinct().ToListAsync();
                                }
                            default:
                                return await _db.Users.Where(user => EF.Functions.Like(user.UserName, $"%{query}%")).Distinct().ToListAsync();
                        }
                    }

                case 1:
                    {
                        switch (userStatusEnum)
                        {
                            case 0:
                                {
                                    return await _db.Users.Where(user => EF.Functions.Like(user.Email, $"%{query}%")).Distinct().ToListAsync();
                                }
                            case 1:
                                {
                                    return await _db.Users.Where(user => EF.Functions.Like(user.Email, $"%{query}%") && user.IsDelete).Distinct().ToListAsync();
                                }
                            case 2:
                                {
                                    return await _db.Users.Where(user => EF.Functions.Like(user.Email, $"%{query}%") && !user.IsActive).Distinct().ToListAsync();
                                }
                            case 3:
                                {
                                    return await _db.Users.Where(user => EF.Functions.Like(user.Email, $"%{query}%") && user.IsActive).Distinct().ToListAsync();
                                }
                            case 4:
                                {
                                    return await _db.Users.Where(user => EF.Functions.Like(user.UserName, $"%{query}%") && user.IsActive && user.IsDelete).Distinct().ToListAsync();
                                }
                            default:
                                return await _db.Users.Where(user => EF.Functions.Like(user.Email, $"%{query}%")).Distinct().ToListAsync();
                        }
                    }

                case 2:
                    {
                        switch (userStatusEnum)
                        {
                            case 0:
                                {
                                    return await _db.Users.Where(user => EF.Functions.Like(user.Mobile, $"%{query}%")).Distinct().ToListAsync();
                                }
                            case 1:
                                {
                                    return await _db.Users.Where(user => EF.Functions.Like(user.Mobile, $"%{query}%") && user.IsDelete).Distinct().ToListAsync();
                                }
                            case 2:
                                {
                                    return await _db.Users.Where(user => EF.Functions.Like(user.Mobile, $"%{query}%") && !user.IsActive).Distinct().ToListAsync();
                                }
                            case 3:
                                {
                                    return await _db.Users.Where(user => EF.Functions.Like(user.Mobile, $"%{query}%") && user.IsActive).Distinct().ToListAsync();
                                }
                            case 4:
                                {
                                    return await _db.Users.Where(user => EF.Functions.Like(user.UserName, $"%{query}%") && user.IsActive && user.IsDelete).Distinct().ToListAsync();
                                }
                            default:
                                return await _db.Users.Where(user => EF.Functions.Like(user.Mobile, $"%{query}%")).Distinct().ToListAsync();
                        }
                    }

                default:
                    return await _db.Users.Where(user => EF.Functions.Like(user.UserName, $"%{query}%")).Distinct().ToListAsync();
            }
        }

        public async Task<int> FilterUsersCountAsync(string query, int userSearchMode = 0, int userStatusEnum = 0)
        {
            switch (userSearchMode)
            {
                case 0:
                    {
                        switch (userStatusEnum)
                        {
                            case 0:
                                {
                                    return await _db.Users.Where(user => EF.Functions.Like(user.UserName, $"%{query}%")).Distinct().CountAsync();
                                }
                            case 1:
                                {
                                    return await _db.Users.Where(user => EF.Functions.Like(user.UserName, $"%{query}%") && user.IsDelete).Distinct().CountAsync();
                                }
                            case 2:
                                {
                                    return await _db.Users.Where(user => EF.Functions.Like(user.UserName, $"%{query}%") && !user.IsActive).Distinct().CountAsync();
                                }
                            case 3:
                                {
                                    return await _db.Users.Where(user => EF.Functions.Like(user.UserName, $"%{query}%") && user.IsActive).Distinct().CountAsync();
                                }
                            default:
                                return await _db.Users.Where(user => EF.Functions.Like(user.UserName, $"%{query}%")).Distinct().CountAsync();
                        }
                    }

                case 1:
                    {
                        switch (userStatusEnum)
                        {
                            case 0:
                                {
                                    return await _db.Users.Where(user => EF.Functions.Like(user.Email, $"%{query}%")).Distinct().CountAsync();
                                }
                            case 1:
                                {
                                    return await _db.Users.Where(user => EF.Functions.Like(user.Email, $"%{query}%") && user.IsDelete).Distinct().CountAsync();
                                }
                            case 2:
                                {
                                    return await _db.Users.Where(user => EF.Functions.Like(user.Email, $"%{query}%") && !user.IsActive).Distinct().CountAsync();
                                }
                            case 3:
                                {
                                    return await _db.Users.Where(user => EF.Functions.Like(user.Email, $"%{query}%") && user.IsActive).Distinct().CountAsync();
                                }
                            default:
                                return await _db.Users.Where(user => EF.Functions.Like(user.Email, $"%{query}%")).Distinct().CountAsync();
                        }
                    }

                case 2:
                    {
                        switch (userStatusEnum)
                        {
                            case 0:
                                {
                                    return await _db.Users.Where(user => EF.Functions.Like(user.Mobile, $"%{query}%")).Distinct().CountAsync();
                                }
                            case 1:
                                {
                                    return await _db.Users.Where(user => EF.Functions.Like(user.Mobile, $"%{query}%") && user.IsDelete).Distinct().CountAsync();
                                }
                            case 2:
                                {
                                    return await _db.Users.Where(user => EF.Functions.Like(user.Mobile, $"%{query}%") && !user.IsActive).Distinct().CountAsync();
                                }
                            case 3:
                                {
                                    return await _db.Users.Where(user => EF.Functions.Like(user.Mobile, $"%{query}%") && user.IsActive).Distinct().CountAsync();
                                }
                            default:
                                return await _db.Users.Where(user => EF.Functions.Like(user.Mobile, $"%{query}%")).Distinct().CountAsync();
                        }
                    }

                default:
                    return await _db.Users.Where(user => EF.Functions.Like(user.UserName, $"%{query}%")).Distinct().CountAsync();
            }
        }

        public async Task AddUserAsync(Users user)
        {
            await _db.AddAsync(user);
        }

        public async Task<Users> GetUserByIdAsync(int userId)
        {
            return await _db.Users.FindAsync(userId);
        }

        public async Task<IList<Users>> GetUsersAsync(Expression<Func<Users, bool>> where)
        {
            return await _db.Users.Where(where).ToListAsync();
        }

        public async Task<Users> GetUserAsync(Expression<Func<Users, bool>> where)
        {
            return await _db.Users.FirstOrDefaultAsync(where);
        }

        public async Task<bool> IsUserExistAsync(Expression<Func<Users, bool>> where)
        {
            return await _db.Users.AnyAsync(where);
        }

        public void UpdateUser(Users user)
        {
            _db.Update(user);
        }

        public void DeleteUser(Users user)
        {
            _db.Remove(user);
        }

        public async Task DeleteUserByIdAsync(int userId)
        {
            Users user = await GetUserByIdAsync(userId);
            DeleteUser(user);
        }

        #region Seller

        public async Task<Tuple<IList<Users>, int>> FillterSellersAsync(string filterEmail = "", string filterFamily = "", string filterUserName = "", int userStatusEnum = 0)
        {
            IList<Users> users = new List<Users>();
            if (!string.IsNullOrEmpty(filterUserName))
            {
                users = await _db.Users.Where(u => !u.IsDelete && u.UserBanks.Any() && EF.Functions.Like(u.UserName, $"%{filterUserName}%")).ToListAsync();
            }
            if (!string.IsNullOrEmpty(filterFamily))
            {
                users = await _db.Users.Where(u => !u.IsDelete && u.UserBanks.Any() && EF.Functions.Like(u.Family, $"%{filterFamily}%") || EF.Functions.Like(u.Name, $"%{filterFamily}%")).ToListAsync();
            }
            if (!string.IsNullOrEmpty(filterEmail))
            {
                users = await _db.Users.Where(u => !u.IsDelete && u.UserBanks.Any() && EF.Functions.Like(u.Email, $"%{filterEmail}%")).ToListAsync();
            }

            if (userStatusEnum == 3)
            {
                users = await _db.Users.Where(u => !u.IsDelete && u.UserBanks.Any(b => !b.IsReject && b.IsActive)).ToListAsync();
            }

            if (userStatusEnum == 2)
            {
                users = await _db.Users.Where(u => !u.IsDelete && u.UserBanks.Any(b => !b.IsReject && !b.IsActive)).ToListAsync();
            }

            if (userStatusEnum == 4)
            {
                users = await _db.Users.Where(u => !u.IsDelete && u.UserBanks.Any(b => b.IsReject)).ToListAsync();
            }

            return Tuple.Create(users, users.Count);
        }

        #endregion Seller

        #endregion Users

        #region UserBank

        public async Task AddUserBankAsync(UserBanks userBank)
        {
            await _db.AddAsync(userBank);
        }

        public void UpdateUserBank(UserBanks userBank)
        {
            _db.Update(userBank);
        }

        public async Task<UserBanks> GetUserBankByIdAsync(int userBankId)
        {
            return await _db.UserBanks.FindAsync(userBankId);
        }

        public async Task<IList<UserBanks>> GetUsersBanksAsync(Expression<Func<UserBanks, bool>> where)
        {
            return await _db.UserBanks.Where(where).ToListAsync();
        }

        public async Task<UserBanks> GetUserBankAsync(Expression<Func<UserBanks, bool>> where)
        {
            return await _db.UserBanks.FirstOrDefaultAsync(where);
        }

        public async Task<bool> IsUserBankExistAsync(Expression<Func<UserBanks, bool>> where)
        {
            return await _db.UserBanks.AnyAsync(where);
        }

        public async Task InsertUserBankMessageAsync(UserBankMessages bankRejectMessage)
        {
            await _db.AddAsync(bankRejectMessage);
        }

        #endregion UserBank

        public void Save()
        {
            _db.SaveChanges();
        }

        public async Task SaveAsync()
        {
            await _db.SaveChangesAsync();
        }
    }
}