﻿using Microsoft.EntityFrameworkCore;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

using ThemeShop.Domain.Interfaces;
using ThemeShop.Domain.Models;
using ThemeShop.Infra.Data.Context;

namespace ThemeShop.Infra.Data.Repository
{
    public class OrderRepository : IOrderRepository
    {
        private readonly ThemeShop_DBContext _db;

        public OrderRepository(ThemeShop_DBContext db)
        {
            _db = db;
        }

        public async Task<IList<TemplateInfoes>> GetAllUserPaymentsAsync()
        {
            return await _db.TemplateInfoes.ToListAsync();
        }

        public async Task<IList<OrderDetails>> GetOrderDetailsAsync(Expression<Func<OrderDetails, bool>> where, byte page, byte take)
        {
            if (page == 0 && take == 0)
            {
                return await _db.OrderDetails.Where(where).ToListAsync();
            }
            return await _db.OrderDetails.Where(where).Skip((page - 1) * take).Take(take).ToListAsync();
        }

        public async Task<int> GetOrderDetailsCountAsync(Expression<Func<OrderDetails, bool>> where)
        {
            return await _db.OrderDetails.CountAsync(where);
        }
    }
}