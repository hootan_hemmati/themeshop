﻿using Microsoft.EntityFrameworkCore.Migrations;

using System;

namespace ThemeShop.Infra.Data.Migrations
{
    public partial class Initial_Database : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Ads",
                columns: table => new
                {
                    AdId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AdTitle = table.Column<string>(maxLength: 500, nullable: false),
                    AdUrl = table.Column<string>(maxLength: 500, nullable: false),
                    AdImageName = table.Column<string>(maxLength: 500, nullable: true),
                    CreateDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    StartDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    EndDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    ShowInChild = table.Column<bool>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    IsDelete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Ads", x => x.AdId);
                });

            migrationBuilder.CreateTable(
                name: "ArticleCategories",
                columns: table => new
                {
                    CategoryID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(maxLength: 260, nullable: false),
                    NameInUrl = table.Column<string>(maxLength: 200, nullable: false),
                    ParentID = table.Column<int>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    IsDelete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dbo.ArticleCategories", x => x.CategoryID);
                    table.ForeignKey(
                        name: "FK_dbo.ArticleCategories_dbo.ArticleCategories_ParentID",
                        column: x => x.ParentID,
                        principalTable: "ArticleCategories",
                        principalColumn: "CategoryID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CacheSettings",
                columns: table => new
                {
                    CacheID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(maxLength: 300, nullable: false),
                    Name = table.Column<string>(maxLength: 200, nullable: false),
                    ControllerName = table.Column<string>(maxLength: 200, nullable: false),
                    ActionName = table.Column<string>(maxLength: 200, nullable: false),
                    CacheDuration = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dbo.CacheSettings", x => x.CacheID);
                });

            migrationBuilder.CreateTable(
                name: "Complaints",
                columns: table => new
                {
                    ComplaintId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ComplaintTitle = table.Column<string>(maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Complaints", x => x.ComplaintId);
                });

            migrationBuilder.CreateTable(
                name: "EmailSettings",
                columns: table => new
                {
                    EmailID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    From = table.Column<string>(maxLength: 200, nullable: false),
                    DisplayName = table.Column<string>(maxLength: 200, nullable: false),
                    SMTP = table.Column<string>(maxLength: 200, nullable: false),
                    Port = table.Column<int>(nullable: false),
                    UserName = table.Column<string>(maxLength: 200, nullable: false),
                    Password = table.Column<string>(maxLength: 200, nullable: false),
                    EnableSSL = table.Column<bool>(nullable: false),
                    IsDefaultEmail = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dbo.EmailSettings", x => x.EmailID);
                });

            migrationBuilder.CreateTable(
                name: "LinkedIns",
                columns: table => new
                {
                    LinkID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(maxLength: 200, nullable: false),
                    Description = table.Column<string>(maxLength: 200, nullable: true),
                    target = table.Column<string>(nullable: true),
                    url = table.Column<string>(maxLength: 500, nullable: false),
                    StartDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EndDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    IsActive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dbo.LinkedIns", x => x.LinkID);
                });

            migrationBuilder.CreateTable(
                name: "NewsLatterSites",
                columns: table => new
                {
                    NewsLetterID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Email = table.Column<string>(maxLength: 200, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dbo.NewsLatterSites", x => x.NewsLetterID);
                });

            migrationBuilder.CreateTable(
                name: "Permissions",
                columns: table => new
                {
                    PermissionID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(maxLength: 300, nullable: false),
                    Name = table.Column<string>(maxLength: 300, nullable: false),
                    ParentID = table.Column<int>(nullable: true),
                    DisplayPriority = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Permissions", x => x.PermissionID);
                    table.ForeignKey(
                        name: "FK_dbo.Permissions_dbo.Permissions_ParentID",
                        column: x => x.ParentID,
                        principalTable: "Permissions",
                        principalColumn: "PermissionID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Roles",
                columns: table => new
                {
                    RoleID = table.Column<int>(nullable: false),
                    RoleTitle = table.Column<string>(maxLength: 100, nullable: false),
                    IsDefaultRole = table.Column<bool>(nullable: false),
                    IsDelete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Roles", x => x.RoleID);
                });

            migrationBuilder.CreateTable(
                name: "SiteAbouUs",
                columns: table => new
                {
                    AboutID = table.Column<int>(nullable: false),
                    BrowserTitle = table.Column<string>(maxLength: 300, nullable: false),
                    Title = table.Column<string>(maxLength: 400, nullable: false),
                    ShortDescription = table.Column<string>(maxLength: 700, nullable: false),
                    Text = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dbo.SiteAbouUs", x => x.AboutID);
                });

            migrationBuilder.CreateTable(
                name: "SiteSettings",
                columns: table => new
                {
                    SettingID = table.Column<int>(nullable: false),
                    SiteName = table.Column<string>(maxLength: 300, nullable: false),
                    SiteTitle = table.Column<string>(maxLength: 360, nullable: false),
                    SiteDescription = table.Column<string>(maxLength: 500, nullable: false),
                    SiteEmail = table.Column<string>(maxLength: 300, nullable: false),
                    SiteTell = table.Column<string>(maxLength: 250, nullable: true),
                    SiteMobile = table.Column<string>(maxLength: 200, nullable: true),
                    Address = table.Column<string>(nullable: true),
                    Map = table.Column<string>(nullable: true),
                    SiteFax = table.Column<string>(maxLength: 200, nullable: true),
                    SupportTell = table.Column<string>(maxLength: 200, nullable: true),
                    SupportEmail = table.Column<string>(maxLength: 250, nullable: true),
                    SiteDomain = table.Column<string>(maxLength: 300, nullable: true),
                    LogoName = table.Column<string>(maxLength: 100, nullable: true),
                    CopyRight = table.Column<string>(nullable: true),
                    UserWalletOwenrId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dbo.SiteSettings", x => x.SettingID);
                });

            migrationBuilder.CreateTable(
                name: "SiteStaticTexts",
                columns: table => new
                {
                    Name = table.Column<string>(maxLength: 128, nullable: false),
                    Text = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dbo.SiteStaticTexts", x => x.Name);
                });

            migrationBuilder.CreateTable(
                name: "SocialNetworks",
                columns: table => new
                {
                    SocialID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(maxLength: 100, nullable: false),
                    IconName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dbo.SocialNetworks", x => x.SocialID);
                });

            migrationBuilder.CreateTable(
                name: "ThemeCategories",
                columns: table => new
                {
                    CategoryID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(maxLength: 260, nullable: false),
                    NameInUrl = table.Column<string>(maxLength: 200, nullable: false),
                    ParentID = table.Column<int>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    IsDelete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dbo.ThemeCategories", x => x.CategoryID);
                    table.ForeignKey(
                        name: "FK_dbo.ThemeCategories_dbo.ThemeCategories_ParentID",
                        column: x => x.ParentID,
                        principalTable: "ThemeCategories",
                        principalColumn: "CategoryID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    UserID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserName = table.Column<string>(maxLength: 100, nullable: false),
                    Email = table.Column<string>(maxLength: 250, nullable: false),
                    Mobile = table.Column<string>(maxLength: 50, nullable: true),
                    Password = table.Column<string>(maxLength: 100, nullable: false),
                    ActiveCode = table.Column<string>(maxLength: 100, nullable: true),
                    UserDescription = table.Column<string>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    RegisterDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    Avatar = table.Column<string>(maxLength: 50, nullable: true),
                    RegisterIP = table.Column<string>(maxLength: 150, nullable: true),
                    IsDelete = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(maxLength: 250, nullable: true),
                    Family = table.Column<string>(maxLength: 250, nullable: true),
                    ThemeSellerPercentage = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.UserID);
                });

            migrationBuilder.CreateTable(
                name: "VisitSites",
                columns: table => new
                {
                    VisitID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Ip = table.Column<string>(maxLength: 60, nullable: true),
                    Date = table.Column<DateTime>(type: "datetime", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dbo.VisitSites", x => x.VisitID);
                });

            migrationBuilder.CreateTable(
                name: "WalletTransactionTypes",
                columns: table => new
                {
                    TransactionTypeID = table.Column<int>(nullable: false),
                    TransactionTitle = table.Column<string>(maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dbo.WalletTransactionTypes", x => x.TransactionTypeID);
                });

            migrationBuilder.CreateTable(
                name: "WalletTypes",
                columns: table => new
                {
                    TypeID = table.Column<int>(nullable: false),
                    TypeTitle = table.Column<string>(maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dbo.WalletTypes", x => x.TypeID);
                });

            migrationBuilder.CreateTable(
                name: "RolePersmissions",
                columns: table => new
                {
                    RolePermissionID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RoleID = table.Column<int>(nullable: false),
                    PermissionID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dbo.RolePersmissions", x => x.RolePermissionID);
                    table.ForeignKey(
                        name: "FK_dbo.RolePersmissions_dbo.Permissions_PermissionID",
                        column: x => x.PermissionID,
                        principalTable: "Permissions",
                        principalColumn: "PermissionID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_dbo.RolePersmissions_dbo.Roles_RoleID",
                        column: x => x.RoleID,
                        principalTable: "Roles",
                        principalColumn: "RoleID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SiteSocialNetworks",
                columns: table => new
                {
                    NetworkID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SocialID = table.Column<int>(nullable: false),
                    Url = table.Column<string>(maxLength: 360, nullable: false),
                    IsActive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dbo.SiteSocialNetworks", x => x.NetworkID);
                    table.ForeignKey(
                        name: "FK_dbo.SiteSocialNetworks_dbo.SocialNetworks_SocialID",
                        column: x => x.SocialID,
                        principalTable: "SocialNetworks",
                        principalColumn: "SocialID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AdThemeCategories",
                columns: table => new
                {
                    AdThemeCategoryId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AdId = table.Column<int>(nullable: false),
                    CategoryId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AdThemeCategories", x => x.AdThemeCategoryId);
                    table.ForeignKey(
                        name: "FK_dbo.AdThemeCategories_dbo.Ads_AdId",
                        column: x => x.AdId,
                        principalTable: "Ads",
                        principalColumn: "AdId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_dbo.AdThemeCategories_dbo.ThemeCategories_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "ThemeCategories",
                        principalColumn: "CategoryID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Articles",
                columns: table => new
                {
                    ArticleID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AuthorID = table.Column<int>(nullable: false),
                    ArticleTitle = table.Column<string>(maxLength: 400, nullable: false),
                    ShortDescription = table.Column<string>(maxLength: 500, nullable: false),
                    ArticleText = table.Column<string>(nullable: false),
                    ArticleImageName = table.Column<string>(maxLength: 200, nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    IsVip = table.Column<bool>(nullable: false),
                    CanInsertComment = table.Column<bool>(nullable: false),
                    CreateDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    IsDelete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Articles", x => x.ArticleID);
                    table.ForeignKey(
                        name: "FK_dbo.Articles_dbo.Users_AuthorID",
                        column: x => x.AuthorID,
                        principalTable: "Users",
                        principalColumn: "UserID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ContactUs",
                columns: table => new
                {
                    ContactID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<int>(nullable: true),
                    Name = table.Column<string>(maxLength: 200, nullable: false),
                    Email = table.Column<string>(maxLength: 250, nullable: false),
                    Mobile = table.Column<string>(maxLength: 100, nullable: true),
                    Message = table.Column<string>(maxLength: 2000, nullable: false),
                    Answer = table.Column<string>(nullable: true),
                    UserIP = table.Column<string>(maxLength: 100, nullable: true),
                    IsRead = table.Column<bool>(nullable: false),
                    CreateDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    Subject = table.Column<string>(maxLength: 500, nullable: false, defaultValueSql: "('')")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dbo.ContactUs", x => x.ContactID);
                    table.ForeignKey(
                        name: "FK_dbo.ContactUs_dbo.Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "UserID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Orders",
                columns: table => new
                {
                    OrderID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserID = table.Column<int>(nullable: false),
                    CreateDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    IsFinaly = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Orders", x => x.OrderID);
                    table.ForeignKey(
                        name: "FK_dbo.Orders_dbo.Users_UserID",
                        column: x => x.UserID,
                        principalTable: "Users",
                        principalColumn: "UserID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Pages",
                columns: table => new
                {
                    PageID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AuthorID = table.Column<int>(nullable: false),
                    PageTitle = table.Column<string>(maxLength: 400, nullable: false),
                    BrowserTitle = table.Column<string>(maxLength: 300, nullable: true),
                    PageUrl = table.Column<string>(maxLength: 500, nullable: false),
                    ShortDescription = table.Column<string>(maxLength: 500, nullable: false),
                    PageText = table.Column<string>(nullable: false),
                    PageImage = table.Column<string>(nullable: true),
                    CreateDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    IsDelete = table.Column<bool>(nullable: false),
                    StartDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    EndDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    IsCommentActive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Pages", x => x.PageID);
                    table.ForeignKey(
                        name: "FK_dbo.Pages_dbo.Users_AuthorID",
                        column: x => x.AuthorID,
                        principalTable: "Users",
                        principalColumn: "UserID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Themes",
                columns: table => new
                {
                    ThemeID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SellerID = table.Column<int>(nullable: false),
                    ThemeTitle = table.Column<string>(maxLength: 400, nullable: false),
                    ShortDescription = table.Column<string>(maxLength: 500, nullable: false),
                    Description = table.Column<string>(nullable: false),
                    Featurs = table.Column<string>(nullable: false),
                    ThemeImageName = table.Column<string>(maxLength: 200, nullable: true),
                    Price = table.Column<int>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    CanInsertComment = table.Column<bool>(nullable: false),
                    CreateDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    IsDelete = table.Column<bool>(nullable: false),
                    Theme_ThemeID = table.Column<int>(nullable: true),
                    IsRejected = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Themes", x => x.ThemeID);
                    table.ForeignKey(
                        name: "FK_dbo.Themes_dbo.Users_SellerID",
                        column: x => x.SellerID,
                        principalTable: "Users",
                        principalColumn: "UserID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_dbo.Themes_dbo.Themes_Theme_ThemeID",
                        column: x => x.Theme_ThemeID,
                        principalTable: "Themes",
                        principalColumn: "ThemeID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UserBanks",
                columns: table => new
                {
                    UserBankId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<int>(nullable: false),
                    CodeMeli = table.Column<string>(maxLength: 10, nullable: false),
                    ImageKartMeli = table.Column<string>(maxLength: 250, nullable: false),
                    SellerAccountHolder = table.Column<string>(maxLength: 250, nullable: false),
                    BankName = table.Column<string>(maxLength: 250, nullable: false),
                    SellerAccountNumber = table.Column<string>(maxLength: 250, nullable: false),
                    SellerCardNumber = table.Column<string>(maxLength: 250, nullable: false),
                    SellerShabaNumber = table.Column<string>(maxLength: 250, nullable: false),
                    ImageCardBank = table.Column<string>(maxLength: 250, nullable: false),
                    CreateDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    IsReject = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserBanks", x => x.UserBankId);
                    table.ForeignKey(
                        name: "FK_dbo.UserBanks_dbo.Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "UserID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserLogs",
                columns: table => new
                {
                    LogID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserID = table.Column<int>(nullable: false),
                    Description = table.Column<string>(maxLength: 1000, nullable: false),
                    DateTime = table.Column<DateTime>(type: "datetime", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dbo.UserLogs", x => x.LogID);
                    table.ForeignKey(
                        name: "FK_dbo.UserLogs_dbo.Users_UserID",
                        column: x => x.UserID,
                        principalTable: "Users",
                        principalColumn: "UserID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserRoles",
                columns: table => new
                {
                    UserRoleID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserID = table.Column<int>(nullable: false),
                    RoleID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserRoles", x => x.UserRoleID);
                    table.ForeignKey(
                        name: "FK_dbo.UserRoles_dbo.Roles_RoleID",
                        column: x => x.RoleID,
                        principalTable: "Roles",
                        principalColumn: "RoleID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_dbo.UserRoles_dbo.Users_UserID",
                        column: x => x.UserID,
                        principalTable: "Users",
                        principalColumn: "UserID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ArticleCategory",
                columns: table => new
                {
                    ArticleCategoryID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ArticleID = table.Column<int>(nullable: false),
                    CategoryID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ArticleCategory", x => x.ArticleCategoryID);
                    table.ForeignKey(
                        name: "FK_dbo.ArticleCategories_dbo.Articles_ArticleID",
                        column: x => x.ArticleID,
                        principalTable: "Articles",
                        principalColumn: "ArticleID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_dbo.ArticleCategories_dbo.ArticleCategories_CategoryID",
                        column: x => x.CategoryID,
                        principalTable: "ArticleCategories",
                        principalColumn: "CategoryID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ArticleComments",
                columns: table => new
                {
                    CommantID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ArticleID = table.Column<int>(nullable: false),
                    UserID = table.Column<int>(nullable: false),
                    Comment = table.Column<string>(maxLength: 800, nullable: false),
                    CreateDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    IsDelete = table.Column<bool>(nullable: false),
                    IsRead = table.Column<bool>(nullable: false),
                    ParentID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dbo.ArticleComments", x => x.CommantID);
                    table.ForeignKey(
                        name: "FK_dbo.ArticleComments_dbo.Articles_ArticleID",
                        column: x => x.ArticleID,
                        principalTable: "Articles",
                        principalColumn: "ArticleID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_dbo.ArticleComments_dbo.ArticleComments_ParentID",
                        column: x => x.ParentID,
                        principalTable: "ArticleComments",
                        principalColumn: "CommantID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_dbo.ArticleComments_dbo.Users_UserID",
                        column: x => x.UserID,
                        principalTable: "Users",
                        principalColumn: "UserID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ArticleRelateds",
                columns: table => new
                {
                    RelatedID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ArticleID = table.Column<int>(nullable: false),
                    ArticleRelatedID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dbo.ArticleRelateds", x => x.RelatedID);
                    table.ForeignKey(
                        name: "FK_dbo.ArticleRelateds_dbo.Articles_ArticleID",
                        column: x => x.ArticleID,
                        principalTable: "Articles",
                        principalColumn: "ArticleID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_dbo.ArticleRelateds_dbo.Articles_ArticleRelatedID",
                        column: x => x.ArticleRelatedID,
                        principalTable: "Articles",
                        principalColumn: "ArticleID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ArticleTags",
                columns: table => new
                {
                    ArticleTagID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ArticleID = table.Column<int>(nullable: false),
                    Tag = table.Column<string>(maxLength: 200, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ArticleTags", x => x.ArticleTagID);
                    table.ForeignKey(
                        name: "FK_dbo.ArticleTags_dbo.Articles_ArticleID",
                        column: x => x.ArticleID,
                        principalTable: "Articles",
                        principalColumn: "ArticleID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ArticleVisits",
                columns: table => new
                {
                    VisitID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ArticleID = table.Column<int>(nullable: false),
                    IP = table.Column<string>(maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dbo.ArticleVisits", x => x.VisitID);
                    table.ForeignKey(
                        name: "FK_dbo.ArticleVisits_dbo.Articles_ArticleID",
                        column: x => x.ArticleID,
                        principalTable: "Articles",
                        principalColumn: "ArticleID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WalletUsers",
                columns: table => new
                {
                    WalletID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TransactionTypeID = table.Column<int>(nullable: false),
                    TypeID = table.Column<int>(nullable: false),
                    UserID = table.Column<int>(nullable: false),
                    Price = table.Column<int>(nullable: false),
                    OrderID = table.Column<int>(nullable: true),
                    IsPay = table.Column<bool>(nullable: false),
                    CreateDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    Description = table.Column<string>(maxLength: 500, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dbo.WalletUsers", x => x.WalletID);
                    table.ForeignKey(
                        name: "FK_dbo.WalletUsers_dbo.Orders_OrderID",
                        column: x => x.OrderID,
                        principalTable: "Orders",
                        principalColumn: "OrderID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_dbo.WalletUsers_dbo.WalletTransactionTypes_TransactionTypeID",
                        column: x => x.TransactionTypeID,
                        principalTable: "WalletTransactionTypes",
                        principalColumn: "TransactionTypeID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_dbo.WalletUsers_dbo.WalletTypes_TypeID",
                        column: x => x.TypeID,
                        principalTable: "WalletTypes",
                        principalColumn: "TypeID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_dbo.WalletUsers_dbo.Users_UserID",
                        column: x => x.UserID,
                        principalTable: "Users",
                        principalColumn: "UserID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PageComments",
                columns: table => new
                {
                    CommantID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PageID = table.Column<int>(nullable: false),
                    UserID = table.Column<int>(nullable: false),
                    Comment = table.Column<string>(maxLength: 800, nullable: false),
                    CreateDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    IsDelete = table.Column<bool>(nullable: false),
                    IsRead = table.Column<bool>(nullable: false),
                    ParentID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dbo.PageComments", x => x.CommantID);
                    table.ForeignKey(
                        name: "FK_dbo.PageComments_dbo.Pages_PageID",
                        column: x => x.PageID,
                        principalTable: "Pages",
                        principalColumn: "PageID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_dbo.PageComments_dbo.PageComments_ParentID",
                        column: x => x.ParentID,
                        principalTable: "PageComments",
                        principalColumn: "CommantID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_dbo.PageComments_dbo.Users_UserID",
                        column: x => x.UserID,
                        principalTable: "Users",
                        principalColumn: "UserID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PageTags",
                columns: table => new
                {
                    TagID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PageID = table.Column<int>(nullable: false),
                    TagTitle = table.Column<string>(maxLength: 300, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dbo.PageTags", x => x.TagID);
                    table.ForeignKey(
                        name: "FK_dbo.PageTags_dbo.Pages_PageID",
                        column: x => x.PageID,
                        principalTable: "Pages",
                        principalColumn: "PageID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PageVisits",
                columns: table => new
                {
                    VisitID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PageID = table.Column<int>(nullable: false),
                    IP = table.Column<string>(maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dbo.PageVisits", x => x.VisitID);
                    table.ForeignKey(
                        name: "FK_dbo.PageVisits_dbo.Pages_PageID",
                        column: x => x.PageID,
                        principalTable: "Pages",
                        principalColumn: "PageID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ComplaintThemes",
                columns: table => new
                {
                    ComplaintQuestionId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ComplaintId = table.Column<int>(nullable: false),
                    ThemeId = table.Column<int>(nullable: false),
                    Date = table.Column<DateTime>(type: "datetime", nullable: false),
                    IsSeen = table.Column<bool>(nullable: false),
                    Description = table.Column<string>(maxLength: 500, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dbo.ComplaintThemes", x => x.ComplaintQuestionId);
                    table.ForeignKey(
                        name: "FK_dbo.ComplaintThemes_dbo.Complaints_ComplaintId",
                        column: x => x.ComplaintId,
                        principalTable: "Complaints",
                        principalColumn: "ComplaintId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_dbo.ComplaintThemes_dbo.Themes_ThemeId",
                        column: x => x.ThemeId,
                        principalTable: "Themes",
                        principalColumn: "ThemeID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DiscontCodes",
                columns: table => new
                {
                    DisCountCodeId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Code = table.Column<string>(nullable: true),
                    Value = table.Column<int>(nullable: false),
                    UsageCount = table.Column<int>(nullable: false),
                    ThemeId = table.Column<int>(nullable: true),
                    ExpireTime = table.Column<DateTime>(type: "datetime", nullable: true),
                    CreateOn = table.Column<DateTime>(type: "datetime", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dbo.DiscontCodes", x => x.DisCountCodeId);
                    table.ForeignKey(
                        name: "FK_dbo.DiscontCodes_dbo.Themes_ThemeId",
                        column: x => x.ThemeId,
                        principalTable: "Themes",
                        principalColumn: "ThemeID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DownloadTokens",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Token = table.Column<string>(nullable: true),
                    UserId = table.Column<int>(nullable: false),
                    GeneratedOn = table.Column<DateTime>(type: "datetime", nullable: false),
                    IsExpired = table.Column<bool>(nullable: false),
                    ThemeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DownloadTokens", x => x.Id);
                    table.ForeignKey(
                        name: "FK_dbo.DownloadTokens_dbo.Themes_ThemeId",
                        column: x => x.ThemeId,
                        principalTable: "Themes",
                        principalColumn: "ThemeID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_dbo.DownloadTokens_dbo.Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "UserID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "OrderDetails",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    OrderId = table.Column<int>(nullable: false),
                    ThemeId = table.Column<int>(nullable: false),
                    Price = table.Column<int>(nullable: false),
                    Dicount = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_dbo.OrderDetails_dbo.Orders_OrderId",
                        column: x => x.OrderId,
                        principalTable: "Orders",
                        principalColumn: "OrderID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_dbo.OrderDetails_dbo.Themes_ThemeId",
                        column: x => x.ThemeId,
                        principalTable: "Themes",
                        principalColumn: "ThemeID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TemplateInfoes",
                columns: table => new
                {
                    TemplateInfoId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreateBuy = table.Column<DateTime>(type: "datetime", nullable: false),
                    Price = table.Column<int>(nullable: false),
                    UserId = table.Column<int>(nullable: false),
                    ThemeId = table.Column<int>(nullable: false),
                    SellerId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TemplateInfoes", x => x.TemplateInfoId);
                    table.ForeignKey(
                        name: "FK_dbo.TemplateInfoes_dbo.Users_SellerId",
                        column: x => x.SellerId,
                        principalTable: "Users",
                        principalColumn: "UserID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_dbo.TemplateInfoes_dbo.Themes_ThemeId",
                        column: x => x.ThemeId,
                        principalTable: "Themes",
                        principalColumn: "ThemeID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_dbo.TemplateInfoes_dbo.Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "UserID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ThemeAttachments",
                columns: table => new
                {
                    AttachID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ThemeID = table.Column<int>(nullable: false),
                    Title = table.Column<string>(maxLength: 250, nullable: false),
                    FileName = table.Column<string>(maxLength: 200, nullable: true),
                    FileExtension = table.Column<string>(maxLength: 50, nullable: true),
                    FileSize = table.Column<int>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    DownloadCount = table.Column<int>(nullable: false),
                    IsFree = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dbo.ThemeAttachments", x => x.AttachID);
                    table.ForeignKey(
                        name: "FK_dbo.ThemeAttachments_dbo.Themes_ThemeID",
                        column: x => x.ThemeID,
                        principalTable: "Themes",
                        principalColumn: "ThemeID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ThemeChangeStatusMessages",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ThemeId = table.Column<int>(nullable: false),
                    Status = table.Column<int>(nullable: false),
                    SentDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    Message = table.Column<string>(maxLength: 1000, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ThemeChangeStatusMessages", x => x.Id);
                    table.ForeignKey(
                        name: "FK_dbo.ThemeChangeStatusMessages_dbo.Themes_ThemeId",
                        column: x => x.ThemeId,
                        principalTable: "Themes",
                        principalColumn: "ThemeID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ThemeComments",
                columns: table => new
                {
                    CommantID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ThemeID = table.Column<int>(nullable: false),
                    UserID = table.Column<int>(nullable: false),
                    Comment = table.Column<string>(maxLength: 800, nullable: false),
                    CreateDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    IsDelete = table.Column<bool>(nullable: false),
                    IsRead = table.Column<bool>(nullable: false),
                    ParentID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dbo.ThemeComments", x => x.CommantID);
                    table.ForeignKey(
                        name: "FK_dbo.ThemeComments_dbo.ThemeComments_ParentID",
                        column: x => x.ParentID,
                        principalTable: "ThemeComments",
                        principalColumn: "CommantID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_dbo.ThemeComments_dbo.Themes_ThemeID",
                        column: x => x.ThemeID,
                        principalTable: "Themes",
                        principalColumn: "ThemeID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_dbo.ThemeComments_dbo.Users_UserID",
                        column: x => x.UserID,
                        principalTable: "Users",
                        principalColumn: "UserID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ThemeFavorits",
                columns: table => new
                {
                    FavoritID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ThemeID = table.Column<int>(nullable: false),
                    UserID = table.Column<int>(nullable: false),
                    CreatDate = table.Column<DateTime>(type: "datetime", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dbo.ThemeFavorits", x => x.FavoritID);
                    table.ForeignKey(
                        name: "FK_dbo.ThemeFavorits_dbo.Themes_ThemeID",
                        column: x => x.ThemeID,
                        principalTable: "Themes",
                        principalColumn: "ThemeID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_dbo.ThemeFavorits_dbo.Users_UserID",
                        column: x => x.UserID,
                        principalTable: "Users",
                        principalColumn: "UserID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ThemeGalleries",
                columns: table => new
                {
                    GalleryID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ThemeID = table.Column<int>(nullable: false),
                    GalleryImageName = table.Column<string>(maxLength: 200, nullable: false),
                    ImageTitle = table.Column<string>(maxLength: 200, nullable: false, defaultValueSql: "('')")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dbo.ThemeGalleries", x => x.GalleryID);
                    table.ForeignKey(
                        name: "FK_dbo.ThemeGalleries_dbo.Themes_ThemeID",
                        column: x => x.ThemeID,
                        principalTable: "Themes",
                        principalColumn: "ThemeID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ThemeLikes",
                columns: table => new
                {
                    LikeID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserID = table.Column<int>(nullable: true),
                    ThemeID = table.Column<int>(nullable: false),
                    IP = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dbo.ThemeLikes", x => x.LikeID);
                    table.ForeignKey(
                        name: "FK_dbo.ThemeLikes_dbo.Themes_ThemeID",
                        column: x => x.ThemeID,
                        principalTable: "Themes",
                        principalColumn: "ThemeID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_dbo.ThemeLikes_dbo.Users_UserID",
                        column: x => x.UserID,
                        principalTable: "Users",
                        principalColumn: "UserID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ThemeOfferTodays",
                columns: table => new
                {
                    OfferID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ThemeID = table.Column<int>(nullable: false),
                    StartDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    EndDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    VisitCount = table.Column<int>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dbo.ThemeOfferTodays", x => x.OfferID);
                    table.ForeignKey(
                        name: "FK_dbo.ThemeOfferTodays_dbo.Themes_ThemeID",
                        column: x => x.ThemeID,
                        principalTable: "Themes",
                        principalColumn: "ThemeID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ThemePreviews",
                columns: table => new
                {
                    PreviewID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ThemeID = table.Column<int>(nullable: false),
                    RootFolder = table.Column<string>(maxLength: 400, nullable: false),
                    PageTitle = table.Column<string>(maxLength: 400, nullable: false),
                    PageName = table.Column<string>(maxLength: 400, nullable: false),
                    IsDefaultPage = table.Column<bool>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dbo.ThemePreviews", x => x.PreviewID);
                    table.ForeignKey(
                        name: "FK_dbo.ThemePreviews_dbo.Themes_ThemeID",
                        column: x => x.ThemeID,
                        principalTable: "Themes",
                        principalColumn: "ThemeID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ThemeRelateds",
                columns: table => new
                {
                    RelatedID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ThemeID = table.Column<int>(nullable: false),
                    themeRelatedID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dbo.ThemeRelateds", x => x.RelatedID);
                    table.ForeignKey(
                        name: "FK_dbo.ThemeRelateds_dbo.Themes_ThemeID",
                        column: x => x.ThemeID,
                        principalTable: "Themes",
                        principalColumn: "ThemeID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_dbo.ThemeRelateds_dbo.Themes_themeRelatedID",
                        column: x => x.themeRelatedID,
                        principalTable: "Themes",
                        principalColumn: "ThemeID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ThemeSelectedCategories",
                columns: table => new
                {
                    SelectedID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ThemeID = table.Column<int>(nullable: false),
                    CategoryID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dbo.ThemeSelectedCategories", x => x.SelectedID);
                    table.ForeignKey(
                        name: "FK_dbo.ThemeSelectedCategories_dbo.ThemeCategories_CategoryID",
                        column: x => x.CategoryID,
                        principalTable: "ThemeCategories",
                        principalColumn: "CategoryID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_dbo.ThemeSelectedCategories_dbo.Themes_ThemeID",
                        column: x => x.ThemeID,
                        principalTable: "Themes",
                        principalColumn: "ThemeID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ThemeTags",
                columns: table => new
                {
                    ThemeTagID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ThemeID = table.Column<int>(nullable: false),
                    Tag = table.Column<string>(maxLength: 200, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ThemeTags", x => x.ThemeTagID);
                    table.ForeignKey(
                        name: "FK_dbo.ThemeTags_dbo.Themes_ThemeID",
                        column: x => x.ThemeID,
                        principalTable: "Themes",
                        principalColumn: "ThemeID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ThemeVisits",
                columns: table => new
                {
                    VisitID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ThemeID = table.Column<int>(nullable: false),
                    IP = table.Column<string>(maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dbo.ThemeVisits", x => x.VisitID);
                    table.ForeignKey(
                        name: "FK_dbo.ThemeVisits_dbo.Themes_ThemeID",
                        column: x => x.ThemeID,
                        principalTable: "Themes",
                        principalColumn: "ThemeID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Ticketings",
                columns: table => new
                {
                    TicketId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    User_UserID = table.Column<int>(nullable: true),
                    Title = table.Column<string>(maxLength: 300, nullable: false, defaultValueSql: "('')"),
                    CreatedOn = table.Column<DateTime>(type: "datetime", nullable: false),
                    IsClose = table.Column<bool>(nullable: false),
                    IsDelete = table.Column<bool>(nullable: false),
                    ThemeId = table.Column<int>(nullable: true),
                    Sender = table.Column<int>(nullable: false),
                    Reciver = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dbo.Ticketings", x => x.TicketId);
                    table.ForeignKey(
                        name: "FK_dbo.Ticketings_dbo.Themes_ThemeId",
                        column: x => x.ThemeId,
                        principalTable: "Themes",
                        principalColumn: "ThemeID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_dbo.Ticketings_dbo.Users_User_UserID",
                        column: x => x.User_UserID,
                        principalTable: "Users",
                        principalColumn: "UserID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UserBankRejectMessages",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserBankId = table.Column<int>(nullable: false),
                    SentDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    Message = table.Column<string>(maxLength: 1000, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserBankRejectMessages", x => x.Id);
                    table.ForeignKey(
                        name: "FK_dbo.UserBankRejectMessages_dbo.UserBanks_UserBankId",
                        column: x => x.UserBankId,
                        principalTable: "UserBanks",
                        principalColumn: "UserBankId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DiscountCodeUses",
                columns: table => new
                {
                    DiscountCodeUseId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DiscontId = table.Column<int>(nullable: false),
                    UserId = table.Column<int>(nullable: false),
                    OrderId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DiscountCodeUses", x => x.DiscountCodeUseId);
                    table.ForeignKey(
                        name: "FK_dbo.DiscountCodeUses_dbo.DiscontCodes_DiscontId",
                        column: x => x.DiscontId,
                        principalTable: "DiscontCodes",
                        principalColumn: "DisCountCodeId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_dbo.DiscountCodeUses_dbo.Orders_OrderId",
                        column: x => x.OrderId,
                        principalTable: "Orders",
                        principalColumn: "OrderID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_dbo.DiscountCodeUses_dbo.Users_UsedBy_UserID",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "UserID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TicketMessages",
                columns: table => new
                {
                    TicketMessageId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Text = table.Column<string>(nullable: true),
                    TicketId = table.Column<int>(nullable: false),
                    IsSeen = table.Column<bool>(nullable: false),
                    CreatedOn = table.Column<DateTime>(type: "datetime", nullable: false),
                    OwnerId = table.Column<int>(nullable: false),
                    Owner_UserID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TicketMessages", x => x.TicketMessageId);
                    table.ForeignKey(
                        name: "FK_dbo.TicketMessages_dbo.Users_Owner_UserID",
                        column: x => x.Owner_UserID,
                        principalTable: "Users",
                        principalColumn: "UserID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_dbo.TicketMessages_dbo.Ticketings_TicketId",
                        column: x => x.TicketId,
                        principalTable: "Ticketings",
                        principalColumn: "TicketId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AdId",
                table: "AdThemeCategories",
                column: "AdId");

            migrationBuilder.CreateIndex(
                name: "IX_CategoryId",
                table: "AdThemeCategories",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_ParentID",
                table: "ArticleCategories",
                column: "ParentID");

            migrationBuilder.CreateIndex(
                name: "IX_ArticleID",
                table: "ArticleCategory",
                column: "ArticleID");

            migrationBuilder.CreateIndex(
                name: "IX_CategoryID",
                table: "ArticleCategory",
                column: "CategoryID");

            migrationBuilder.CreateIndex(
                name: "IX_ArticleID",
                table: "ArticleComments",
                column: "ArticleID");

            migrationBuilder.CreateIndex(
                name: "IX_ParentID",
                table: "ArticleComments",
                column: "ParentID");

            migrationBuilder.CreateIndex(
                name: "IX_UserID",
                table: "ArticleComments",
                column: "UserID");

            migrationBuilder.CreateIndex(
                name: "IX_ArticleID",
                table: "ArticleRelateds",
                column: "ArticleID");

            migrationBuilder.CreateIndex(
                name: "IX_ArticleRelatedID",
                table: "ArticleRelateds",
                column: "ArticleRelatedID");

            migrationBuilder.CreateIndex(
                name: "IX_AuthorID",
                table: "Articles",
                column: "AuthorID");

            migrationBuilder.CreateIndex(
                name: "IX_ArticleID",
                table: "ArticleTags",
                column: "ArticleID");

            migrationBuilder.CreateIndex(
                name: "IX_ArticleID",
                table: "ArticleVisits",
                column: "ArticleID");

            migrationBuilder.CreateIndex(
                name: "IX_ComplaintId",
                table: "ComplaintThemes",
                column: "ComplaintId");

            migrationBuilder.CreateIndex(
                name: "IX_ThemeId",
                table: "ComplaintThemes",
                column: "ThemeId");

            migrationBuilder.CreateIndex(
                name: "IX_UserId",
                table: "ContactUs",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_ThemeId",
                table: "DiscontCodes",
                column: "ThemeId");

            migrationBuilder.CreateIndex(
                name: "IX_DiscontId",
                table: "DiscountCodeUses",
                column: "DiscontId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderId",
                table: "DiscountCodeUses",
                column: "OrderId");

            migrationBuilder.CreateIndex(
                name: "IX_UserId",
                table: "DiscountCodeUses",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_ThemeId",
                table: "DownloadTokens",
                column: "ThemeId");

            migrationBuilder.CreateIndex(
                name: "IX_UserId",
                table: "DownloadTokens",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderId",
                table: "OrderDetails",
                column: "OrderId");

            migrationBuilder.CreateIndex(
                name: "IX_ThemeId",
                table: "OrderDetails",
                column: "ThemeId");

            migrationBuilder.CreateIndex(
                name: "IX_UserID",
                table: "Orders",
                column: "UserID");

            migrationBuilder.CreateIndex(
                name: "IX_PageID",
                table: "PageComments",
                column: "PageID");

            migrationBuilder.CreateIndex(
                name: "IX_ParentID",
                table: "PageComments",
                column: "ParentID");

            migrationBuilder.CreateIndex(
                name: "IX_UserID",
                table: "PageComments",
                column: "UserID");

            migrationBuilder.CreateIndex(
                name: "IX_AuthorID",
                table: "Pages",
                column: "AuthorID");

            migrationBuilder.CreateIndex(
                name: "IX_PageID",
                table: "PageTags",
                column: "PageID");

            migrationBuilder.CreateIndex(
                name: "IX_PageID",
                table: "PageVisits",
                column: "PageID");

            migrationBuilder.CreateIndex(
                name: "IX_ParentID",
                table: "Permissions",
                column: "ParentID");

            migrationBuilder.CreateIndex(
                name: "IX_PermissionID",
                table: "RolePersmissions",
                column: "PermissionID");

            migrationBuilder.CreateIndex(
                name: "IX_RoleID",
                table: "RolePersmissions",
                column: "RoleID");

            migrationBuilder.CreateIndex(
                name: "IX_SocialID",
                table: "SiteSocialNetworks",
                column: "SocialID");

            migrationBuilder.CreateIndex(
                name: "IX_SellerId",
                table: "TemplateInfoes",
                column: "SellerId");

            migrationBuilder.CreateIndex(
                name: "IX_ThemeId",
                table: "TemplateInfoes",
                column: "ThemeId");

            migrationBuilder.CreateIndex(
                name: "IX_UserId",
                table: "TemplateInfoes",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_ThemeID",
                table: "ThemeAttachments",
                column: "ThemeID");

            migrationBuilder.CreateIndex(
                name: "IX_ParentID",
                table: "ThemeCategories",
                column: "ParentID");

            migrationBuilder.CreateIndex(
                name: "IX_ThemeId",
                table: "ThemeChangeStatusMessages",
                column: "ThemeId");

            migrationBuilder.CreateIndex(
                name: "IX_ParentID",
                table: "ThemeComments",
                column: "ParentID");

            migrationBuilder.CreateIndex(
                name: "IX_ThemeID",
                table: "ThemeComments",
                column: "ThemeID");

            migrationBuilder.CreateIndex(
                name: "IX_UserID",
                table: "ThemeComments",
                column: "UserID");

            migrationBuilder.CreateIndex(
                name: "IX_ThemeID",
                table: "ThemeFavorits",
                column: "ThemeID");

            migrationBuilder.CreateIndex(
                name: "IX_UserID",
                table: "ThemeFavorits",
                column: "UserID");

            migrationBuilder.CreateIndex(
                name: "IX_ThemeID",
                table: "ThemeGalleries",
                column: "ThemeID");

            migrationBuilder.CreateIndex(
                name: "IX_ThemeID",
                table: "ThemeLikes",
                column: "ThemeID");

            migrationBuilder.CreateIndex(
                name: "IX_UserID",
                table: "ThemeLikes",
                column: "UserID");

            migrationBuilder.CreateIndex(
                name: "IX_ThemeID",
                table: "ThemeOfferTodays",
                column: "ThemeID");

            migrationBuilder.CreateIndex(
                name: "IX_ThemeID",
                table: "ThemePreviews",
                column: "ThemeID");

            migrationBuilder.CreateIndex(
                name: "IX_ThemeID",
                table: "ThemeRelateds",
                column: "ThemeID");

            migrationBuilder.CreateIndex(
                name: "IX_themeRelatedID",
                table: "ThemeRelateds",
                column: "themeRelatedID");

            migrationBuilder.CreateIndex(
                name: "IX_SellerID",
                table: "Themes",
                column: "SellerID");

            migrationBuilder.CreateIndex(
                name: "IX_Theme_ThemeID",
                table: "Themes",
                column: "Theme_ThemeID");

            migrationBuilder.CreateIndex(
                name: "IX_CategoryID",
                table: "ThemeSelectedCategories",
                column: "CategoryID");

            migrationBuilder.CreateIndex(
                name: "IX_ThemeID",
                table: "ThemeSelectedCategories",
                column: "ThemeID");

            migrationBuilder.CreateIndex(
                name: "IX_ThemeID",
                table: "ThemeTags",
                column: "ThemeID");

            migrationBuilder.CreateIndex(
                name: "IX_ThemeID",
                table: "ThemeVisits",
                column: "ThemeID");

            migrationBuilder.CreateIndex(
                name: "IX_ThemeId",
                table: "Ticketings",
                column: "ThemeId");

            migrationBuilder.CreateIndex(
                name: "IX_User_UserID",
                table: "Ticketings",
                column: "User_UserID");

            migrationBuilder.CreateIndex(
                name: "IX_Owner_UserID",
                table: "TicketMessages",
                column: "Owner_UserID");

            migrationBuilder.CreateIndex(
                name: "IX_TicketId",
                table: "TicketMessages",
                column: "TicketId");

            migrationBuilder.CreateIndex(
                name: "IX_UserBankId",
                table: "UserBankRejectMessages",
                column: "UserBankId");

            migrationBuilder.CreateIndex(
                name: "IX_UserId",
                table: "UserBanks",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_UserID",
                table: "UserLogs",
                column: "UserID");

            migrationBuilder.CreateIndex(
                name: "IX_RoleID",
                table: "UserRoles",
                column: "RoleID");

            migrationBuilder.CreateIndex(
                name: "IX_UserID",
                table: "UserRoles",
                column: "UserID");

            migrationBuilder.CreateIndex(
                name: "IX_OrderID",
                table: "WalletUsers",
                column: "OrderID");

            migrationBuilder.CreateIndex(
                name: "IX_TransactionTypeID",
                table: "WalletUsers",
                column: "TransactionTypeID");

            migrationBuilder.CreateIndex(
                name: "IX_TypeID",
                table: "WalletUsers",
                column: "TypeID");

            migrationBuilder.CreateIndex(
                name: "IX_UserID",
                table: "WalletUsers",
                column: "UserID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AdThemeCategories");

            migrationBuilder.DropTable(
                name: "ArticleCategory");

            migrationBuilder.DropTable(
                name: "ArticleComments");

            migrationBuilder.DropTable(
                name: "ArticleRelateds");

            migrationBuilder.DropTable(
                name: "ArticleTags");

            migrationBuilder.DropTable(
                name: "ArticleVisits");

            migrationBuilder.DropTable(
                name: "CacheSettings");

            migrationBuilder.DropTable(
                name: "ComplaintThemes");

            migrationBuilder.DropTable(
                name: "ContactUs");

            migrationBuilder.DropTable(
                name: "DiscountCodeUses");

            migrationBuilder.DropTable(
                name: "DownloadTokens");

            migrationBuilder.DropTable(
                name: "EmailSettings");

            migrationBuilder.DropTable(
                name: "LinkedIns");

            migrationBuilder.DropTable(
                name: "NewsLatterSites");

            migrationBuilder.DropTable(
                name: "OrderDetails");

            migrationBuilder.DropTable(
                name: "PageComments");

            migrationBuilder.DropTable(
                name: "PageTags");

            migrationBuilder.DropTable(
                name: "PageVisits");

            migrationBuilder.DropTable(
                name: "RolePersmissions");

            migrationBuilder.DropTable(
                name: "SiteAbouUs");

            migrationBuilder.DropTable(
                name: "SiteSettings");

            migrationBuilder.DropTable(
                name: "SiteSocialNetworks");

            migrationBuilder.DropTable(
                name: "SiteStaticTexts");

            migrationBuilder.DropTable(
                name: "TemplateInfoes");

            migrationBuilder.DropTable(
                name: "ThemeAttachments");

            migrationBuilder.DropTable(
                name: "ThemeChangeStatusMessages");

            migrationBuilder.DropTable(
                name: "ThemeComments");

            migrationBuilder.DropTable(
                name: "ThemeFavorits");

            migrationBuilder.DropTable(
                name: "ThemeGalleries");

            migrationBuilder.DropTable(
                name: "ThemeLikes");

            migrationBuilder.DropTable(
                name: "ThemeOfferTodays");

            migrationBuilder.DropTable(
                name: "ThemePreviews");

            migrationBuilder.DropTable(
                name: "ThemeRelateds");

            migrationBuilder.DropTable(
                name: "ThemeSelectedCategories");

            migrationBuilder.DropTable(
                name: "ThemeTags");

            migrationBuilder.DropTable(
                name: "ThemeVisits");

            migrationBuilder.DropTable(
                name: "TicketMessages");

            migrationBuilder.DropTable(
                name: "UserBankRejectMessages");

            migrationBuilder.DropTable(
                name: "UserLogs");

            migrationBuilder.DropTable(
                name: "UserRoles");

            migrationBuilder.DropTable(
                name: "VisitSites");

            migrationBuilder.DropTable(
                name: "WalletUsers");

            migrationBuilder.DropTable(
                name: "Ads");

            migrationBuilder.DropTable(
                name: "ArticleCategories");

            migrationBuilder.DropTable(
                name: "Articles");

            migrationBuilder.DropTable(
                name: "Complaints");

            migrationBuilder.DropTable(
                name: "DiscontCodes");

            migrationBuilder.DropTable(
                name: "Pages");

            migrationBuilder.DropTable(
                name: "Permissions");

            migrationBuilder.DropTable(
                name: "SocialNetworks");

            migrationBuilder.DropTable(
                name: "ThemeCategories");

            migrationBuilder.DropTable(
                name: "Ticketings");

            migrationBuilder.DropTable(
                name: "UserBanks");

            migrationBuilder.DropTable(
                name: "Roles");

            migrationBuilder.DropTable(
                name: "Orders");

            migrationBuilder.DropTable(
                name: "WalletTransactionTypes");

            migrationBuilder.DropTable(
                name: "WalletTypes");

            migrationBuilder.DropTable(
                name: "Themes");

            migrationBuilder.DropTable(
                name: "Users");
        }
    }
}