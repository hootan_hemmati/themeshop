﻿using Microsoft.EntityFrameworkCore.Migrations;

using System;

namespace ThemeShop.Infra.Data.Migrations
{
    public partial class ArticleCategories_MainCategories : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_dbo.AdThemeCategories_dbo.Ads_AdId",
                table: "AdThemeCategories");

            migrationBuilder.DropForeignKey(
                name: "FK_dbo.AdThemeCategories_dbo.ThemeCategories_CategoryId",
                table: "AdThemeCategories");

            migrationBuilder.DropForeignKey(
                name: "FK_dbo.ArticleCategories_dbo.ArticleCategories_ParentID",
                table: "ArticleCategories");

            migrationBuilder.DropForeignKey(
                name: "FK_dbo.ArticleCategories_dbo.ArticleCategories_CategoryID",
                table: "ArticleCategory");

            migrationBuilder.DropForeignKey(
                name: "FK_dbo.ArticleCategories_dbo.Articles_ArticleID",
                table: "ArticleCategory");

            migrationBuilder.DropForeignKey(
                name: "FK_dbo.ArticleComments_dbo.ArticleComments_ParentID",
                table: "ArticleComments");

            migrationBuilder.DropForeignKey(
                name: "FK_dbo.ArticleComments_dbo.Articles_ArticleID",
                table: "ArticleComments");

            migrationBuilder.DropForeignKey(
                name: "FK_dbo.ArticleComments_dbo.Users_UserID",
                table: "ArticleComments");

            migrationBuilder.DropForeignKey(
                name: "FK_dbo.ArticleRelateds_dbo.Articles_ArticleID",
                table: "ArticleRelateds");

            migrationBuilder.DropForeignKey(
                name: "FK_dbo.ArticleRelateds_dbo.Articles_ArticleRelatedID",
                table: "ArticleRelateds");

            migrationBuilder.DropForeignKey(
                name: "FK_dbo.Articles_dbo.Users_AuthorID",
                table: "Articles");

            migrationBuilder.DropForeignKey(
                name: "FK_dbo.ArticleTags_dbo.Articles_ArticleID",
                table: "ArticleTags");

            migrationBuilder.DropForeignKey(
                name: "FK_dbo.ArticleVisits_dbo.Articles_ArticleID",
                table: "ArticleVisits");

            migrationBuilder.DropForeignKey(
                name: "FK_dbo.ComplaintThemes_dbo.Complaints_ComplaintId",
                table: "ComplaintThemes");

            migrationBuilder.DropForeignKey(
                name: "FK_dbo.ComplaintThemes_dbo.Themes_ThemeId",
                table: "ComplaintThemes");

            migrationBuilder.DropForeignKey(
                name: "FK_dbo.ContactUs_dbo.Users_UserId",
                table: "ContactUs");

            migrationBuilder.DropForeignKey(
                name: "FK_dbo.DownloadTokens_dbo.Themes_ThemeId",
                table: "DownloadTokens");

            migrationBuilder.DropForeignKey(
                name: "FK_dbo.DownloadTokens_dbo.Users_UserId",
                table: "DownloadTokens");

            migrationBuilder.DropForeignKey(
                name: "FK_dbo.OrderDetails_dbo.Orders_OrderId",
                table: "OrderDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_dbo.OrderDetails_dbo.Themes_ThemeId",
                table: "OrderDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_dbo.Orders_dbo.Users_UserID",
                table: "Orders");

            migrationBuilder.DropForeignKey(
                name: "FK_dbo.Permissions_dbo.Permissions_ParentID",
                table: "Permissions");

            migrationBuilder.DropForeignKey(
                name: "FK_dbo.ThemeAttachments_dbo.Themes_ThemeID",
                table: "ThemeAttachments");

            migrationBuilder.DropForeignKey(
                name: "FK_dbo.ThemeCategories_dbo.ThemeCategories_ParentID",
                table: "ThemeCategories");

            migrationBuilder.DropForeignKey(
                name: "FK_dbo.ThemeChangeStatusMessages_dbo.Themes_ThemeId",
                table: "ThemeChangeStatusMessages");

            migrationBuilder.DropForeignKey(
                name: "FK_dbo.ThemeComments_dbo.ThemeComments_ParentID",
                table: "ThemeComments");

            migrationBuilder.DropForeignKey(
                name: "FK_dbo.ThemeComments_dbo.Themes_ThemeID",
                table: "ThemeComments");

            migrationBuilder.DropForeignKey(
                name: "FK_dbo.ThemeComments_dbo.Users_UserID",
                table: "ThemeComments");

            migrationBuilder.DropForeignKey(
                name: "FK_dbo.ThemeFavorits_dbo.Themes_ThemeID",
                table: "ThemeFavorits");

            migrationBuilder.DropForeignKey(
                name: "FK_dbo.ThemeFavorits_dbo.Users_UserID",
                table: "ThemeFavorits");

            migrationBuilder.DropForeignKey(
                name: "FK_dbo.ThemeGalleries_dbo.Themes_ThemeID",
                table: "ThemeGalleries");

            migrationBuilder.DropForeignKey(
                name: "FK_dbo.ThemeLikes_dbo.Themes_ThemeID",
                table: "ThemeLikes");

            migrationBuilder.DropForeignKey(
                name: "FK_dbo.ThemeLikes_dbo.Users_UserID",
                table: "ThemeLikes");

            migrationBuilder.DropForeignKey(
                name: "FK_dbo.ThemeOfferTodays_dbo.Themes_ThemeID",
                table: "ThemeOfferTodays");

            migrationBuilder.DropForeignKey(
                name: "FK_dbo.ThemePreviews_dbo.Themes_ThemeID",
                table: "ThemePreviews");

            migrationBuilder.DropForeignKey(
                name: "FK_dbo.ThemeRelateds_dbo.Themes_ThemeID",
                table: "ThemeRelateds");

            migrationBuilder.DropForeignKey(
                name: "FK_dbo.ThemeRelateds_dbo.Themes_themeRelatedID",
                table: "ThemeRelateds");

            migrationBuilder.DropForeignKey(
                name: "FK_dbo.Themes_dbo.Themes_Theme_ThemeID",
                table: "Themes");

            migrationBuilder.DropForeignKey(
                name: "FK_dbo.Themes_dbo.Users_SellerID",
                table: "Themes");

            migrationBuilder.DropForeignKey(
                name: "FK_dbo.ThemeSelectedCategories_dbo.ThemeCategories_CategoryID",
                table: "ThemeSelectedCategories");

            migrationBuilder.DropForeignKey(
                name: "FK_dbo.ThemeSelectedCategories_dbo.Themes_ThemeID",
                table: "ThemeSelectedCategories");

            migrationBuilder.DropForeignKey(
                name: "FK_dbo.ThemeTags_dbo.Themes_ThemeID",
                table: "ThemeTags");

            migrationBuilder.DropForeignKey(
                name: "FK_dbo.ThemeVisits_dbo.Themes_ThemeID",
                table: "ThemeVisits");

            migrationBuilder.DropForeignKey(
                name: "FK_dbo.Ticketings_dbo.Themes_ThemeId",
                table: "Ticketings");

            migrationBuilder.DropForeignKey(
                name: "FK_dbo.Ticketings_dbo.Users_User_UserID",
                table: "Ticketings");

            migrationBuilder.DropForeignKey(
                name: "FK_dbo.TicketMessages_dbo.Ticketings_TicketId",
                table: "TicketMessages");

            migrationBuilder.DropForeignKey(
                name: "FK_dbo.TicketMessages_dbo.Users_Owner_UserID",
                table: "TicketMessages");

            migrationBuilder.DropForeignKey(
                name: "FK_dbo.UserBanks_dbo.Users_UserId",
                table: "UserBanks");

            migrationBuilder.DropForeignKey(
                name: "FK_dbo.UserLogs_dbo.Users_UserID",
                table: "UserLogs");

            migrationBuilder.DropTable(
                name: "CacheSettings");

            migrationBuilder.DropTable(
                name: "DiscountCodeUses");

            migrationBuilder.DropTable(
                name: "EmailSettings");

            migrationBuilder.DropTable(
                name: "LinkedIns");

            migrationBuilder.DropTable(
                name: "NewsLatterSites");

            migrationBuilder.DropTable(
                name: "PageComments");

            migrationBuilder.DropTable(
                name: "PageTags");

            migrationBuilder.DropTable(
                name: "PageVisits");

            migrationBuilder.DropTable(
                name: "RolePersmissions");

            migrationBuilder.DropTable(
                name: "SiteAbouUs");

            migrationBuilder.DropTable(
                name: "SiteSettings");

            migrationBuilder.DropTable(
                name: "SiteSocialNetworks");

            migrationBuilder.DropTable(
                name: "SiteStaticTexts");

            migrationBuilder.DropTable(
                name: "UserBankRejectMessages");

            migrationBuilder.DropTable(
                name: "VisitSites");

            migrationBuilder.DropTable(
                name: "WalletUsers");

            migrationBuilder.DropTable(
                name: "DiscontCodes");

            migrationBuilder.DropTable(
                name: "Pages");

            migrationBuilder.DropTable(
                name: "SocialNetworks");

            migrationBuilder.DropTable(
                name: "WalletTransactionTypes");

            migrationBuilder.DropTable(
                name: "WalletTypes");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Users",
                table: "Users");

            migrationBuilder.DropPrimaryKey(
                name: "PK_UserRoles",
                table: "UserRoles");

            migrationBuilder.DropPrimaryKey(
                name: "PK_UserBanks",
                table: "UserBanks");

            migrationBuilder.DropIndex(
                name: "IX_Owner_UserID",
                table: "TicketMessages");

            migrationBuilder.DropPrimaryKey(
                name: "PK_dbo.Ticketings",
                table: "Ticketings");

            migrationBuilder.DropIndex(
                name: "IX_User_UserID",
                table: "Ticketings");

            migrationBuilder.DropPrimaryKey(
                name: "PK_dbo.ThemeVisits",
                table: "ThemeVisits");

            migrationBuilder.DropPrimaryKey(
                name: "PK_dbo.ThemeSelectedCategories",
                table: "ThemeSelectedCategories");

            migrationBuilder.DropIndex(
                name: "IX_Theme_ThemeID",
                table: "Themes");

            migrationBuilder.DropPrimaryKey(
                name: "PK_dbo.ThemeRelateds",
                table: "ThemeRelateds");

            migrationBuilder.DropIndex(
                name: "IX_themeRelatedID",
                table: "ThemeRelateds");

            migrationBuilder.DropPrimaryKey(
                name: "PK_dbo.ThemePreviews",
                table: "ThemePreviews");

            migrationBuilder.DropPrimaryKey(
                name: "PK_dbo.ThemeOfferTodays",
                table: "ThemeOfferTodays");

            migrationBuilder.DropPrimaryKey(
                name: "PK_dbo.ThemeLikes",
                table: "ThemeLikes");

            migrationBuilder.DropIndex(
                name: "IX_UserID",
                table: "ThemeLikes");

            migrationBuilder.DropPrimaryKey(
                name: "PK_dbo.ThemeGalleries",
                table: "ThemeGalleries");

            migrationBuilder.DropPrimaryKey(
                name: "PK_dbo.ThemeFavorits",
                table: "ThemeFavorits");

            migrationBuilder.DropIndex(
                name: "IX_UserID",
                table: "ThemeFavorits");

            migrationBuilder.DropPrimaryKey(
                name: "PK_dbo.ThemeComments",
                table: "ThemeComments");

            migrationBuilder.DropIndex(
                name: "IX_ParentID",
                table: "ThemeComments");

            migrationBuilder.DropIndex(
                name: "IX_UserID",
                table: "ThemeComments");

            migrationBuilder.DropPrimaryKey(
                name: "PK_dbo.ThemeCategories",
                table: "ThemeCategories");

            migrationBuilder.DropIndex(
                name: "IX_ParentID",
                table: "ThemeCategories");

            migrationBuilder.DropPrimaryKey(
                name: "PK_dbo.ThemeAttachments",
                table: "ThemeAttachments");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Roles",
                table: "Roles");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Permissions",
                table: "Permissions");

            migrationBuilder.DropIndex(
                name: "IX_ParentID",
                table: "Permissions");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Orders",
                table: "Orders");

            migrationBuilder.DropIndex(
                name: "IX_UserId",
                table: "DownloadTokens");

            migrationBuilder.DropIndex(
                name: "IX_UserId",
                table: "ContactUs");

            migrationBuilder.DropPrimaryKey(
                name: "PK_dbo.ComplaintThemes",
                table: "ComplaintThemes");

            migrationBuilder.DropPrimaryKey(
                name: "PK_dbo.ArticleVisits",
                table: "ArticleVisits");

            migrationBuilder.DropIndex(
                name: "IX_AuthorID",
                table: "Articles");

            migrationBuilder.DropPrimaryKey(
                name: "PK_dbo.ArticleRelateds",
                table: "ArticleRelateds");

            migrationBuilder.DropPrimaryKey(
                name: "PK_dbo.ArticleComments",
                table: "ArticleComments");

            migrationBuilder.DropIndex(
                name: "IX_ParentID",
                table: "ArticleComments");

            migrationBuilder.DropIndex(
                name: "IX_UserID",
                table: "ArticleComments");

            migrationBuilder.DropPrimaryKey(
                name: "PK_dbo.ArticleCategories",
                table: "ArticleCategories");

            migrationBuilder.DropIndex(
                name: "IX_ParentID",
                table: "ArticleCategories");

            migrationBuilder.DropColumn(
                name: "RegisterIP",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "Description",
                table: "UserLogs");

            migrationBuilder.DropColumn(
                name: "Theme_ThemeID",
                table: "Themes");

            migrationBuilder.DropColumn(
                name: "CommantID",
                table: "ThemeComments");

            migrationBuilder.DropColumn(
                name: "ParentID",
                table: "ThemeComments");

            migrationBuilder.DropColumn(
                name: "ParentID",
                table: "ThemeCategories");

            migrationBuilder.DropColumn(
                name: "IsDefaultRole",
                table: "Roles");

            migrationBuilder.DropColumn(
                name: "ParentID",
                table: "Permissions");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "ContactUs");

            migrationBuilder.DropColumn(
                name: "CommantID",
                table: "ArticleComments");

            migrationBuilder.DropColumn(
                name: "ParentID",
                table: "ArticleComments");

            migrationBuilder.DropColumn(
                name: "ParentID",
                table: "ArticleCategories");

            migrationBuilder.EnsureSchema(
                name: "themeshop");

            migrationBuilder.RenameTable(
                name: "Users",
                newName: "Users",
                newSchema: "themeshop");

            migrationBuilder.RenameTable(
                name: "UserRoles",
                newName: "UserRoles",
                newSchema: "themeshop");

            migrationBuilder.RenameTable(
                name: "UserLogs",
                newName: "UserLogs",
                newSchema: "themeshop");

            migrationBuilder.RenameTable(
                name: "UserBanks",
                newName: "UserBanks",
                newSchema: "themeshop");

            migrationBuilder.RenameTable(
                name: "TicketMessages",
                newName: "TicketMessages",
                newSchema: "themeshop");

            migrationBuilder.RenameTable(
                name: "Ticketings",
                newName: "Ticketings",
                newSchema: "themeshop");

            migrationBuilder.RenameTable(
                name: "ThemeVisits",
                newName: "ThemeVisits",
                newSchema: "themeshop");

            migrationBuilder.RenameTable(
                name: "ThemeTags",
                newName: "ThemeTags",
                newSchema: "themeshop");

            migrationBuilder.RenameTable(
                name: "ThemeSelectedCategories",
                newName: "ThemeSelectedCategories",
                newSchema: "themeshop");

            migrationBuilder.RenameTable(
                name: "Themes",
                newName: "Themes",
                newSchema: "themeshop");

            migrationBuilder.RenameTable(
                name: "ThemeRelateds",
                newName: "ThemeRelateds",
                newSchema: "themeshop");

            migrationBuilder.RenameTable(
                name: "ThemePreviews",
                newName: "ThemePreviews",
                newSchema: "themeshop");

            migrationBuilder.RenameTable(
                name: "ThemeOfferTodays",
                newName: "ThemeOfferTodays",
                newSchema: "themeshop");

            migrationBuilder.RenameTable(
                name: "ThemeLikes",
                newName: "ThemeLikes",
                newSchema: "themeshop");

            migrationBuilder.RenameTable(
                name: "ThemeGalleries",
                newName: "ThemeGalleries",
                newSchema: "themeshop");

            migrationBuilder.RenameTable(
                name: "ThemeFavorits",
                newName: "ThemeFavorits",
                newSchema: "themeshop");

            migrationBuilder.RenameTable(
                name: "ThemeComments",
                newName: "ThemeComments",
                newSchema: "themeshop");

            migrationBuilder.RenameTable(
                name: "ThemeChangeStatusMessages",
                newName: "ThemeChangeStatusMessages",
                newSchema: "themeshop");

            migrationBuilder.RenameTable(
                name: "ThemeCategories",
                newName: "ThemeCategories",
                newSchema: "themeshop");

            migrationBuilder.RenameTable(
                name: "ThemeAttachments",
                newName: "ThemeAttachments",
                newSchema: "themeshop");

            migrationBuilder.RenameTable(
                name: "TemplateInfoes",
                newName: "TemplateInfoes",
                newSchema: "themeshop");

            migrationBuilder.RenameTable(
                name: "Roles",
                newName: "Roles",
                newSchema: "themeshop");

            migrationBuilder.RenameTable(
                name: "Permissions",
                newName: "Permissions",
                newSchema: "themeshop");

            migrationBuilder.RenameTable(
                name: "Orders",
                newName: "Orders",
                newSchema: "themeshop");

            migrationBuilder.RenameTable(
                name: "OrderDetails",
                newName: "OrderDetails",
                newSchema: "themeshop");

            migrationBuilder.RenameTable(
                name: "DownloadTokens",
                newName: "DownloadTokens",
                newSchema: "themeshop");

            migrationBuilder.RenameTable(
                name: "ContactUs",
                newName: "ContactUs",
                newSchema: "themeshop");

            migrationBuilder.RenameTable(
                name: "ComplaintThemes",
                newName: "ComplaintThemes",
                newSchema: "themeshop");

            migrationBuilder.RenameTable(
                name: "Complaints",
                newName: "Complaints",
                newSchema: "themeshop");

            migrationBuilder.RenameTable(
                name: "ArticleVisits",
                newName: "ArticleVisits",
                newSchema: "themeshop");

            migrationBuilder.RenameTable(
                name: "ArticleTags",
                newName: "ArticleTags",
                newSchema: "themeshop");

            migrationBuilder.RenameTable(
                name: "Articles",
                newName: "Articles",
                newSchema: "themeshop");

            migrationBuilder.RenameTable(
                name: "ArticleRelateds",
                newName: "ArticleRelateds",
                newSchema: "themeshop");

            migrationBuilder.RenameTable(
                name: "ArticleComments",
                newName: "ArticleComments",
                newSchema: "themeshop");

            migrationBuilder.RenameTable(
                name: "ArticleCategory",
                newName: "ArticleCategory",
                newSchema: "themeshop");

            migrationBuilder.RenameTable(
                name: "ArticleCategories",
                newName: "ArticleCategories",
                newSchema: "themeshop");

            migrationBuilder.RenameTable(
                name: "AdThemeCategories",
                newName: "AdThemeCategories",
                newSchema: "themeshop");

            migrationBuilder.RenameTable(
                name: "Ads",
                newName: "Ads",
                newSchema: "themeshop");

            migrationBuilder.RenameColumn(
                name: "UserID",
                schema: "themeshop",
                table: "Users",
                newName: "UserId");

            migrationBuilder.RenameColumn(
                name: "UserID",
                schema: "themeshop",
                table: "UserRoles",
                newName: "UserId");

            migrationBuilder.RenameColumn(
                name: "RoleID",
                schema: "themeshop",
                table: "UserRoles",
                newName: "RoleId");

            migrationBuilder.RenameColumn(
                name: "UserRoleID",
                schema: "themeshop",
                table: "UserRoles",
                newName: "UserRoleId");

            migrationBuilder.RenameIndex(
                name: "IX_UserID",
                schema: "themeshop",
                table: "UserRoles",
                newName: "IX_UserRoles_UserId");

            migrationBuilder.RenameIndex(
                name: "IX_RoleID",
                schema: "themeshop",
                table: "UserRoles",
                newName: "IX_UserRoles_RoleId");

            migrationBuilder.RenameColumn(
                name: "UserID",
                schema: "themeshop",
                table: "UserLogs",
                newName: "UserId");

            migrationBuilder.RenameColumn(
                name: "LogID",
                schema: "themeshop",
                table: "UserLogs",
                newName: "LogId");

            migrationBuilder.RenameColumn(
                name: "DateTime",
                schema: "themeshop",
                table: "UserLogs",
                newName: "LogTime");

            migrationBuilder.RenameIndex(
                name: "IX_UserID",
                schema: "themeshop",
                table: "UserLogs",
                newName: "IX_UserLogs_UserId");

            migrationBuilder.RenameColumn(
                name: "SellerAccountHolder",
                schema: "themeshop",
                table: "UserBanks",
                newName: "NationalCardImage");

            migrationBuilder.RenameColumn(
                name: "ImageKartMeli",
                schema: "themeshop",
                table: "UserBanks",
                newName: "BankCardImage");

            migrationBuilder.RenameColumn(
                name: "ImageCardBank",
                schema: "themeshop",
                table: "UserBanks",
                newName: "BankAccountHolder");

            migrationBuilder.RenameColumn(
                name: "CodeMeli",
                schema: "themeshop",
                table: "UserBanks",
                newName: "NationalCode");

            migrationBuilder.RenameIndex(
                name: "IX_UserId",
                schema: "themeshop",
                table: "UserBanks",
                newName: "IX_UserBanks_UserId");

            migrationBuilder.RenameColumn(
                name: "Owner_UserID",
                schema: "themeshop",
                table: "TicketMessages",
                newName: "UserId");

            migrationBuilder.RenameIndex(
                name: "IX_TicketId",
                schema: "themeshop",
                table: "TicketMessages",
                newName: "IX_TicketMessages_TicketId");

            migrationBuilder.RenameColumn(
                name: "User_UserID",
                schema: "themeshop",
                table: "Ticketings",
                newName: "UserId");

            migrationBuilder.RenameIndex(
                name: "IX_ThemeId",
                schema: "themeshop",
                table: "Ticketings",
                newName: "IX_Ticketings_ThemeId");

            migrationBuilder.RenameColumn(
                name: "ThemeID",
                schema: "themeshop",
                table: "ThemeVisits",
                newName: "ThemeId");

            migrationBuilder.RenameColumn(
                name: "VisitID",
                schema: "themeshop",
                table: "ThemeVisits",
                newName: "VisitId");

            migrationBuilder.RenameIndex(
                name: "IX_ThemeID",
                schema: "themeshop",
                table: "ThemeVisits",
                newName: "IX_ThemeVisits_ThemeId");

            migrationBuilder.RenameColumn(
                name: "ThemeID",
                schema: "themeshop",
                table: "ThemeTags",
                newName: "ThemeId");

            migrationBuilder.RenameColumn(
                name: "ThemeTagID",
                schema: "themeshop",
                table: "ThemeTags",
                newName: "ThemeTagId");

            migrationBuilder.RenameIndex(
                name: "IX_ThemeID",
                schema: "themeshop",
                table: "ThemeTags",
                newName: "IX_ThemeTags_ThemeId");

            migrationBuilder.RenameColumn(
                name: "ThemeID",
                schema: "themeshop",
                table: "ThemeSelectedCategories",
                newName: "ThemeId");

            migrationBuilder.RenameColumn(
                name: "CategoryID",
                schema: "themeshop",
                table: "ThemeSelectedCategories",
                newName: "ThemeCategoryId");

            migrationBuilder.RenameColumn(
                name: "SelectedID",
                schema: "themeshop",
                table: "ThemeSelectedCategories",
                newName: "ThemeSelectedId");

            migrationBuilder.RenameIndex(
                name: "IX_ThemeID",
                schema: "themeshop",
                table: "ThemeSelectedCategories",
                newName: "IX_ThemeSelectedCategories_ThemeId");

            migrationBuilder.RenameIndex(
                name: "IX_CategoryID",
                schema: "themeshop",
                table: "ThemeSelectedCategories",
                newName: "IX_ThemeSelectedCategories_ThemeCategoryId");

            migrationBuilder.RenameColumn(
                name: "SellerID",
                schema: "themeshop",
                table: "Themes",
                newName: "SellerId");

            migrationBuilder.RenameColumn(
                name: "ThemeID",
                schema: "themeshop",
                table: "Themes",
                newName: "ThemeId");

            migrationBuilder.RenameIndex(
                name: "IX_SellerID",
                schema: "themeshop",
                table: "Themes",
                newName: "IX_Themes_SellerId");

            migrationBuilder.RenameColumn(
                name: "themeRelatedID",
                schema: "themeshop",
                table: "ThemeRelateds",
                newName: "ThemeRelatedId");

            migrationBuilder.RenameColumn(
                name: "ThemeID",
                schema: "themeshop",
                table: "ThemeRelateds",
                newName: "ThemeId");

            migrationBuilder.RenameColumn(
                name: "RelatedID",
                schema: "themeshop",
                table: "ThemeRelateds",
                newName: "RelatedId");

            migrationBuilder.RenameIndex(
                name: "IX_ThemeID",
                schema: "themeshop",
                table: "ThemeRelateds",
                newName: "IX_ThemeRelateds_ThemeId");

            migrationBuilder.RenameColumn(
                name: "ThemeID",
                schema: "themeshop",
                table: "ThemePreviews",
                newName: "ThemeId");

            migrationBuilder.RenameColumn(
                name: "PreviewID",
                schema: "themeshop",
                table: "ThemePreviews",
                newName: "PreviewId");

            migrationBuilder.RenameIndex(
                name: "IX_ThemeID",
                schema: "themeshop",
                table: "ThemePreviews",
                newName: "IX_ThemePreviews_ThemeId");

            migrationBuilder.RenameColumn(
                name: "ThemeID",
                schema: "themeshop",
                table: "ThemeOfferTodays",
                newName: "ThemeId");

            migrationBuilder.RenameColumn(
                name: "OfferID",
                schema: "themeshop",
                table: "ThemeOfferTodays",
                newName: "OfferId");

            migrationBuilder.RenameIndex(
                name: "IX_ThemeID",
                schema: "themeshop",
                table: "ThemeOfferTodays",
                newName: "IX_ThemeOfferTodays_ThemeId");

            migrationBuilder.RenameColumn(
                name: "UserID",
                schema: "themeshop",
                table: "ThemeLikes",
                newName: "UserId");

            migrationBuilder.RenameColumn(
                name: "ThemeID",
                schema: "themeshop",
                table: "ThemeLikes",
                newName: "ThemeId");

            migrationBuilder.RenameColumn(
                name: "LikeID",
                schema: "themeshop",
                table: "ThemeLikes",
                newName: "LikeId");

            migrationBuilder.RenameIndex(
                name: "IX_ThemeID",
                schema: "themeshop",
                table: "ThemeLikes",
                newName: "IX_ThemeLikes_ThemeId");

            migrationBuilder.RenameColumn(
                name: "ThemeID",
                schema: "themeshop",
                table: "ThemeGalleries",
                newName: "ThemeId");

            migrationBuilder.RenameColumn(
                name: "GalleryID",
                schema: "themeshop",
                table: "ThemeGalleries",
                newName: "GalleryId");

            migrationBuilder.RenameIndex(
                name: "IX_ThemeID",
                schema: "themeshop",
                table: "ThemeGalleries",
                newName: "IX_ThemeGalleries_ThemeId");

            migrationBuilder.RenameColumn(
                name: "UserID",
                schema: "themeshop",
                table: "ThemeFavorits",
                newName: "UserId");

            migrationBuilder.RenameColumn(
                name: "ThemeID",
                schema: "themeshop",
                table: "ThemeFavorits",
                newName: "ThemeId");

            migrationBuilder.RenameColumn(
                name: "FavoritID",
                schema: "themeshop",
                table: "ThemeFavorits",
                newName: "FavoritId");

            migrationBuilder.RenameIndex(
                name: "IX_ThemeID",
                schema: "themeshop",
                table: "ThemeFavorits",
                newName: "IX_ThemeFavorits_ThemeId");

            migrationBuilder.RenameColumn(
                name: "UserID",
                schema: "themeshop",
                table: "ThemeComments",
                newName: "UserId");

            migrationBuilder.RenameColumn(
                name: "ThemeID",
                schema: "themeshop",
                table: "ThemeComments",
                newName: "ThemeId");

            migrationBuilder.RenameIndex(
                name: "IX_ThemeID",
                schema: "themeshop",
                table: "ThemeComments",
                newName: "IX_ThemeComments_ThemeId");

            migrationBuilder.RenameColumn(
                name: "Id",
                schema: "themeshop",
                table: "ThemeChangeStatusMessages",
                newName: "StatusMessagesId");

            migrationBuilder.RenameIndex(
                name: "IX_ThemeId",
                schema: "themeshop",
                table: "ThemeChangeStatusMessages",
                newName: "IX_ThemeChangeStatusMessages_ThemeId");

            migrationBuilder.RenameColumn(
                name: "CategoryID",
                schema: "themeshop",
                table: "ThemeCategories",
                newName: "ThemeCategoryId");

            migrationBuilder.RenameColumn(
                name: "ThemeID",
                schema: "themeshop",
                table: "ThemeAttachments",
                newName: "ThemeId");

            migrationBuilder.RenameColumn(
                name: "AttachID",
                schema: "themeshop",
                table: "ThemeAttachments",
                newName: "AttachId");

            migrationBuilder.RenameIndex(
                name: "IX_ThemeID",
                schema: "themeshop",
                table: "ThemeAttachments",
                newName: "IX_ThemeAttachments_ThemeId");

            migrationBuilder.RenameColumn(
                name: "CreateBuy",
                schema: "themeshop",
                table: "TemplateInfoes",
                newName: "CreateDate");

            migrationBuilder.RenameIndex(
                name: "IX_UserId",
                schema: "themeshop",
                table: "TemplateInfoes",
                newName: "IX_TemplateInfoes_UserId");

            migrationBuilder.RenameIndex(
                name: "IX_ThemeId",
                schema: "themeshop",
                table: "TemplateInfoes",
                newName: "IX_TemplateInfoes_ThemeId");

            migrationBuilder.RenameIndex(
                name: "IX_SellerId",
                schema: "themeshop",
                table: "TemplateInfoes",
                newName: "IX_TemplateInfoes_SellerId");

            migrationBuilder.RenameColumn(
                name: "RoleID",
                schema: "themeshop",
                table: "Roles",
                newName: "RoleId");

            migrationBuilder.RenameColumn(
                name: "IsDelete",
                schema: "themeshop",
                table: "Roles",
                newName: "IsDefualtRole");

            migrationBuilder.RenameColumn(
                name: "PermissionID",
                schema: "themeshop",
                table: "Permissions",
                newName: "PermissionId");

            migrationBuilder.RenameColumn(
                name: "UserID",
                schema: "themeshop",
                table: "Orders",
                newName: "UserId");

            migrationBuilder.RenameColumn(
                name: "OrderID",
                schema: "themeshop",
                table: "Orders",
                newName: "OrderId");

            migrationBuilder.RenameColumn(
                name: "IsFinaly",
                schema: "themeshop",
                table: "Orders",
                newName: "IsPay");

            migrationBuilder.RenameColumn(
                name: "CreateDate",
                schema: "themeshop",
                table: "Orders",
                newName: "CreateTime");

            migrationBuilder.RenameIndex(
                name: "IX_UserID",
                schema: "themeshop",
                table: "Orders",
                newName: "IX_Orders_UserId");

            migrationBuilder.RenameColumn(
                name: "Id",
                schema: "themeshop",
                table: "OrderDetails",
                newName: "OrderDetailId");

            migrationBuilder.RenameIndex(
                name: "IX_ThemeId",
                schema: "themeshop",
                table: "OrderDetails",
                newName: "IX_OrderDetails_ThemeId");

            migrationBuilder.RenameIndex(
                name: "IX_OrderId",
                schema: "themeshop",
                table: "OrderDetails",
                newName: "IX_OrderDetails_OrderId");

            migrationBuilder.RenameColumn(
                name: "Id",
                schema: "themeshop",
                table: "DownloadTokens",
                newName: "DownloadTokenId");

            migrationBuilder.RenameIndex(
                name: "IX_ThemeId",
                schema: "themeshop",
                table: "DownloadTokens",
                newName: "IX_DownloadTokens_ThemeId");

            migrationBuilder.RenameColumn(
                name: "ContactID",
                schema: "themeshop",
                table: "ContactUs",
                newName: "ContactId");

            migrationBuilder.RenameColumn(
                name: "UserIP",
                schema: "themeshop",
                table: "ContactUs",
                newName: "IPAddress");

            migrationBuilder.RenameIndex(
                name: "IX_ThemeId",
                schema: "themeshop",
                table: "ComplaintThemes",
                newName: "IX_ComplaintThemes_ThemeId");

            migrationBuilder.RenameIndex(
                name: "IX_ComplaintId",
                schema: "themeshop",
                table: "ComplaintThemes",
                newName: "IX_ComplaintThemes_ComplaintId");

            migrationBuilder.RenameColumn(
                name: "ArticleID",
                schema: "themeshop",
                table: "ArticleVisits",
                newName: "ArticleId");

            migrationBuilder.RenameColumn(
                name: "VisitID",
                schema: "themeshop",
                table: "ArticleVisits",
                newName: "VisitId");

            migrationBuilder.RenameIndex(
                name: "IX_ArticleID",
                schema: "themeshop",
                table: "ArticleVisits",
                newName: "IX_ArticleVisits_ArticleId");

            migrationBuilder.RenameColumn(
                name: "ArticleID",
                schema: "themeshop",
                table: "ArticleTags",
                newName: "ArticleId");

            migrationBuilder.RenameColumn(
                name: "ArticleTagID",
                schema: "themeshop",
                table: "ArticleTags",
                newName: "ArticleTagId");

            migrationBuilder.RenameIndex(
                name: "IX_ArticleID",
                schema: "themeshop",
                table: "ArticleTags",
                newName: "IX_ArticleTags_ArticleId");

            migrationBuilder.RenameColumn(
                name: "AuthorID",
                schema: "themeshop",
                table: "Articles",
                newName: "AuthorId");

            migrationBuilder.RenameColumn(
                name: "ArticleID",
                schema: "themeshop",
                table: "Articles",
                newName: "ArticleId");

            migrationBuilder.RenameColumn(
                name: "ArticleID",
                schema: "themeshop",
                table: "ArticleRelateds",
                newName: "ArticleId");

            migrationBuilder.RenameColumn(
                name: "RelatedID",
                schema: "themeshop",
                table: "ArticleRelateds",
                newName: "RelatedId");

            migrationBuilder.RenameColumn(
                name: "ArticleRelatedID",
                schema: "themeshop",
                table: "ArticleRelateds",
                newName: "MainCategoryId");

            migrationBuilder.RenameIndex(
                name: "IX_ArticleRelatedID",
                schema: "themeshop",
                table: "ArticleRelateds",
                newName: "IX_ArticleRelateds_MainCategoryId");

            migrationBuilder.RenameIndex(
                name: "IX_ArticleID",
                schema: "themeshop",
                table: "ArticleRelateds",
                newName: "IX_ArticleRelateds_ArticleId");

            migrationBuilder.RenameColumn(
                name: "UserID",
                schema: "themeshop",
                table: "ArticleComments",
                newName: "UserId");

            migrationBuilder.RenameColumn(
                name: "ArticleID",
                schema: "themeshop",
                table: "ArticleComments",
                newName: "ArticleId");

            migrationBuilder.RenameIndex(
                name: "IX_ArticleID",
                schema: "themeshop",
                table: "ArticleComments",
                newName: "IX_ArticleComments_ArticleId");

            migrationBuilder.RenameColumn(
                name: "CategoryID",
                schema: "themeshop",
                table: "ArticleCategory",
                newName: "CategoryId");

            migrationBuilder.RenameColumn(
                name: "ArticleID",
                schema: "themeshop",
                table: "ArticleCategory",
                newName: "ArticleId");

            migrationBuilder.RenameColumn(
                name: "ArticleCategoryID",
                schema: "themeshop",
                table: "ArticleCategory",
                newName: "ArticleCategoryId");

            migrationBuilder.RenameIndex(
                name: "IX_CategoryID",
                schema: "themeshop",
                table: "ArticleCategory",
                newName: "IX_ArticleCategory_CategoryId");

            migrationBuilder.RenameIndex(
                name: "IX_ArticleID",
                schema: "themeshop",
                table: "ArticleCategory",
                newName: "IX_ArticleCategory_ArticleId");

            migrationBuilder.RenameColumn(
                name: "CategoryID",
                schema: "themeshop",
                table: "ArticleCategories",
                newName: "CategoryId");

            migrationBuilder.RenameColumn(
                name: "CategoryId",
                schema: "themeshop",
                table: "AdThemeCategories",
                newName: "ThemeCategoryId");

            migrationBuilder.RenameIndex(
                name: "IX_CategoryId",
                schema: "themeshop",
                table: "AdThemeCategories",
                newName: "IX_AdThemeCategories_ThemeCategoryId");

            migrationBuilder.RenameIndex(
                name: "IX_AdId",
                schema: "themeshop",
                table: "AdThemeCategories",
                newName: "IX_AdThemeCategories_AdId");

            migrationBuilder.AlterDatabase(
                collation: "SQL_Latin1_General_CP1_CI_AS");

            migrationBuilder.AlterColumn<string>(
                name: "Password",
                schema: "themeshop",
                table: "Users",
                type: "nvarchar(10)",
                maxLength: 10,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(100)",
                oldMaxLength: 100);

            migrationBuilder.AlterColumn<string>(
                name: "Mobile",
                schema: "themeshop",
                table: "Users",
                type: "nvarchar(13)",
                maxLength: 13,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(50)",
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Email",
                schema: "themeshop",
                table: "Users",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(250)",
                oldMaxLength: 250);

            migrationBuilder.AddColumn<long>(
                name: "AccountBalance",
                schema: "themeshop",
                table: "Users",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<bool>(
                name: "IsDelete",
                schema: "themeshop",
                table: "UserRoles",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "LogCategoryId",
                schema: "themeshop",
                table: "UserLogs",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "LogDescribtion",
                schema: "themeshop",
                table: "UserLogs",
                type: "nvarchar(250)",
                maxLength: 250,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<long>(
                name: "LogPrice",
                schema: "themeshop",
                table: "UserLogs",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AlterColumn<string>(
                name: "Text",
                schema: "themeshop",
                table: "TicketMessages",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Title",
                schema: "themeshop",
                table: "Ticketings",
                type: "nvarchar(300)",
                maxLength: 300,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(300)",
                oldMaxLength: 300,
                oldDefaultValueSql: "('')");

            migrationBuilder.AlterColumn<int>(
                name: "ThemeId",
                schema: "themeshop",
                table: "Ticketings",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsAnswered",
                schema: "themeshop",
                table: "Ticketings",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AlterColumn<string>(
                name: "IP",
                schema: "themeshop",
                table: "ThemeVisits",
                type: "nvarchar(100)",
                maxLength: 100,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(100)",
                oldMaxLength: 100);

            migrationBuilder.AlterColumn<string>(
                name: "IP",
                schema: "themeshop",
                table: "ThemeLikes",
                type: "nvarchar(100)",
                maxLength: 100,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "ImageTitle",
                schema: "themeshop",
                table: "ThemeGalleries",
                type: "nvarchar(200)",
                maxLength: 200,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(200)",
                oldMaxLength: 200,
                oldDefaultValueSql: "('')");

            migrationBuilder.AddColumn<long>(
                name: "CommentId",
                schema: "themeshop",
                table: "ThemeComments",
                type: "bigint",
                nullable: false,
                defaultValue: 0L)
                .Annotation("SqlServer:Identity", "1, 1");

            migrationBuilder.AlterColumn<int>(
                name: "RoleId",
                schema: "themeshop",
                table: "Roles",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int")
                .Annotation("SqlServer:Identity", "1, 1");

            migrationBuilder.AlterColumn<int>(
                name: "DisplayPriority",
                schema: "themeshop",
                table: "Permissions",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CompleteTime",
                schema: "themeshop",
                table: "Orders",
                type: "datetime",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<bool>(
                name: "IsActive",
                schema: "themeshop",
                table: "Orders",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<long>(
                name: "TotalPrice",
                schema: "themeshop",
                table: "Orders",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AlterColumn<string>(
                name: "Subject",
                schema: "themeshop",
                table: "ContactUs",
                type: "nvarchar(500)",
                maxLength: 500,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(500)",
                oldMaxLength: 500,
                oldDefaultValueSql: "('')");

            migrationBuilder.AddColumn<bool>(
                name: "IsAnswered",
                schema: "themeshop",
                table: "ContactUs",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "SellerId",
                schema: "themeshop",
                table: "ComplaintThemes",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CategoryId",
                schema: "themeshop",
                table: "ArticleRelateds",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<long>(
                name: "CommentId",
                schema: "themeshop",
                table: "ArticleComments",
                type: "bigint",
                nullable: false,
                defaultValue: 0L)
                .Annotation("SqlServer:Identity", "1, 1");

            migrationBuilder.AddColumn<int>(
                name: "MainCategoryId",
                schema: "themeshop",
                table: "ArticleCategory",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "MainArticleId",
                schema: "themeshop",
                table: "ArticleCategories",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddPrimaryKey(
                name: "PK_dbo.Users",
                schema: "themeshop",
                table: "Users",
                column: "UserId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_dbo.UserRoles",
                schema: "themeshop",
                table: "UserRoles",
                column: "UserRoleId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_dbo.UserBanks",
                schema: "themeshop",
                table: "UserBanks",
                column: "UserBankId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Ticketings",
                schema: "themeshop",
                table: "Ticketings",
                column: "TicketId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ThemeVisits",
                schema: "themeshop",
                table: "ThemeVisits",
                column: "VisitId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ThemeSelectedCategories",
                schema: "themeshop",
                table: "ThemeSelectedCategories",
                column: "ThemeSelectedId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ThemeRelateds",
                schema: "themeshop",
                table: "ThemeRelateds",
                column: "RelatedId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ThemePreviews",
                schema: "themeshop",
                table: "ThemePreviews",
                column: "PreviewId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ThemeOfferTodays",
                schema: "themeshop",
                table: "ThemeOfferTodays",
                column: "OfferId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ThemeLikes",
                schema: "themeshop",
                table: "ThemeLikes",
                column: "LikeId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ThemeGalleries",
                schema: "themeshop",
                table: "ThemeGalleries",
                column: "GalleryId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ThemeFavorits",
                schema: "themeshop",
                table: "ThemeFavorits",
                column: "FavoritId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ThemeComments",
                schema: "themeshop",
                table: "ThemeComments",
                column: "CommentId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ThemeCategories",
                schema: "themeshop",
                table: "ThemeCategories",
                column: "ThemeCategoryId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ThemeAttachments",
                schema: "themeshop",
                table: "ThemeAttachments",
                column: "AttachId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_dbo.Roles",
                schema: "themeshop",
                table: "Roles",
                column: "RoleId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_dbo.Permissions",
                schema: "themeshop",
                table: "Permissions",
                column: "PermissionId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_dbo.Orders",
                schema: "themeshop",
                table: "Orders",
                column: "OrderId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ComplaintThemes",
                schema: "themeshop",
                table: "ComplaintThemes",
                column: "ComplaintQuestionId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ArticleVisits",
                schema: "themeshop",
                table: "ArticleVisits",
                column: "VisitId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ArticleRelateds",
                schema: "themeshop",
                table: "ArticleRelateds",
                column: "RelatedId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ArticleComments",
                schema: "themeshop",
                table: "ArticleComments",
                column: "CommentId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ArticleCategories",
                schema: "themeshop",
                table: "ArticleCategories",
                column: "CategoryId");

            migrationBuilder.CreateTable(
                name: "ArticleReplyComments",
                schema: "themeshop",
                columns: table => new
                {
                    ReplyCommentId = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CommentId = table.Column<long>(type: "bigint", nullable: false),
                    ArticleId = table.Column<int>(type: "int", nullable: false),
                    UserId = table.Column<int>(type: "int", nullable: false),
                    Comment = table.Column<string>(type: "nvarchar(800)", maxLength: 800, nullable: false),
                    CreateDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    IsDelete = table.Column<bool>(type: "bit", nullable: false),
                    IsRead = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ArticleReplyComments", x => x.ReplyCommentId);
                    table.ForeignKey(
                        name: "FK_ArticleReplyComments_ArticleComments",
                        column: x => x.CommentId,
                        principalSchema: "themeshop",
                        principalTable: "ArticleComments",
                        principalColumn: "CommentId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ArticleReplyComments_Articles",
                        column: x => x.ArticleId,
                        principalSchema: "themeshop",
                        principalTable: "Articles",
                        principalColumn: "ArticleId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Discounts",
                schema: "themeshop",
                columns: table => new
                {
                    DiscountId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Count = table.Column<int>(type: "int", nullable: false),
                    UsedCount = table.Column<int>(type: "int", nullable: false),
                    ExpireDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    CreateDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    Percentage = table.Column<int>(type: "int", nullable: false),
                    Code = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dbo.Discounts", x => x.DiscountId);
                });

            migrationBuilder.CreateTable(
                name: "LogCategories",
                schema: "themeshop",
                columns: table => new
                {
                    LogCategoryId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    LogCategoryTitle = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true),
                    LogCategoryDescribtion = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dbo.LogCategories", x => x.LogCategoryId);
                });

            migrationBuilder.CreateTable(
                name: "MainArticleCategories",
                schema: "themeshop",
                columns: table => new
                {
                    MainCatagoryId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(type: "nvarchar(260)", maxLength: 260, nullable: false),
                    NameInUrl = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    IsDelete = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MainArticleCategories", x => x.MainCatagoryId);
                });

            migrationBuilder.CreateTable(
                name: "OnlineUserReport",
                schema: "themeshop",
                columns: table => new
                {
                    OnlineUserReportId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    OnlineUserCount = table.Column<long>(type: "bigint", nullable: true),
                    RepostDate = table.Column<DateTime>(type: "datetime", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OnlineUserReport", x => x.OnlineUserReportId);
                });

            migrationBuilder.CreateTable(
                name: "OnlineUsers",
                schema: "themeshop",
                columns: table => new
                {
                    UserConnectionId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserName = table.Column<string>(type: "nchar(100)", fixedLength: true, maxLength: 100, nullable: true),
                    IP = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    StartTime = table.Column<DateTime>(type: "datetime", nullable: false),
                    VisitPath = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    OnlineStatus = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OnlineUsers", x => x.UserConnectionId);
                });

            migrationBuilder.CreateTable(
                name: "ReportAbuse",
                schema: "themeshop",
                columns: table => new
                {
                    ReportAbuseId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreateDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    IP = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    IsSeen = table.Column<bool>(type: "bit", nullable: false),
                    IsAnswered = table.Column<bool>(type: "bit", nullable: false),
                    ThemeId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ReportAbuse", x => x.ReportAbuseId);
                    table.ForeignKey(
                        name: "FK_ReportAbuse_Themes",
                        column: x => x.ThemeId,
                        principalSchema: "themeshop",
                        principalTable: "Themes",
                        principalColumn: "ThemeId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "RolePremissions",
                schema: "themeshop",
                columns: table => new
                {
                    RolePermissionsId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RoleId = table.Column<int>(type: "int", nullable: false),
                    PermissionId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dbo.RolePremissions", x => x.RolePermissionsId);
                    table.ForeignKey(
                        name: "FK_dbo.RolePremissions_dbo.Permissions_PermissionId",
                        column: x => x.PermissionId,
                        principalSchema: "themeshop",
                        principalTable: "Permissions",
                        principalColumn: "PermissionId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_dbo.RolePremissions_dbo.Roles_RoleId",
                        column: x => x.RoleId,
                        principalSchema: "themeshop",
                        principalTable: "Roles",
                        principalColumn: "RoleId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ThemeCategoriesChilds",
                schema: "themeshop",
                columns: table => new
                {
                    CategoryChildId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ThemeCategoryId = table.Column<int>(type: "int", nullable: false),
                    ThemeCategoryParentId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ThemeCategoriesChilds", x => x.CategoryChildId);
                    table.ForeignKey(
                        name: "FK_ThemeCategoriesChilds_ThemeCategories",
                        column: x => x.ThemeCategoryId,
                        principalSchema: "themeshop",
                        principalTable: "ThemeCategories",
                        principalColumn: "ThemeCategoryId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ThemeCategoriesChilds_ThemeCategories1",
                        column: x => x.ThemeCategoryParentId,
                        principalSchema: "themeshop",
                        principalTable: "ThemeCategories",
                        principalColumn: "ThemeCategoryId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ThemeReplyComments",
                schema: "themeshop",
                columns: table => new
                {
                    CommentReplyId = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CommentId = table.Column<long>(type: "bigint", nullable: false),
                    ThemeId = table.Column<int>(type: "int", nullable: false),
                    UserId = table.Column<int>(type: "int", nullable: false),
                    Comment = table.Column<string>(type: "nvarchar(800)", maxLength: 800, nullable: false),
                    CreateDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    IsSellerReply = table.Column<bool>(type: "bit", nullable: false),
                    IsDelete = table.Column<bool>(type: "bit", nullable: false),
                    IsRead = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ThemeReplyComments", x => x.CommentReplyId);
                    table.ForeignKey(
                        name: "FK_ThemeReplyComments_ThemeComments",
                        column: x => x.CommentId,
                        principalSchema: "themeshop",
                        principalTable: "ThemeComments",
                        principalColumn: "CommentId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ThemeReplyComments_Themes",
                        column: x => x.ThemeId,
                        principalSchema: "themeshop",
                        principalTable: "Themes",
                        principalColumn: "ThemeId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UserBankMessages",
                schema: "themeshop",
                columns: table => new
                {
                    UserbankMessagesId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserBankId = table.Column<int>(type: "int", nullable: false),
                    AnswerTime = table.Column<DateTime>(type: "datetime", nullable: false),
                    Message = table.Column<string>(type: "nvarchar(1000)", maxLength: 1000, nullable: false),
                    IsAccept = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserBankMessages", x => x.UserbankMessagesId);
                    table.ForeignKey(
                        name: "FK_dbo.UserBankMessages_dbo.UserBanks_UserBankId",
                        column: x => x.UserBankId,
                        principalSchema: "themeshop",
                        principalTable: "UserBanks",
                        principalColumn: "UserBankId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Wallets",
                schema: "themeshop",
                columns: table => new
                {
                    WalletId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Charge = table.Column<long>(type: "bigint", nullable: false),
                    IsAdmin = table.Column<bool>(type: "bit", nullable: false),
                    UserId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dbo.Wallets", x => x.WalletId);
                    table.ForeignKey(
                        name: "FK_Wallets_Users",
                        column: x => x.UserId,
                        principalSchema: "themeshop",
                        principalTable: "Users",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DiscountLogs",
                schema: "themeshop",
                columns: table => new
                {
                    DiscountLogId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    LogTime = table.Column<DateTime>(type: "datetime", nullable: false),
                    IsUsed = table.Column<bool>(type: "bit", nullable: false),
                    UserId = table.Column<int>(type: "int", nullable: false),
                    Text = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Discount_DiscountId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dbo.DiscountLogs", x => x.DiscountLogId);
                    table.ForeignKey(
                        name: "FK_dbo.DiscountLogs_dbo.Discounts_Discount_DiscountId",
                        column: x => x.Discount_DiscountId,
                        principalSchema: "themeshop",
                        principalTable: "Discounts",
                        principalColumn: "DiscountId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DiscountSelectedTheme",
                schema: "themeshop",
                columns: table => new
                {
                    DiscountSelectedThemeId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DiscountId = table.Column<int>(type: "int", nullable: false),
                    ThemeId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DiscountSelectedTheme", x => x.DiscountSelectedThemeId);
                    table.ForeignKey(
                        name: "FK_DiscountSelectedTheme_Discounts",
                        column: x => x.DiscountId,
                        principalSchema: "themeshop",
                        principalTable: "Discounts",
                        principalColumn: "DiscountId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DiscountSelectedTheme_Themes",
                        column: x => x.ThemeId,
                        principalSchema: "themeshop",
                        principalTable: "Themes",
                        principalColumn: "ThemeId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UserDiscounts",
                schema: "themeshop",
                columns: table => new
                {
                    UserDiscountId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<int>(type: "int", nullable: false),
                    DiscountId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dbo.UserDiscounts", x => x.UserDiscountId);
                    table.ForeignKey(
                        name: "FK_dbo.UserDiscounts_dbo.Discounts_DiscountId",
                        column: x => x.DiscountId,
                        principalSchema: "themeshop",
                        principalTable: "Discounts",
                        principalColumn: "DiscountId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserDiscounts_Users",
                        column: x => x.UserId,
                        principalSchema: "themeshop",
                        principalTable: "Users",
                        principalColumn: "UserId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "WalletLogs",
                schema: "themeshop",
                columns: table => new
                {
                    WalletLogId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    LogTime = table.Column<DateTime>(type: "datetime", nullable: false),
                    Text = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    IsCharge = table.Column<bool>(type: "bit", nullable: false),
                    WalletId = table.Column<int>(type: "int", nullable: false),
                    LogPrice = table.Column<long>(type: "bigint", nullable: false),
                    LogDescription = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dbo.WalletLogs", x => x.WalletLogId);
                    table.ForeignKey(
                        name: "FK_dbo.WalletLogs_dbo.Wallets_WalletId",
                        column: x => x.WalletId,
                        principalSchema: "themeshop",
                        principalTable: "Wallets",
                        principalColumn: "WalletId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_UserLogs_LogCategoryId",
                schema: "themeshop",
                table: "UserLogs",
                column: "LogCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_ArticleRelateds_CategoryId",
                schema: "themeshop",
                table: "ArticleRelateds",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_ArticleCategory_MainCategoryId",
                schema: "themeshop",
                table: "ArticleCategory",
                column: "MainCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_ArticleCategories_MainArticleId",
                schema: "themeshop",
                table: "ArticleCategories",
                column: "MainArticleId");

            migrationBuilder.CreateIndex(
                name: "IX_ArticleReplyComments_ArticleId",
                schema: "themeshop",
                table: "ArticleReplyComments",
                column: "ArticleId");

            migrationBuilder.CreateIndex(
                name: "IX_ArticleReplyComments_CommentId",
                schema: "themeshop",
                table: "ArticleReplyComments",
                column: "CommentId");

            migrationBuilder.CreateIndex(
                name: "IX_DiscountLogs_Discount_DiscountId",
                schema: "themeshop",
                table: "DiscountLogs",
                column: "Discount_DiscountId");

            migrationBuilder.CreateIndex(
                name: "IX_DiscountSelectedTheme_DiscountId",
                schema: "themeshop",
                table: "DiscountSelectedTheme",
                column: "DiscountId");

            migrationBuilder.CreateIndex(
                name: "IX_DiscountSelectedTheme_ThemeId",
                schema: "themeshop",
                table: "DiscountSelectedTheme",
                column: "ThemeId");

            migrationBuilder.CreateIndex(
                name: "IX_ReportAbuse_ThemeId",
                schema: "themeshop",
                table: "ReportAbuse",
                column: "ThemeId");

            migrationBuilder.CreateIndex(
                name: "IX_RolePremissions_PermissionId",
                schema: "themeshop",
                table: "RolePremissions",
                column: "PermissionId");

            migrationBuilder.CreateIndex(
                name: "IX_RolePremissions_RoleId",
                schema: "themeshop",
                table: "RolePremissions",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_ThemeCategoriesChilds_ThemeCategoryId",
                schema: "themeshop",
                table: "ThemeCategoriesChilds",
                column: "ThemeCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_ThemeCategoriesChilds_ThemeCategoryParentId",
                schema: "themeshop",
                table: "ThemeCategoriesChilds",
                column: "ThemeCategoryParentId");

            migrationBuilder.CreateIndex(
                name: "IX_ThemeReplyComments_CommentId",
                schema: "themeshop",
                table: "ThemeReplyComments",
                column: "CommentId");

            migrationBuilder.CreateIndex(
                name: "IX_ThemeReplyComments_ThemeId",
                schema: "themeshop",
                table: "ThemeReplyComments",
                column: "ThemeId");

            migrationBuilder.CreateIndex(
                name: "IX_UserBankMessages_UserBankId",
                schema: "themeshop",
                table: "UserBankMessages",
                column: "UserBankId");

            migrationBuilder.CreateIndex(
                name: "IX_UserDiscounts_DiscountId",
                schema: "themeshop",
                table: "UserDiscounts",
                column: "DiscountId");

            migrationBuilder.CreateIndex(
                name: "IX_UserDiscounts_UserId",
                schema: "themeshop",
                table: "UserDiscounts",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_WalletLogs_WalletId",
                schema: "themeshop",
                table: "WalletLogs",
                column: "WalletId");

            migrationBuilder.CreateIndex(
                name: "IX_Wallets_UserId",
                schema: "themeshop",
                table: "Wallets",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_AdThemeCategories_Ads",
                schema: "themeshop",
                table: "AdThemeCategories",
                column: "AdId",
                principalSchema: "themeshop",
                principalTable: "Ads",
                principalColumn: "AdId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AdThemeCategories_ThemeCategories",
                schema: "themeshop",
                table: "AdThemeCategories",
                column: "ThemeCategoryId",
                principalSchema: "themeshop",
                principalTable: "ThemeCategories",
                principalColumn: "ThemeCategoryId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ArticleCategories_MainArticleCategories",
                schema: "themeshop",
                table: "ArticleCategories",
                column: "MainArticleId",
                principalSchema: "themeshop",
                principalTable: "MainArticleCategories",
                principalColumn: "MainCatagoryId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ArticleCategory_ArticleCategories",
                schema: "themeshop",
                table: "ArticleCategory",
                column: "CategoryId",
                principalSchema: "themeshop",
                principalTable: "ArticleCategories",
                principalColumn: "CategoryId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ArticleCategory_Articles",
                schema: "themeshop",
                table: "ArticleCategory",
                column: "ArticleId",
                principalSchema: "themeshop",
                principalTable: "Articles",
                principalColumn: "ArticleId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ArticleCategory_MainArticleCategories",
                schema: "themeshop",
                table: "ArticleCategory",
                column: "MainCategoryId",
                principalSchema: "themeshop",
                principalTable: "MainArticleCategories",
                principalColumn: "MainCatagoryId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ArticleComments_Articles",
                schema: "themeshop",
                table: "ArticleComments",
                column: "ArticleId",
                principalSchema: "themeshop",
                principalTable: "Articles",
                principalColumn: "ArticleId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ArticleRelateds_ArticleCategory",
                schema: "themeshop",
                table: "ArticleRelateds",
                column: "CategoryId",
                principalSchema: "themeshop",
                principalTable: "ArticleCategories",
                principalColumn: "CategoryId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ArticleRelateds_Articles",
                schema: "themeshop",
                table: "ArticleRelateds",
                column: "ArticleId",
                principalSchema: "themeshop",
                principalTable: "Articles",
                principalColumn: "ArticleId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ArticleRelateds_MainArticleCategories",
                schema: "themeshop",
                table: "ArticleRelateds",
                column: "MainCategoryId",
                principalSchema: "themeshop",
                principalTable: "MainArticleCategories",
                principalColumn: "MainCatagoryId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ArticleTags_Articles",
                schema: "themeshop",
                table: "ArticleTags",
                column: "ArticleId",
                principalSchema: "themeshop",
                principalTable: "Articles",
                principalColumn: "ArticleId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ArticleVisits_Articles",
                schema: "themeshop",
                table: "ArticleVisits",
                column: "ArticleId",
                principalSchema: "themeshop",
                principalTable: "Articles",
                principalColumn: "ArticleId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ComplaintThemes_Complaints",
                schema: "themeshop",
                table: "ComplaintThemes",
                column: "ComplaintId",
                principalSchema: "themeshop",
                principalTable: "Complaints",
                principalColumn: "ComplaintId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ComplaintThemes_Themes",
                schema: "themeshop",
                table: "ComplaintThemes",
                column: "ThemeId",
                principalSchema: "themeshop",
                principalTable: "Themes",
                principalColumn: "ThemeId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_DownloadTokens_Themes",
                schema: "themeshop",
                table: "DownloadTokens",
                column: "ThemeId",
                principalSchema: "themeshop",
                principalTable: "Themes",
                principalColumn: "ThemeId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_OrderDetails_Orders",
                schema: "themeshop",
                table: "OrderDetails",
                column: "OrderId",
                principalSchema: "themeshop",
                principalTable: "Orders",
                principalColumn: "OrderId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_OrderDetails_Themes",
                schema: "themeshop",
                table: "OrderDetails",
                column: "ThemeId",
                principalSchema: "themeshop",
                principalTable: "Themes",
                principalColumn: "ThemeId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Orders_Users",
                schema: "themeshop",
                table: "Orders",
                column: "UserId",
                principalSchema: "themeshop",
                principalTable: "Users",
                principalColumn: "UserId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ThemeAttachments_Themes",
                schema: "themeshop",
                table: "ThemeAttachments",
                column: "ThemeId",
                principalSchema: "themeshop",
                principalTable: "Themes",
                principalColumn: "ThemeId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ThemeChangeStatusMessages_Themes",
                schema: "themeshop",
                table: "ThemeChangeStatusMessages",
                column: "ThemeId",
                principalSchema: "themeshop",
                principalTable: "Themes",
                principalColumn: "ThemeId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ThemeComments_Themes",
                schema: "themeshop",
                table: "ThemeComments",
                column: "ThemeId",
                principalSchema: "themeshop",
                principalTable: "Themes",
                principalColumn: "ThemeId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ThemeFavorits_Themes",
                schema: "themeshop",
                table: "ThemeFavorits",
                column: "ThemeId",
                principalSchema: "themeshop",
                principalTable: "Themes",
                principalColumn: "ThemeId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ThemeGalleries_Themes",
                schema: "themeshop",
                table: "ThemeGalleries",
                column: "ThemeId",
                principalSchema: "themeshop",
                principalTable: "Themes",
                principalColumn: "ThemeId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ThemeLikes_Themes",
                schema: "themeshop",
                table: "ThemeLikes",
                column: "ThemeId",
                principalSchema: "themeshop",
                principalTable: "Themes",
                principalColumn: "ThemeId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ThemeOfferTodays_ThemeOfferTodays",
                schema: "themeshop",
                table: "ThemeOfferTodays",
                column: "ThemeId",
                principalSchema: "themeshop",
                principalTable: "Themes",
                principalColumn: "ThemeId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ThemePreviews_Themes",
                schema: "themeshop",
                table: "ThemePreviews",
                column: "ThemeId",
                principalSchema: "themeshop",
                principalTable: "Themes",
                principalColumn: "ThemeId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ThemeRelateds_Themes",
                schema: "themeshop",
                table: "ThemeRelateds",
                column: "ThemeId",
                principalSchema: "themeshop",
                principalTable: "Themes",
                principalColumn: "ThemeId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Themes_Users",
                schema: "themeshop",
                table: "Themes",
                column: "SellerId",
                principalSchema: "themeshop",
                principalTable: "Users",
                principalColumn: "UserId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ThemeSelectedCategories_ThemeCategories",
                schema: "themeshop",
                table: "ThemeSelectedCategories",
                column: "ThemeCategoryId",
                principalSchema: "themeshop",
                principalTable: "ThemeCategories",
                principalColumn: "ThemeCategoryId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ThemeSelectedCategories_Themes",
                schema: "themeshop",
                table: "ThemeSelectedCategories",
                column: "ThemeId",
                principalSchema: "themeshop",
                principalTable: "Themes",
                principalColumn: "ThemeId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ThemeTags_Themes",
                schema: "themeshop",
                table: "ThemeTags",
                column: "ThemeId",
                principalSchema: "themeshop",
                principalTable: "Themes",
                principalColumn: "ThemeId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ThemeVisits_Themes",
                schema: "themeshop",
                table: "ThemeVisits",
                column: "ThemeId",
                principalSchema: "themeshop",
                principalTable: "Themes",
                principalColumn: "ThemeId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Ticketings_Themes",
                schema: "themeshop",
                table: "Ticketings",
                column: "ThemeId",
                principalSchema: "themeshop",
                principalTable: "Themes",
                principalColumn: "ThemeId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TicketMessages_Ticketings",
                schema: "themeshop",
                table: "TicketMessages",
                column: "TicketId",
                principalSchema: "themeshop",
                principalTable: "Ticketings",
                principalColumn: "TicketId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_UserBanks_Users",
                schema: "themeshop",
                table: "UserBanks",
                column: "UserId",
                principalSchema: "themeshop",
                principalTable: "Users",
                principalColumn: "UserId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_dbo.UserLogs_dbo.LogCategories_LogCategoryId",
                schema: "themeshop",
                table: "UserLogs",
                column: "LogCategoryId",
                principalSchema: "themeshop",
                principalTable: "LogCategories",
                principalColumn: "LogCategoryId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_dbo.UserLogs_dbo.Users_UserId",
                schema: "themeshop",
                table: "UserLogs",
                column: "UserId",
                principalSchema: "themeshop",
                principalTable: "Users",
                principalColumn: "UserId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AdThemeCategories_Ads",
                schema: "themeshop",
                table: "AdThemeCategories");

            migrationBuilder.DropForeignKey(
                name: "FK_AdThemeCategories_ThemeCategories",
                schema: "themeshop",
                table: "AdThemeCategories");

            migrationBuilder.DropForeignKey(
                name: "FK_ArticleCategories_MainArticleCategories",
                schema: "themeshop",
                table: "ArticleCategories");

            migrationBuilder.DropForeignKey(
                name: "FK_ArticleCategory_ArticleCategories",
                schema: "themeshop",
                table: "ArticleCategory");

            migrationBuilder.DropForeignKey(
                name: "FK_ArticleCategory_Articles",
                schema: "themeshop",
                table: "ArticleCategory");

            migrationBuilder.DropForeignKey(
                name: "FK_ArticleCategory_MainArticleCategories",
                schema: "themeshop",
                table: "ArticleCategory");

            migrationBuilder.DropForeignKey(
                name: "FK_ArticleComments_Articles",
                schema: "themeshop",
                table: "ArticleComments");

            migrationBuilder.DropForeignKey(
                name: "FK_ArticleRelateds_ArticleCategory",
                schema: "themeshop",
                table: "ArticleRelateds");

            migrationBuilder.DropForeignKey(
                name: "FK_ArticleRelateds_Articles",
                schema: "themeshop",
                table: "ArticleRelateds");

            migrationBuilder.DropForeignKey(
                name: "FK_ArticleRelateds_MainArticleCategories",
                schema: "themeshop",
                table: "ArticleRelateds");

            migrationBuilder.DropForeignKey(
                name: "FK_ArticleTags_Articles",
                schema: "themeshop",
                table: "ArticleTags");

            migrationBuilder.DropForeignKey(
                name: "FK_ArticleVisits_Articles",
                schema: "themeshop",
                table: "ArticleVisits");

            migrationBuilder.DropForeignKey(
                name: "FK_ComplaintThemes_Complaints",
                schema: "themeshop",
                table: "ComplaintThemes");

            migrationBuilder.DropForeignKey(
                name: "FK_ComplaintThemes_Themes",
                schema: "themeshop",
                table: "ComplaintThemes");

            migrationBuilder.DropForeignKey(
                name: "FK_DownloadTokens_Themes",
                schema: "themeshop",
                table: "DownloadTokens");

            migrationBuilder.DropForeignKey(
                name: "FK_OrderDetails_Orders",
                schema: "themeshop",
                table: "OrderDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_OrderDetails_Themes",
                schema: "themeshop",
                table: "OrderDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_Orders_Users",
                schema: "themeshop",
                table: "Orders");

            migrationBuilder.DropForeignKey(
                name: "FK_ThemeAttachments_Themes",
                schema: "themeshop",
                table: "ThemeAttachments");

            migrationBuilder.DropForeignKey(
                name: "FK_ThemeChangeStatusMessages_Themes",
                schema: "themeshop",
                table: "ThemeChangeStatusMessages");

            migrationBuilder.DropForeignKey(
                name: "FK_ThemeComments_Themes",
                schema: "themeshop",
                table: "ThemeComments");

            migrationBuilder.DropForeignKey(
                name: "FK_ThemeFavorits_Themes",
                schema: "themeshop",
                table: "ThemeFavorits");

            migrationBuilder.DropForeignKey(
                name: "FK_ThemeGalleries_Themes",
                schema: "themeshop",
                table: "ThemeGalleries");

            migrationBuilder.DropForeignKey(
                name: "FK_ThemeLikes_Themes",
                schema: "themeshop",
                table: "ThemeLikes");

            migrationBuilder.DropForeignKey(
                name: "FK_ThemeOfferTodays_ThemeOfferTodays",
                schema: "themeshop",
                table: "ThemeOfferTodays");

            migrationBuilder.DropForeignKey(
                name: "FK_ThemePreviews_Themes",
                schema: "themeshop",
                table: "ThemePreviews");

            migrationBuilder.DropForeignKey(
                name: "FK_ThemeRelateds_Themes",
                schema: "themeshop",
                table: "ThemeRelateds");

            migrationBuilder.DropForeignKey(
                name: "FK_Themes_Users",
                schema: "themeshop",
                table: "Themes");

            migrationBuilder.DropForeignKey(
                name: "FK_ThemeSelectedCategories_ThemeCategories",
                schema: "themeshop",
                table: "ThemeSelectedCategories");

            migrationBuilder.DropForeignKey(
                name: "FK_ThemeSelectedCategories_Themes",
                schema: "themeshop",
                table: "ThemeSelectedCategories");

            migrationBuilder.DropForeignKey(
                name: "FK_ThemeTags_Themes",
                schema: "themeshop",
                table: "ThemeTags");

            migrationBuilder.DropForeignKey(
                name: "FK_ThemeVisits_Themes",
                schema: "themeshop",
                table: "ThemeVisits");

            migrationBuilder.DropForeignKey(
                name: "FK_Ticketings_Themes",
                schema: "themeshop",
                table: "Ticketings");

            migrationBuilder.DropForeignKey(
                name: "FK_TicketMessages_Ticketings",
                schema: "themeshop",
                table: "TicketMessages");

            migrationBuilder.DropForeignKey(
                name: "FK_UserBanks_Users",
                schema: "themeshop",
                table: "UserBanks");

            migrationBuilder.DropForeignKey(
                name: "FK_dbo.UserLogs_dbo.LogCategories_LogCategoryId",
                schema: "themeshop",
                table: "UserLogs");

            migrationBuilder.DropForeignKey(
                name: "FK_dbo.UserLogs_dbo.Users_UserId",
                schema: "themeshop",
                table: "UserLogs");

            migrationBuilder.DropTable(
                name: "ArticleReplyComments",
                schema: "themeshop");

            migrationBuilder.DropTable(
                name: "DiscountLogs",
                schema: "themeshop");

            migrationBuilder.DropTable(
                name: "DiscountSelectedTheme",
                schema: "themeshop");

            migrationBuilder.DropTable(
                name: "LogCategories",
                schema: "themeshop");

            migrationBuilder.DropTable(
                name: "MainArticleCategories",
                schema: "themeshop");

            migrationBuilder.DropTable(
                name: "OnlineUserReport",
                schema: "themeshop");

            migrationBuilder.DropTable(
                name: "OnlineUsers",
                schema: "themeshop");

            migrationBuilder.DropTable(
                name: "ReportAbuse",
                schema: "themeshop");

            migrationBuilder.DropTable(
                name: "RolePremissions",
                schema: "themeshop");

            migrationBuilder.DropTable(
                name: "ThemeCategoriesChilds",
                schema: "themeshop");

            migrationBuilder.DropTable(
                name: "ThemeReplyComments",
                schema: "themeshop");

            migrationBuilder.DropTable(
                name: "UserBankMessages",
                schema: "themeshop");

            migrationBuilder.DropTable(
                name: "UserDiscounts",
                schema: "themeshop");

            migrationBuilder.DropTable(
                name: "WalletLogs",
                schema: "themeshop");

            migrationBuilder.DropTable(
                name: "Discounts",
                schema: "themeshop");

            migrationBuilder.DropTable(
                name: "Wallets",
                schema: "themeshop");

            migrationBuilder.DropPrimaryKey(
                name: "PK_dbo.Users",
                schema: "themeshop",
                table: "Users");

            migrationBuilder.DropPrimaryKey(
                name: "PK_dbo.UserRoles",
                schema: "themeshop",
                table: "UserRoles");

            migrationBuilder.DropIndex(
                name: "IX_UserLogs_LogCategoryId",
                schema: "themeshop",
                table: "UserLogs");

            migrationBuilder.DropPrimaryKey(
                name: "PK_dbo.UserBanks",
                schema: "themeshop",
                table: "UserBanks");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Ticketings",
                schema: "themeshop",
                table: "Ticketings");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ThemeVisits",
                schema: "themeshop",
                table: "ThemeVisits");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ThemeSelectedCategories",
                schema: "themeshop",
                table: "ThemeSelectedCategories");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ThemeRelateds",
                schema: "themeshop",
                table: "ThemeRelateds");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ThemePreviews",
                schema: "themeshop",
                table: "ThemePreviews");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ThemeOfferTodays",
                schema: "themeshop",
                table: "ThemeOfferTodays");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ThemeLikes",
                schema: "themeshop",
                table: "ThemeLikes");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ThemeGalleries",
                schema: "themeshop",
                table: "ThemeGalleries");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ThemeFavorits",
                schema: "themeshop",
                table: "ThemeFavorits");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ThemeComments",
                schema: "themeshop",
                table: "ThemeComments");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ThemeCategories",
                schema: "themeshop",
                table: "ThemeCategories");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ThemeAttachments",
                schema: "themeshop",
                table: "ThemeAttachments");

            migrationBuilder.DropPrimaryKey(
                name: "PK_dbo.Roles",
                schema: "themeshop",
                table: "Roles");

            migrationBuilder.DropPrimaryKey(
                name: "PK_dbo.Permissions",
                schema: "themeshop",
                table: "Permissions");

            migrationBuilder.DropPrimaryKey(
                name: "PK_dbo.Orders",
                schema: "themeshop",
                table: "Orders");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ComplaintThemes",
                schema: "themeshop",
                table: "ComplaintThemes");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ArticleVisits",
                schema: "themeshop",
                table: "ArticleVisits");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ArticleRelateds",
                schema: "themeshop",
                table: "ArticleRelateds");

            migrationBuilder.DropIndex(
                name: "IX_ArticleRelateds_CategoryId",
                schema: "themeshop",
                table: "ArticleRelateds");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ArticleComments",
                schema: "themeshop",
                table: "ArticleComments");

            migrationBuilder.DropIndex(
                name: "IX_ArticleCategory_MainCategoryId",
                schema: "themeshop",
                table: "ArticleCategory");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ArticleCategories",
                schema: "themeshop",
                table: "ArticleCategories");

            migrationBuilder.DropIndex(
                name: "IX_ArticleCategories_MainArticleId",
                schema: "themeshop",
                table: "ArticleCategories");

            migrationBuilder.DropColumn(
                name: "AccountBalance",
                schema: "themeshop",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "IsDelete",
                schema: "themeshop",
                table: "UserRoles");

            migrationBuilder.DropColumn(
                name: "LogCategoryId",
                schema: "themeshop",
                table: "UserLogs");

            migrationBuilder.DropColumn(
                name: "LogDescribtion",
                schema: "themeshop",
                table: "UserLogs");

            migrationBuilder.DropColumn(
                name: "LogPrice",
                schema: "themeshop",
                table: "UserLogs");

            migrationBuilder.DropColumn(
                name: "IsAnswered",
                schema: "themeshop",
                table: "Ticketings");

            migrationBuilder.DropColumn(
                name: "CommentId",
                schema: "themeshop",
                table: "ThemeComments");

            migrationBuilder.DropColumn(
                name: "CompleteTime",
                schema: "themeshop",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "IsActive",
                schema: "themeshop",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "TotalPrice",
                schema: "themeshop",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "IsAnswered",
                schema: "themeshop",
                table: "ContactUs");

            migrationBuilder.DropColumn(
                name: "SellerId",
                schema: "themeshop",
                table: "ComplaintThemes");

            migrationBuilder.DropColumn(
                name: "CategoryId",
                schema: "themeshop",
                table: "ArticleRelateds");

            migrationBuilder.DropColumn(
                name: "CommentId",
                schema: "themeshop",
                table: "ArticleComments");

            migrationBuilder.DropColumn(
                name: "MainCategoryId",
                schema: "themeshop",
                table: "ArticleCategory");

            migrationBuilder.DropColumn(
                name: "MainArticleId",
                schema: "themeshop",
                table: "ArticleCategories");

            migrationBuilder.RenameTable(
                name: "Users",
                schema: "themeshop",
                newName: "Users");

            migrationBuilder.RenameTable(
                name: "UserRoles",
                schema: "themeshop",
                newName: "UserRoles");

            migrationBuilder.RenameTable(
                name: "UserLogs",
                schema: "themeshop",
                newName: "UserLogs");

            migrationBuilder.RenameTable(
                name: "UserBanks",
                schema: "themeshop",
                newName: "UserBanks");

            migrationBuilder.RenameTable(
                name: "TicketMessages",
                schema: "themeshop",
                newName: "TicketMessages");

            migrationBuilder.RenameTable(
                name: "Ticketings",
                schema: "themeshop",
                newName: "Ticketings");

            migrationBuilder.RenameTable(
                name: "ThemeVisits",
                schema: "themeshop",
                newName: "ThemeVisits");

            migrationBuilder.RenameTable(
                name: "ThemeTags",
                schema: "themeshop",
                newName: "ThemeTags");

            migrationBuilder.RenameTable(
                name: "ThemeSelectedCategories",
                schema: "themeshop",
                newName: "ThemeSelectedCategories");

            migrationBuilder.RenameTable(
                name: "Themes",
                schema: "themeshop",
                newName: "Themes");

            migrationBuilder.RenameTable(
                name: "ThemeRelateds",
                schema: "themeshop",
                newName: "ThemeRelateds");

            migrationBuilder.RenameTable(
                name: "ThemePreviews",
                schema: "themeshop",
                newName: "ThemePreviews");

            migrationBuilder.RenameTable(
                name: "ThemeOfferTodays",
                schema: "themeshop",
                newName: "ThemeOfferTodays");

            migrationBuilder.RenameTable(
                name: "ThemeLikes",
                schema: "themeshop",
                newName: "ThemeLikes");

            migrationBuilder.RenameTable(
                name: "ThemeGalleries",
                schema: "themeshop",
                newName: "ThemeGalleries");

            migrationBuilder.RenameTable(
                name: "ThemeFavorits",
                schema: "themeshop",
                newName: "ThemeFavorits");

            migrationBuilder.RenameTable(
                name: "ThemeComments",
                schema: "themeshop",
                newName: "ThemeComments");

            migrationBuilder.RenameTable(
                name: "ThemeChangeStatusMessages",
                schema: "themeshop",
                newName: "ThemeChangeStatusMessages");

            migrationBuilder.RenameTable(
                name: "ThemeCategories",
                schema: "themeshop",
                newName: "ThemeCategories");

            migrationBuilder.RenameTable(
                name: "ThemeAttachments",
                schema: "themeshop",
                newName: "ThemeAttachments");

            migrationBuilder.RenameTable(
                name: "TemplateInfoes",
                schema: "themeshop",
                newName: "TemplateInfoes");

            migrationBuilder.RenameTable(
                name: "Roles",
                schema: "themeshop",
                newName: "Roles");

            migrationBuilder.RenameTable(
                name: "Permissions",
                schema: "themeshop",
                newName: "Permissions");

            migrationBuilder.RenameTable(
                name: "Orders",
                schema: "themeshop",
                newName: "Orders");

            migrationBuilder.RenameTable(
                name: "OrderDetails",
                schema: "themeshop",
                newName: "OrderDetails");

            migrationBuilder.RenameTable(
                name: "DownloadTokens",
                schema: "themeshop",
                newName: "DownloadTokens");

            migrationBuilder.RenameTable(
                name: "ContactUs",
                schema: "themeshop",
                newName: "ContactUs");

            migrationBuilder.RenameTable(
                name: "ComplaintThemes",
                schema: "themeshop",
                newName: "ComplaintThemes");

            migrationBuilder.RenameTable(
                name: "Complaints",
                schema: "themeshop",
                newName: "Complaints");

            migrationBuilder.RenameTable(
                name: "ArticleVisits",
                schema: "themeshop",
                newName: "ArticleVisits");

            migrationBuilder.RenameTable(
                name: "ArticleTags",
                schema: "themeshop",
                newName: "ArticleTags");

            migrationBuilder.RenameTable(
                name: "Articles",
                schema: "themeshop",
                newName: "Articles");

            migrationBuilder.RenameTable(
                name: "ArticleRelateds",
                schema: "themeshop",
                newName: "ArticleRelateds");

            migrationBuilder.RenameTable(
                name: "ArticleComments",
                schema: "themeshop",
                newName: "ArticleComments");

            migrationBuilder.RenameTable(
                name: "ArticleCategory",
                schema: "themeshop",
                newName: "ArticleCategory");

            migrationBuilder.RenameTable(
                name: "ArticleCategories",
                schema: "themeshop",
                newName: "ArticleCategories");

            migrationBuilder.RenameTable(
                name: "AdThemeCategories",
                schema: "themeshop",
                newName: "AdThemeCategories");

            migrationBuilder.RenameTable(
                name: "Ads",
                schema: "themeshop",
                newName: "Ads");

            migrationBuilder.RenameColumn(
                name: "UserId",
                table: "Users",
                newName: "UserID");

            migrationBuilder.RenameColumn(
                name: "UserId",
                table: "UserRoles",
                newName: "UserID");

            migrationBuilder.RenameColumn(
                name: "RoleId",
                table: "UserRoles",
                newName: "RoleID");

            migrationBuilder.RenameColumn(
                name: "UserRoleId",
                table: "UserRoles",
                newName: "UserRoleID");

            migrationBuilder.RenameIndex(
                name: "IX_UserRoles_UserId",
                table: "UserRoles",
                newName: "IX_UserID");

            migrationBuilder.RenameIndex(
                name: "IX_UserRoles_RoleId",
                table: "UserRoles",
                newName: "IX_RoleID");

            migrationBuilder.RenameColumn(
                name: "UserId",
                table: "UserLogs",
                newName: "UserID");

            migrationBuilder.RenameColumn(
                name: "LogId",
                table: "UserLogs",
                newName: "LogID");

            migrationBuilder.RenameColumn(
                name: "LogTime",
                table: "UserLogs",
                newName: "DateTime");

            migrationBuilder.RenameIndex(
                name: "IX_UserLogs_UserId",
                table: "UserLogs",
                newName: "IX_UserID");

            migrationBuilder.RenameColumn(
                name: "NationalCode",
                table: "UserBanks",
                newName: "CodeMeli");

            migrationBuilder.RenameColumn(
                name: "NationalCardImage",
                table: "UserBanks",
                newName: "SellerAccountHolder");

            migrationBuilder.RenameColumn(
                name: "BankCardImage",
                table: "UserBanks",
                newName: "ImageKartMeli");

            migrationBuilder.RenameColumn(
                name: "BankAccountHolder",
                table: "UserBanks",
                newName: "ImageCardBank");

            migrationBuilder.RenameIndex(
                name: "IX_UserBanks_UserId",
                table: "UserBanks",
                newName: "IX_UserId");

            migrationBuilder.RenameColumn(
                name: "UserId",
                table: "TicketMessages",
                newName: "Owner_UserID");

            migrationBuilder.RenameIndex(
                name: "IX_TicketMessages_TicketId",
                table: "TicketMessages",
                newName: "IX_TicketId");

            migrationBuilder.RenameColumn(
                name: "UserId",
                table: "Ticketings",
                newName: "User_UserID");

            migrationBuilder.RenameIndex(
                name: "IX_Ticketings_ThemeId",
                table: "Ticketings",
                newName: "IX_ThemeId");

            migrationBuilder.RenameColumn(
                name: "ThemeId",
                table: "ThemeVisits",
                newName: "ThemeID");

            migrationBuilder.RenameColumn(
                name: "VisitId",
                table: "ThemeVisits",
                newName: "VisitID");

            migrationBuilder.RenameIndex(
                name: "IX_ThemeVisits_ThemeId",
                table: "ThemeVisits",
                newName: "IX_ThemeID");

            migrationBuilder.RenameColumn(
                name: "ThemeId",
                table: "ThemeTags",
                newName: "ThemeID");

            migrationBuilder.RenameColumn(
                name: "ThemeTagId",
                table: "ThemeTags",
                newName: "ThemeTagID");

            migrationBuilder.RenameIndex(
                name: "IX_ThemeTags_ThemeId",
                table: "ThemeTags",
                newName: "IX_ThemeID");

            migrationBuilder.RenameColumn(
                name: "ThemeId",
                table: "ThemeSelectedCategories",
                newName: "ThemeID");

            migrationBuilder.RenameColumn(
                name: "ThemeCategoryId",
                table: "ThemeSelectedCategories",
                newName: "CategoryID");

            migrationBuilder.RenameColumn(
                name: "ThemeSelectedId",
                table: "ThemeSelectedCategories",
                newName: "SelectedID");

            migrationBuilder.RenameIndex(
                name: "IX_ThemeSelectedCategories_ThemeId",
                table: "ThemeSelectedCategories",
                newName: "IX_ThemeID");

            migrationBuilder.RenameIndex(
                name: "IX_ThemeSelectedCategories_ThemeCategoryId",
                table: "ThemeSelectedCategories",
                newName: "IX_CategoryID");

            migrationBuilder.RenameColumn(
                name: "SellerId",
                table: "Themes",
                newName: "SellerID");

            migrationBuilder.RenameColumn(
                name: "ThemeId",
                table: "Themes",
                newName: "ThemeID");

            migrationBuilder.RenameIndex(
                name: "IX_Themes_SellerId",
                table: "Themes",
                newName: "IX_SellerID");

            migrationBuilder.RenameColumn(
                name: "ThemeRelatedId",
                table: "ThemeRelateds",
                newName: "themeRelatedID");

            migrationBuilder.RenameColumn(
                name: "ThemeId",
                table: "ThemeRelateds",
                newName: "ThemeID");

            migrationBuilder.RenameColumn(
                name: "RelatedId",
                table: "ThemeRelateds",
                newName: "RelatedID");

            migrationBuilder.RenameIndex(
                name: "IX_ThemeRelateds_ThemeId",
                table: "ThemeRelateds",
                newName: "IX_ThemeID");

            migrationBuilder.RenameColumn(
                name: "ThemeId",
                table: "ThemePreviews",
                newName: "ThemeID");

            migrationBuilder.RenameColumn(
                name: "PreviewId",
                table: "ThemePreviews",
                newName: "PreviewID");

            migrationBuilder.RenameIndex(
                name: "IX_ThemePreviews_ThemeId",
                table: "ThemePreviews",
                newName: "IX_ThemeID");

            migrationBuilder.RenameColumn(
                name: "ThemeId",
                table: "ThemeOfferTodays",
                newName: "ThemeID");

            migrationBuilder.RenameColumn(
                name: "OfferId",
                table: "ThemeOfferTodays",
                newName: "OfferID");

            migrationBuilder.RenameIndex(
                name: "IX_ThemeOfferTodays_ThemeId",
                table: "ThemeOfferTodays",
                newName: "IX_ThemeID");

            migrationBuilder.RenameColumn(
                name: "UserId",
                table: "ThemeLikes",
                newName: "UserID");

            migrationBuilder.RenameColumn(
                name: "ThemeId",
                table: "ThemeLikes",
                newName: "ThemeID");

            migrationBuilder.RenameColumn(
                name: "LikeId",
                table: "ThemeLikes",
                newName: "LikeID");

            migrationBuilder.RenameIndex(
                name: "IX_ThemeLikes_ThemeId",
                table: "ThemeLikes",
                newName: "IX_ThemeID");

            migrationBuilder.RenameColumn(
                name: "ThemeId",
                table: "ThemeGalleries",
                newName: "ThemeID");

            migrationBuilder.RenameColumn(
                name: "GalleryId",
                table: "ThemeGalleries",
                newName: "GalleryID");

            migrationBuilder.RenameIndex(
                name: "IX_ThemeGalleries_ThemeId",
                table: "ThemeGalleries",
                newName: "IX_ThemeID");

            migrationBuilder.RenameColumn(
                name: "UserId",
                table: "ThemeFavorits",
                newName: "UserID");

            migrationBuilder.RenameColumn(
                name: "ThemeId",
                table: "ThemeFavorits",
                newName: "ThemeID");

            migrationBuilder.RenameColumn(
                name: "FavoritId",
                table: "ThemeFavorits",
                newName: "FavoritID");

            migrationBuilder.RenameIndex(
                name: "IX_ThemeFavorits_ThemeId",
                table: "ThemeFavorits",
                newName: "IX_ThemeID");

            migrationBuilder.RenameColumn(
                name: "UserId",
                table: "ThemeComments",
                newName: "UserID");

            migrationBuilder.RenameColumn(
                name: "ThemeId",
                table: "ThemeComments",
                newName: "ThemeID");

            migrationBuilder.RenameIndex(
                name: "IX_ThemeComments_ThemeId",
                table: "ThemeComments",
                newName: "IX_ThemeID");

            migrationBuilder.RenameColumn(
                name: "StatusMessagesId",
                table: "ThemeChangeStatusMessages",
                newName: "Id");

            migrationBuilder.RenameIndex(
                name: "IX_ThemeChangeStatusMessages_ThemeId",
                table: "ThemeChangeStatusMessages",
                newName: "IX_ThemeId");

            migrationBuilder.RenameColumn(
                name: "ThemeCategoryId",
                table: "ThemeCategories",
                newName: "CategoryID");

            migrationBuilder.RenameColumn(
                name: "ThemeId",
                table: "ThemeAttachments",
                newName: "ThemeID");

            migrationBuilder.RenameColumn(
                name: "AttachId",
                table: "ThemeAttachments",
                newName: "AttachID");

            migrationBuilder.RenameIndex(
                name: "IX_ThemeAttachments_ThemeId",
                table: "ThemeAttachments",
                newName: "IX_ThemeID");

            migrationBuilder.RenameColumn(
                name: "CreateDate",
                table: "TemplateInfoes",
                newName: "CreateBuy");

            migrationBuilder.RenameIndex(
                name: "IX_TemplateInfoes_UserId",
                table: "TemplateInfoes",
                newName: "IX_UserId");

            migrationBuilder.RenameIndex(
                name: "IX_TemplateInfoes_ThemeId",
                table: "TemplateInfoes",
                newName: "IX_ThemeId");

            migrationBuilder.RenameIndex(
                name: "IX_TemplateInfoes_SellerId",
                table: "TemplateInfoes",
                newName: "IX_SellerId");

            migrationBuilder.RenameColumn(
                name: "RoleId",
                table: "Roles",
                newName: "RoleID");

            migrationBuilder.RenameColumn(
                name: "IsDefualtRole",
                table: "Roles",
                newName: "IsDelete");

            migrationBuilder.RenameColumn(
                name: "PermissionId",
                table: "Permissions",
                newName: "PermissionID");

            migrationBuilder.RenameColumn(
                name: "UserId",
                table: "Orders",
                newName: "UserID");

            migrationBuilder.RenameColumn(
                name: "OrderId",
                table: "Orders",
                newName: "OrderID");

            migrationBuilder.RenameColumn(
                name: "IsPay",
                table: "Orders",
                newName: "IsFinaly");

            migrationBuilder.RenameColumn(
                name: "CreateTime",
                table: "Orders",
                newName: "CreateDate");

            migrationBuilder.RenameIndex(
                name: "IX_Orders_UserId",
                table: "Orders",
                newName: "IX_UserID");

            migrationBuilder.RenameColumn(
                name: "OrderDetailId",
                table: "OrderDetails",
                newName: "Id");

            migrationBuilder.RenameIndex(
                name: "IX_OrderDetails_ThemeId",
                table: "OrderDetails",
                newName: "IX_ThemeId");

            migrationBuilder.RenameIndex(
                name: "IX_OrderDetails_OrderId",
                table: "OrderDetails",
                newName: "IX_OrderId");

            migrationBuilder.RenameColumn(
                name: "DownloadTokenId",
                table: "DownloadTokens",
                newName: "Id");

            migrationBuilder.RenameIndex(
                name: "IX_DownloadTokens_ThemeId",
                table: "DownloadTokens",
                newName: "IX_ThemeId");

            migrationBuilder.RenameColumn(
                name: "ContactId",
                table: "ContactUs",
                newName: "ContactID");

            migrationBuilder.RenameColumn(
                name: "IPAddress",
                table: "ContactUs",
                newName: "UserIP");

            migrationBuilder.RenameIndex(
                name: "IX_ComplaintThemes_ThemeId",
                table: "ComplaintThemes",
                newName: "IX_ThemeId");

            migrationBuilder.RenameIndex(
                name: "IX_ComplaintThemes_ComplaintId",
                table: "ComplaintThemes",
                newName: "IX_ComplaintId");

            migrationBuilder.RenameColumn(
                name: "ArticleId",
                table: "ArticleVisits",
                newName: "ArticleID");

            migrationBuilder.RenameColumn(
                name: "VisitId",
                table: "ArticleVisits",
                newName: "VisitID");

            migrationBuilder.RenameIndex(
                name: "IX_ArticleVisits_ArticleId",
                table: "ArticleVisits",
                newName: "IX_ArticleID");

            migrationBuilder.RenameColumn(
                name: "ArticleId",
                table: "ArticleTags",
                newName: "ArticleID");

            migrationBuilder.RenameColumn(
                name: "ArticleTagId",
                table: "ArticleTags",
                newName: "ArticleTagID");

            migrationBuilder.RenameIndex(
                name: "IX_ArticleTags_ArticleId",
                table: "ArticleTags",
                newName: "IX_ArticleID");

            migrationBuilder.RenameColumn(
                name: "AuthorId",
                table: "Articles",
                newName: "AuthorID");

            migrationBuilder.RenameColumn(
                name: "ArticleId",
                table: "Articles",
                newName: "ArticleID");

            migrationBuilder.RenameColumn(
                name: "ArticleId",
                table: "ArticleRelateds",
                newName: "ArticleID");

            migrationBuilder.RenameColumn(
                name: "RelatedId",
                table: "ArticleRelateds",
                newName: "RelatedID");

            migrationBuilder.RenameColumn(
                name: "MainCategoryId",
                table: "ArticleRelateds",
                newName: "ArticleRelatedID");

            migrationBuilder.RenameIndex(
                name: "IX_ArticleRelateds_MainCategoryId",
                table: "ArticleRelateds",
                newName: "IX_ArticleRelatedID");

            migrationBuilder.RenameIndex(
                name: "IX_ArticleRelateds_ArticleId",
                table: "ArticleRelateds",
                newName: "IX_ArticleID");

            migrationBuilder.RenameColumn(
                name: "UserId",
                table: "ArticleComments",
                newName: "UserID");

            migrationBuilder.RenameColumn(
                name: "ArticleId",
                table: "ArticleComments",
                newName: "ArticleID");

            migrationBuilder.RenameIndex(
                name: "IX_ArticleComments_ArticleId",
                table: "ArticleComments",
                newName: "IX_ArticleID");

            migrationBuilder.RenameColumn(
                name: "CategoryId",
                table: "ArticleCategory",
                newName: "CategoryID");

            migrationBuilder.RenameColumn(
                name: "ArticleId",
                table: "ArticleCategory",
                newName: "ArticleID");

            migrationBuilder.RenameColumn(
                name: "ArticleCategoryId",
                table: "ArticleCategory",
                newName: "ArticleCategoryID");

            migrationBuilder.RenameIndex(
                name: "IX_ArticleCategory_CategoryId",
                table: "ArticleCategory",
                newName: "IX_CategoryID");

            migrationBuilder.RenameIndex(
                name: "IX_ArticleCategory_ArticleId",
                table: "ArticleCategory",
                newName: "IX_ArticleID");

            migrationBuilder.RenameColumn(
                name: "CategoryId",
                table: "ArticleCategories",
                newName: "CategoryID");

            migrationBuilder.RenameColumn(
                name: "ThemeCategoryId",
                table: "AdThemeCategories",
                newName: "CategoryId");

            migrationBuilder.RenameIndex(
                name: "IX_AdThemeCategories_ThemeCategoryId",
                table: "AdThemeCategories",
                newName: "IX_CategoryId");

            migrationBuilder.RenameIndex(
                name: "IX_AdThemeCategories_AdId",
                table: "AdThemeCategories",
                newName: "IX_AdId");

            migrationBuilder.AlterDatabase(
                oldCollation: "SQL_Latin1_General_CP1_CI_AS");

            migrationBuilder.AlterColumn<string>(
                name: "Password",
                table: "Users",
                type: "nvarchar(100)",
                maxLength: 100,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(10)",
                oldMaxLength: 10);

            migrationBuilder.AlterColumn<string>(
                name: "Mobile",
                table: "Users",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(13)",
                oldMaxLength: 13,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Email",
                table: "Users",
                type: "nvarchar(250)",
                maxLength: 250,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(50)",
                oldMaxLength: 50);

            migrationBuilder.AddColumn<string>(
                name: "RegisterIP",
                table: "Users",
                type: "nvarchar(150)",
                maxLength: 150,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "UserLogs",
                type: "nvarchar(1000)",
                maxLength: 1000,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AlterColumn<string>(
                name: "Text",
                table: "TicketMessages",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.AlterColumn<string>(
                name: "Title",
                table: "Ticketings",
                type: "nvarchar(300)",
                maxLength: 300,
                nullable: false,
                defaultValueSql: "('')",
                oldClrType: typeof(string),
                oldType: "nvarchar(300)",
                oldMaxLength: 300);

            migrationBuilder.AlterColumn<int>(
                name: "ThemeId",
                table: "Ticketings",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<string>(
                name: "IP",
                table: "ThemeVisits",
                type: "nvarchar(100)",
                maxLength: 100,
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "nvarchar(100)",
                oldMaxLength: 100,
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Theme_ThemeID",
                table: "Themes",
                type: "int",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "IP",
                table: "ThemeLikes",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(100)",
                oldMaxLength: 100,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "ImageTitle",
                table: "ThemeGalleries",
                type: "nvarchar(200)",
                maxLength: 200,
                nullable: false,
                defaultValueSql: "('')",
                oldClrType: typeof(string),
                oldType: "nvarchar(200)",
                oldMaxLength: 200);

            migrationBuilder.AddColumn<int>(
                name: "CommantID",
                table: "ThemeComments",
                type: "int",
                nullable: false,
                defaultValue: 0)
                .Annotation("SqlServer:Identity", "1, 1");

            migrationBuilder.AddColumn<int>(
                name: "ParentID",
                table: "ThemeComments",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ParentID",
                table: "ThemeCategories",
                type: "int",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "RoleID",
                table: "Roles",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int")
                .OldAnnotation("SqlServer:Identity", "1, 1");

            migrationBuilder.AddColumn<bool>(
                name: "IsDefaultRole",
                table: "Roles",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AlterColumn<int>(
                name: "DisplayPriority",
                table: "Permissions",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddColumn<int>(
                name: "ParentID",
                table: "Permissions",
                type: "int",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Subject",
                table: "ContactUs",
                type: "nvarchar(500)",
                maxLength: 500,
                nullable: false,
                defaultValueSql: "('')",
                oldClrType: typeof(string),
                oldType: "nvarchar(500)",
                oldMaxLength: 500);

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "ContactUs",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CommantID",
                table: "ArticleComments",
                type: "int",
                nullable: false,
                defaultValue: 0)
                .Annotation("SqlServer:Identity", "1, 1");

            migrationBuilder.AddColumn<int>(
                name: "ParentID",
                table: "ArticleComments",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ParentID",
                table: "ArticleCategories",
                type: "int",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Users",
                table: "Users",
                column: "UserID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_UserRoles",
                table: "UserRoles",
                column: "UserRoleID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_UserBanks",
                table: "UserBanks",
                column: "UserBankId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_dbo.Ticketings",
                table: "Ticketings",
                column: "TicketId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_dbo.ThemeVisits",
                table: "ThemeVisits",
                column: "VisitID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_dbo.ThemeSelectedCategories",
                table: "ThemeSelectedCategories",
                column: "SelectedID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_dbo.ThemeRelateds",
                table: "ThemeRelateds",
                column: "RelatedID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_dbo.ThemePreviews",
                table: "ThemePreviews",
                column: "PreviewID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_dbo.ThemeOfferTodays",
                table: "ThemeOfferTodays",
                column: "OfferID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_dbo.ThemeLikes",
                table: "ThemeLikes",
                column: "LikeID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_dbo.ThemeGalleries",
                table: "ThemeGalleries",
                column: "GalleryID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_dbo.ThemeFavorits",
                table: "ThemeFavorits",
                column: "FavoritID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_dbo.ThemeComments",
                table: "ThemeComments",
                column: "CommantID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_dbo.ThemeCategories",
                table: "ThemeCategories",
                column: "CategoryID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_dbo.ThemeAttachments",
                table: "ThemeAttachments",
                column: "AttachID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Roles",
                table: "Roles",
                column: "RoleID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Permissions",
                table: "Permissions",
                column: "PermissionID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Orders",
                table: "Orders",
                column: "OrderID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_dbo.ComplaintThemes",
                table: "ComplaintThemes",
                column: "ComplaintQuestionId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_dbo.ArticleVisits",
                table: "ArticleVisits",
                column: "VisitID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_dbo.ArticleRelateds",
                table: "ArticleRelateds",
                column: "RelatedID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_dbo.ArticleComments",
                table: "ArticleComments",
                column: "CommantID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_dbo.ArticleCategories",
                table: "ArticleCategories",
                column: "CategoryID");

            migrationBuilder.CreateTable(
                name: "CacheSettings",
                columns: table => new
                {
                    CacheID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ActionName = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    CacheDuration = table.Column<int>(type: "int", nullable: false),
                    ControllerName = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    Name = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    Title = table.Column<string>(type: "nvarchar(300)", maxLength: 300, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dbo.CacheSettings", x => x.CacheID);
                });

            migrationBuilder.CreateTable(
                name: "DiscontCodes",
                columns: table => new
                {
                    DisCountCodeId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Code = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreateOn = table.Column<DateTime>(type: "datetime", nullable: false),
                    ExpireTime = table.Column<DateTime>(type: "datetime", nullable: true),
                    ThemeId = table.Column<int>(type: "int", nullable: true),
                    UsageCount = table.Column<int>(type: "int", nullable: false),
                    Value = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dbo.DiscontCodes", x => x.DisCountCodeId);
                    table.ForeignKey(
                        name: "FK_dbo.DiscontCodes_dbo.Themes_ThemeId",
                        column: x => x.ThemeId,
                        principalTable: "Themes",
                        principalColumn: "ThemeID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "EmailSettings",
                columns: table => new
                {
                    EmailID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DisplayName = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    EnableSSL = table.Column<bool>(type: "bit", nullable: false),
                    From = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    IsDefaultEmail = table.Column<bool>(type: "bit", nullable: false),
                    Password = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    Port = table.Column<int>(type: "int", nullable: false),
                    SMTP = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    UserName = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dbo.EmailSettings", x => x.EmailID);
                });

            migrationBuilder.CreateTable(
                name: "LinkedIns",
                columns: table => new
                {
                    LinkID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Description = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true),
                    EndDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    StartDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    target = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Title = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    url = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dbo.LinkedIns", x => x.LinkID);
                });

            migrationBuilder.CreateTable(
                name: "NewsLatterSites",
                columns: table => new
                {
                    NewsLetterID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Email = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dbo.NewsLatterSites", x => x.NewsLetterID);
                });

            migrationBuilder.CreateTable(
                name: "Pages",
                columns: table => new
                {
                    PageID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AuthorID = table.Column<int>(type: "int", nullable: false),
                    BrowserTitle = table.Column<string>(type: "nvarchar(300)", maxLength: 300, nullable: true),
                    CreateDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    EndDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    IsCommentActive = table.Column<bool>(type: "bit", nullable: false),
                    IsDelete = table.Column<bool>(type: "bit", nullable: false),
                    PageImage = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PageText = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    PageTitle = table.Column<string>(type: "nvarchar(400)", maxLength: 400, nullable: false),
                    PageUrl = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: false),
                    ShortDescription = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: false),
                    StartDate = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Pages", x => x.PageID);
                    table.ForeignKey(
                        name: "FK_dbo.Pages_dbo.Users_AuthorID",
                        column: x => x.AuthorID,
                        principalTable: "Users",
                        principalColumn: "UserID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "RolePersmissions",
                columns: table => new
                {
                    RolePermissionID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PermissionID = table.Column<int>(type: "int", nullable: false),
                    RoleID = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dbo.RolePersmissions", x => x.RolePermissionID);
                    table.ForeignKey(
                        name: "FK_dbo.RolePersmissions_dbo.Permissions_PermissionID",
                        column: x => x.PermissionID,
                        principalTable: "Permissions",
                        principalColumn: "PermissionID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_dbo.RolePersmissions_dbo.Roles_RoleID",
                        column: x => x.RoleID,
                        principalTable: "Roles",
                        principalColumn: "RoleID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SiteAbouUs",
                columns: table => new
                {
                    AboutID = table.Column<int>(type: "int", nullable: false),
                    BrowserTitle = table.Column<string>(type: "nvarchar(300)", maxLength: 300, nullable: false),
                    ShortDescription = table.Column<string>(type: "nvarchar(700)", maxLength: 700, nullable: false),
                    Text = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Title = table.Column<string>(type: "nvarchar(400)", maxLength: 400, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dbo.SiteAbouUs", x => x.AboutID);
                });

            migrationBuilder.CreateTable(
                name: "SiteSettings",
                columns: table => new
                {
                    SettingID = table.Column<int>(type: "int", nullable: false),
                    Address = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CopyRight = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LogoName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    Map = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SiteDescription = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: false),
                    SiteDomain = table.Column<string>(type: "nvarchar(300)", maxLength: 300, nullable: true),
                    SiteEmail = table.Column<string>(type: "nvarchar(300)", maxLength: 300, nullable: false),
                    SiteFax = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true),
                    SiteMobile = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true),
                    SiteName = table.Column<string>(type: "nvarchar(300)", maxLength: 300, nullable: false),
                    SiteTell = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: true),
                    SiteTitle = table.Column<string>(type: "nvarchar(360)", maxLength: 360, nullable: false),
                    SupportEmail = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: true),
                    SupportTell = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true),
                    UserWalletOwenrId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dbo.SiteSettings", x => x.SettingID);
                });

            migrationBuilder.CreateTable(
                name: "SiteStaticTexts",
                columns: table => new
                {
                    Name = table.Column<string>(type: "nvarchar(128)", maxLength: 128, nullable: false),
                    Text = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dbo.SiteStaticTexts", x => x.Name);
                });

            migrationBuilder.CreateTable(
                name: "SocialNetworks",
                columns: table => new
                {
                    SocialID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IconName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Title = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dbo.SocialNetworks", x => x.SocialID);
                });

            migrationBuilder.CreateTable(
                name: "UserBankRejectMessages",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Message = table.Column<string>(type: "nvarchar(1000)", maxLength: 1000, nullable: false),
                    SentDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    UserBankId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserBankRejectMessages", x => x.Id);
                    table.ForeignKey(
                        name: "FK_dbo.UserBankRejectMessages_dbo.UserBanks_UserBankId",
                        column: x => x.UserBankId,
                        principalTable: "UserBanks",
                        principalColumn: "UserBankId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "VisitSites",
                columns: table => new
                {
                    VisitID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Date = table.Column<DateTime>(type: "datetime", nullable: false),
                    Ip = table.Column<string>(type: "nvarchar(60)", maxLength: 60, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dbo.VisitSites", x => x.VisitID);
                });

            migrationBuilder.CreateTable(
                name: "WalletTransactionTypes",
                columns: table => new
                {
                    TransactionTypeID = table.Column<int>(type: "int", nullable: false),
                    TransactionTitle = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dbo.WalletTransactionTypes", x => x.TransactionTypeID);
                });

            migrationBuilder.CreateTable(
                name: "WalletTypes",
                columns: table => new
                {
                    TypeID = table.Column<int>(type: "int", nullable: false),
                    TypeTitle = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dbo.WalletTypes", x => x.TypeID);
                });

            migrationBuilder.CreateTable(
                name: "DiscountCodeUses",
                columns: table => new
                {
                    DiscountCodeUseId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DiscontId = table.Column<int>(type: "int", nullable: false),
                    OrderId = table.Column<int>(type: "int", nullable: false),
                    UserId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DiscountCodeUses", x => x.DiscountCodeUseId);
                    table.ForeignKey(
                        name: "FK_dbo.DiscountCodeUses_dbo.DiscontCodes_DiscontId",
                        column: x => x.DiscontId,
                        principalTable: "DiscontCodes",
                        principalColumn: "DisCountCodeId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_dbo.DiscountCodeUses_dbo.Orders_OrderId",
                        column: x => x.OrderId,
                        principalTable: "Orders",
                        principalColumn: "OrderID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_dbo.DiscountCodeUses_dbo.Users_UsedBy_UserID",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "UserID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PageComments",
                columns: table => new
                {
                    CommantID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Comment = table.Column<string>(type: "nvarchar(800)", maxLength: 800, nullable: false),
                    CreateDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    IsDelete = table.Column<bool>(type: "bit", nullable: false),
                    IsRead = table.Column<bool>(type: "bit", nullable: false),
                    PageID = table.Column<int>(type: "int", nullable: false),
                    ParentID = table.Column<int>(type: "int", nullable: true),
                    UserID = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dbo.PageComments", x => x.CommantID);
                    table.ForeignKey(
                        name: "FK_dbo.PageComments_dbo.PageComments_ParentID",
                        column: x => x.ParentID,
                        principalTable: "PageComments",
                        principalColumn: "CommantID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_dbo.PageComments_dbo.Pages_PageID",
                        column: x => x.PageID,
                        principalTable: "Pages",
                        principalColumn: "PageID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_dbo.PageComments_dbo.Users_UserID",
                        column: x => x.UserID,
                        principalTable: "Users",
                        principalColumn: "UserID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PageTags",
                columns: table => new
                {
                    TagID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PageID = table.Column<int>(type: "int", nullable: false),
                    TagTitle = table.Column<string>(type: "nvarchar(300)", maxLength: 300, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dbo.PageTags", x => x.TagID);
                    table.ForeignKey(
                        name: "FK_dbo.PageTags_dbo.Pages_PageID",
                        column: x => x.PageID,
                        principalTable: "Pages",
                        principalColumn: "PageID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PageVisits",
                columns: table => new
                {
                    VisitID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IP = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    PageID = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dbo.PageVisits", x => x.VisitID);
                    table.ForeignKey(
                        name: "FK_dbo.PageVisits_dbo.Pages_PageID",
                        column: x => x.PageID,
                        principalTable: "Pages",
                        principalColumn: "PageID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SiteSocialNetworks",
                columns: table => new
                {
                    NetworkID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    SocialID = table.Column<int>(type: "int", nullable: false),
                    Url = table.Column<string>(type: "nvarchar(360)", maxLength: 360, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dbo.SiteSocialNetworks", x => x.NetworkID);
                    table.ForeignKey(
                        name: "FK_dbo.SiteSocialNetworks_dbo.SocialNetworks_SocialID",
                        column: x => x.SocialID,
                        principalTable: "SocialNetworks",
                        principalColumn: "SocialID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WalletUsers",
                columns: table => new
                {
                    WalletID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreateDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true),
                    IsPay = table.Column<bool>(type: "bit", nullable: false),
                    OrderID = table.Column<int>(type: "int", nullable: true),
                    Price = table.Column<int>(type: "int", nullable: false),
                    TransactionTypeID = table.Column<int>(type: "int", nullable: false),
                    TypeID = table.Column<int>(type: "int", nullable: false),
                    UserID = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dbo.WalletUsers", x => x.WalletID);
                    table.ForeignKey(
                        name: "FK_dbo.WalletUsers_dbo.Orders_OrderID",
                        column: x => x.OrderID,
                        principalTable: "Orders",
                        principalColumn: "OrderID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_dbo.WalletUsers_dbo.Users_UserID",
                        column: x => x.UserID,
                        principalTable: "Users",
                        principalColumn: "UserID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_dbo.WalletUsers_dbo.WalletTransactionTypes_TransactionTypeID",
                        column: x => x.TransactionTypeID,
                        principalTable: "WalletTransactionTypes",
                        principalColumn: "TransactionTypeID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_dbo.WalletUsers_dbo.WalletTypes_TypeID",
                        column: x => x.TypeID,
                        principalTable: "WalletTypes",
                        principalColumn: "TypeID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Owner_UserID",
                table: "TicketMessages",
                column: "Owner_UserID");

            migrationBuilder.CreateIndex(
                name: "IX_User_UserID",
                table: "Ticketings",
                column: "User_UserID");

            migrationBuilder.CreateIndex(
                name: "IX_Theme_ThemeID",
                table: "Themes",
                column: "Theme_ThemeID");

            migrationBuilder.CreateIndex(
                name: "IX_themeRelatedID",
                table: "ThemeRelateds",
                column: "themeRelatedID");

            migrationBuilder.CreateIndex(
                name: "IX_UserID",
                table: "ThemeLikes",
                column: "UserID");

            migrationBuilder.CreateIndex(
                name: "IX_UserID",
                table: "ThemeFavorits",
                column: "UserID");

            migrationBuilder.CreateIndex(
                name: "IX_ParentID",
                table: "ThemeComments",
                column: "ParentID");

            migrationBuilder.CreateIndex(
                name: "IX_UserID",
                table: "ThemeComments",
                column: "UserID");

            migrationBuilder.CreateIndex(
                name: "IX_ParentID",
                table: "ThemeCategories",
                column: "ParentID");

            migrationBuilder.CreateIndex(
                name: "IX_ParentID",
                table: "Permissions",
                column: "ParentID");

            migrationBuilder.CreateIndex(
                name: "IX_UserId",
                table: "DownloadTokens",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_UserId",
                table: "ContactUs",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AuthorID",
                table: "Articles",
                column: "AuthorID");

            migrationBuilder.CreateIndex(
                name: "IX_ParentID",
                table: "ArticleComments",
                column: "ParentID");

            migrationBuilder.CreateIndex(
                name: "IX_UserID",
                table: "ArticleComments",
                column: "UserID");

            migrationBuilder.CreateIndex(
                name: "IX_ParentID",
                table: "ArticleCategories",
                column: "ParentID");

            migrationBuilder.CreateIndex(
                name: "IX_ThemeId",
                table: "DiscontCodes",
                column: "ThemeId");

            migrationBuilder.CreateIndex(
                name: "IX_DiscontId",
                table: "DiscountCodeUses",
                column: "DiscontId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderId",
                table: "DiscountCodeUses",
                column: "OrderId");

            migrationBuilder.CreateIndex(
                name: "IX_UserId",
                table: "DiscountCodeUses",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_PageID",
                table: "PageComments",
                column: "PageID");

            migrationBuilder.CreateIndex(
                name: "IX_ParentID",
                table: "PageComments",
                column: "ParentID");

            migrationBuilder.CreateIndex(
                name: "IX_UserID",
                table: "PageComments",
                column: "UserID");

            migrationBuilder.CreateIndex(
                name: "IX_AuthorID",
                table: "Pages",
                column: "AuthorID");

            migrationBuilder.CreateIndex(
                name: "IX_PageID",
                table: "PageTags",
                column: "PageID");

            migrationBuilder.CreateIndex(
                name: "IX_PageID",
                table: "PageVisits",
                column: "PageID");

            migrationBuilder.CreateIndex(
                name: "IX_PermissionID",
                table: "RolePersmissions",
                column: "PermissionID");

            migrationBuilder.CreateIndex(
                name: "IX_RoleID",
                table: "RolePersmissions",
                column: "RoleID");

            migrationBuilder.CreateIndex(
                name: "IX_SocialID",
                table: "SiteSocialNetworks",
                column: "SocialID");

            migrationBuilder.CreateIndex(
                name: "IX_UserBankId",
                table: "UserBankRejectMessages",
                column: "UserBankId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderID",
                table: "WalletUsers",
                column: "OrderID");

            migrationBuilder.CreateIndex(
                name: "IX_TransactionTypeID",
                table: "WalletUsers",
                column: "TransactionTypeID");

            migrationBuilder.CreateIndex(
                name: "IX_TypeID",
                table: "WalletUsers",
                column: "TypeID");

            migrationBuilder.CreateIndex(
                name: "IX_UserID",
                table: "WalletUsers",
                column: "UserID");

            migrationBuilder.AddForeignKey(
                name: "FK_dbo.AdThemeCategories_dbo.Ads_AdId",
                table: "AdThemeCategories",
                column: "AdId",
                principalTable: "Ads",
                principalColumn: "AdId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_dbo.AdThemeCategories_dbo.ThemeCategories_CategoryId",
                table: "AdThemeCategories",
                column: "CategoryId",
                principalTable: "ThemeCategories",
                principalColumn: "CategoryID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_dbo.ArticleCategories_dbo.ArticleCategories_ParentID",
                table: "ArticleCategories",
                column: "ParentID",
                principalTable: "ArticleCategories",
                principalColumn: "CategoryID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_dbo.ArticleCategories_dbo.ArticleCategories_CategoryID",
                table: "ArticleCategory",
                column: "CategoryID",
                principalTable: "ArticleCategories",
                principalColumn: "CategoryID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_dbo.ArticleCategories_dbo.Articles_ArticleID",
                table: "ArticleCategory",
                column: "ArticleID",
                principalTable: "Articles",
                principalColumn: "ArticleID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_dbo.ArticleComments_dbo.ArticleComments_ParentID",
                table: "ArticleComments",
                column: "ParentID",
                principalTable: "ArticleComments",
                principalColumn: "CommantID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_dbo.ArticleComments_dbo.Articles_ArticleID",
                table: "ArticleComments",
                column: "ArticleID",
                principalTable: "Articles",
                principalColumn: "ArticleID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_dbo.ArticleComments_dbo.Users_UserID",
                table: "ArticleComments",
                column: "UserID",
                principalTable: "Users",
                principalColumn: "UserID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_dbo.ArticleRelateds_dbo.Articles_ArticleID",
                table: "ArticleRelateds",
                column: "ArticleID",
                principalTable: "Articles",
                principalColumn: "ArticleID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_dbo.ArticleRelateds_dbo.Articles_ArticleRelatedID",
                table: "ArticleRelateds",
                column: "ArticleRelatedID",
                principalTable: "Articles",
                principalColumn: "ArticleID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_dbo.Articles_dbo.Users_AuthorID",
                table: "Articles",
                column: "AuthorID",
                principalTable: "Users",
                principalColumn: "UserID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_dbo.ArticleTags_dbo.Articles_ArticleID",
                table: "ArticleTags",
                column: "ArticleID",
                principalTable: "Articles",
                principalColumn: "ArticleID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_dbo.ArticleVisits_dbo.Articles_ArticleID",
                table: "ArticleVisits",
                column: "ArticleID",
                principalTable: "Articles",
                principalColumn: "ArticleID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_dbo.ComplaintThemes_dbo.Complaints_ComplaintId",
                table: "ComplaintThemes",
                column: "ComplaintId",
                principalTable: "Complaints",
                principalColumn: "ComplaintId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_dbo.ComplaintThemes_dbo.Themes_ThemeId",
                table: "ComplaintThemes",
                column: "ThemeId",
                principalTable: "Themes",
                principalColumn: "ThemeID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_dbo.ContactUs_dbo.Users_UserId",
                table: "ContactUs",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "UserID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_dbo.DownloadTokens_dbo.Themes_ThemeId",
                table: "DownloadTokens",
                column: "ThemeId",
                principalTable: "Themes",
                principalColumn: "ThemeID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_dbo.DownloadTokens_dbo.Users_UserId",
                table: "DownloadTokens",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "UserID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_dbo.OrderDetails_dbo.Orders_OrderId",
                table: "OrderDetails",
                column: "OrderId",
                principalTable: "Orders",
                principalColumn: "OrderID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_dbo.OrderDetails_dbo.Themes_ThemeId",
                table: "OrderDetails",
                column: "ThemeId",
                principalTable: "Themes",
                principalColumn: "ThemeID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_dbo.Orders_dbo.Users_UserID",
                table: "Orders",
                column: "UserID",
                principalTable: "Users",
                principalColumn: "UserID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_dbo.Permissions_dbo.Permissions_ParentID",
                table: "Permissions",
                column: "ParentID",
                principalTable: "Permissions",
                principalColumn: "PermissionID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_dbo.ThemeAttachments_dbo.Themes_ThemeID",
                table: "ThemeAttachments",
                column: "ThemeID",
                principalTable: "Themes",
                principalColumn: "ThemeID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_dbo.ThemeCategories_dbo.ThemeCategories_ParentID",
                table: "ThemeCategories",
                column: "ParentID",
                principalTable: "ThemeCategories",
                principalColumn: "CategoryID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_dbo.ThemeChangeStatusMessages_dbo.Themes_ThemeId",
                table: "ThemeChangeStatusMessages",
                column: "ThemeId",
                principalTable: "Themes",
                principalColumn: "ThemeID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_dbo.ThemeComments_dbo.ThemeComments_ParentID",
                table: "ThemeComments",
                column: "ParentID",
                principalTable: "ThemeComments",
                principalColumn: "CommantID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_dbo.ThemeComments_dbo.Themes_ThemeID",
                table: "ThemeComments",
                column: "ThemeID",
                principalTable: "Themes",
                principalColumn: "ThemeID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_dbo.ThemeComments_dbo.Users_UserID",
                table: "ThemeComments",
                column: "UserID",
                principalTable: "Users",
                principalColumn: "UserID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_dbo.ThemeFavorits_dbo.Themes_ThemeID",
                table: "ThemeFavorits",
                column: "ThemeID",
                principalTable: "Themes",
                principalColumn: "ThemeID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_dbo.ThemeFavorits_dbo.Users_UserID",
                table: "ThemeFavorits",
                column: "UserID",
                principalTable: "Users",
                principalColumn: "UserID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_dbo.ThemeGalleries_dbo.Themes_ThemeID",
                table: "ThemeGalleries",
                column: "ThemeID",
                principalTable: "Themes",
                principalColumn: "ThemeID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_dbo.ThemeLikes_dbo.Themes_ThemeID",
                table: "ThemeLikes",
                column: "ThemeID",
                principalTable: "Themes",
                principalColumn: "ThemeID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_dbo.ThemeLikes_dbo.Users_UserID",
                table: "ThemeLikes",
                column: "UserID",
                principalTable: "Users",
                principalColumn: "UserID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_dbo.ThemeOfferTodays_dbo.Themes_ThemeID",
                table: "ThemeOfferTodays",
                column: "ThemeID",
                principalTable: "Themes",
                principalColumn: "ThemeID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_dbo.ThemePreviews_dbo.Themes_ThemeID",
                table: "ThemePreviews",
                column: "ThemeID",
                principalTable: "Themes",
                principalColumn: "ThemeID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_dbo.ThemeRelateds_dbo.Themes_ThemeID",
                table: "ThemeRelateds",
                column: "ThemeID",
                principalTable: "Themes",
                principalColumn: "ThemeID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_dbo.ThemeRelateds_dbo.Themes_themeRelatedID",
                table: "ThemeRelateds",
                column: "themeRelatedID",
                principalTable: "Themes",
                principalColumn: "ThemeID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_dbo.Themes_dbo.Themes_Theme_ThemeID",
                table: "Themes",
                column: "Theme_ThemeID",
                principalTable: "Themes",
                principalColumn: "ThemeID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_dbo.Themes_dbo.Users_SellerID",
                table: "Themes",
                column: "SellerID",
                principalTable: "Users",
                principalColumn: "UserID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_dbo.ThemeSelectedCategories_dbo.ThemeCategories_CategoryID",
                table: "ThemeSelectedCategories",
                column: "CategoryID",
                principalTable: "ThemeCategories",
                principalColumn: "CategoryID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_dbo.ThemeSelectedCategories_dbo.Themes_ThemeID",
                table: "ThemeSelectedCategories",
                column: "ThemeID",
                principalTable: "Themes",
                principalColumn: "ThemeID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_dbo.ThemeTags_dbo.Themes_ThemeID",
                table: "ThemeTags",
                column: "ThemeID",
                principalTable: "Themes",
                principalColumn: "ThemeID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_dbo.ThemeVisits_dbo.Themes_ThemeID",
                table: "ThemeVisits",
                column: "ThemeID",
                principalTable: "Themes",
                principalColumn: "ThemeID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_dbo.Ticketings_dbo.Themes_ThemeId",
                table: "Ticketings",
                column: "ThemeId",
                principalTable: "Themes",
                principalColumn: "ThemeID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_dbo.Ticketings_dbo.Users_User_UserID",
                table: "Ticketings",
                column: "User_UserID",
                principalTable: "Users",
                principalColumn: "UserID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_dbo.TicketMessages_dbo.Ticketings_TicketId",
                table: "TicketMessages",
                column: "TicketId",
                principalTable: "Ticketings",
                principalColumn: "TicketId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_dbo.TicketMessages_dbo.Users_Owner_UserID",
                table: "TicketMessages",
                column: "Owner_UserID",
                principalTable: "Users",
                principalColumn: "UserID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_dbo.UserBanks_dbo.Users_UserId",
                table: "UserBanks",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "UserID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_dbo.UserLogs_dbo.Users_UserID",
                table: "UserLogs",
                column: "UserID",
                principalTable: "Users",
                principalColumn: "UserID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
