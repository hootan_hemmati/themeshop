﻿using Microsoft.AspNetCore.Mvc;

using System.Threading.Tasks;

using ThemeShop.Application.Interfaces;
using ThemeShop.Application.ViewModels.User;

namespace ThemeShop.Api.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class SellerController : ControllerBase
    {
        private readonly IUserService _userService;

        public SellerController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet]
        public async Task<SellerIndexViewModel> GetSellers(byte page = 1, byte take = 20)
        {

            return await _userService.GetAllSellersAsync(page, take);
        }
        [HttpPost]
        public async Task<SellerIndexViewModel> FilterSeller(SellerFilterViewModel Filter, byte page = 1, byte take = 24)
        {
            return await _userService.FilterSellersAsync(page, take, Filter);
        }
        [HttpGet]
        public async Task<SellerViewModel> GetSellerById(int id)
        {
            return await _userService.GetSellerByuserIdIdForConfirmationAsync(id);
        }
        [HttpPost]
        public async Task<bool> ConfirmSeller(int userId, int userBankId)
        {
            return await _userService.ConfirmSellerAsync(userId, userBankId);
        }
        [HttpPost]
        public async Task<bool> RejectSeller(int userId, int userBankId, string message)
        {
            return await _userService.RejectSellerAsync(userId, userBankId, message);
        }
    }
}
