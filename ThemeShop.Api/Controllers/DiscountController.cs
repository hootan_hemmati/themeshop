﻿using Microsoft.AspNetCore.Mvc;

using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using ThemeShop.Application.Interfaces;
using ThemeShop.Application.RequestModel;
using ThemeShop.Domain.Models;

namespace ThemeShop.Api.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class DiscountController : ControllerBase
    {
        private readonly IDiscountService _discountService;

        public DiscountController(IDiscountService discountService)
        {
            _discountService = discountService;
        }

        [HttpGet]
        public async Task<Tuple<IList<Discounts>, int>> GetDiscounts(byte page = 1)
        {
            return new Tuple<IList<Discounts>, int>(
                await _discountService.GetDiscountsAsync(page),
                await _discountService.GetDiscountCountAsync());
        }

        [HttpGet]
        public async Task<Tuple<IList<Discounts>, int>> FilterDiscountAsync(FilterDiscount filter, byte page = 1)
        {
            return await _discountService.GetDiscountsAsync(filter, page);
        }

        [HttpPost]
        public async Task<bool> InsertDiscount(DiscountRequst discount)
        {
            return await _discountService.AddDiscountAsync(discount);
        }

        [HttpPut]
        public async Task<bool> UpdateDiscount(DiscountRequst discount)
        {
            return await _discountService.UpdateDiscountAsync(discount);
        }

        [HttpDelete]
        public async Task<bool> DeleteDiscount(int discountId, int userId)
        {
            return await _discountService.DeleteDiscountAsync(discountId, userId);
        }
    }
}
