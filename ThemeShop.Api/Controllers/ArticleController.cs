﻿using Microsoft.AspNetCore.Mvc;

using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using ThemeShop.Application.Interfaces;
using ThemeShop.Application.RequestModel;
using ThemeShop.Application.ViewModels.Article;
using ThemeShop.Domain.Models;

namespace ThemeShop.Api.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ArticleController : ControllerBase
    {
        private readonly IArticelService _articelService;

        public ArticleController(IArticelService articelService)
        {
            _articelService = articelService;
        }

        #region Article Category
        [HttpGet]
        public async Task<MainArticleCategories> GetArticleCategoy(int categoryId)
        {
            return await _articelService.GetArticleCategoryAsync(categoryId);
        }

        [HttpPost]
        public async Task<bool> InsertArticleCategory(ArticleCategoryRequest articleCategory)
        {
            return await _articelService.CreateArticleCategoryAsync(articleCategory);
        }

        [HttpPost]
        public async Task<bool> UpdateArticleCategory(ArticleCategoryRequest articleCategories)
        {
            return await _articelService.UpdateArticleCategoryAsync(articleCategories);
        }

        [HttpDelete]
        public async Task<bool> DeleteArticleCategory(int id)
        {
            return await _articelService.DeleteArticleCategoryAsync(id);
        }

        [HttpDelete]
        public async Task<bool> DeleteMainArticleCategory(int id)
        {
            return await _articelService.DeleteMainArticleCategoryAsync(id);
        }

        [HttpGet]
        public async Task<IList<MainArticleCategories>> GetAllCategories()
        {
            return await _articelService.GetAllArticleCategoriesAsync();
        }

        #endregion Article Category

        #region Article
        [HttpGet]
        public async Task<Tuple<IList<ListArticleViewModel>, int>> GetArticles(byte page = 1)
        {
            return Tuple.Create(
               await _articelService.GetArticlesAsync(page),
              await _articelService.GetArticleCountAsync());
        }

        [HttpPost]
        public async Task<Tuple<IList<ListArticleViewModel>, int>> GetArticleAsync(FilterArticle filter, byte page = 1)
        {
            return await _articelService.GetArticlesAsync(filter, page);
        }

        [HttpPost]
        public async Task<bool> CreateArticle(ArticleRequest article)
        {
            return await _articelService.CreateAricleAsync(article);
        }

        [HttpGet]
        public async Task<Articles> GetArticle(int id)
        {
            return await _articelService.GetArticleAsync(id);
        }

        [HttpPost]
        public async Task<bool> UpdateArticle(ArticleRequest articles)
        {
            return await _articelService.UpdateArticleAsync(articles);
        }

        [HttpDelete]
        public async Task<bool> RemoveArticle(int id)
        {
            return await _articelService.RemoveArticleAsync(id);
        }

        [HttpGet]
        public async Task<bool> ActivateArticle(int id)
        {
            return await _articelService.ActivateArticleAsync(id);
        }

        [HttpDelete]
        public async Task<bool> DeActivateArticle(int id)
        {
            return await _articelService.DeActivateArticleAsync(id);
        }
        #endregion

        #region Aerticle Related
        [HttpGet]
        public async Task<IList<ArticleRelatedViewModel>> GetArticleRelated(int articleId)
        {
            return await _articelService.GetArticleRelatedsAsync(articleId);
        }

        [HttpPost]
        public async Task<bool> AddArticleRelated(ArticleRelateds articleRelated)
        {
            return await _articelService.AddArticleRelatedAsync(articleRelated);
        }

        [HttpDelete]
        public async Task<bool> DeleteArticleRelated(int id)
        {
            return await _articelService.DeleteArticleRelatedAsync(id);
        }
        #endregion

        #region Article Comments
        [HttpGet]
        public async Task<IList<ArticleComments>> GetUnreadComments()
        {
            return await _articelService.GetUnreadsCommentsAsync();
        }

        [HttpGet]
        public async Task<ArticleComments> GetArticleComment(int id)
        {
            return await _articelService.GetArticleCommentsAsync(id);
        }

        [HttpDelete]
        public async Task<bool> DeleteArticleComment(int id)
        {
            return await _articelService.RemoveArticleCommentAsync(id);
        }

        [HttpGet]
        public async Task<IList<ArticleComments>> GetArticleComments(int articleId)
        {
            return await _articelService.GetArticleComments(articleId);
        }
        #endregion
    }
}
