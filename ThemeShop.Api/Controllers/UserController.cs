﻿using Microsoft.AspNetCore.Mvc;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using ThemeShop.Application.Interfaces;
using ThemeShop.Domain.Models;

namespace ThemeShop.Api.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IPermissionService _permissionService;
        private readonly IRoleService _roleService;
        private readonly ITicketService _ticketService;
        public UserController(IUserService userService, IPermissionService permissionService, IRoleService roleService, ITicketService ticketService)
        {
            _userService = userService;
            _permissionService = permissionService;
            _roleService = roleService;
            _ticketService = ticketService;
        }
        #region Users
        [HttpGet]
        public async Task<IList<Users>> GetAllUsersAsync(int skip, int take = 30)
        {
            return await _userService.GetAllUsersAsync(skip, take);
        }

        [HttpGet]
        public async Task<Users> GetUserByIdAsync(int userId)
        {
            return await _userService.GetUserByIdAsync(userId);
        }

        [HttpGet]
        public async Task<int> GetAllUsersCountAsync()
        {
            return await _userService.GetAllUsersCountAsync();
        }

        [HttpPut]
        public async Task DeActiveUserAsync(UserRequest userRequest)
        {
            await _userService.DeActiveUserAsync(userRequest.UserId);
        }

        [HttpPut]
        public async Task ActiveUserAsync(UserRequest userRequest)
        {
            await _userService.ActiveUserAccountAsync(userRequest.UserId);
        }

        [HttpDelete]
        public async Task DeleteUserAsync(int userId)
        {
            await _userService.DeleteUserAsync(userId);
        }

        [HttpGet]
        public async Task<IList<Users>> FilterUsersAsync(string query, int pageId = 1, int userSearchMode = 0, int userStatusEnum = 0)
        {
            IList<Users> users = await _userService.FilterUsersAsync(query, userSearchMode, userStatusEnum);
            return users.Skip(pageId - 1).Take(30).ToList();
        }

        [HttpGet]
        public async Task<int> FilterUsersCountAsync(string query, int userSearchMode = 0, int userStatusEnum = 0)
        {
            return await _userService.FilterUsersCountAsync(query, userSearchMode, userStatusEnum);
        }

        [HttpPut]
        public void UpdateUser(Users user)
        {
            _userService.UpdateUser(user);
        }

        #endregion

        #region OnlineUsers
        [HttpGet]
        public async Task<OnlineUsers> GetOnlineUsersAsync(string userName = "", string ip = "")
        {
            return await _userService.GetOnlineUserAsync(userName, ip);
        }

        [HttpGet]
        public async Task<IList<OnlineUsers>> GetAllOnlineUsersAsync()
        {
            return await _userService.GetTodayOnlineUsersAsync();
        }

        [HttpPut]
        public async Task UpdateOnlineUserAsync(OnlineUsers onlineUser)
        {
            _userService.UpdateOnlineUserInformation(onlineUser);
            await _userService.SaveAsync();
        }

        #endregion

        #region UserRoles
        [HttpGet]
        public async Task<IList<UserRoles>> GetUserRolesByUserIdAsync(int userId)
        {
            return await _roleService.GetUserRolesByUserIdAsync(userId);
        }

        [HttpPost]
        public async Task AddUserRoleAsync(UserRequest userRequest)
        {
            await _roleService.AddUserRoleAsync(userRequest.UserId, userRequest.RoleId);
        }

        [HttpDelete]
        public async Task RemoveUserRoleAsync(UserRequest userRequest)
        {
            await _roleService.RemoveUserRoleAsync(userRequest.UserId, userRequest.RoleId);
        }

        [HttpDelete]
        public async Task RemoveAllUserRoleByUserIdAsync(int userId)
        {
            await _roleService.RemoveAllUserRoleByUserIdAsync(userId);
        }
        #endregion

    }
}
