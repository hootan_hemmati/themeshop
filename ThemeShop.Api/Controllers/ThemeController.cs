﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using ThemeShop.Application.Interfaces;
using ThemeShop.Application.RequestModel;
using ThemeShop.Application.ViewModels.Order;
using ThemeShop.Application.ViewModels.Theme;
using ThemeShop.Domain.Models;

namespace ThemeShop.Api.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ThemeController : ControllerBase
    {
        private readonly IThemeService _themeService;
        private readonly IOrderService _orderService;

        public ThemeController(IThemeService themeService, IOrderService orderService)
        {
            _themeService = themeService;
            _orderService = orderService;

        }


        #region Theme Offer


        [HttpGet]
        public async Task<Tuple<IList<ThemeOfferTodays>, int>> GetOffer(int page = 1)
        {
            return await _themeService.GetAllThemeOfferAsync(page);
        }

        [HttpPost]
        public async Task<Tuple<IList<ThemeOfferTodays>, int>> GetThemeOfferByTime(DateTime date, int page = 1)
        {
            return await _themeService.GetThemeOfferAsync(page, date);
        }

        [HttpPost]
        public async Task<bool> AddThemeOffer(ThemeOfferTodays themeOffer)
        {
            themeOffer.VisitCount = 0;

            return await _themeService.AddThemeOfferAsync(themeOffer);
        }

        [HttpGet]
        public async Task RemoveThemeOffer(int themeOfferId)
        {
            await _themeService.DeleteThemeOfferAsync(themeOfferId);
        }

        [HttpGet]
        public async Task ChangeThemeOfferState(int themeOfferId)
        {
            await _themeService.ChangeThemeOfferStateAsync(themeOfferId);
        }
        #endregion

        #region Theme Categories
        [HttpGet]
        public async Task<IList<ThemeCategories>> GetThemeCategoriesAsync()
        {
            return await _themeService.GetThemeCategoriesAsync();
        }
        [HttpPost]
        public async Task AddThemeCategory(ThemeCategories category)
        {
            await _themeService.AddCategoriesAsync(category);
        }
        [HttpGet]
        public async Task<ThemeCategories> GetThemeCategory(int categoyId)
        {
            return await _themeService.GetThemeCategoryAsync(categoyId);
        }
        [HttpPost]
        public async Task EditThemeCategory(ThemeCategories category)
        {
            await _themeService.EditThemeCategory(category);
        }
        [HttpGet]
        public async Task DeleteThemeCategory(int themeCategoryId)
        {
            await _themeService.DeleteCategories(themeCategoryId);
        }
        #endregion

        #region Theme 
        [HttpPost]
        public async Task<Tuple<IList<ListThemeViewModel>, int>> GetThemeAsync(ThemeRequest request, int page = 1)
        {
            // the user's userId
            //var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);

            return await _themeService.GetThemeAsync(page, request.Title, request.SellerId, request.CategoryId, request.IsAdmintheme, request.Status);
        }
        [HttpGet]
        public async Task RejectTheme(int id, string message)
        {
            await _themeService.RejectThemeAsync(id, message);
        }
        [HttpGet]
        public async Task<IList<ThemeChangeStatusMessages>> GetThemeMessagesAsync(int id)
        {
            return await _themeService.GetThemeChangeStatusMessagesAsync(id);
        }
        [HttpGet]
        //#TODO
        public Task EditTheme()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Theme Comments
        [HttpGet]
        public async Task<Tuple<IList<ThemeComments>, int>> GetUnreadComments(int page = 1, byte take = 24)
        {
            return await _themeService.GetUnreadCommentsAsync(page, take);
        }
        [HttpGet]
        public async Task DeleteComment(int id)
        {
            await _themeService.DeleteThemeCommentAsync(id);
        }
        [HttpGet]
        public async Task<AdminThemeViewModel> ShowComment(int id)
        {
            return await _themeService.GetAdminThemeAsync(id);
        }
        #endregion

        #region Theme Preview
        [HttpGet]
        public async Task<Tuple<IList<ThemePreviews>, int>> ThemePreview(int id, byte page = 1, byte take = 24)
        {
            return await _themeService.GetThemePreviewsAsync(id, page, take);
        }
        [HttpPost]
        public async Task<bool> ThemePreview(ThemePreviews preview)
        {
            return await _themeService.InsertThemePreviewAsync(preview);


        }
        [HttpGet]
        public async Task<bool> DefaultPreview(int id)
        {
            return await _themeService.InsertDefultPreviewPageAsync(id);
        }
        [HttpGet]
        public async Task ChangePreviewState(int id)
        {
            await _themeService.ChangeThemePreviewStateAsync(id);
        }
        [HttpGet]

        public async Task DeletePreview(int id)
        {
            await _themeService.DeleteThemePreviewAsync(id);
        }
        #endregion

        #region Theme Sale
        [HttpGet]
        public async Task<Tuple<List<UserPaymentViewModel>, int>> GetSaleReport(int id, byte page = 1, byte take = 24)
        {
            return await _orderService.GetUserPaymentsAsync(id, page, take);
        }
        #endregion

        #region Theme Related
        [HttpGet]
        public async Task<Tuple<IList<ThemeRelatedViewModel>, int>> ListRelatedTheme(int id, byte page, byte take)
        {
            return await _themeService.GetThemeRelatedForAdminAsync(id, page, take);
        }

        [HttpPost]
        public async Task AddRelatedTheme(ThemeRelateds theme)
        {
            await _themeService.AddRelatedThemeAsync(theme);
        }

        [HttpGet]
        public async Task DeleteRelatedTheme(int id)
        {
            await _themeService.DeleteRelatedThemeById(id);
        }

        #endregion

        #region Theme Galleries

        [HttpGet]
        public async Task<Themes> ThemeGallery(int id)
        {
            return await _themeService.GetThemeAsync(id);
        }

        [HttpPost]
        public async Task<string> AddThemeGalley(ThemeGalleries themeGallery, IFormFile themeImage)
        {
            return await _themeService.AddThemeGalleryAsync(themeGallery, themeImage);
        }

        //[Authorize(Roles = "EditTheme")]
        [HttpGet]
        public async Task<IList<ThemeGalleries>> GetThemeGaleries(int id)
        {
            return await _themeService.GetThemeGalleriesAsync(id);
        }

        //[Authorize(Roles = "EditTheme")]
        [HttpGet]
        public async Task DeleteThemeGallery(int id)
        {
            await _themeService.DeleteThemeGallerieAsync(id);
        }
        #endregion

        #region Theme Attachments

        //[Authorize(Roles = "EditTheme")]
        [HttpGet]
        public async Task<IList<ThemeAttachments>> ThemeListAttachment(int id)
        {
            return await _themeService.GetThemeAttachmentsAsync(id);
        }

        //[Authorize(Roles = "EditTheme")]
        [HttpPost]
        public async Task<string> CreateThemeAttachment(ThemeAttachments themeAttachment, IFormFile attachmentFile)
        {
            return await _themeService.AddThemeAttachmentAsync(attachmentFile, themeAttachment);
        }

        //[Authorize(Roles = "EditTheme")]
        [HttpGet]
        public async Task<string> DeleteThemeAttachment(int id)
        {
            return await _themeService.DeleteThemeAttachmentAsync(id);
        }

        //[Authorize(Roles = "EditTheme")]
        [HttpGet]
        public async Task<bool> ChangeStateAttachment(int id)
        {
            return await _themeService.ChangeThemeAttachmentStateAsync(id);
        }

        //[Authorize(Roles = "EditTheme")]
        [HttpGet]
        public async Task<bool> ChangeStateFreeAttachment(int id)
        {
            return await _themeService.ChangeThemeAttachmentIsFreeAsync(id);
        }

        #endregion
    }
}
