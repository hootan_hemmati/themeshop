﻿using Microsoft.AspNetCore.Mvc;

using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using ThemeShop.Application.Interfaces;
using ThemeShop.Application.RequestModel;
using ThemeShop.Application.ViewModels.Wallet;

namespace ThemeShop.Api.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class WalletController : ControllerBase
    {
        private readonly IWalletService _walletService;

        public WalletController(IWalletService walletService)
        {
            _walletService = walletService;
        }

        [HttpGet]
        public async Task<Tuple<IList<WalletViewModel>, int>> GetPaymentTransActionAsync(int pageId = 1)
        {
            return new Tuple<IList<WalletViewModel>, int>(
           await _walletService.GetAllPaymentTransactionAsync(pageId),
          await _walletService.GetAllPaymentCountAsync());
        }

        [HttpGet]
        public async Task<Tuple<IList<WalletViewModel>, int>> GetBuyTransactionAsync(int pageId = 1)
        {
            return new Tuple<IList<WalletViewModel>, int>(
               await _walletService.GetAllBuyTransactionAsync(pageId),
             await _walletService.GetAllBuyTransactionCountAsync());
        }

        [HttpGet]
        public async Task<Tuple<IList<WalletViewModel>, int>> FilterTransactionAsync([FromQuery] FilterTransaction filter, int pageId)
        {
            return await _walletService.GetTransactionAsync(pageId, filter);
        }
    }
}
