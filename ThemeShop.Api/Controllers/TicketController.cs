﻿using Microsoft.AspNetCore.Mvc;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using ThemeShop.Application.Interfaces;
using ThemeShop.Domain.Models;

namespace ThemeShop.Api.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class TicketController : ControllerBase
    {
        private readonly ITicketService _ticketService;
        private readonly IContactUsService _contactUsService;
        private readonly IReportAbuseService _reportAbuseService;

        public TicketController(ITicketService ticketService, IContactUsService contactUsService, IReportAbuseService reportAbuseService)
        {
            _ticketService = ticketService;
            _contactUsService = contactUsService;
            _reportAbuseService = reportAbuseService;
        }

        #region Ticketing

        [HttpGet]
        public async Task<IList<Ticketings>> GetAllTicketsAsync(int pageId = 1, int take = 30)
        {
            IList<Ticketings> ticket = await _ticketService.GetAllTicketsAsync();
            return ticket
                .Distinct().
                OrderByDescending(ticket => !ticket.IsAnswered).Skip((pageId - 1) * take).Take(take).ToList();
        }

        [HttpGet]
        public async Task<int> GetAllTicketsCountAsync()
        {
            return await _ticketService.GetAllTicketsCountAsync();
        }

        [HttpGet]
        public async Task<IList<Ticketings>> FilterTicketsAsync(int pageId, string query = "", int take = 30)
        {
            return await _ticketService.FilterTicketsAsync(query, pageId, take);
        }

        [HttpGet]
        public async Task<int> FilterTicketsCountAsync(string query = "")
        {
            return await _ticketService.FilterTicketsCountAsync(query);
        }

        [HttpGet]
        public async Task<Ticketings> GetTicketByIdAsync(int ticketId)
        {
            return await _ticketService.GetTicketByIdAsync(ticketId);
        }

        [HttpDelete]
        public async Task DeleteTicketAsync(int ticketId)
        {
            await _ticketService.DeleteTicketAsync(ticketId);
        }

        [HttpPut]
        public async Task RecoverTicketAsync(Ticketings ticket)
        {
            await _ticketService.RecoverTicketAsync(ticket);
        }

        [HttpPut]
        public async Task CloseTicketAsync(Ticketings ticket)
        {
            await _ticketService.CloseTicketAsync(ticket);
        }

        [HttpPut]
        public async Task OpenTicketAsync(Ticketings ticket)
        {
            await _ticketService.OpenTicketAsync(ticket);
        }

        #endregion Ticketing

        #region NotReadedTickets

        [HttpGet]
        public async Task<IList<Ticketings>> GetNotReadedTicketsAsync()
        {
            return await _ticketService.GetNotReadedTicketsAsync();
        }

        [HttpGet]
        public async Task<int> GetNotReadedTicketsCountAsync()
        {
            return await _ticketService.GetNotReadedTicketsCountAsync();
        }

        [HttpGet]
        public async Task<IList<Ticketings>> FilterNotReadedTickets(int skip, string query = "", int take = 30)
        {
            return await _ticketService.FilterNotReadedTicketsAsync(query, skip, take);
        }

        [HttpGet]
        public async Task<int> FilterNotReadedTicketsCountAsync(string query)
        {
            return await _ticketService.FilterNotReadedTicketsCountAsync(query);
        }

        #endregion NotReadedTickets

        #region SentTickets

        [HttpGet]
        public async Task<IList<Ticketings>> GetSentTicketsAsync()
        {
            return await _ticketService.GetSentTicketsAsync();
        }

        [HttpGet]
        public async Task<int> GetSentTicketsCountAsync()
        {
            return await _ticketService.GetSentTicketsCountAsync();
        }

        [HttpGet]
        public async Task<IList<Ticketings>> FilterSentTicketsAsync(int skip, string query = "", int take = 30)
        {
            return await _ticketService.FilterSentTicketsAsync(query, skip, take);
        }

        [HttpGet]
        public async Task<int> FilterSentTicketsCountAsync(string query)
        {
            return await _ticketService.FilterSentTicketsCountAsync(query);
        }

        #endregion SentTickets

        #region DeletedTickets

        [HttpGet]
        public async Task<IList<Ticketings>> GetDeletedTicketsAsync()
        {
            return await _ticketService.GetDeletedTicketsAsync();
        }

        [HttpGet]
        public async Task<int> GetDeletedTicketsCountAsync()
        {
            return await _ticketService.GetDeletedTicketsCountAsync();
        }

        [HttpGet]
        public async Task<IList<Ticketings>> FilterDeletedTicketsAsync(int skip, string query = "", int take = 30)
        {
            return await _ticketService.FilterDeletedTicketsAsync(query, skip, take);
        }

        [HttpGet]
        public async Task<int> FilterDeletedTicketsCountAsync(string query)
        {
            return await _ticketService.FilterDeletedTicketsCountAsync(query);
        }

        #endregion DeletedTickets

        #region ClosedTickets

        [HttpGet]
        public async Task<IList<Ticketings>> GetClosedTicketsAsync()
        {
            return await _ticketService.GetClosedTicketsAsync();
        }

        [HttpGet]
        public async Task<int> GetClosedTicketsCountAsync()
        {
            return await _ticketService.GetClosedTicketsCountAsync();
        }

        [HttpGet]
        public async Task<IList<Ticketings>> FilterClosedTicketsAsync(int skip, string query = "", int take = 30)
        {
            return await _ticketService.FilterClosedTicketsAsync(query, skip, take);
        }

        [HttpGet]
        public async Task<int> FilterClosedTicketsCountAsync(string query)
        {
            return await _ticketService.FilterClosedTicketsCountAsync(query);
        }

        #endregion ClosedTickets

        #region TicketMessage

        [HttpGet]
        public async Task<IList<TicketMessages>> GetTicketMessagesAsync(int ticketId)
        {
            return await _ticketService.GetTicketMessagesAsync(ticketId);
        }

        [HttpGet]
        public async Task<bool> IsTicketReadOrNotAsync(int ticketId)
        {
            return await _ticketService.IsTicketReadOrNotAsync(ticketId);
        }

        [HttpGet]
        public async Task<bool> ReadAllTicketMessagesAsync(int ticketId)
        {
            await _ticketService.ReadAllTicketMessagesAsync(ticketId);
            return true;
        }

        [HttpPost]
        public async Task SendTicketMessageByAdminAsync(TicketMessages ticketMessage)
        {
            await _ticketService.SendTicketMessageByAdminAsync(ticketMessage);
        }

        #endregion TicketMessage

        #region Contact Us

        [HttpGet]
        public async Task<Tuple<IList<ContactUs>, int>> GetAllContactUsAsync(string query = "", int pageId = 1)
        {
            return await _contactUsService.GetContactUsAsync(query, pageId);
        }

        [HttpGet]
        public async Task<Tuple<IList<ContactUs>, int>> GetUnreadContactUsAsync(string query = "", int pageId = 1)
        {
            return await _contactUsService.GetContactUsNotReadAsync(query, pageId);
        }

        [HttpPost]
        public async Task<bool> AnswerContactUsAsync(ContactUsRequest contactUsRequest)
        {
            return await _contactUsService.AnswerContactAsync(contactUsRequest.ContactId, contactUsRequest.Answer);
        }

        [HttpGet]
        public async Task<ContactUs> GetContactUsAsync(int id)
        {
            return await _contactUsService.GetContactUsAsync(id);
        }

        [HttpPost]
        public async Task<bool> AddContactUsAsync(ContactUs contact)
        {
            return await _contactUsService.AddContactUsAsync(contact);
        }

        #endregion Contact Us

        #region ReportAbuse
        [HttpGet]
        public async Task<Tuple<IList<ReportAbuse>, int>> GetAllReportAbuseAsync(string query = "", int pageId = 1)
        {
            return await _reportAbuseService.GetReportAbuseAsync(query, pageId);
        }

        [HttpGet]
        public async Task<Tuple<IList<ReportAbuse>, int>> GetUnreadReportAbuseAsync(string query = "", int pageId = 1)
        {
            return await _reportAbuseService.GetReportAbuseNotReadAsync(query, pageId);
        }

        [HttpPost]
        public async Task<bool> AnswerReportAbuseAsync(ReportAbuseRequest reportAbuseRequest)
        {
            return await _reportAbuseService.AnswerReportAbuseAsync(reportAbuseRequest.ReportAbuseId, reportAbuseRequest.AnswerText);
        }

        [HttpGet]
        public async Task<ReportAbuse> GetReportAbuseByIdAsync(int id)
        {
            return await _reportAbuseService.GetReportAbuseByIdAsync(id);
        }

        [HttpPost]
        public async Task<bool> AddReportAbuseAsync(ReportAbuse reportAbuse)
        {
            return await _reportAbuseService.AddReportAbuseAsync(reportAbuse);
        }

        [HttpGet]
        public async Task ReadReportAbuseAsync(int id)
        {
            await _reportAbuseService.ReadReportAbuseAsync(id);
        }
        #endregion
    }
}