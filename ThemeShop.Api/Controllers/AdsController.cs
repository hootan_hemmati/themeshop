﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using ThemeShop.Application.Interfaces;
using ThemeShop.Application.RequestModel;
using ThemeShop.Domain.Models;

namespace ThemeShop.Api.Controllers
{

    [Route("api/[controller]/[action]")]
    [ApiController]
    public class AdsController : ControllerBase
    {
        private readonly IAdService _adService;
        public AdsController(IAdService adService)
        {
            _adService = adService;
        }

        [HttpGet]
        public async Task<Tuple<IList<Ads>, int>> GetAdsAsync(byte page = 1)
        {
            return new Tuple<IList<Ads>, int>(
               await _adService.GetAdsAsync(page),
               await _adService.GetAdsCountAsync());
        }

        [HttpGet]
        public async Task<Tuple<IList<Ads>, int>> FilterAdsAsync([FromQuery] FilterAds filter, byte pageId)
        {
            return await _adService.GetAdsAsync(filter, pageId);
        }

        [HttpPost]
        public async Task<bool> InsertAds(AdsRequest ads, IFormFile img)
        {
            return await _adService.CreateAdAsync(ads, img);
        }

        [HttpPut]
        public async Task<bool> EditAds(AdsRequest ads, IFormFile img)
        {
            if (img != null)
            {
                await _adService.EditAdImageAsync(ads.AdId, img);
            }
            return await _adService.EditAdAsync(ads);
        }

        [HttpDelete]
        public async Task DeleteAds(int adId)
        {
            await _adService.RemoveAd(adId);
        }

        [HttpPut]
        public async Task DeactiveAd(int adId)
        {
            await _adService.DeActivateAd(adId);
        }

        [HttpPut]
        public async Task ActivateAd(int adId)
        {
            await _adService.ActivateAd(adId);
        }

        [HttpGet]
        public async Task<Ads> GetAllAd(int adId)
        {
            return await _adService.GetAdsAsync(adId);
        }
    }
}
