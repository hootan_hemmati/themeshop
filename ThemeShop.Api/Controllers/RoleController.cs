﻿using Microsoft.AspNetCore.Mvc;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using ThemeShop.Application.Interfaces;
using ThemeShop.Domain.Models;

namespace ThemeShop.Api.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class RoleController : ControllerBase
    {
        private readonly IPermissionService _permissionService;
        private readonly IRoleService _roleService;

        public RoleController(IPermissionService permissionService, IRoleService roleService)
        {
            _permissionService = permissionService;
            _roleService = roleService;
        }

        #region Roles
        [HttpGet]
        public async Task<IList<Roles>> GetAllRolesAsync(int take = 10, int pageId = 1)
        {
            IList<Roles> roles = await _roleService.GetAllRolesAsync();
            return roles.OrderBy(role => role.RoleTitle).Skip((pageId - 1) * take).Take(take).ToList();
        }

        [HttpGet]
        public async Task<IList<Roles>> GetRolesAsync()
        {
            return await _roleService.GetAllRolesAsync();
        }

        [HttpGet]
        public async Task<IList<Roles>> FilterRolesAsync(int pageId = 1, int take = 10, string query = "")
        {
            return await _roleService.FilterRolesAsync(pageId, take, query);
        }

        [HttpGet]
        public Task<int> FilterRolesCountAsync(string query = "")
        {
            return _roleService.FilterRolesCountAsync(query);
        }

        [HttpGet]
        public async Task<Roles> GetRoleByIdAsync(int roleId)
        {
            return await _roleService.GetRoleByIdAsync(roleId);
        }

        [HttpGet]
        public async Task<int> GetAllRolesCountAsync()
        {
            IList<Roles> roles = await _roleService.GetAllRolesAsync();
            return roles.Count();
        }

        [HttpPost]
        public async Task AddRoleAsync(Roles role)
        {
            await _roleService.AddRoleAsync(role);
        }

        [HttpGet]
        public async Task<Roles> GetRoleByNameAsync(string roleTitle)
        {
            return await _roleService.GetRoleByTitleAsync(roleTitle);
        }

        [HttpDelete]
        public void DeleteRole(Roles role)
        {
            _roleService.DeleteRole(role);
        }

        [HttpDelete]
        public async Task DeleteRoleByIdAsync(int roleId)
        {
            await _roleService.DeleteRoleByIdAsync(roleId);
        }

        [HttpPut]
        public async Task UpdateRoleAsync(Roles role)
        {
            await _roleService.UpdateRoleAsync(role);
        }

        [HttpGet]
        public async Task<bool> IsRoleExistByIdAsync(int roleId)
        {
            return await _roleService.IsRoleExistByIdAsync(roleId);
        }
        #endregion

        #region RolesPermissions
        [HttpPost]
        public async Task AddRolePermissionAsync(PermissionRequest permissionRequest)
        {
            await _roleService.AddRolePermissionAsync(permissionRequest.RoleId, permissionRequest.RolePermissionsId);
        }

        [HttpPut]
        public async Task UpdateRolePermissionAsync(PermissionRequest permissionRequest)
        {
            await _roleService.UpdateRolePermissionAsync(permissionRequest.RoleId, permissionRequest.RolePermissionsId);
        }

        [HttpDelete]
        public async Task RemoveRolePermissionAsync(int roleId)
        {
            await _roleService.RemoveRolePermissionAsync(roleId);
        }

        [HttpGet]
        public async Task<IList<RolePremissions>> GetPremissionsByRoleIdAsync(int roleId)
        {
            return await _roleService.GetPremissionsByRoleIdAsync(roleId);
        }
        #endregion

        #region Permissions
        [HttpGet]
        public async Task<IList<Permissions>> GetAllPermissionsAsync()
        {
            return await _permissionService.GetPermissionsAsync();
        }
        #endregion
    }
}
