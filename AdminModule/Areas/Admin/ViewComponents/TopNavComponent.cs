﻿using Microsoft.AspNetCore.Mvc;

using System.Threading.Tasks;

namespace AdminModule.Areas.Admin.ViewComponents
{
    public class TopNavComponent : ViewComponent
    {
        public TopNavComponent()
        {

        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            return await Task.FromResult((IViewComponentResult)View("TopNavComponent"));
        }
    }
}
