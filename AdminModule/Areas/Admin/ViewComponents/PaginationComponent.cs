﻿using Microsoft.AspNetCore.Mvc;

using System.Threading.Tasks;

using ThemeShop.Application.ViewModels.Public;

namespace AdminModule.Areas.Admin.ViewComponents
{
    public class PaginationComponent : ViewComponent
    {
        private readonly PaggingViewModel _paggingViewModel;
        public PaginationComponent(PaggingViewModel paggingViewModel)
        {
            _paggingViewModel = paggingViewModel;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            return await Task.FromResult((IViewComponentResult)View("PaginationComponent", _paggingViewModel));
        }
    }
}
