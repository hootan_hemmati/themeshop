﻿using Microsoft.AspNetCore.Mvc;

using System.Threading.Tasks;

namespace AdminModule.Areas.Admin.ViewComponents
{
    public class NavSlideBarComponent : ViewComponent
    {
        public NavSlideBarComponent()
        {

        }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            return await Task.FromResult((IViewComponentResult)View("NavSlideBarComponent"));
        }
    }
}
