﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

using ThemeShop.Application.ViewModels.Ticket;
using ThemeShop.Domain.Models;

namespace AdminModule.Areas.Admin.ViewComponents
{
    public class TicketBoxComponent : ViewComponent
    {
        private readonly TicketCountViewModel _ticketCountViewModel;
        private IConfiguration _configuration { get; set; }
        public TicketBoxComponent(TicketCountViewModel ticketCountViewModel, IConfiguration configuration)
        {
            _ticketCountViewModel = ticketCountViewModel;
            _configuration = configuration;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            using (HttpClient httpClient = new HttpClient())
            {
                //TODO:Impediment Contact US And Report Later
                _ticketCountViewModel.NotReadTicketCount = await httpClient.GetFromJsonAsync<int>($"{_configuration.GetSection("ApiUrlAddress").Value}/Ticket/GetNotReadedTicketsCount");
                Tuple<IList<ContactUs>, int> contactUs = await httpClient.GetFromJsonAsync<Tuple<IList<ContactUs>, int>>($"{_configuration.GetSection("ApiUrlAddress").Value}/Ticket/GetUnreadContactUs");
                _ticketCountViewModel.ContactUsTicketCount = contactUs.Item2;
                Tuple<IList<ReportAbuse>, int> reportAbuse = await httpClient.GetFromJsonAsync<Tuple<IList<ReportAbuse>, int>>($"{_configuration.GetSection("ApiUrlAddress").Value}/Ticket/GetUnreadReportAbuse");
                _ticketCountViewModel.ReportAbuseTicketCount = reportAbuse.Item2;
            }
            return await Task.FromResult((IViewComponentResult)View("TicketBoxComponent", _ticketCountViewModel));
        }
    }
}
