﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using System;
using System.IO;
using System.Threading.Tasks;

using ThemeShop.Application.Utilities;

namespace AdminModule.Areas.Admin.Controllers
{
    public class UploadController : Controller
    {
        [Route("/admin/ticket/fileupload")]
        [HttpPost]
        public async Task<IActionResult> UploadImage(IFormFile upload, string CKEditorFuncNum, string CKEditor, string langCode)
        {
            if (upload.Length <= 0)
            {
                return null;
            }

            string fileName = Guid.NewGuid() + Path.GetExtension(upload.FileName).ToLower();



            string path = Path.Combine(SiteDirectories.TicketsImagesImagePhysicalPath(),
                fileName);

            using (FileStream stream = new FileStream(path, FileMode.Create))
            {
                await upload.CopyToAsync(stream);
            }


            string url = $"{$"/{SiteDirectories.TicketsImagesPath.Replace("wwwroot/", "")}/"}{fileName}";


            return new JsonResult(new { uploaded = true, url });
        }
    }
}
