﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;

using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

using ThemeShop.Domain.Models;

namespace AdminModule.Areas.Admin.Pages.Ticket
{
    public class AnswerTicketModel : PageModel
    {
        [TempData]
        public string Message { get; set; }

        public Ticketings Ticket { get; set; }

        public IList<TicketMessages> TicketMessages { get; set; }

        [BindProperty]
        public TicketMessages TicketMessage { get; set; }

        public Users Sender { get; set; }

        public Users Reciver { get; set; }

        public IConfiguration Configuration { get; set; }


        public AnswerTicketModel(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public async Task<IActionResult> OnGetAsync(int ticketId)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                Ticket = await httpClient.GetFromJsonAsync<Ticketings>($"{Configuration.GetSection("ApiUrlAddress").Value}/Ticket/GetTicketById?ticketId={ticketId}");
                if (Ticket != null && Ticket.IsDelete != true)
                {
                    TicketMessages = await httpClient.GetFromJsonAsync<IList<TicketMessages>>($"{Configuration.GetSection("ApiUrlAddress").Value}/Ticket/GetTicketMessages?ticketId={ticketId}");
                    Sender = await GetUserByIdAsync(Ticket.Sender);
                    Reciver = await GetUserByIdAsync(Ticket.Reciver);
                    await httpClient.GetFromJsonAsync<bool>($"{Configuration.GetSection("ApiUrlAddress").Value}/Ticket/ReadAllTicketMessages?ticketId={ticketId}");
                    NavSideBar();
                    return Page();
                }
                else
                {
                    Message = $"تیکت مورد نظر یا بسته شده و یا در سطل زباله هست";
                    return RedirectToPage("TicketsList");
                }
            }
        }

        public async Task<Users> GetUserByIdAsync(int userId)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                return await httpClient.GetFromJsonAsync<Users>($"{Configuration.GetSection("ApiUrlAddress").Value}/User/GetUserById?userId={userId}");
            }
        }

        public async Task<IActionResult> OnGetDeleteTicketAsync(int ticketId)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                await httpClient.DeleteAsync($"{Configuration.GetSection("ApiUrlAddress").Value}/Ticket/DeleteTicket?ticketId={ticketId}");
                Ticketings ticket = await httpClient.GetFromJsonAsync<Ticketings>($"{Configuration.GetSection("ApiUrlAddress").Value}/Ticket/GetTicketById?ticketId={ticketId}");
                Message = $"تیکت '{ticket.Title}' با موفقیت به سطل زباله انتقال یافت";
            }
            return RedirectToPage("TicketsList");
        }

        public async Task<IActionResult> OnGetCloseTicketAsync(int ticketId)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                Ticketings ticket = await httpClient.GetFromJsonAsync<Ticketings>($"{Configuration.GetSection("ApiUrlAddress").Value}/Ticket/GetTicketById?ticketId={ticketId}");
                await httpClient.PutAsJsonAsync($"{Configuration.GetSection("ApiUrlAddress").Value}/Ticket/CloseTicket", ticket);
                Message = $"تیکت '{ticket.Title}' با موفقیت بسته و غیرفعال شد";
            }
            return RedirectToPage("TicketsList");
        }

        public async Task<IActionResult> OnPostAnswerTicketAsync()
        {
            TicketMessage.IsSeen = false;
            TicketMessage.CreatedOn = DateTime.Now;
            using (HttpClient httpClient = new HttpClient())
            {
                Ticketings ticket = await httpClient.GetFromJsonAsync<Ticketings>($"{Configuration.GetSection("ApiUrlAddress").Value}/Ticket/GetTicketById?ticketId={TicketMessage.TicketId}");
                await httpClient.PostAsJsonAsync($"{Configuration.GetSection("ApiUrlAddress").Value}/Ticket/SendTicketMessageByAdmin", TicketMessage);
                Message = $"تیکت '{ticket.Title}' با موفقیت پاسخ داده شد";
            }
            return RedirectToPage("TicketsList");
        }

        private void NavSideBar()
        {
            ViewData["support"] = true;
            ViewData["listTicket"] = true;
        }
    }
}
