﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;

using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

using ThemeShop.Application.ViewModels.Public;
using ThemeShop.Domain.Models;

namespace AdminModule.Areas.Admin.Pages.Ticket
{
    public class ReportAbuseTicketModel : PageModel
    {

        [TempData]
        public string Message { get; set; }

        public IList<ReportAbuse> ReportAbuse { get; set; }

        public IConfiguration Configuration { get; set; }

        public PaggingViewModel PaggingViewModel { get; set; }

        public ReportAbuseTicketModel(IConfiguration configuration, PaggingViewModel paggingViewModel)
        {
            Configuration = configuration;
            PaggingViewModel = paggingViewModel;
        }

        public async Task OnGetAsync(string query, int pageId = 1)
        {
            if (!string.IsNullOrEmpty(query) || !string.IsNullOrWhiteSpace(query))
            {
                using (HttpClient httpClient = new HttpClient())
                {
                    Tuple<IList<ReportAbuse>, int> reportAbuseResponse = await httpClient.GetFromJsonAsync<Tuple<IList<ReportAbuse>, int>>($"{Configuration.GetSection("ApiUrlAddress").Value}/Ticket/GetAllReportAbuse?query={query}&pageId={pageId}");
                    ReportAbuse = reportAbuseResponse.Item1;
                    //Pagging
                    PaggingViewModel.ActivePage = pageId;
                    PaggingViewModel.PageCount = (int)Math.Ceiling(reportAbuseResponse.Item2 / 50.00);
                    PaggingViewModel.ParametrUrlName = "pageId";
                    PaggingViewModel.Query = query;
                }
                ViewData["query"] = query;
            }
            else
            {
                using (HttpClient httpClient = new HttpClient())
                {
                    Tuple<IList<ReportAbuse>, int> reportAbuseResponse = await httpClient.GetFromJsonAsync<Tuple<IList<ReportAbuse>, int>>($"{Configuration.GetSection("ApiUrlAddress").Value}/Ticket/GetAllReportAbuse?pageId={pageId}");
                    ReportAbuse = reportAbuseResponse.Item1;
                    //Pagging
                    PaggingViewModel.ActivePage = pageId;
                    PaggingViewModel.PageCount = (int)Math.Ceiling(reportAbuseResponse.Item2 / 30.00);
                    PaggingViewModel.ParametrUrlName = "pageId";
                    PaggingViewModel.Query = query;
                }
            }

            NavSideBar();
        }

        public async Task<IActionResult> OnPostAsync(int reportAbuseId, string answerText)
        {
            using HttpClient httpClient = new HttpClient();
            ReportAbuse reportAbuse = await httpClient.GetFromJsonAsync<ReportAbuse>($"{Configuration.GetSection("ApiUrlAddress").Value}/Ticket/GetReportAbuseById?id={reportAbuseId}");
            await httpClient.PostAsJsonAsync($"{Configuration.GetSection("ApiUrlAddress").Value}/Ticket/AnswerReportAbuse", new ReportAbuseRequest() { ReportAbuseId = reportAbuseId, AnswerText = answerText });
            Message = $"پاسخ تیکت با موفقیت ارسال شد";
            return RedirectToPage("ReportAbuseTicket");
        }

        public async Task OnGetSeenTicketAsync(int reportAbuseId)
        {
            using HttpClient httpClient = new HttpClient();
            ReportAbuse reportAbuse = await httpClient.GetFromJsonAsync<ReportAbuse>($"{Configuration.GetSection("ApiUrlAddress").Value}/Ticket/ReadReportAbuse?id={reportAbuseId}");
        }

        //ToDo:Impeliment when theme controoler finish
        //public async Task<Themes> GetThemeTitleByIdAsync(int reportAbuseId)
        //{
        //    using HttpClient httpClient = new HttpClient();
        //    var reportAbuse= await httpClient.GetFromJsonAsync<Themes>($"{Configuration.GetSection("ApiUrlAddress").Value}/Ticket/GetReportAbuseById?id={reportAbuseId}");
        //    return 
        //}

        private void NavSideBar()
        {
            ViewData["support"] = true;
            ViewData["listTicket"] = true;
        }
    }
}
