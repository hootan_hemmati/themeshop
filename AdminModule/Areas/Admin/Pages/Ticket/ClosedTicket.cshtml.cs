﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;

using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

using ThemeShop.Application.ViewModels.Public;
using ThemeShop.Domain.Models;

namespace AdminModule.Areas.Admin.Pages.Ticket
{
    public class ClosedTicketModel : PageModel
    {
        [TempData]
        public string Message { get; set; }

        public IList<Ticketings> Ticketings { get; set; }

        public IConfiguration Configuration { get; set; }

        public PaggingViewModel PaggingViewModel { get; set; }

        public ClosedTicketModel(IConfiguration configuration, PaggingViewModel paggingViewModel)
        {
            Configuration = configuration;
            PaggingViewModel = paggingViewModel;
        }
        public async Task OnGetAsync(string query, int pageId = 1)
        {
            if (!string.IsNullOrEmpty(query) || !string.IsNullOrWhiteSpace(query))
            {
                using (HttpClient httpClient = new HttpClient())
                {
                    Ticketings = await httpClient.GetFromJsonAsync<IList<Ticketings>>($"{Configuration.GetSection("ApiUrlAddress").Value}/Ticket/FilterClosedTickets?query={query}&pageId={pageId}");
                    //Pagging
                    PaggingViewModel.ActivePage = pageId;
                    PaggingViewModel.PageCount = (int)Math.Ceiling(await httpClient.GetFromJsonAsync<double>($"{Configuration.GetSection("ApiUrlAddress").Value}/Ticket/FilterClosedTicketsCount?query={query}") / 30.00);
                    PaggingViewModel.ParametrUrlName = "pageId";
                    PaggingViewModel.Query = query;
                }
                ViewData["query"] = query;
            }
            else
            {
                using (HttpClient httpClient = new HttpClient())
                {
                    Ticketings = await httpClient.GetFromJsonAsync<IList<Ticketings>>($"{Configuration.GetSection("ApiUrlAddress").Value}/Ticket/GetClosedTickets?pageId={pageId}");
                    //Pagging
                    PaggingViewModel.ActivePage = pageId;
                    PaggingViewModel.PageCount = (int)Math.Ceiling(await httpClient.GetFromJsonAsync<double>($"{Configuration.GetSection("ApiUrlAddress").Value}/Ticket/GetClosedTicketsCount") / 30.00);
                    PaggingViewModel.ParametrUrlName = "pageId";
                    PaggingViewModel.Query = query;
                }
            }

            NavSideBar();
        }

        public async Task<Users> GetUserByIdAsync(int userId)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                return await httpClient.GetFromJsonAsync<Users>($"{Configuration.GetSection("ApiUrlAddress").Value}/User/GetUserById?userId={userId}");
            }
        }

        public async Task<IActionResult> OnGetReOpenTicketAsync(int ticketId)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                Ticketings ticket = await httpClient.GetFromJsonAsync<Ticketings>($"{Configuration.GetSection("ApiUrlAddress").Value}/Ticket/GetTicketById?ticketId={ticketId}");
                await httpClient.PutAsJsonAsync($"{Configuration.GetSection("ApiUrlAddress").Value}/Ticket/OpenTicket", ticket);
                Message = $"تیکت '{ticket.Title}' با موفقیت باز و فعال شد";
            }
            return RedirectToPage("TicketsList");
        }

        private void NavSideBar()
        {
            ViewData["support"] = true;
            ViewData["listTicket"] = true;
        }
    }
}
