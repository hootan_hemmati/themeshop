﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;

using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

using ThemeShop.Application.ViewModels.Public;
using ThemeShop.Domain.Models;

namespace AdminModule.Areas.Admin.Pages.Ticket
{
    public class ContactUsTicketModel : PageModel
    {
        [TempData]
        public string Message { get; set; }

        public IList<ContactUs> ContactUs { get; set; }

        public IConfiguration Configuration { get; set; }

        public PaggingViewModel PaggingViewModel { get; set; }

        public ContactUsTicketModel(IConfiguration configuration, PaggingViewModel paggingViewModel)
        {
            Configuration = configuration;
            PaggingViewModel = paggingViewModel;
        }

        public async Task OnGetAsync(string query, int pageId = 1)
        {
            if (!string.IsNullOrEmpty(query) || !string.IsNullOrWhiteSpace(query))
            {
                using (HttpClient httpClient = new HttpClient())
                {
                    Tuple<IList<ContactUs>, int> contactUsResponse = await httpClient.GetFromJsonAsync<Tuple<IList<ContactUs>, int>>($"{Configuration.GetSection("ApiUrlAddress").Value}/Ticket/GetAllContactUs?query={query}&pageId={pageId}");
                    ContactUs = contactUsResponse.Item1;
                    //Pagging
                    PaggingViewModel.ActivePage = pageId;
                    PaggingViewModel.PageCount = (int)Math.Ceiling(contactUsResponse.Item2 / 50.00);
                    PaggingViewModel.ParametrUrlName = "pageId";
                    PaggingViewModel.Query = query;
                }
                ViewData["query"] = query;
            }
            else
            {
                using (HttpClient httpClient = new HttpClient())
                {
                    Tuple<IList<ContactUs>, int> contactUsResponse = await httpClient.GetFromJsonAsync<Tuple<IList<ContactUs>, int>>($"{Configuration.GetSection("ApiUrlAddress").Value}/Ticket/GetAllContactUs?pageId={pageId}");
                    ContactUs = contactUsResponse.Item1;
                    //Pagging
                    PaggingViewModel.ActivePage = pageId;
                    PaggingViewModel.PageCount = (int)Math.Ceiling(contactUsResponse.Item2 / 30.00);
                    PaggingViewModel.ParametrUrlName = "pageId";
                    PaggingViewModel.Query = query;
                }
            }

            NavSideBar();
        }

        public async Task<IActionResult> OnPostAsync(int contactUsId, string answerText)
        {
            using HttpClient httpClient = new HttpClient();
            ContactUs contactUs = await httpClient.GetFromJsonAsync<ContactUs>($"{Configuration.GetSection("ApiUrlAddress").Value}/Ticket/GetContactUs?id={contactUsId}");
            await httpClient.PostAsJsonAsync($"{Configuration.GetSection("ApiUrlAddress").Value}/Ticket/AnswerContactUs", new ContactUsRequest() { ContactId = contactUsId, Answer = answerText });
            Message = $"پاسخ تیکت {contactUs.Subject} با موفقیت ارسال شد";
            return RedirectToPage("ContactUsTicket");
        }

        private void NavSideBar()
        {
            ViewData["support"] = true;
            ViewData["listTicket"] = true;
        }
    }
}