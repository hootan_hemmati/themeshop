﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;

using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

using ThemeShop.Domain.Models;

namespace AdminModule.Areas.Admin.Pages.Role
{
    public class AddRoleModel : PageModel
    {
        [TempData]
        public string Message { get; set; }
        [BindProperty]
        public Roles Role { get; set; }

        public IConfiguration Configuration { get; set; }

        public IList<Permissions> Permissions { get; set; }

        public AddRoleModel(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public async Task OnGet()
        {
            using (HttpClient httpClient = new HttpClient())
            {
                Permissions = await httpClient.GetFromJsonAsync<IList<Permissions>>($"{Configuration.GetSection("ApiUrlAddress").Value}/Role/GetAllPermissions");
            }
            NavSideBar();
        }

        [ValidateAntiForgeryToken]
        public async Task<IActionResult> OnPost(IList<int> permissionsIds)
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }
            using (HttpClient httpClient = new HttpClient())
            {
                await httpClient.PostAsJsonAsync($"{Configuration.GetSection("ApiUrlAddress").Value}/Role/AddRole", Role);
                Roles role = await httpClient.GetFromJsonAsync<Roles>($"{Configuration.GetSection("ApiUrlAddress").Value}/Role/GetRoleByName?roleTitle={Role.RoleTitle}");
                foreach (int permissionId in permissionsIds.ToList())
                {
                    await httpClient.PostAsJsonAsync($"{Configuration.GetSection("ApiUrlAddress").Value}/Role/AddRolePermission", new PermissionRequest() { RoleId = role.RoleId, RolePermissionsId = permissionId });
                }
            }
            Message = $"نقش {Role.RoleTitle} با موفقیت اضافه شد";
            return RedirectToPage("RolesList");
        }

        private void NavSideBar()
        {
            ViewData["manageAccess"] = true;
            ViewData["addRole"] = true;
        }
    }
}
