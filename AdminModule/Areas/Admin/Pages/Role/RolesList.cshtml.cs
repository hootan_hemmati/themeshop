using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;

using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

using ThemeShop.Application.ViewModels.Public;
using ThemeShop.Domain.Models;

namespace AdminModule.Areas.Admin.Pages.Role
{
    public class RolesListModel : PageModel
    {
        [TempData]
        public string Message { get; set; }
        public IList<Roles> Roles { get; set; }

        public IConfiguration Configuration { get; set; }

        public PaggingViewModel PaggingViewModel { get; set; }

        public RolesListModel(IConfiguration configuration, PaggingViewModel paggingViewModel)
        {
            Configuration = configuration;
            PaggingViewModel = paggingViewModel;
        }
        public async Task OnGetAsync(string query, int pageId = 1)
        {
            if (!string.IsNullOrEmpty(query) || !string.IsNullOrWhiteSpace(query))
            {
                using (HttpClient httpClient = new HttpClient())
                {
                    Roles = await httpClient.GetFromJsonAsync<IList<Roles>>($"{Configuration.GetSection("ApiUrlAddress").Value}/Role/FilterRoles?query={query}&pageId={pageId}");
                    //Pagging
                    PaggingViewModel.ActivePage = pageId;
                    PaggingViewModel.PageCount = (int)Math.Ceiling(await httpClient.GetFromJsonAsync<double>($"{Configuration.GetSection("ApiUrlAddress").Value}/Role/FilterRolesCount?query={query}") / 10.00);
                    PaggingViewModel.ParametrUrlName = "pageId";
                    PaggingViewModel.Query = query;
                }
                ViewData["query"] = query;
            }
            else
            {
                using (HttpClient httpClient = new HttpClient())
                {
                    Roles = await httpClient.GetFromJsonAsync<IList<Roles>>($"{Configuration.GetSection("ApiUrlAddress").Value}/Role/GetAllRoles?pageId={pageId}");
                    //Pagging
                    PaggingViewModel.ActivePage = pageId;
                    PaggingViewModel.PageCount = (int)Math.Ceiling(await httpClient.GetFromJsonAsync<double>($"{Configuration.GetSection("ApiUrlAddress").Value}/Role/GetAllRolesCount") / 10.00);
                    PaggingViewModel.ParametrUrlName = "pageId";
                }
            }

            NavSideBar();
        }

        public async Task<IActionResult> OnGetDeleteRoleAsync(int roleId)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                await httpClient.DeleteAsync($"{Configuration.GetSection("ApiUrlAddress").Value}/Role/DeleteRoleById?roleId={roleId}");
            }
            return RedirectToPage("RolesList");
        }

        private void NavSideBar()
        {
            ViewData["manageAccess"] = true;
            ViewData["listRole"] = true;
        }
    }
}
