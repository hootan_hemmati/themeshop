﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;

using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

using ThemeShop.Domain.Models;

namespace AdminModule.Areas.Admin.Pages.Role
{
    public class EditRoleModel : PageModel
    {
        [TempData]
        public string Message { get; set; }

        [BindProperty]
        public Roles Role { get; set; }

        public IConfiguration Configuration { get; set; }

        public IList<Permissions> Permissions { get; set; }
        public IList<RolePremissions> RolePermissions { get; set; }

        public EditRoleModel(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        public async Task<IActionResult> OnGet(int roleId)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                if (await httpClient.GetFromJsonAsync<bool>($"{Configuration.GetSection("ApiUrlAddress").Value}/Role/IsRoleExistById?roleId={roleId}"))
                {
                    Role = await httpClient.GetFromJsonAsync<Roles>($"{Configuration.GetSection("ApiUrlAddress").Value}/Role/GetRoleById?roleId={roleId}");
                    Permissions = await httpClient.GetFromJsonAsync<IList<Permissions>>($"{Configuration.GetSection("ApiUrlAddress").Value}/Role/GetAllPermissions");
                    RolePermissions = await httpClient.GetFromJsonAsync<IList<RolePremissions>>($"{Configuration.GetSection("ApiUrlAddress").Value}/Role/GetPremissionsByRoleId?roleId={roleId}");
                    NavSideBar();
                    return Page();
                }
                else
                {
                    return RedirectToPage("RolesList");
                }
            }
        }

        [ValidateAntiForgeryToken]
        public async Task<IActionResult> OnPost(IList<int> permissionsIds)
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }
            using (HttpClient httpClient = new HttpClient())
            {
                await httpClient.DeleteAsync($"{Configuration.GetSection("ApiUrlAddress").Value}/Role/RemoveRolePermission?roleId={Role.RoleId}");
                foreach (int permissionId in permissionsIds.ToList())
                {
                    await httpClient.PutAsJsonAsync($"{Configuration.GetSection("ApiUrlAddress").Value}/Role/UpdateRolePermission", new PermissionRequest() { RoleId = Role.RoleId, RolePermissionsId = permissionId });
                }
                await httpClient.PutAsJsonAsync($"{Configuration.GetSection("ApiUrlAddress").Value}/Role/UpdateRole", Role);
                Message = $"نقش {Role.RoleTitle} با موفقیت ویرایش شد";
            }
            return RedirectToPage("RolesList");
        }

        private void NavSideBar()
        {
            ViewData["manageAccess"] = true;
        }
    }
}
