using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

using ThemeShop.Application.ViewModels.Public;
using ThemeShop.Application.ViewModels.Wallet;

namespace AdminModule.Areas.Admin.Pages.Wallet
{
    public class ShoppingListModel : PageModel
    {
        public IList<WalletViewModel> WalletViewModels { get; set; }

        public IConfiguration Configuration { get; set; }

        public PaggingViewModel PaggingViewModel { get; set; }

        public ShoppingListModel(IConfiguration configuration, PaggingViewModel paggingViewModel)
        {
            Configuration = configuration;
            PaggingViewModel = paggingViewModel;
        }

        public async Task OnGetAsync(string userName, string transactionType, long minimumPrice, long maximumPrice, DateTime startDate, DateTime endDate, string description, int pageId = 1)
        {
            if (!string.IsNullOrEmpty(userName) || !string.IsNullOrEmpty(transactionType) || minimumPrice != 0 || maximumPrice != 0 || !string.IsNullOrEmpty(description))
            {
                using HttpClient httpClient = new HttpClient();
                Tuple<IList<WalletViewModel>, int> result = await httpClient.GetFromJsonAsync<Tuple<IList<WalletViewModel>, int>>($"{Configuration.GetSection("ApiUrlAddress").Value}/Wallet/FilterTransaction?UserName={userName}&TransactionType={transactionType}&MinimumPrice={minimumPrice}&MaximumPrice={maximumPrice}&StartDate={startDate}&EndDate={endDate}&Describtion={description}&pageId={pageId}");
                WalletViewModels = result.Item1;
                //Pagging
                PaggingViewModel.ActivePage = pageId;
                PaggingViewModel.PageCount = (int)Math.Ceiling(Convert.ToDouble(result.Item2) / 20.00);
                PaggingViewModel.ParametrUrlName = "pageId";
                ViewData["query"] = userName;
                //Url Parameters
                ListDictionary parameters = new ListDictionary
                {
                    { "userName", $"{userName}" },
                    { "transactionType", $"{transactionType}"},
                    { "minimumPrice", $"{minimumPrice}" },
                    { "maximumPrice", $"{maximumPrice}" },
                    { "startDate", $"{startDate}" },
                    { "endDate", $"{endDate}" },
                    { "description", $"{description}" }
                };

                //Pass Value
                PaggingViewModel.UrlParameters = parameters;
            }
            else
            {
                using HttpClient httpClient = new HttpClient();
                Tuple<IList<WalletViewModel>, int> result = await httpClient.GetFromJsonAsync<Tuple<IList<WalletViewModel>, int>>($"{Configuration.GetSection("ApiUrlAddress").Value}/Wallet/GetBuyTransaction?pageId={pageId}");
                WalletViewModels = result.Item1;
                //Pagging
                PaggingViewModel.ActivePage = pageId;
                PaggingViewModel.PageCount = (int)Math.Ceiling(Convert.ToDouble(result.Item2) / 20.00);
                PaggingViewModel.ParametrUrlName = "pageId";
            }

            NavSideBar();
        }

        private void NavSideBar()
        {
            ViewData["manageWallet"] = true;
            ViewData["listShop"] = true;
        }
    }
}
