using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

using ThemeShop.Application.ViewModels.Public;
using ThemeShop.Domain.Models;

namespace AdminModule.Areas.Admin.Pages.Ad
{
    public class AdsListModel : PageModel
    {
        [TempData]
        public string Message { get; set; }

        public IList<Ads> Ads { get; set; }

        public IConfiguration Configuration { get; set; }

        public PaggingViewModel PaggingViewModel { get; set; }

        public AdsListModel(IConfiguration configuration, PaggingViewModel paggingViewModel)
        {
            Configuration = configuration;
            PaggingViewModel = paggingViewModel;
        }

        public async Task OnGetAsync(string adTitle, string addUrl, DateTime startTime, DateTime endDate, bool? isActive, bool? isDeleted, int pageId = 1)
        {
            if (!string.IsNullOrEmpty(adTitle) || !string.IsNullOrWhiteSpace(adTitle) || !string.IsNullOrEmpty(addUrl) || !string.IsNullOrWhiteSpace(addUrl))
            {
                using HttpClient httpClient = new HttpClient();
                Tuple<IList<Ads>, int> adsList = await httpClient.GetFromJsonAsync<Tuple<IList<Ads>, int>>($"{Configuration.GetSection("ApiUrlAddress").Value}/Ads/FilterAds?AdTitle={adTitle}&AddUrl={addUrl}&StartTime={startTime}&EndDate={endDate}&IsActive={isActive}&IsDeleted={isDeleted}&pageId={pageId}");
                Ads = adsList.Item1;
                //Pagging
                PaggingViewModel.ActivePage = pageId;
                PaggingViewModel.PageCount = (int)Math.Ceiling(Convert.ToDouble(adsList.Item2) / 20.00);
                PaggingViewModel.ParametrUrlName = "pageId";
                PaggingViewModel.Query = adTitle;
                //Url Parameters
                ListDictionary parameters = new ListDictionary
                {
                    { "adTitle", $"{adTitle}" },
                    { "addUrl", $"{addUrl}"},
                    { "startTime", $"{startTime}" },
                    { "endDate", $"{endDate}" },
                    { "isActive", $"{isActive}" },
                    { "isDeleted", $"{isDeleted}" },
                };
                //Pass Value
                PaggingViewModel.UrlParameters = parameters;
                ViewData["query"] = adTitle;
            }
            else
            {
                using (HttpClient httpClient = new HttpClient())
                {
                    Tuple<IList<Ads>, int> adsList = await httpClient.GetFromJsonAsync<Tuple<IList<Ads>, int>>($"{Configuration.GetSection("ApiUrlAddress").Value}/Ads/GetAds?pageId={pageId}");
                    Ads = adsList.Item1;
                    //Pagging
                    PaggingViewModel.ActivePage = pageId;
                    PaggingViewModel.PageCount = (int)Math.Ceiling(Convert.ToDouble(adsList.Item2) / 20.00);
                    PaggingViewModel.ParametrUrlName = "pageId";
                }
            }
            NavSideBar();
        }


        private void NavSideBar()
        {
            ViewData["manageAds"] = true;
            ViewData["adsList"] = true;
        }
    }
}
