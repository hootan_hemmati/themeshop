using Microsoft.AspNetCore.Mvc.RazorPages;

namespace AdminModule.Areas.Admin.Pages.Dashboard
{
    public class DashboardModel : PageModel
    {

        public void OnGet()
        {
            NavSideBar();
        }




        private void NavSideBar()
        {
            ViewData["dashboard"] = true;
            ViewData["mainDashboard"] = true;
        }
    }
}
