using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;

using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

using ThemeShop.Domain.Models;

namespace AdminModule.Areas.Admin.Pages.User
{
    public class OnlineUsersModel : PageModel
    {
        public IList<OnlineUsers> OnlineUsers { get; set; }

        public IConfiguration Configuration { get; set; }

        public OnlineUsersModel(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public async Task OnGet()
        {
            using (HttpClient httpClient = new HttpClient())
            {
                OnlineUsers = await httpClient.GetFromJsonAsync<IList<OnlineUsers>>($"{Configuration.GetSection("ApiUrlAddress").Value}/User/GetAllOnlineUsers");
            }
            NavSideBar();
        }

        private void NavSideBar()
        {
            ViewData["manageUser"] = true;
            ViewData["manageOnlineUser"] = true;
        }
    }
}
