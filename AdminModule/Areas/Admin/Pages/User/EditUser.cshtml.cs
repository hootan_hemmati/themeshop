﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

using ThemeShop.Domain.Models;

namespace AdminModule.Areas.Admin.Pages.User
{
    public class EditUserModel : PageModel
    {
        [TempData]
        public string Message { get; set; }

        [BindProperty]
        public Users Customer { get; set; }

        public IConfiguration Configuration { get; set; }

        public EditUserModel(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public async Task<IActionResult> OnGet(int userId)
        {
            if (userId != 0)
            {
                using (HttpClient httpClient = new HttpClient())
                {
                    Customer = await httpClient.GetFromJsonAsync<Users>($"{Configuration.GetSection("ApiUrlAddress").Value}/User/GetUserById?userId={userId}");
                }
                NavSideBar();
                return Page();
            }
            else
            {
                return RedirectToPage("UsersList");
            }
        }

        public async Task<IActionResult> OnPost(string prDate, IList<int> rolesIds)
        {
            Customer.RegisterDate = DateTime.Parse(prDate);
            using (HttpClient httpClient = new HttpClient())
            {
                await httpClient.DeleteAsync($"{Configuration.GetSection("ApiUrlAddress").Value}/User/RemoveAllUserRoleByUserId?userId={Customer.UserId}");
                foreach (int roleId in rolesIds)
                {
                    await httpClient.PostAsJsonAsync<UserRequest>($"{Configuration.GetSection("ApiUrlAddress").Value}/User/AddUserRole", new UserRequest() { UserId = Customer.UserId, RoleId = roleId });
                }
                await httpClient.PutAsJsonAsync<Users>($"{Configuration.GetSection("ApiUrlAddress").Value}/User/UpdateUser", Customer);
            }
            Message = $"{Customer.UserName} با موفقیت ویرایش شد";
            return RedirectToPage("UsersList");
        }

        public async Task<IList<Roles>> GetUserRolesAsync(int userId)
        {
            IList<Roles> roles = new List<Roles>();
            using (HttpClient httpClient = new HttpClient())
            {
                IList<UserRoles> userRoles = await httpClient.GetFromJsonAsync<IList<UserRoles>>($"{Configuration.GetSection("ApiUrlAddress").Value}/User/GetUserRolesByUserId?userId={userId}");
                foreach (UserRoles role in userRoles.ToList())
                {
                    roles.Add(await httpClient.GetFromJsonAsync<Roles>($"{Configuration.GetSection("ApiUrlAddress").Value}/Role/GetRoleById?roleId={role.RoleId}"));
                }
                return roles;
            }
        }

        public async Task<IList<Roles>> GetAllRolesAsync()
        {
            using (HttpClient httpClient = new HttpClient())
            {
                return await httpClient.GetFromJsonAsync<IList<Roles>>($"{Configuration.GetSection("ApiUrlAddress").Value}/Role/GetRoles");
            }
        }


        private void NavSideBar()
        {
            ViewData["manageUser"] = true;
            ViewData["userList"] = true;
        }
    }
}
