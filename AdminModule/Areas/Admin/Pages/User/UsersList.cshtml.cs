﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

using ThemeShop.Application.ViewModels.Public;
using ThemeShop.Domain.Models;

namespace AdminModule.Areas.Admin.Pages.User
{
    public class UsersListModel : PageModel
    {
        [TempData]
        public string Message { get; set; }
        public IList<Users> Users { get; set; }

        public IConfiguration Configuration { get; set; }

        public PaggingViewModel PaggingViewModel { get; set; }


        public UsersListModel(IConfiguration configuration, PaggingViewModel paggingViewModel)
        {
            Configuration = configuration;
            PaggingViewModel = paggingViewModel;
        }


        public async Task OnGet(string query, int pageId = 1)
        {
            if (!string.IsNullOrEmpty(query) || !string.IsNullOrWhiteSpace(query))
            {
                using (HttpClient httpClient = new HttpClient())
                {
                    Users = await httpClient.GetFromJsonAsync<IList<Users>>($"{Configuration.GetSection("ApiUrlAddress").Value}/User/FilterUsers?query={query}&pageId={pageId}");
                    //Pagging
                    PaggingViewModel.ActivePage = pageId;
                    PaggingViewModel.PageCount = (int)Math.Ceiling(await httpClient.GetFromJsonAsync<double>($"{Configuration.GetSection("ApiUrlAddress").Value}/User/FilterUsersCount?query={query}") / 30.00);
                    PaggingViewModel.ParametrUrlName = "pageId";
                    PaggingViewModel.Query = query;
                }
                ViewData["query"] = query;
            }
            else
            {
                using (HttpClient httpClient = new HttpClient())
                {
                    Users = await httpClient.GetFromJsonAsync<IList<Users>>($"{Configuration.GetSection("ApiUrlAddress").Value}/User/GetAllUsers?pageId={pageId}");
                    //Pagging
                    PaggingViewModel.ActivePage = pageId;
                    PaggingViewModel.PageCount = (int)Math.Ceiling(await httpClient.GetFromJsonAsync<double>($"{Configuration.GetSection("ApiUrlAddress").Value}/User/GetAllUsersCount") / 30.00);
                    PaggingViewModel.ParametrUrlName = "pageId";
                }
            }

            NavSideBar();
        }

        public async Task<IActionResult> OnGetDeleteUserAsync(int userId)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                await httpClient.DeleteAsync($"{Configuration.GetSection("ApiUrlAddress").Value}/User/DeleteUser?userId={userId}");
            }
            Message = "کاربر مورد نظر حذف شد";
            return RedirectToPage("UsersList");
        }

        public async Task<IActionResult> OnGetDisableUserAsync(int userId)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                await httpClient.PutAsJsonAsync($"{Configuration.GetSection("ApiUrlAddress").Value}/User/DeActiveUser", new UserRequest() { UserId = userId });
            }
            Message = "کاربر مورد نظر غیر فعال شد";
            return RedirectToPage("UsersList");
        }

        public async Task<IActionResult> OnGetEnableUserAsync(int userId)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                await httpClient.PutAsJsonAsync
                    ($"{Configuration.GetSection("ApiUrlAddress").Value}/User/ActiveUser", new UserRequest() { UserId = userId });
            }
            Message = "کاربر مورد نظر فعال شد";
            return RedirectToPage("UsersList");
        }

        public async Task<IList<Roles>> GetUserRolesAsync(int userId)
        {
            IList<Roles> roles = new List<Roles>();
            using (HttpClient httpClient = new HttpClient())
            {
                IList<UserRoles> userRoles = await httpClient.GetFromJsonAsync<IList<UserRoles>>($"{Configuration.GetSection("ApiUrlAddress").Value}/User/GetUserRolesByUserId?userId={userId}");
                foreach (UserRoles role in userRoles.ToList())
                {
                    roles.Add(await httpClient.GetFromJsonAsync<Roles>($"{Configuration.GetSection("ApiUrlAddress").Value}/Role/GetRoleById?roleId={role.RoleId}"));
                }
                return roles;
            }
        }

        private void NavSideBar()
        {
            ViewData["manageUser"] = true;
            ViewData["userList"] = true;
        }
    }
}
