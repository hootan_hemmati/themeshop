﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using ThemeShop.Domain.Models;

namespace ThemeShop.Application.Interfaces
{
    public interface IReportAbuseService
    {
        #region ReportAbuseService
        Task<Tuple<IList<ReportAbuse>, int>> GetReportAbuseAsync(string query = "", int pageId = 1);

        Task<Tuple<IList<ReportAbuse>, int>> GetReportAbuseNotReadAsync(string query = "", int pageId = 1);

        Task ReadReportAbuseAsync(int id);

        Task<bool> AnswerReportAbuseAsync(int reportAbuseId, string answerMessage);

        Task<ReportAbuse> GetReportAbuseByIdAsync(int id);

        Task<bool> AddReportAbuseAsync(ReportAbuse reportAbuse);

        #endregion
    }
}
