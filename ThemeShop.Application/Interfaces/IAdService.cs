﻿using Microsoft.AspNetCore.Http;

using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using ThemeShop.Application.RequestModel;
using ThemeShop.Domain.Models;

namespace ThemeShop.Application.Interfaces
{
    public interface IAdService
    {
        Task<bool> CreateAdAsync(AdsRequest ad, IFormFile img);

        Task<bool> EditAdAsync(AdsRequest ad);

        Task<bool> EditAdImageAsync(int id, IFormFile img);

        Task RemoveAd(int adId);

        Task RemoveAd(Ads ad);

        Task ActivateAd(int adId);

        Task ActivateAd(Ads ad);

        Task DeleteAdCategoriesByAdId(int adId);

        Task DeActivateAd(int adId);

        Task DeActivateAd(Ads ad);

        Task<IList<Ads>> GetAdsAsync(byte page = 1, byte take = 20);

        Task<int> GetAdsCountAsync();

        Task<Tuple<IList<Ads>, int>> GetAdsAsync(FilterAds filter, byte page = 1, byte take = 20);

        Task<Ads> GetAdsAsync(int adId);
    }
}