﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using ThemeShop.Application.RequestModel;
using ThemeShop.Domain.Models;

namespace ThemeShop.Application.Interfaces
{
    public interface IDiscountService
    {
        Task<bool> AddDiscountAsync(DiscountRequst discount);

        Task<bool> UpdateDiscountAsync(DiscountRequst discount);

        Task<bool> DeleteDiscountAsync(Discounts discounts, int userId);

        Task<bool> DeleteDiscountAsync(int discountsId, int userId);

        Task<IList<DiscountLogs>> GetDiscountLogsAsync(int discountId, int page);

        Task<IList<Discounts>> GetDiscountsAsync(byte page, byte take = 20);

        Task<int> GetDiscountCountAsync();

        Task<Tuple<IList<Discounts>, int>> GetDiscountsAsync(FilterDiscount filter, byte page, byte take = 20);
    }
}