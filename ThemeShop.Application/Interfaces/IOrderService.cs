﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using ThemeShop.Application.ViewModels.Order;

namespace ThemeShop.Application.Interfaces
{
    public interface IOrderService
    {
        Task<IList<UserPaymentViewModel>> GetAllUserPaymentsAsync();
        Task<Tuple<List<UserPaymentViewModel>, int>> GetUserPaymentsAsync(int themeId, byte page, byte take);
    }
}