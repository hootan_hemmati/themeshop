﻿using System.Collections.Generic;
using System.Threading.Tasks;

using ThemeShop.Domain.Models;

namespace ThemeShop.Application.Interfaces
{
    public interface IPermissionService
    {
        #region Permissions

        Task<IList<Permissions>> GetPermissionsAsync();

        Task<Permissions> GetPermissionByNameAsync(string permissionName);

        #endregion Permissions
    }
}