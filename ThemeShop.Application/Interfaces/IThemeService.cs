﻿using Microsoft.AspNetCore.Http;

using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using ThemeShop.Application.ViewModels.Theme;
using ThemeShop.Domain.Models;

namespace ThemeShop.Application.Interfaces
{
    public interface IThemeService
    {
        Task<Themes> GetThemeAsync(int id);
        Task<bool> InsertDefultPreviewPageAsync(int id);
        Task<bool> InsertThemePreviewAsync(ThemePreviews previews);
        Task<Tuple<IList<ThemePreviews>, int>> GetThemePreviewsAsync(int previewId, byte page, byte take);

        Task<AdminThemeViewModel> GetAdminThemeAsync(int id);
        Task<IList<ThemeChangeStatusMessages>> GetThemeChangeStatusMessagesAsync(int themeId);

        Task<Tuple<IList<ListThemeViewModel>, int>> GetThemeAsync(int page = 1, string title = "", int sellerId = 0,
            int categoryId = 0, bool isAdmintheme = false, ThemeStatus status = ThemeStatus.NoFilter);

        Task AddThemeCategoriesAsync(int themeId, List<int> categoriesId);

        Task RejectThemeAsync(int themeId, string message);
        Task AddCategoriesAsync(ThemeCategories category);

        void DeleteTheme(Themes theme);

        Task DeleteTheme(int themeId);

        void ActivateTheme(Themes theme);

        Task ActivateTheme(int themeId);

        void DeactiveTheme(Themes theme);

        Task DeactiveTheme(int themeId);

        Task DeleteCategories(int categoryId);

        void DeleteCategories(ThemeCategories category);

        Task<bool> AddRelatedThemeAsync(ThemeRelateds themeRelateds);
        Task<bool> AddThemeOfferAsync(ThemeOfferTodays themeOffer);

        Task<Tuple<IList<ThemeRelatedViewModel>, int>> GetThemeRelatedForAdminAsync(int themeId, byte page, byte take);
        Task<ThemeCategories> GetThemeCategoryAsync(int categoryId);

        Task DeleteRelatedThemeById(int relatedThemId);


        Task<Tuple<IList<ThemeOfferTodays>, int>> GetAllThemeOfferAsync(int page);

        Task<Tuple<IList<ThemeOfferTodays>, int>> GetThemeOfferAsync(int page, DateTime date);

        Task DeleteThemeOfferAsync(int themeOfferId);

        void ChangeThemeOfferState(ThemeOfferTodays themeOffer);

        Task ChangeThemeOfferStateAsync(int themeOfferId);

        Task ChangeThemePreviewStateAsync(int themePreviewId);

        void ChangeThemePreviewState(ThemePreviews themePreview);

        Task DeleteThemePreviewAsync(int themePreviewId);

        Task<Tuple<IList<ThemeComments>, int>> GetUnreadCommentsAsync(int page, byte take);

        Task DeleteThemeCommentAsync(int themeCommentId);

        void DeleteThemeComment(ThemeComments comments);

        Task<IList<ThemeComments>> GetCommentsByThemeIdAsync(int themeId, bool showDeletedComments = false);
        Task<IList<ThemeCategories>> GetThemeCategoriesAsync();
        Task EditThemeCategory(ThemeCategories category);
        Task<string> AddThemeGalleryAsync(ThemeGalleries galleries, IFormFile themeImage);
        Task<IList<ThemeGalleries>> GetThemeGalleriesAsync(int themeId);
        Task<bool> DeleteThemeGallerieAsync(int themeGalleryId);
        Task<IList<ThemeAttachments>> GetThemeAttachmentsAsync(int themeId);
        Task<string> AddThemeAttachmentAsync(IFormFile attachmentFile, ThemeAttachments themeAttachments);
        Task<string> DeleteThemeAttachmentAsync(int id);
        Task<bool> ChangeThemeAttachmentStateAsync(int id);
        Task<bool> ChangeThemeAttachmentIsFreeAsync(int id);
    }
}