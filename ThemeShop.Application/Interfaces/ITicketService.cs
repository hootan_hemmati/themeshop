﻿using System.Collections.Generic;
using System.Threading.Tasks;

using ThemeShop.Domain.Models;

namespace ThemeShop.Application.Interfaces
{
    public interface ITicketService
    {
        #region Ticketing

        Task<IList<Ticketings>> GetAllTicketsAsync();

        Task<IList<Ticketings>> FilterTicketsAsync(string query, int skip, int take = 30);

        Task<int> FilterTicketsCountAsync(string query);

        Task<int> GetAllTicketsCountAsync();

        Task<Ticketings> GetTicketByIdAsync(int ticketId);

        Task DeleteTicketAsync(int ticketId);

        Task DeleteTicketAsync(Ticketings ticket);

        Task RecoverTicketAsync(int ticketId);

        Task RecoverTicketAsync(Ticketings ticket);

        Task CloseTicketAsync(int ticketId);

        Task CloseTicketAsync(Ticketings ticket);

        Task OpenTicketAsync(int ticketId);

        Task OpenTicketAsync(Ticketings ticket);

        #endregion Ticketing

        #region NotReadedTickets

        Task<IList<Ticketings>> GetNotReadedTicketsAsync();

        Task<int> GetNotReadedTicketsCountAsync();

        Task<IList<Ticketings>> FilterNotReadedTicketsAsync(string query, int skip, int take = 30);

        Task<int> FilterNotReadedTicketsCountAsync(string query);

        #endregion NotReadedTickets

        #region SentTickets

        Task<IList<Ticketings>> GetSentTicketsAsync();

        Task<int> GetSentTicketsCountAsync();

        Task<IList<Ticketings>> FilterSentTicketsAsync(string query, int skip, int take = 30);

        Task<int> FilterSentTicketsCountAsync(string query);

        #endregion SentTickets

        #region DeletedTickets

        Task<IList<Ticketings>> GetDeletedTicketsAsync();

        Task<int> GetDeletedTicketsCountAsync();

        Task<IList<Ticketings>> FilterDeletedTicketsAsync(string query, int skip, int take = 30);

        Task<int> FilterDeletedTicketsCountAsync(string query);

        #endregion DeletedTickets

        #region ClosedTickets

        Task<IList<Ticketings>> GetClosedTicketsAsync();

        Task<int> GetClosedTicketsCountAsync();

        Task<IList<Ticketings>> FilterClosedTicketsAsync(string query, int skip, int take = 30);

        Task<int> FilterClosedTicketsCountAsync(string query);

        #endregion ClosedTickets

        #region TicketMessage

        Task<IList<TicketMessages>> GetTicketMessagesAsync(int ticketId);

        Task<bool> IsTicketReadOrNotAsync(int ticketId);

        Task ReadAllTicketMessagesAsync(int ticketId);

        Task SendTicketMessageByAdminAsync(TicketMessages ticketMessage);

        #endregion TicketMessage
    }
}