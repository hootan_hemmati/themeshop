﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using ThemeShop.Domain.Models;

namespace ThemeShop.Application.Interfaces
{
    public interface IContactUsService
    {
        Task<Tuple<IList<ContactUs>, int>> GetContactUsAsync(string query = "", int pageId = 1);

        Task<Tuple<IList<ContactUs>, int>> GetContactUsNotReadAsync(string query = "", int pageId = 1);

        Task ReadContactUsAsync(int id);

        Task<bool> AnswerContactAsync(int contactId, string answerMessage);

        Task<ContactUs> GetContactUsAsync(int id);

        Task<bool> AddContactUsAsync(ContactUs contact);
    }
}