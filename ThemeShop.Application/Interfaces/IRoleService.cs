﻿using System.Collections.Generic;
using System.Threading.Tasks;

using ThemeShop.Domain.Models;

namespace ThemeShop.Application.Interfaces
{
    public interface IRoleService
    {
        #region Roles

        Task<IList<Roles>> GetAllRolesAsync();

        Task<IList<Roles>> FilterRolesAsync(int skip, int take = 10, string filterRoles = "");

        Task<int> FilterRolesCountAsync(string filterRoles = "");

        Task<Roles> GetRoleByIdAsync(int roleId);

        Task<Roles> GetRoleByTitleAsync(string roleTitle);

        Task AddRoleAsync(Roles role);

        Task UpdateRoleAsync(Roles role);

        void DeleteRole(Roles role);

        Task DeleteRoleByIdAsync(int roleId);

        Task<bool> IsRoleExistByIdAsync(int roleId);

        #endregion Roles

        #region UserRoles

        Task<IList<UserRoles>> GetUserRolesByUserIdAsync(int userId);

        Task<IList<UserRoles>> GetAllUserRolesInRoleIdAsync(int roleId);

        Task AddUserRoleAsync(int userId, int roleId);

        Task RemoveUserRoleAsync(int userId, int roleId);

        Task RemoveAllUserRoleByUserIdAsync(int userId);

        Task<bool> IsUserInPermissionAsync(int userId, int permissionId);

        #endregion UserRoles

        #region RolesPermissions

        Task AddRolePermissionAsync(int roleId, int rolePermissionId);

        Task UpdateRolePermissionAsync(int roleId, int rolePermissionId);

        Task RemoveRolePermissionAsync(int roleId);

        Task<IList<RolePremissions>> GetPremissionsByRoleIdAsync(int roleId);

        Task<IList<RolePremissions>> GetRolePremissionsByPremissionIdAsync(int permissionId);

        #endregion RolesPermissions

        void Save();

        Task SaveAsync();
    }
}