﻿using Microsoft.AspNetCore.Http;

using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using ThemeShop.Application.RequestModel;
using ThemeShop.Application.ViewModels.Article;
using ThemeShop.Domain.Models;

namespace ThemeShop.Application.Interfaces
{
    public interface IArticelService
    {
        Task<IList<ListArticleViewModel>> GetArticlesAsync(byte page, byte take = 20);

        Task<int> GetArticleCountAsync();

        Task<Tuple<IList<ListArticleViewModel>, int>> GetArticlesAsync(FilterArticle filter, byte page, byte take = 20);

        Task<bool> CreateAricleAsync(ArticleRequest articleRequest);

        Task<MainArticleCategories> GetArticleCategoryAsync(int id);

        Task<bool> CreateArticleCategoryAsync(ArticleCategoryRequest articleCategory);

        Task<bool> UpdateArticleCategoryAsync(ArticleCategoryRequest articleCategory);

        Task<bool> DeleteArticleCategoryAsync(int id);

        Task<bool> DeleteMainArticleCategoryAsync(int id);

        bool DeleteArticleCategoryAsync(ArticleCategories categories);

        Task<IList<MainArticleCategories>> GetAllArticleCategoriesAsync();

        Task AddArticleTagsAsync(string tag, int articleId);

        Task SaveArticlePhysicalImageAsync(IFormFile file, string imageName);

        Task AddArticleCategoryAsync(int articleId, List<int> categoryId);

        Task<Articles> GetArticleAsync(int articleId);

        Task<bool> UpdateArticleAsync(ArticleRequest request);

        Task RemoveArticleCategoryAsync(int articleId);

        Task RemoveArticleTagsAsync(int articleId);

        bool RemoveArticleImage(string imageName);

        Task<bool> RemoveArticleAsync(int id);

        Task<bool> ActivateArticleAsync(int id);

        Task<bool> DeActivateArticleAsync(int id);

        Task<IList<ArticleRelatedViewModel>> GetArticleRelatedsAsync(int articleId);

        Task<bool> AddArticleRelatedAsync(ArticleRelateds relateds);

        Task<bool> DeleteArticleRelatedAsync(int id);

        Task<IList<ArticleComments>> GetUnreadsCommentsAsync();

        Task<ArticleComments> GetArticleCommentsAsync(int id);

        Task MarkCommentsAsRead(ArticleComments articleComments);

        Task<bool> RemoveArticleCommentAsync(int id);

        Task<IList<ArticleComments>> GetArticleComments(int articleId);
    }
}