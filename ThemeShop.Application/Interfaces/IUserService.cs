﻿using System.Collections.Generic;
using System.Threading.Tasks;

using ThemeShop.Application.ViewModels.User;
using ThemeShop.Domain.Models;

namespace ThemeShop.Application.Interfaces
{
    public interface IUserService
    {
        #region OnlineUser

        Task<IList<OnlineUsers>> GetTodayOnlineUsersAsync(int take = 100);

        void UpdateOnlineUserInformation(OnlineUsers onlineUser);

        Task<OnlineUsers> GetOnlineUserAsync(string userName, string ip);

        #endregion OnlineUser

        #region Users

        Task<IList<Users>> GetAllUsersAsync(int skip, int take = 30);

        Task<int> GetAllUsersCountAsync();

        Task InsertUserAsync(Users user);

        void UpdateUser(Users user);

        Task<IList<Users>> GetUserRegisterToDayAsync();

        Task<Users> GetUserByIdAsync(int userId);

        Task<int> GetUserIdByEmailAsync(string email);

        Task ActiveUserAccountAsync(int userId);

        Task<Users> ActiveUserAccountAsync(string activeCode);

        Task DeActiveUserAsync(int userId);

        Task<bool> IsActiveUserAsync(int userId);

        Task<bool> IsActiveUserAsync(string email);

        Task DeleteUserAsync(int userId);

        void DeleteUser(Users user);

        Task ChangeUserPasswordAsync(int userId, string newPssword);

        Task<bool> IsOldPasswordCorrectAsync(int userId, string oldPassword);

        Task<bool> IsUserEmailChangeAsync(int userId, string email);

        Task<bool> IsUserNameChangeAsync(int userId, string userName);

        Task<bool> IsExistUserByActiveCodeAsync(string activeCode);

        Task RecoveryPasswordUserAsync(string activeCode, string newPasssword);

        Task ChangeActiveCodeAsync(int userId);

        Task ActiveUserForSellerAsync(int userId);

        Task InActiveUserForSellerAsync(int userId);

        Task<string> GetEmailByUserIdAsync(int userId);

        Task<IList<Users>> FilterUsersAsync(string query, int userSearchMode = 0, int userStatusEnum = 0);

        Task<int> FilterUsersCountAsync(string query, int userSearchMode = 0, int userStatusEnum = 0);

        #region Seller

        Task<SellerIndexViewModel> FilterSellersAsync(byte pageId, byte take, SellerFilterViewModel filter);
        Task<SellerIndexViewModel> GetAllSellersAsync(byte pageId, byte take);

        Task<SellerViewModel> GetSellerByuserIdIdForConfirmationAsync(int userId);

        Task<bool> IsSellerAsync(int userId);

        #endregion Seller

        #endregion Users

        #region UserBank

        Task ActiveUserBankInfoesAsync(int userBankId);

        //TODO:ShowThemeListViewModel Impediment Later And Return ShowThemeListViewModel
        //Task<ShowProfileViewModel> GetUserProfileAsync(string userName, int pageId);
        Task AddUserBankInformationAsync(SellerViewModel sellerModel);

        Task RejectUserBankAsync(int userBankId, string message);

        #endregion UserBank

        Task<bool> ConfirmSellerAsync(int userId, int userBankId);
        Task<bool> RejectSellerAsync(int userId, int userBankId, string message);
        void Save();

        Task SaveAsync();
    }
}