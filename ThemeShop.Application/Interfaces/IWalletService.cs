﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using ThemeShop.Application.RequestModel;
using ThemeShop.Application.ViewModels.Wallet;

namespace ThemeShop.Application.Interfaces
{
    public interface IWalletService
    {
        Task<IList<WalletViewModel>> GetAllPaymentTransactionAsync(int page, byte take = 20);

        Task<int> GetAllPaymentCountAsync();

        Task<IList<WalletViewModel>> GetAllBuyTransactionAsync(int page, byte take = 20);

        Task<int> GetAllBuyTransactionCountAsync();

        Task<Tuple<IList<WalletViewModel>, int>> GetTransactionAsync(int page, FilterTransaction filter, byte take = 20);
    }
}