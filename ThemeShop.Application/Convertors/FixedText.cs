﻿using System;

namespace ThemeShop.Application.Convertors
{
    public class FixedText
    {
        public static string FixUserName(string userName)
        {
            return userName.Trim().ToLower();
        }

        public static string FixIp(string ip)
        {
            return ip.Trim();
        }

        public static string FixEmail(string email)
        {
            return email.Trim().ToLower();
        }

        public static string ConvertNewLineToBr(string text)
        {
            if (string.IsNullOrEmpty(text))
            {
                return string.Empty;
            }

            return text.Replace(Environment.NewLine, "<br/>");
        }

        public static string[] SplitTags(string tags)
        {
            return tags.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
        }

    }
}