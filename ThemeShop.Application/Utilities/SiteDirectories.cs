﻿using System.IO;

namespace ThemeShop.Application.Utilities
{
    public class SiteDirectories
    {
        #region Statics Paths

        public static string AdsPath = "wwwroot/Images/Ads/";
        public static string TicketsImagesPath = "wwwroot/Images/TicketImages/";
        public static string ArticleImagesPath = "wwwroot/Images/ArticleImages/";
        public static string ThemeGalleriesPath = "wwwroot/Images/ThemeImages/Gallery/Image/";
        public static string ThemeGalleriesThumbPath = "wwwroot/Images/ThemeImages/Gallery/Thumb/";
        public static string ThemeAttachmentsPath = "wwwroot/Files/Theme/";

        #endregion Statics Paths

        public static string AdsImagePhysicalPath()
        {
            IsPathExist(Path.Combine(Directory.GetCurrentDirectory(), AdsPath));
            return Path.Combine(Directory.GetCurrentDirectory(), AdsPath);
        }
        public static string ArticleImagePhysicalPath()
        {
            IsPathExist(Path.Combine(Directory.GetCurrentDirectory(), ArticleImagesPath));
            return Path.Combine(Directory.GetCurrentDirectory(), ArticleImagesPath);
        }
        public static string TicketsImagesImagePhysicalPath()
        {
            IsPathExist(Path.Combine(Directory.GetCurrentDirectory(), TicketsImagesPath));
            return Path.Combine(Directory.GetCurrentDirectory(), TicketsImagesPath);
        }
        public static string ThemeGalleriesImagePhysicalPath()
        {
            IsPathExist(Path.Combine(Directory.GetCurrentDirectory(), ThemeGalleriesPath));
            return Path.Combine(Directory.GetCurrentDirectory(), ThemeGalleriesPath);
        }
        public static string ThemeGalleriesThumbPhysicalPath()
        {
            IsPathExist(Path.Combine(Directory.GetCurrentDirectory(), ThemeGalleriesThumbPath));
            return Path.Combine(Directory.GetCurrentDirectory(), ThemeGalleriesThumbPath);
        }
        public static string ThemeAttachmentsPhysicalPath()
        {
            IsPathExist(Path.Combine(Directory.GetCurrentDirectory(), ThemeAttachmentsPath));
            return Path.Combine(Directory.GetCurrentDirectory(), ThemeAttachmentsPath);
        }

        #region Is Exist

        private static void IsPathExist(string path)
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(Path.Combine(Directory.GetCurrentDirectory(), path));
            }
        }

        #endregion Is Exist
    }
}