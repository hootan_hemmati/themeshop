﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Configuration;

using System;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

using ThemeShop.Application.Interfaces;
using ThemeShop.Application.Utilities;
using ThemeShop.Domain.Models;

namespace ThemeShop.Application.Hubs
{
    public class TrackUserHub : Hub
    {
        private readonly IUserService _userService;
        private readonly IHttpContextAccessor _httpContextAccessor;
        public IConfiguration Configuration { get; set; }

        public TrackUserHub(IUserService userService, IConfiguration configuration, IHttpContextAccessor httpContextAccessor)
        {
            _userService = userService;
            _httpContextAccessor = httpContextAccessor;
            Configuration = configuration;
        }

        #region OnConnect

        public override async Task OnConnectedAsync()
        {
            HttpContext httpcontext = Context.GetHttpContext();
            Microsoft.Extensions.Primitives.StringValues pathVisit = httpcontext.Request.Query["pagename"];
            string ip = IpAddress.GetUserIPAddress();
            string userName = (_httpContextAccessor.HttpContext.User.Identity.IsAuthenticated) ? _httpContextAccessor.HttpContext.User.Identity.Name : "کاربر مهمان";
            using (HttpClient httpClient = new HttpClient())
            {
                OnlineUsers onlineUser = await httpClient.GetFromJsonAsync<OnlineUsers>($"{Configuration.GetSection("ApiUrlAddress").Value}/User/GetOnlineUsers?userName={userName}&ip={ip}");
                onlineUser.OnlineStatus = true;
                onlineUser.StartTime = DateTime.Now;
                onlineUser.VisitPath = pathVisit;
                await httpClient.PutAsJsonAsync($"{Configuration.GetSection("ApiUrlAddress").Value}/User/UpdateOnlineUser", onlineUser);
            }

            await base.OnConnectedAsync();
        }

        #endregion OnConnect

        #region OnDisconnect

        public override async Task OnDisconnectedAsync(Exception exception)
        {
            HttpContext httpcontext = Context.GetHttpContext();
            Microsoft.Extensions.Primitives.StringValues pathVisit = httpcontext.Request.Query["pagename"];
            string ip = IpAddress.GetUserIPAddress();
            string userName = (_httpContextAccessor.HttpContext.User.Identity.IsAuthenticated) ? _httpContextAccessor.HttpContext.User.Identity.Name : "کاربر مهمان";
            using (HttpClient httpClient = new HttpClient())
            {
                OnlineUsers onlineUser = await httpClient.GetFromJsonAsync<OnlineUsers>($"{Configuration.GetSection("ApiUrlAddress").Value}/User/GetOnlineUsers?userName={userName}&ip={ip}");
                onlineUser.OnlineStatus = false;
                await httpClient.PutAsJsonAsync($"{Configuration.GetSection("ApiUrlAddress").Value}/User/UpdateOnlineUser", onlineUser);
            }

            await base.OnDisconnectedAsync(exception);
        }

        #endregion OnDisconnect
    }
}