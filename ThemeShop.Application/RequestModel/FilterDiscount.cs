﻿using System;

namespace ThemeShop.Application.RequestModel
{
    public class FilterDiscount
    {
        public int UsageCount { get; set; }
        public int UsedCount { get; set; }
        public DateTime ExpireDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public int MinDiscountPercentage { get; set; }
        public int MaxDiscountPercentage { get; set; }
    }
}