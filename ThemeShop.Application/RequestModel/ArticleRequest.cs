﻿using Microsoft.AspNetCore.Http;

using System.Collections.Generic;

namespace ThemeShop.Application.RequestModel
{
    public class ArticleRequest
    {
        public int ArticleId { get; set; }
        public int AuthorId { get; set; }
        public string ArticleTitle { get; set; }
        public string ShortDescription { get; set; }
        public string ArticleText { get; set; }
        public string Tags { get; set; }
        public bool IsVip { get; set; }
        public bool CanInsertComment { get; set; }
        public IFormFile ArticleImage { get; set; }
        public List<int> SelectedCategories { get; set; }
        public List<int> RelatedThemes { get; set; }
    }
}