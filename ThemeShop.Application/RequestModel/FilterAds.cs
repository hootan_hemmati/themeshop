﻿using System;

namespace ThemeShop.Application.RequestModel
{
    public class FilterAds
    {
        public string AdTitle { get; set; }
        public string AddUrl { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndDate { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDeleted { get; set; }
    }
}