﻿using System;

namespace ThemeShop.Application.RequestModel
{
    public class FilterTransaction
    {
        public string TransactionType { get; set; }
        public string UserName { get; set; }
        public long? MinimumPrice { get; set; }
        public long? MaximumPrice { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Describtion { get; set; }
    }
}