﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ThemeShop.Application.RequestModel
{
    public class AdsRequest
    {
        public int AdId { get; set; }

        [Display(Name = "عنوان تبلیغ")]
        [Required(ErrorMessage = "لطفا فیلد {0} را وارد کنید")]
        [MaxLength(500, ErrorMessage = "{0} میتواند حداکثر {1} کاراکتر داشته باشد")]
        public string AdTitle { get; set; }

        [Display(Name = "آدرس")]
        [Required(ErrorMessage = "لطفا فیلد {0} را وارد کنید")]
        [MaxLength(500, ErrorMessage = "{0} میتواند حداکثر {1} کاراکتر داشته باشد")]
        public string AdUrl { get; set; }

        [Display(Name = "تصویر")]
        [MaxLength(500, ErrorMessage = "{0} میتواند حداکثر {1} کاراکتر داشته باشد")]
        public string AdImageName { get; set; }

        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool ShowInChild { get; set; }
        public List<int> SelectedCategories { get; set; }
    }
}