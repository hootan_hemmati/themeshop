﻿using System;

namespace ThemeShop.Application.RequestModel
{
    public class FilterArticle
    {
        public string ArticleTitle { get; set; }
        public int? CategoryId { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsVip { get; set; }
    }
}