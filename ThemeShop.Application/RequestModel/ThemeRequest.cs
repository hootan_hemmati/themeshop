﻿using ThemeShop.Application.ViewModels.Theme;

namespace ThemeShop.Application.RequestModel
{
    public class ThemeRequest
    {
        public string Title { get; set; }
        public int SellerId { get; set; }
        public int CategoryId { get; set; }
        public bool IsAdmintheme { get; set; }
        public ThemeStatus Status { get; set; }

    }
}
