﻿namespace ThemeShop.Application.RequestModel
{
    public class ArticleCategoryRequest
    {
        public int Id { get; set; }
        public int? MainCategoryId { get; set; }
        public int MainArticleId { get; set; }

        public string Title { get; set; }
        public string NameInUrl { get; set; }
    }
}
