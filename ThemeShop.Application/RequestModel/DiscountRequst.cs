﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ThemeShop.Application.RequestModel
{
    public class DiscountRequst
    {
        public int DiscountId { get; set; }

        [Display(Name = "ظرفیت استفاده")]
        public int Count { get; set; }

        [Display(Name = "تعداد استفاده شده")]
        public int UsedCount { get; set; }

        [Display(Name = "تاریخ انقضا")]
        public DateTime ExpireDate { get; set; }

        [Display(Name = "تاریخ ایجاد")]
        public DateTime CreateDate { get; set; }

        [Display(Name = "میزان تخفیف")]
        public int Percentage { get; set; }

        public string Code { get; set; }
        public List<int> SelectedCategories { get; set; }
        public int UserId { get; set; }
    }
}