﻿using System.Collections.Generic;

using ThemeShop.Application.ViewModels.Public;
using ThemeShop.Application.ViewModels.Theme;

namespace ThemeShop.Application.ViewModels.User
{
    public class ShowProfileViewModel
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string Description { get; set; }
        public string Avatar { get; set; }
        public int CountMounthActiveUser { get; set; }
        public int ThemeCount { get; set; }
        public int SellCount { get; set; }
        public int SupportCount { get; set; }
        public int? CommentCount { get; set; }
        public int? Rate { get; set; }
        public IList<ShowThemeListViewModel> Themes { get; set; }
        public PaggingViewModel Pagging { get; set; }
    }
}