﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ThemeShop.Application.ViewModels.User
{
    public class EditProfileViewModel
    {
        public EditProfileViewModel()
        {
            SelectedEmailSetting = new List<int>();
        }

        [Display(Name = "نام کاربری")]
        [Required(ErrorMessage = "لطفا {0} را وارد کنید")]
        [MaxLength(150)]
        public string UserName { get; set; }

        [Display(Name = "ایمیل")]
        [Required(ErrorMessage = "لطفا {0} را وارد کنید")]
        [MaxLength(250)]
        [EmailAddress(ErrorMessage = "ایمیل وارد شده معتبر نمی باشد")]
        public string Email { get; set; }

        [Display(Name = "شماره تماس")]
        public string Mobile { get; set; }

        [Display(Name = "شرح مختصر")]
        [DataType(DataType.MultilineText)]
        public string UserDescription { get; set; }

        public string Avatar { get; set; }

        public List<int> SelectedEmailSetting { get; set; }

        public DateTime RegisterDate { get; set; }
    }
}