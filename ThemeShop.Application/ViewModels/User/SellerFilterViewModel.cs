﻿using ThemeShop.DomainClasses.Users;

namespace ThemeShop.Application.ViewModels.User
{
    public class SellerFilterViewModel
    {
        public string FilterEmail { get; set; }
        public string FilterUserName { get; set; }
        public string FilterNameFamily { get; set; }
        public UserStatusEnum Status { get; set; }
    }
}