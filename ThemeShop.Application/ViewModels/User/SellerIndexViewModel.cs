﻿using System.Collections.Generic;

using ThemeShop.Application.ViewModels.Public;

namespace ThemeShop.Application.ViewModels.User
{
    public class SellerIndexViewModel
    {
        public IList<SellerIndexForAdminViewmodel> Sellers { get; set; }
        public PaggingViewModel PaggingViewModel { get; set; }
    }
}