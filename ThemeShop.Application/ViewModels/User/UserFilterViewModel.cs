﻿using ThemeShop.DomainClasses.Users;

namespace ThemeShop.Application.ViewModels.User
{
    public class UserFilterViewModel
    {
        public int PageId { get; set; }
        public string FilterEmail { get; set; }
        public string FilterMobile { get; set; }
        public string FilterUserName { get; set; }
        public UserStatusEnum Status { get; set; }
    }
}