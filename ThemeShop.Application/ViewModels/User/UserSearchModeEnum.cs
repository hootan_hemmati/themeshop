﻿namespace ThemeShop.Application.ViewModels.User
{
    public enum UserSearchMode
    {
        UserName,
        Email,
        PhoneNumber
    }
}