﻿namespace ThemeShop.Application.ViewModels.User
{
    public class PagingUserViewModel
    {
        public int PageCount { get; set; }
        public int ActivePage { get; set; }
    }
}