﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ThemeShop.Application.ViewModels.User
{
    public class SellerViewModel
    {
        public int UserBankId { get; set; }
        public int UserId { get; set; }

        [Display(Name = "نام")]
        [Required(ErrorMessage = "لطفا {0} را وارد کنید")]
        public string Name { get; set; }

        [Display(Name = "نام خانوادگی")]
        [Required(ErrorMessage = "لطفا {0} را وارد کنید")]
        public string Family { get; set; }

        [Display(Name = "موبایل")]
        [Required(ErrorMessage = "لطفا {0} را وارد کنید")]
        public string Mobile { get; set; }

        [Display(Name = "کد ملی")]
        public string CodeMeli { get; set; }

        [Display(Name = "تصویر کارت ملی")]
        public string ImageKartMeli { get; set; }

        [Display(Name = "نام صاحب حساب")]
        [Required(ErrorMessage = "لطفا {0} را وارد کنید")]
        public string SellerAccountHolder { get; set; }//نام صاحب حساب

        [Display(Name = "نام بانک")]
        [Required(ErrorMessage = "لطفا {0} را وارد کنید")]
        public string BankName { get; set; }

        [Display(Name = "شماره حساب")]
        [Required(ErrorMessage = "لطفا {0} را وارد کنید")]
        public string SellerAccountNumber { get; set; }//شماره حساب

        [Display(Name = "شماره کارت")]
        [Required(ErrorMessage = "لطفا {0} را وارد کنید")]
        public string SellerCardNumber { get; set; }//شماره کارت

        [Display(Name = "شماره شبا")]
        [Required(ErrorMessage = "لطفا {0} را وارد کنید")]
        public string SellerShabaNumber { get; set; }

        [Display(Name = "تصویر کارت بانکی")]
        [Required(ErrorMessage = "لطفا {0} را وارد کنید")]
        public string ImageCardBank { get; set; }

        [Display(Name = "در تاریخ")]
        public DateTime CreateDate { get; set; }

        [Display(Name = "وضعیت")]
        public bool IsActive { get; set; }
    }
}