﻿namespace ThemeShop.Application.ViewModels.User
{
    public class SellerInformationViewModel
    {
        public int UserId { get; set; }
        public int UserBankId { get; set; }
        public string Name { get; set; }
        public string Family { get; set; }
    }
}