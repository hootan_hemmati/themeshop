﻿namespace ThemeShop.DomainClasses.Users
{
    public enum UserStatusEnum
    {
        All,
        Deleted,
        NotConfirmed,
        Confirmed,
        Rejected
    }
}