﻿namespace ThemeShop.Application.ViewModels.States
{
    public class FooterSateViewModel
    {
        public int ThemeCount { get; set; }
        public int ColleagueCount { get; set; }
        public int SellCount { get; set; }
    }
}