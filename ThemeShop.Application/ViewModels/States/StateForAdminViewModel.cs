﻿namespace ThemeShop.Application.ViewModels.States
{
    public class StateForAdminViewModel
    {
        public int UserCount { get; set; }
        public int SellerCount { get; set; }
        public int NewSellerCount { get; set; }
        public int ThemeCount { get; set; }
        public int NewThemeCount { get; set; }
        public int ArticleCount { get; set; }
        public int CommentThemeCount { get; set; }
        public int ArticleCommentCount { get; set; }
        public int PageCount { get; set; }
        public int SaleTodayCount { get; set; }
        public int SaleMonthCount { get; set; }
        public string AdminAccountBalance { get; set; }
    }
}