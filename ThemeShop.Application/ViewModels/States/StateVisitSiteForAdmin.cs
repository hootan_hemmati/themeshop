﻿namespace ThemeShop.Application.ViewModels.States
{
    public class StateVisitSiteForAdmin
    {
        public int VisitSite { get; set; }
        public int TodayVisit { get; set; }
        public int YesterdayVisit { get; set; }
        public int ThisWeekVisit { get; set; }
        public int ThisMonthVisit { get; set; }
        public int ThisYearVisit { get; set; }
    }
}