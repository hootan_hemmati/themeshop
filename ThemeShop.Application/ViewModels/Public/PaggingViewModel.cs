﻿using System.Collections.Specialized;

namespace ThemeShop.Application.ViewModels.Public
{
    public class PaggingViewModel
    {
        public int PageCount { get; set; }
        public int ActivePage { get; set; }
        public string ParametrUrlName { get; set; }
        public ListDictionary UrlParameters { get; set; }
        public string Query { get; set; }
    }
}