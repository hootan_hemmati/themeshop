﻿using System.Collections.Generic;

using ThemeShop.Domain.Models;

namespace ThemeShop.Application.ViewModels.Theme
{
    public class ListThemeViewModel
    {
        public ListThemeViewModel()
        {
            ThemeCategories = new List<ThemeCategories>();
        }

        public int ThemeID { get; set; }
        public string Title { get; set; }
        public IList<string> CategoriesTitle { get; set; }
        public string SellerName { get; set; }
        public int VisitCount { get; set; }
        public int CommentCount { get; set; }
        public int GalleryCount { get; set; }
        public int AttachCount { get; set; }
        public int Rate { get; set; }
        public int RelatedThemeCount { get; set; }
        public bool IsActive { get; set; }
        public bool IsReject { get; set; }
        public bool IsCommentTheme { get; set; }
        public int Price { get; set; }
        public List<ThemeCategories> ThemeCategories { get; set; }
    }
}