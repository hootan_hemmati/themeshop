﻿namespace ThemeShop.Application.ViewModels.Theme
{
    public enum ThemeStatus
    {
        Rejected = 1,
        Confirmed = 2,
        NoFilter = 3,
        DontReject = 4,
        Deleted = 5,
    }
}