﻿namespace ThemeShop.Application.ViewModels.Theme
{
    public class SellerListItemViewModel
    {
        public int SellerID { get; set; }
        public string SellerName { get; set; }
    }
}