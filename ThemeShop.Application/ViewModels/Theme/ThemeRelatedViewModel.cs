﻿namespace ThemeShop.Application.ViewModels.Theme
{
    public class ThemeRelatedViewModel
    {
        public int RelatedID { get; set; }
        public string Title { get; set; }
    }
}