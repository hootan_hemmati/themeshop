﻿using System;

namespace ThemeShop.Application.ViewModels.Theme
{
    public class ThemeViewModel
    {
        public int ThemeID { get; set; }
        public string Title { get; set; }
        public string ShortDescription { get; set; }
        public string ImageName { get; set; }
        public int VisitCount { get; set; }
        public int CommentCount { get; set; }
        public DateTime CreateDate { get; set; }
    }
}