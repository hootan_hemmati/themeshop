﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ThemeShop.Application.ViewModels.Theme
{
    public class ComplaintViewModel
    {
        public int ComplaintId { get; set; }

        [Display(Name = "نوع تخلف")]
        public string ComplaintTitle { get; set; }

        [Display(Name = "قالب")]
        public string ThemeId { get; set; }

        [Display(Name = "تاریخ")]
        public DateTime Date { get; set; }

        [Display(Name = "وضعیت")]
        public bool IsSeen { get; set; }

        [Display(Name = "شرح تخلف")]
        [DataType(DataType.MultilineText)]
        [MaxLength(500)]
        public string Description { get; set; }
    }
}