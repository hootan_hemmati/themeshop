﻿using System;
using System.Collections.Generic;

namespace ThemeShop.Application.ViewModels.Theme
{
    public class ThemeCommentViewModel
    {
        public int CommentID { get; set; }
        public int UserID { get; set; }
        public string UserAvatar { get; set; }
        public string UserName { get; set; }
        public string Comment { get; set; }
        public int? ParentID { get; set; }
        public List<ThemeCommentViewModel> subComments { get; set; }
        public DateTime CreateDate { get; set; }

        public ThemeCommentViewModel()
        {
            subComments = new List<ThemeCommentViewModel>();
        }
    }
}