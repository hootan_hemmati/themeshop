﻿using System;

namespace ThemeShop.Application.ViewModels.Theme
{
    public class ShowThemeListViewModel
    {
        public int ThemeID { get; set; }
        public string Title { get; set; }
        public string ShortDescription { get; set; }
        public string CategoryTitle { get; set; }
        public string CategoryName { get; set; }
        public int Price { get; set; }
        public int SellCount { get; set; }
        public int VisitCount { get; set; }
        public int DownloadCount { get; set; }
        public int CommentCount { get; set; }
        public string ImageName { get; set; }
        public DateTime CreateDate { get; set; }
    }
}