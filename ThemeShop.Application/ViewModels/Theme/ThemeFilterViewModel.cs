﻿using ThemeShop.Domain.Models;

namespace ThemeShop.Application.ViewModels.Theme
{
    public class ThemeFilterViewModel
    {
        public int PageId { get; set; }
        public string FilterSeller { get; set; }
        public string FilterTitle { get; set; }
        public bool IsAdminTemplate { get; set; }
        public ThemeChangeStatusMessages Status { get; set; }
    }
}