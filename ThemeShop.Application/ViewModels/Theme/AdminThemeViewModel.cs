﻿using System.Collections.Generic;

using ThemeShop.Domain.Models;

namespace ThemeShop.Application.ViewModels.Theme
{
    public class AdminThemeViewModel
    {
        public Themes Themes { get; set; }
        public IList<ThemeComments> ThemeComments { get; set; }
    }
}
