﻿namespace ThemeShop.Application.ViewModels.Article
{
    public class AuthorArticleViewModel
    {
        public string AuthorName { get; set; }
        public string AuthorAvatar { get; set; }
        public string AuthorDescription { get; set; }
    }
}