﻿namespace ThemeShop.Application.ViewModels.Article
{
    public class ArticleRelatedViewModel
    {
        public int RelatedID { get; set; }
        public string Title { get; set; }
    }
}