﻿namespace ThemeShop.Application.ViewModels.Article
{
    public class AuthorListViewModel
    {
        public int AuthorID { get; set; }
        public string AuthorName { get; set; }
    }
}