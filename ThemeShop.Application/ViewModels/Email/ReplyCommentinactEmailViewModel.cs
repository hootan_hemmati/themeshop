﻿namespace ThemeShop.Application.ViewModels.Email
{
    public class ReplyCommentinactEmailViewModel
    {
        public string UserName { get; set; }
        public string Url { get; set; }
        public string Title { get; set; }
    }
}