﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ThemeShop.Application.ViewModels.Ticket
{
    public class ShowTicketMessageViewModel
    {
        [Display(Name = "شناسه")]
        public int Ticketid { get; set; }

        [Display(Name = "قالب")]
        public int? ThemeId { get; set; }

        [Display(Name = "عنوان")]
        public string Title { get; set; }

        [Display(Name = "تاریخ")]
        public DateTime CreatedOn { get; set; }

        [Display(Name = "مالک")]
        public int OwnerUserID { get; set; }

        [Display(Name = "فرستنده")]
        public string Sender { get; set; }

        public List<TicketMessageContentViewModel> Messages { get; set; }
    }
}