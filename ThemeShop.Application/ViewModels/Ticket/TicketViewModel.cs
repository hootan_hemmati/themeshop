﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ThemeShop.Application.ViewModels.Ticket
{
    public class TicketViewModel
    {
        [Display(Name = "شناسه")]
        public int TicketId { get; set; }

        [Display(Name = "عنوان")]
        public string Title { get; set; }

        [Display(Name = "تاریخ")]
        public DateTime CreatedOn { get; set; }

        [Display(Name = "وضعیت")]
        public bool IsSeen { get; set; }

        public int Reciver { get; set; }
    }
}