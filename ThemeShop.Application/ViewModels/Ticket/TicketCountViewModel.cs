﻿namespace ThemeShop.Application.ViewModels.Ticket
{
    public class TicketCountViewModel
    {
        public int NotReadTicketCount { get; set; }
        public int SentTicketCount { get; set; }
        public int ContactUsTicketCount { get; set; }
        public int ReportAbuseTicketCount { get; set; }
    }
}