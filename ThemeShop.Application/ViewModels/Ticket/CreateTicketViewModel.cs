﻿using System.ComponentModel.DataAnnotations;

namespace ThemeShop.Application.ViewModels.Ticket
{
    public class CreateTicketViewModel
    {
        [Display(Name = "قالب")]
        public int? ThemeId { get; set; }

        [Display(Name = "عنوان")]
        public string Title { get; set; }

        [Display(Name = "پیام")]
        [DataType(DataType.MultilineText)]
        //[AllowHtml]
        public string Text { get; set; }
    }
}