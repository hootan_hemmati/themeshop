﻿namespace ThemeShop.Application.ViewModels.Ticket
{
    public class ThemeRejectEmailViewModel
    {
        public string SellerName { get; set; }
        public string ThemeName { get; set; }
        public string Message { get; set; }
        public string Email { get; set; }
    }
}