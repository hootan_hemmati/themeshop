﻿using System.ComponentModel.DataAnnotations;

namespace ThemeShop.Application.ViewModels.Ticket
{
    public class AddTicketMessageViewModel
    {
        [Display(Name = "پیام")]
        public string Message { get; set; }

        [Display(Name = "شناسه")]
        public int Ticketid { get; set; }
    }
}