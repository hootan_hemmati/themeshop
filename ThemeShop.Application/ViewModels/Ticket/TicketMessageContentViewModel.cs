﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ThemeShop.Application.ViewModels.Ticket
{
    public class TicketMessageContentViewModel
    {
        [Display(Name = "پیام")]
        public string Text { get; set; }

        [Display(Name = "مالک")]
        public int OwnerUserID { get; set; }

        [Display(Name = "تاریخ")]
        public DateTime CreatedOn { get; set; }
    }
}