﻿namespace ThemeShop.Application.ViewModels.SiteSetting
{
    public class ContactSiteViewModel
    {
        public string SiteName { get; set; }
        public string Tell { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public string Map { get; set; }
        public string Address { get; set; }
    }
}