﻿namespace ThemeShop.Application.ViewModels.SiteSetting
{
    public class ContactUsInFooterViewModel
    {
        public string SiteTitle { get; set; }
        public string SiteDomain { get; set; }
        public string SiteDescription { get; set; }
        public string Tell { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public string Address { get; set; }
    }
}