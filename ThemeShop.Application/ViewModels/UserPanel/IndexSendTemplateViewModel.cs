﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ThemeShop.Application.ViewModels.UserPanel
{
    public class IndexSendTemplateViewModel
    {
        [Display(Name = "شناسه")]
        public int ThemeID { get; set; }

        [Display(Name = "عنوان قالب")]
        [Required(ErrorMessage = "لطفا {0} را وارد کنید")]
        [MaxLength(400)]
        public string ThemeTitle { get; set; }

        public int GalleryCount { get; set; }
        public int AttachCount { get; set; }

        [Display(Name = "وضعیت")]
        public bool IsActive { get; set; }

        [Display(Name = "وضعیت")]
        public bool IsRejected { get; set; }

        [Display(Name = "تاریخ ایجاد")]
        [DisplayFormat(DataFormatString = "{0: yyyy/MM/dd}")]
        public DateTime CreateDate { get; set; }
    }
}