﻿using System;

namespace ThemeShop.Application.ViewModels.UserPanel
{
    public class UserPanelViewModel
    {
        public string UserName { get; set; }
        public string Email { get; set; }
        public DateTime RegisterDate { get; set; }
        public string Mobile { get; set; }
        public int ThemeBoughtCount { get; set; }
        public int TicketCount { get; set; }
        public int FavoritThemeCount { get; set; }
        public int WalletInventory { get; set; }
    }
}