﻿using System;

namespace ThemeShop.Application.ViewModels.UserPanel
{
    public class UserFavoritThemeListItemViewModel
    {
        public int ThemeID { get; set; }
        public string ThemeTitle { get; set; }
        public int Price { get; set; }
        public DateTime Date { get; set; }
    }
}