﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ThemeShop.Application.ViewModels.Wallet
{
    public class WalletViewModel
    {
        [Display(Name = "شناسه")]
        public int WalletId { get; set; }

        [Display(Name = "نوع تراکنش")]
        public string TransactionType { get; set; }

        [Display(Name = "نوع تراکنش")]
        public int TransactionTypeId { get; set; }

        [Display(Name = "نوع پرداختی")]
        public string Type { get; set; }

        [Display(Name = "نوع تراکنش")]
        public int TypeId { get; set; }

        [Display(Name = "نام کاربری")]
        public string UserName { get; set; }

        [Display(Name = "نام کاربری")]
        public int UserId { get; set; }

        [Display(Name = "مبلغ شارژ")]
        public long Price { get; set; }

        //[Display(Name = "شماره سفارش")]
        //public int? OrderID { get; set; }

        [Display(Name = "وضعیت")]
        public bool IsPay { get; set; }

        [Display(Name = "تاریخ")]
        public DateTime CreateDate { get; set; }

        [Display(Name = "توضیحات")]
        public string Description { get; set; }
    }
}