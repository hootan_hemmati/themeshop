﻿namespace ThemeShop.Application.ViewModels.Order
{
    public class BascketViewModel
    {
        public int OrderId { get; set; }
        public int Price { get; set; }
        public int ThemeId { get; set; }
        public string ThemeTitle { get; set; }
        public int Discount { get; set; }
    }
}