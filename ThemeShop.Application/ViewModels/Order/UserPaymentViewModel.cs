﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ThemeShop.Application.ViewModels.Order
{
    public class UserPaymentViewModel
    {
        //public List<TemplateInfo> TemplateInfos { get; set; }

        //public int CurrentPage { get; set; }
        //public int PageCount { get; set; }
        [Display(Name = "تاریخ")]
        public DateTime CreateBuy { get; set; }

        [Display(Name = "هزینه پرداختی")]
        public int Price { get; set; }

        [Display(Name = "نام خریدار")]
        public string Username { get; set; }

        [Display(Name = "نام فروشنده")]
        public string UsernameSeller { get; set; }

        [Display(Name = "عنوان قالب")]
        public string ThemeTitle { get; set; }
    }
}