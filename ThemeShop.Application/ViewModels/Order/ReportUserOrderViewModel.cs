﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ThemeShop.Application.ViewModels.Order
{
    public class ReportUserOrderViewModel
    {
        [Display(Name = "نام و نام خانوادگی")]
        public string FullName { get; set; }

        [Display(Name = "تاریخ")]
        public DateTime Date { get; set; }

        [Display(Name = "پرداختی کاربر")]
        public int Price { get; set; }
    }
}