﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using ThemeShop.Application.Interfaces;
using ThemeShop.Application.RequestModel;
using ThemeShop.Domain.Interfaces;
using ThemeShop.Domain.Models;

namespace ThemeShop.Application.Services
{
    internal class DiscountService : IDiscountService
    {
        private readonly IDiscountRepository _discountRepository;

        public DiscountService(IDiscountRepository discountRepository)
        {
            _discountRepository = discountRepository;
        }

        #region Discount

        public async Task<bool> AddDiscountAsync(DiscountRequst discount)
        {
            try
            {
                //Create Item
                Discounts discountItem = new Discounts()
                {
                    Code = discount.Code,
                    Count = discount.Count,
                    CreateDate = DateTime.Now,
                    ExpireDate = discount.ExpireDate,
                    Percentage = discount.Percentage,
                    UsedCount = 0
                };

                //Insert in DB
                int discountId = await _discountRepository.InsertDiscountAsync(discountItem);
                await _discountRepository.SaveAsync();

                //Insert Log
                await _discountRepository.InsertDiscountLogAsync(new DiscountLogs()
                {
                    DiscountDiscountId = discountId,
                    IsUsed = false,
                    LogTime = DateTime.Now,
                    Text = "ایجاد کد تخفیف",
                    UserId = discount.UserId
                });
                await _discountRepository.SaveAsync();

                //Insert Creator
                await _discountRepository.InsertUserDiscountAsync(new UserDiscounts()
                {
                    DiscountId = discountId,
                    UserId = discount.UserId,
                });
                await _discountRepository.SaveAsync();

                //Insert Selected Categories
                foreach (int categoryId in discount.SelectedCategories)
                {
                    await _discountRepository.InsertDiscountSelectedThemeAsync(new DiscountSelectedTheme()
                    {
                        DiscountId = discountId,
                        ThemeId = categoryId
                    });
                }
                await _discountRepository.SaveAsync();

                return true;
            }
            catch (Exception)
            {
                return false;
                throw;
            }
        }

        public async Task<bool> DeleteDiscountAsync(Discounts discounts, int userId)
        {
            try
            {
                //Remove User Used Discount
                _discountRepository.RemoveDiscountUsers(await _discountRepository.GetUserDiscountsAsync(x => x.DiscountId == discounts.DiscountId));
                await _discountRepository.SaveAsync();

                //Insert Log
                await _discountRepository.InsertDiscountLogAsync(new DiscountLogs()
                {
                    DiscountDiscountId = discounts.DiscountId,
                    IsUsed = false,
                    LogTime = DateTime.Now,
                    Text = "حذف کد تخفیف",
                    UserId = userId
                });
                await _discountRepository.SaveAsync();

                //Remove Selected Theme
                _discountRepository.RemoveDiscountTheme(
                     await _discountRepository.GetDiscountSelectedThemesAsync(x => x.DiscountId == discounts.DiscountId)
                      );
                await _discountRepository.SaveAsync();

                //Remove Discount
                _discountRepository.RemoveDiscount(discounts);
                await _discountRepository.SaveAsync();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<bool> DeleteDiscountAsync(int discountsId, int userId)
        {
            return await DeleteDiscountAsync(discountsId, userId);
        }

        public async Task<int> GetDiscountCountAsync()
        {
            return await _discountRepository.GetDicountCountAsync();
        }

        public async Task<Tuple<IList<Discounts>, int>> GetDiscountsAsync(FilterDiscount filter, byte page, byte take = 20)
        {
            //Get All Discount
            IList<Discounts> discounts = await _discountRepository.GetDiscountsAsync();

            //Filter Discount - Begin

            //Filter by CeateTime
            if (filter.CreatedDate != DateTime.MinValue)
            {
                discounts = discounts.Where(x => x.CreateDate >= filter.CreatedDate).ToList();
            }

            //Filter by ExpireTime
            if (filter.ExpireDate != DateTime.MinValue)
            {
                discounts = discounts.Where(x => x.ExpireDate <= filter.ExpireDate).ToList();
            }

            //Filter by Maximum Percentage
            if (filter.MaxDiscountPercentage != 0)
            {
                discounts = discounts.Where(x => x.Percentage <= filter.MaxDiscountPercentage).ToList();
            }

            //Filter by Minimum Percentage
            if (filter.MinDiscountPercentage != 0)
            {
                discounts = discounts.Where(x => x.Percentage >= filter.MinDiscountPercentage).ToList();
            }

            //Filter by Discount Inventory Usage
            if (filter.UsageCount != 0)
            {
                discounts = discounts.Where(x => x.Count == filter.UsageCount).ToList();
            }

            //Filter by Discount Used Count
            if (filter.UsedCount != 0)
            {
                discounts = discounts.Where(x => x.UsedCount == filter.UsedCount).ToList();
            }

            //Filter Discount - Done

            return new Tuple<IList<Discounts>, int>(
                //Discount List
                discounts.Skip((page - 1) * take).Take(take).AsEnumerable().ToList(),
                //All Discount Count for pagging
                discounts.Count());
        }

        public async Task<bool> UpdateDiscountAsync(DiscountRequst discount)
        {
            try
            {
                //Get Item From DB
                Discounts discountItem = await _discountRepository.GetDiscountAsync(discount.DiscountId);

                //Change Value
                discountItem.Code = discount.Code;
                discountItem.Count = discount.Count;
                discountItem.ExpireDate = discount.ExpireDate;
                discountItem.Percentage = discount.Percentage;

                //Update Item
                _discountRepository.UpdateDiscount(discountItem);
                await _discountRepository.SaveAsync();

                //Add Log In DB
                await _discountRepository.InsertDiscountLogAsync(new DiscountLogs()
                {
                    DiscountDiscountId = discount.DiscountId,
                    IsUsed = false,
                    LogTime = DateTime.Now,
                    Text = "بروزرسانی کد تخفیف ",
                    UserId = discount.UserId
                });
                await _discountRepository.SaveAsync();

                //Change Selected Theme - Begin

                //Remove All Previous Selected Theme
                _discountRepository.RemoveDiscountTheme(
                    await _discountRepository.GetDiscountSelectedThemesAsync(x => x.DiscountId == discount.DiscountId)
                    );
                await _discountRepository.SaveAsync();

                //Add Selected Theme
                foreach (int CategoryId in discount.SelectedCategories)
                {
                    await _discountRepository.InsertDiscountSelectedThemeAsync(new DiscountSelectedTheme()
                    {
                        DiscountId = discount.DiscountId,
                        ThemeId = CategoryId
                    });
                }
                //Change Selected Theme - Done

                await _discountRepository.SaveAsync();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        #endregion Discount

        #region DiscountLog

        public async Task<IList<DiscountLogs>> GetDiscountLogsAsync(int discountId, int page)
        {
            IList<DiscountLogs> discountLogs = await _discountRepository.GetDiscountLogsAsync(x => x.DiscountDiscountId == discountId);
            return discountLogs.Skip((page - 1) * 20).Take(20).ToList();
        }

        public async Task<IList<Discounts>> GetDiscountsAsync(byte page, byte take = 20)
        {
            IList<Discounts> discount = await _discountRepository.GetAllDiscountsAsync();
            return discount.Skip((page - 1) * take).Take(take).ToList();
        }

        #endregion DiscountLog
    }
}