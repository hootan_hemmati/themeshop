﻿using Microsoft.AspNetCore.Http;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

using ThemeShop.Application.Convertors;
using ThemeShop.Application.Interfaces;
using ThemeShop.Application.Utilities;
using ThemeShop.Application.ViewModels.Theme;
using ThemeShop.Domain.Interfaces;
using ThemeShop.Domain.Models;

namespace ThemeShop.Application.Services
{
    public class ThemeService : IThemeService
    {
        private readonly IThemeRepository _themeRepository;

        public ThemeService(IThemeRepository themeRepository)
        {
            _themeRepository = themeRepository;
        }

        #region Theme
        public Task<Themes> GetThemeAsync(int id)
        {
            return _themeRepository.GetThemeByIdAsync(id);
        }

        public async Task<AdminThemeViewModel> GetAdminThemeAsync(int id)
        {
            return new AdminThemeViewModel()
            {
                Themes = await _themeRepository.GetThemeByIdAsync(id),
                ThemeComments = await GetCommentsByThemeIdAsync(id)
            };
        }
        public void ActivateTheme(Themes theme)
        {
            //Set Activte Value
            theme.IsActive = true;
            theme.IsRejected = false;
            theme.CreateDate = DateTime.Now;

            //Update Theme
            _themeRepository.UpdateTheme(theme);
        }

        public async Task ActivateTheme(int themeId)
        {
            ActivateTheme(await _themeRepository.GetThemeByIdAsync(themeId));
            await _themeRepository.SaveAsync();
        }

        public void DeactiveTheme(Themes theme)
        {
            //Set Deactive Value
            theme.IsActive = false;

            //Update Theme
            _themeRepository.UpdateTheme(theme);
        }

        public async Task DeactiveTheme(int themeId)
        {
            DeactiveTheme(await _themeRepository.GetThemeByIdAsync(themeId));
            await _themeRepository.SaveAsync();
        }

        public void DeleteTheme(Themes theme)
        {
            //Set Deleted Value
            theme.IsDelete = true;

            //Update Theme
            _themeRepository.UpdateTheme(theme);
        }

        public async Task DeleteTheme(int themeId)
        {
            DeleteTheme(await _themeRepository.GetThemeByIdAsync(themeId));

            await _themeRepository.SaveAsync();
        }

        public async Task<Tuple<IList<ListThemeViewModel>, int>> GetThemeAsync(int page = 1, string title = "", int sellerId = 0, int categoryId = 0, bool isAdmintheme = false, ThemeStatus status = ThemeStatus.NoFilter)
        {
            //Get All Them
            IList<Themes> theme = await _themeRepository.GetThemesAsync();

            //Filter Theme

            //Filter by Title
            theme = title != "" ? theme.Where(t => t.ThemeTitle == title).ToList() : theme;

            //Filter by Seller
            theme = sellerId != 0 ? theme.Where(t => t.SellerId == sellerId).ToList() : theme;

            //Filter by Categories
            theme = theme.Where(t => t.ThemeSelectedCategories.Any(c => c.ThemeCategoryId == categoryId)).ToList();

            //Filter by Is Admin
            theme = isAdmintheme ? theme.Where(t => t.SellerId == sellerId).ToList() : theme.Where(t => t.SellerId != sellerId).ToList();

            //Filter by Theme Status
            theme = status == ThemeStatus.Confirmed ? theme.Where(t => t.IsActive).ToList() : theme;
            theme = status == ThemeStatus.Deleted ? theme.Where(t => t.IsDelete).ToList() : theme;
            theme = status == ThemeStatus.DontReject ? theme.Where(t => !t.IsRejected && !t.IsActive && !t.IsDelete).ToList() : theme;
            theme = status == ThemeStatus.Rejected ? theme.Where(t => t.IsRejected).ToList() : theme;

            //Generate View Model
            IList<ListThemeViewModel> themeList = theme.OrderByDescending(t => t.CreateDate)
                .Select(t => new ListThemeViewModel()
                {
                    ThemeID = t.ThemeId,
                    IsActive = t.IsActive,
                    Title = t.ThemeTitle,
                    SellerName = t.Seller.Name,
                    CommentCount = t.ThemeComments.Count,
                    IsCommentTheme = t.CanInsertComment,
                    Price = t.Price,
                    VisitCount = t.ThemeVisits.Count,
                    RelatedThemeCount = t.ThemeRelateds.Count,
                    AttachCount = t.ThemeAttachments.Count,
                    GalleryCount = t.ThemeGalleries.Count,
                    ThemeCategories = t.ThemeSelectedCategories
                    .Select(c => c.ThemeCategory)
                    .ToList(),
                    IsReject = t.IsRejected
                })
                  .Skip((page - 1) * 50)
                  .Take(50).AsEnumerable().ToList();

            //Return Tuple
            return Tuple.Create(themeList, theme.Count());
        }

        public async Task RejectThemeAsync(int themeId, string message)
        {
            //Get Theme
            Themes theme = await _themeRepository.GetThemeByIdAsync(themeId);

            //Set Rejected Value
            theme.IsActive = false;
            theme.IsRejected = true;

            //Update Theme
            _themeRepository.UpdateTheme(theme);
            await _themeRepository.SaveAsync();

            //Add Status
            await _themeRepository.AddThemeChangeStatusAsync(themeId, (int)ThemeStatus.Rejected, message);
            await _themeRepository.SaveAsync();
        }

        #endregion

        #region Theme Category
        public async Task AddCategoriesAsync(ThemeCategories category)
        {
            //Add Category to Theme
            await _themeRepository.AddCategoryAsync(category);
            await _themeRepository.SaveAsync();

        }

        public async Task AddThemeCategoriesAsync(int themeId, List<int> categoriesId)
        {
            foreach (int categoryId in categoriesId)
            {
                await _themeRepository.AddCategoryThemeAsync(themeId, categoryId);
                await _themeRepository.SaveAsync();
            }
        }

        public async Task DeleteCategories(int categoryId)
        {
            //Remove Theme Selected Categories
            _themeRepository.RemoveThemeSelectedCategories(
                await _themeRepository.GetThemeSelectedCategoriesAsync(
                    c => c.ThemeCategory.ThemeCategoryId == categoryId)
                );
            await _themeRepository.SaveAsync();

            //Remove Category
            DeleteCategories(await _themeRepository.GetCategoryByIdAsync(categoryId));
            await _themeRepository.SaveAsync();

        }

        public void DeleteCategories(ThemeCategories category)
        {
            //Set Deleted Value
            category.IsDelete = true;

            //Update Theme
            _themeRepository.UpdateCategory(category);
        }

        public async Task EditThemeCategory(ThemeCategories category)
        {
            _themeRepository.UpdateCategory(category);
            await _themeRepository.SaveAsync();

        }

        public async Task<IList<ThemeCategories>> GetThemeCategoriesAsync()
        {
            return await _themeRepository.GetAllThemeCategoriesAsync();
        }

        public async Task<ThemeCategories> GetThemeCategoryAsync(int categoryId)
        {
            return await _themeRepository.GetCategoryByIdAsync(categoryId);
        }


        #endregion

        #region Theme Related
        public async Task<bool> AddRelatedThemeAsync(ThemeRelateds themeRelateds)
        {
            //Check Related Theme Exist
            //#TODO Expression Functuion
            if (!await _themeRepository.CheckRelatedExist(themeRelateds.ThemeId, themeRelateds.RelatedId))
            {
                //Add Related Theme
                await _themeRepository.AddRelatedThemeAsync(themeRelateds);
                await _themeRepository.SaveAsync();

                return true;
            }
            else
            {
                return false;
            }
        }

        public async Task DeleteRelatedThemeById(int relatedThemId)
        {
            _themeRepository.DeleteRelatedTheme(await _themeRepository.GetThemeRelated(relatedThemId));
            await _themeRepository.SaveAsync();
        }

        public async Task<Tuple<IList<ThemeRelatedViewModel>, int>> GetThemeRelatedForAdminAsync(int themeId, byte page, byte take)
        {
            IList<ThemeRelateds> themeRelateds = await _themeRepository.GetRelatedsAsync(x => x.ThemeId == themeId, page, take);
            IList<ThemeRelatedViewModel> viewModels = themeRelateds.Select(t => new ThemeRelatedViewModel()
            {
                RelatedID = t.RelatedId,
                Title = t.Theme.ThemeTitle
            }).ToList();
            int themeRelatedCount = await _themeRepository.GetThemePreviewsCountAsync(x => x.ThemeId == themeId);
            return Tuple.Create(viewModels, themeRelatedCount);
        }

        #endregion

        #region Theme Offer

        public async Task<bool> AddThemeOfferAsync(ThemeOfferTodays themeOffer)
        {
            try
            {
                //Check Offer Exist
                if (!await _themeRepository.IsThemeExistAsync(themeOffer.ThemeId))
                {
                    return false;
                }

                //Add Offer
                await _themeRepository.AddOfferAsync(themeOffer);
                await _themeRepository.SaveAsync();

                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }

        public void ChangeThemeOfferState(ThemeOfferTodays themeOffer)
        {
            //Change Active Value
            themeOffer.IsActive = !themeOffer.IsActive;

            //Update Theme
            _themeRepository.UpdateThemeOffer(themeOffer);

        }

        public async Task ChangeThemeOfferStateAsync(int themeOfferId)
        {
            ChangeThemeOfferState(await _themeRepository.GetThemeOfferAsync(themeOfferId));
            await _themeRepository.SaveAsync();
        }

        public async Task DeleteThemeOfferAsync(int themeOfferId)
        {
            _themeRepository.DeleteThemeOffer(await _themeRepository.GetThemeOfferAsync(themeOfferId));
            await _themeRepository.SaveAsync();
        }
        //Get Offer All time
        public async Task<Tuple<IList<ThemeOfferTodays>, int>> GetAllThemeOfferAsync(int page)
        {
            return Tuple.Create(await _themeRepository.GetAllOfferAsync(page), (await _themeRepository.GetOfferCountAsync() / 24) + 1);
        }
        //Get Offer by Date
        public async Task<Tuple<IList<ThemeOfferTodays>, int>> GetThemeOfferAsync(int page, DateTime date)
        {
            return Tuple.Create(await _themeRepository.GetOffersAsync(x => x.StartDate > date && x.EndDate < date), (await _themeRepository.GetOfferCountAsync(x => x.StartDate > date && x.EndDate < date) / 24) + 1);
        }

        #endregion

        #region Theme Preview

        public void ChangeThemePreviewState(ThemePreviews themePreview)
        {
            //Change Value
            themePreview.IsActive = !themePreview.IsActive;

            //Update Theme
            _themeRepository.UpdateThemePreview(themePreview);
        }

        public async Task ChangeThemePreviewStateAsync(int themePreviewId)
        {

            ChangeThemePreviewState(await _themeRepository.GetThemePreviewsAsync(themePreviewId));
            await _themeRepository.SaveAsync();
        }

        public async Task DeleteThemePreviewAsync(int themePreviewId)
        {
            ThemePreviews previews = await _themeRepository.GetThemePreviewsAsync(themePreviewId);
            _themeRepository.DeleteThemePreview(previews);
            await _themeRepository.SaveAsync();
        }

        public async Task<Tuple<IList<ThemePreviews>, int>> GetThemePreviewsAsync(int previewId, byte page, byte take)
        {
            ThemePreviews preview = await _themeRepository.GetThemePreviewsAsync(previewId);
            return Tuple.Create(await _themeRepository.GetThemePreviewsAsync(t => t.ThemeId == preview.ThemeId, page, take),
                await _themeRepository.GetThemePreviewsCountAsync(t => t.ThemeId == preview.ThemeId));
        }

        public async Task<bool> InsertThemePreviewAsync(ThemePreviews previews)
        {
            try
            {
                //Init Values
                previews.RootFolder = previews.RootFolder.Trim();
                previews.PageName = previews.PageName.Trim();

                //Add Theme preview
                await _themeRepository.AddThemePreviewAsync(previews);
                await _themeRepository.SaveAsync();

                //Insert Default preview Page
                return await InsertDefultPreviewPageAsync(previews.PreviewId);
            }
            catch (Exception)
            {
                return false;
                throw;
            }


        }

        public async Task<bool> InsertDefultPreviewPageAsync(int id)
        {
            try
            {
                //Get Selected ThemePreview
                ThemePreviews preview = await _themeRepository.GetThemePreviewsAsync(id);

                //Get All Theme Preview  from theme
                IList<ThemePreviews> themePreviews = await _themeRepository.GetThemePreviewsAsync(t => t.ThemeId == preview.ThemeId, 0, 0);

                //disable previus default page
                foreach (ThemePreviews item in themePreviews)
                {
                    item.IsDefaultPage = false;
                    _themeRepository.UpdateThemePreview(item);
                    await _themeRepository.SaveAsync();
                }

                //Active Selected default Page
                preview.IsDefaultPage = true;
                _themeRepository.UpdateThemePreview(preview);
                await _themeRepository.SaveAsync();

                return true;
            }
            catch (Exception)
            {
                return false;
            }


        }


        #endregion

        #region Theme Comment
        public void DeleteThemeComment(ThemeComments comments)
        {
            //Change Value
            comments.IsDelete = true;

            //Update Theme
            _themeRepository.UpdateComment(comments);

        }

        public async Task DeleteThemeCommentAsync(int themeCommentId)
        {
            DeleteThemeComment(await _themeRepository.GetThemeCommentsAsync(themeCommentId));
            await _themeRepository.SaveAsync();
        }

        public async Task<IList<ThemeComments>> GetCommentsByThemeIdAsync(int themeId, bool showDeletedComments = false)
        {
            //Get All Theme Comments
            IList<ThemeComments> comments = await _themeRepository.GetCommentsbyThemeIdAsync(themeId, showDeletedComments);

            //Change Read State
            foreach (ThemeComments item in comments)
            {
                item.IsRead = true;
            }

            //Update Comments
            _themeRepository.UpdateComment(comments);
            await _themeRepository.SaveAsync();

            return comments;
        }

        public async Task<Tuple<IList<ThemeComments>, int>> GetUnreadCommentsAsync(int page, byte take)
        {
            //Get All Unread Comments
            IList<ThemeComments> comments = await _themeRepository.GetUnreadCommentsAsync(page, take);
            int unreadCommentsCountount = await _themeRepository.GetUnreadCommentsCountAsync();
            //Change read State
            foreach (ThemeComments item in comments)
            {
                item.IsRead = true;
            }

            //Update Comments
            _themeRepository.UpdateComment(comments);
            await _themeRepository.SaveAsync();

            return Tuple.Create(comments, unreadCommentsCountount);
        }

        public async Task<bool> ChangeThemeAttachmentStateAsync(int id)
        {
            try
            {
                ThemeAttachments attach = await _themeRepository.GetThemeAttachmentByIdAsync(id);
                attach.IsActive = !attach.IsActive;
                _themeRepository.UpdateThemeAttachment(attach);
                await _themeRepository.SaveAsync();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<bool> ChangeThemeAttachmentIsFreeAsync(int id)
        {
            try
            {
                ThemeAttachments attach = await _themeRepository.GetThemeAttachmentByIdAsync(id);
                attach.IsFree = !attach.IsFree;
                _themeRepository.UpdateThemeAttachment(attach);
                await _themeRepository.SaveAsync();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        #endregion

        #region Theme Attachments

        public async Task<IList<ThemeChangeStatusMessages>> GetThemeChangeStatusMessagesAsync(int themeId)
        {
            return await _themeRepository.GetThemeChangeStatusMessagesAsync(themeId);
        }

        public async Task<IList<ThemeAttachments>> GetThemeAttachmentsAsync(int themeId)
        {
            return await _themeRepository.GetThemeAttachmentsAsync(themeId);
        }

        public async Task<string> AddThemeAttachmentAsync(IFormFile attachmentFile, ThemeAttachments themeAttachment)
        {
            try
            {
                //Check Filename Fill
                if (themeAttachment.Title != null || themeAttachment.Title != "")
                {
                    //Check Attachment Exist
                    if (File.Exists(SiteDirectories.ThemeAttachmentsPhysicalPath() + themeAttachment.Title))
                    {
                        return "این فایل در حافظه وجود دارد";
                    }
                }
                else
                {
                    //Generate Auto File Name
                    themeAttachment.Title = Guid.NewGuid().ToString();
                }

                //Check File Selected 
                if (attachmentFile == null)
                {
                    return "شما هیچ فایلی انتخاب نکردید";
                }

                //Get File Extention
                themeAttachment.FileExtension = Path.GetExtension(attachmentFile.FileName);
                if (themeAttachment.FileExtension.StartsWith("."))
                {
                    themeAttachment.FileExtension =
                        themeAttachment.FileExtension.Substring(1, themeAttachment.FileExtension.Length - 1);
                }

                if (attachmentFile != null)
                {
                    //Fill Attachment Size 
                    //Save Attachment
                    await attachmentFile.CopyToAsync(System.IO.File.Create(SiteDirectories.ThemeAttachmentsPhysicalPath() + attachmentFile.FileName));
                    themeAttachment.FileSize = (int)attachmentFile.Length;
                }

                //Add ThemeAttachment Item
                await _themeRepository.AddThemeAttachmentAsync(themeAttachment);
                await _themeRepository.SaveAsync();


                return "فایل با موفقیت ثبت شد";
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
                throw;
            }

        }

        public async Task<string> DeleteThemeAttachmentAsync(int id)
        {
            try
            {
                //get Item
                ThemeAttachments themeAttach = await _themeRepository.GetThemeAttachmentByIdAsync(id);
                if (themeAttach == null)
                {
                    return "فایل مورد نظر در پایگاه داده یافت نشد!";
                }

                //Check File Exist
                if (File.Exists(SiteDirectories.ThemeAttachmentsPhysicalPath() + themeAttach.FileName))
                {
                    //Delete File
                    File.Delete(SiteDirectories.ThemeAttachmentsPhysicalPath() + themeAttach.FileName);
                }
                else
                {
                    return "فایل مورد نظر در سیستم یافت نشد!!";
                }

                _themeRepository.DeleteThemeAttachment(themeAttach);
                await _themeRepository.SaveAsync();

                return "فایل با موفقیت حذف شد";
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }

        }
        #endregion

        #region Theme Galleries
        public async Task<string> AddThemeGalleryAsync(ThemeGalleries galleries, IFormFile themeImage)
        {
            try
            {
                ImageConvertor imageResizer = new ImageConvertor();


                //Validate File
                string fileType = Path.GetExtension(themeImage.FileName);
                if (fileType != ".jpg" && fileType != ".png" && fileType != ".jpeg")
                {
                    return "نوع فایل معتبر نمیباشد";
                }

                // Gallery Image URL
                string themeGalleryImageName = galleries.GalleryId + "_" + themeImage.FileName;
                string imageURL = galleries.ImageTitle + Path.GetExtension(themeImage.FileName);
                FileStream physicalURL = File.Create(SiteDirectories.ThemeGalleriesImagePhysicalPath() + imageURL);

                //Galley Resized Image URL
                FileStream resizedURL = File.Create(SiteDirectories.ThemeGalleriesThumbPhysicalPath() + imageURL);

                //Save Gallery Image
                await themeImage.CopyToAsync(physicalURL);

                //Resize IMage
                imageResizer.Image_resize(physicalURL.ToString(), resizedURL.ToString(), 700);

                //Add Theme Gallery
                galleries.GalleryImageName = themeGalleryImageName;
                await _themeRepository.InsertThemeGalleryAsync(galleries);
                await _themeRepository.SaveAsync();

                return "ثبت گالری با موفقیت انجام شد";
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }

        }

        public async Task<IList<ThemeGalleries>> GetThemeGalleriesAsync(int themeId)
        {
            return await _themeRepository.GetThemeGalleriesAsync(x => x.ThemeId == themeId);
        }

        public async Task<bool> DeleteThemeGallerieAsync(int themeGalleryId)
        {
            try
            {
                //Get Theme Galley
                ThemeGalleries themeGalleries = await _themeRepository.GetThemeGalleriesAsync(themeGalleryId);

                //Check Exist
                if (themeGalleries == null)
                {
                    return false;
                }

                //Delete Physical Images
                File.Delete(SiteDirectories.ThemeGalleriesThumbPhysicalPath() + themeGalleries.GalleryImageName);
                File.Delete(SiteDirectories.ThemeGalleriesImagePhysicalPath() + themeGalleries.GalleryImageName);

                //Remove Item From DB
                _themeRepository.DeleteThemeGallery(themeGalleries);
                await _themeRepository.SaveAsync();


                return true;
            }
            catch (Exception)
            {
                return false;
            }


        }


        #endregion
    }
}