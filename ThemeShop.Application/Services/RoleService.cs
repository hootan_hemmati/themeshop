﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using ThemeShop.Application.Interfaces;
using ThemeShop.Domain.Interfaces;
using ThemeShop.Domain.Models;

namespace ThemeShop.Application.Services
{
    public class RoleService : IRoleService
    {
        private readonly IRoleRepository _roleRepository;

        public RoleService(IRoleRepository roleRepository)
        {
            _roleRepository = roleRepository;
        }

        #region Roles

        public async Task<IList<Roles>> GetAllRolesAsync()
        {
            return await _roleRepository.GetAllRolesAsync();
        }

        public async Task<IList<Roles>> FilterRolesAsync(int skip, int take = 10, string filterRoles = "")
        {
            IList<Roles> roles = await _roleRepository.FilterRolesAsync(filterRoles);
            return roles.Distinct().OrderBy(role => role.RoleTitle).Skip((skip - 1) * take).Take(take).ToList();
        }

        public async Task<int> FilterRolesCountAsync(string filterRoles = "")
        {
            return await _roleRepository.FilterRolesCountAsync(filterRoles);
        }

        public async Task<Roles> GetRoleByIdAsync(int roleId)
        {
            return await _roleRepository.GetRoleByIdAsync(roleId);
        }

        public async Task<Roles> GetRoleByTitleAsync(string roleTitle)
        {
            return await _roleRepository.GetRoleByTitleAsync(roleTitle);
        }

        public async Task AddRoleAsync(Roles role)
        {
            if (role.IsDefualtRole == true)
            {
                Roles defualtRole = await _roleRepository.GetDefualtRoleAsync();
                defualtRole.IsDefualtRole = false;
                _roleRepository.UpdateRole(defualtRole);
                await _roleRepository.SaveAsync();
            }
            await _roleRepository.InsertRoleAsync(role);
            await _roleRepository.SaveAsync();
        }

        public void DeleteRole(Roles role)
        {
            _roleRepository.RemoveRole(role);
            _roleRepository.Save();
        }

        public async Task DeleteRoleByIdAsync(int roleId)
        {
            await _roleRepository.RemoveRoleAsync(roleId);
            await _roleRepository.SaveAsync();
        }

        public async Task UpdateRoleAsync(Roles role)
        {
            if (role.IsDefualtRole == true)
            {
                Roles defualtRole = await _roleRepository.GetDefualtRoleAsync();
                defualtRole.IsDefualtRole = false;
                _roleRepository.UpdateRole(defualtRole);
                await _roleRepository.SaveAsync();
            }
            _roleRepository.UpdateRole(role);
            await _roleRepository.SaveAsync();
        }

        public async Task<bool> IsRoleExistByIdAsync(int roleId)
        {
            return await _roleRepository.IsRoleExistByRoleIdAsync(roleId);
        }

        #endregion Roles

        #region UserRoles

        public async Task<IList<UserRoles>> GetUserRolesByUserIdAsync(int userId)
        {
            return await _roleRepository.GetUserRolesAsync(userRole => userRole.UserId == userId);
        }

        public async Task<IList<UserRoles>> GetAllUserRolesInRoleIdAsync(int roleId)
        {
            return await _roleRepository.GetUserRolesAsync(user => user.RoleId == roleId);
        }

        public async Task AddUserRoleAsync(int userId, int roleId)
        {
            await _roleRepository.InsertUserRoleAsync(new UserRoles() { UserId = userId, RoleId = roleId });
            await _roleRepository.SaveAsync();
        }

        public async Task RemoveUserRoleAsync(int userId, int roleId)
        {
            UserRoles userRole = await _roleRepository.GetUserRoleAsync(userRole => userRole.UserId == userId && userRole.RoleId == roleId);
            _roleRepository.DeleteUserRole(userRole);
            await _roleRepository.SaveAsync();
        }

        public async Task RemoveAllUserRoleByUserIdAsync(int userId)
        {
            IList<UserRoles> userRoles = await _roleRepository.GetUserRolesAsync(userRole => userRole.UserId == userId);
            foreach (UserRoles role in userRoles)
            {
                _roleRepository.DeleteUserRole(role);
            }
            await _roleRepository.SaveAsync();
        }

        public async Task<bool> IsUserInPermissionAsync(int userId, int permissionId)
        {
            return await _roleRepository.IsUserRoleExistAsync(u => u.UserId == userId && u.Roles.RolePremissions.Any(p => p.PermissionId == permissionId));
        }

        #endregion UserRoles

        #region RolesPermissions

        public async Task AddRolePermissionAsync(int roleId, int rolePermissionId)
        {
            await _roleRepository.InsertRolePremissionAsync(new RolePremissions() { RoleId = roleId, PermissionId = rolePermissionId });
            await _roleRepository.SaveAsync();
        }

        public async Task UpdateRolePermissionAsync(int roleId, int rolePermissionId)
        {
            await _roleRepository.InsertRolePremissionAsync(new RolePremissions() { RoleId = roleId, PermissionId = rolePermissionId });
            await _roleRepository.SaveAsync();
        }

        public async Task RemoveRolePermissionAsync(int roleId)
        {
            IList<RolePremissions> roles = await _roleRepository.GetAllRolePremissionsByRoleIdAsync(roleId);
            foreach (RolePremissions role in roles.ToList())
            {
                _roleRepository.DeleteRolePremission(role);
            }
            await _roleRepository.SaveAsync();
        }

        public async Task<IList<RolePremissions>> GetPremissionsByRoleIdAsync(int roleId)
        {
            return await _roleRepository.GetAllRolePremissionsByRoleIdAsync(roleId);
        }

        public async Task<IList<RolePremissions>> GetRolePremissionsByPremissionIdAsync(int permissionId)
        {
            return await _roleRepository.GetRolePremissionsAsync(rolePermission => rolePermission.PermissionId == permissionId);
        }

        #endregion RolesPermissions

        public void Save()
        {
            _roleRepository.Save();
        }

        public async Task SaveAsync()
        {
            await _roleRepository.SaveAsync();
        }
    }
}