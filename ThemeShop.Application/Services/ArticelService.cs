﻿using Microsoft.AspNetCore.Http;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

using ThemeShop.Application.Convertors;
using ThemeShop.Application.Interfaces;
using ThemeShop.Application.RequestModel;
using ThemeShop.Application.Utilities;
using ThemeShop.Application.ViewModels.Article;
using ThemeShop.Domain.Interfaces;
using ThemeShop.Domain.Models;

namespace ThemeShop.Application.Services
{
    public class ArticelService : IArticelService
    {
        private readonly IArticleRepository _articleRepository;

        public ArticelService(IArticleRepository articleRepository)
        {
            _articleRepository = articleRepository;
        }


        public async Task<bool> CreateArticleCategoryAsync(ArticleCategoryRequest articleCategory)
        {
            try
            {

                //Add Child Category
                if (articleCategory.MainCategoryId != null && articleCategory.MainCategoryId != 0)
                {
                    await _articleRepository.InsertArticleCategoryAsync(new ArticleCategories()
                    {
                        IsActive = true,
                        IsDelete = false,
                        MainArticleId = articleCategory.MainCategoryId.Value,
                        NameInUrl = articleCategory.NameInUrl,
                        Title = articleCategory.Title
                    });
                }

                //Add Parent Category
                else
                {
                    await _articleRepository.InsertMainArticleCategoryAsync(new MainArticleCategories()
                    {
                        IsActive = true,
                        IsDelete = false,
                        NameInUrl = articleCategory.NameInUrl,
                        Title = articleCategory.Title
                    });
                }

                await _articleRepository.SaveAsync();

                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }

        public async Task<MainArticleCategories> GetArticleCategoryAsync(int id)
        {
            return await _articleRepository.GetMainArticleCategoriesAsync(id);
        }

        public async Task<int> GetArticleCountAsync()
        {
            return await _articleRepository.GetArticleCountAsync();
        }

        public async Task<IList<ListArticleViewModel>> GetArticlesAsync(byte page, byte take = 20)
        {
            //Return 20 Article in ViewModel
            IList<Articles> articel = await _articleRepository.GetArticlesAsync();
            return articel
                .Skip((page - 1) * take)
                .Take(take)
                .Select(x => new ListArticleViewModel()
                {
                    ArticleCategories = x.ArticleCategory,
                    ArticleID = x.ArticleId,
                    CommentCount = x.ArticleComments.Count,
                    CreateDate = x.CreateDate,
                    IsActive = x.IsActive,
                    IsCommentActive = x.CanInsertComment,
                    IsVip = x.IsVip,
                    RelatedArticleCount = x.ArticleRelateds.Count,
                    ShortDescription = x.ShortDescription,
                    Title = x.ArticleTitle,
                    VisitCount = x.ArticleVisits.Count
                }).ToList();
        }

        public async Task<Tuple<IList<ListArticleViewModel>, int>> GetArticlesAsync(FilterArticle filter, byte page, byte take = 20)
        {
            //Get All Article
            IList<Articles> articles = await _articleRepository.GetArticlesAsync();

            //Filter by Article Title
            if (filter.ArticleTitle != null && filter.ArticleTitle != "")
            {
                articles = articles.Where(a => a.ArticleTitle.Contains(filter.ArticleTitle)).ToList();
            }

            //Filter by Article Category
            if (filter.CategoryId != null && filter.CategoryId != 0)
            {
                articles = articles.Where(a => a.ArticleCategory.Any(x => x.CategoryId == filter.CategoryId)).ToList();
            }

            //Filter by Article Time - Begin
            if (filter.FromDate != DateTime.MinValue)
            {
                articles = articles.Where(a => a.CreateDate >= filter.FromDate).ToList();
            }

            if (filter.ToDate != DateTime.MinValue)
            {
                articles = articles.Where(a => a.CreateDate <= filter.ToDate).ToList();
            }
            //Filter by Article Time - Done

            //Filter by Article Status
            if (filter.IsActive != null)
            {
                articles = articles.Where(a => a.IsActive == filter.IsActive).ToList();
            }

            //Filter by Article VIP Status
            if (filter.IsVip != null)
            {
                articles = articles.Where(a => a.IsVip == filter.IsVip).ToList();
            }

            //Return Tuple (20 Article - Filtered Article Count)
            IList<ListArticleViewModel> articlesList = articles.Skip((page - 1) * take)
                .Take(take)
                .Select(x => new ListArticleViewModel()
                {
                    ArticleCategories = x.ArticleCategory,
                    ArticleID = x.ArticleId,
                    CommentCount = x.ArticleComments.Count,
                    CreateDate = x.CreateDate,
                    IsActive = x.IsActive,
                    IsCommentActive = x.CanInsertComment,
                    IsVip = x.IsVip,
                    RelatedArticleCount = x.ArticleRelateds.Count,
                    ShortDescription = x.ShortDescription,
                    Title = x.ArticleTitle,
                    VisitCount = x.ArticleVisits.Count
                }).ToList();
            return Tuple.Create(articlesList, articles.Count);
        }

        public async Task<bool> UpdateArticleCategoryAsync(ArticleCategoryRequest articleCategory)
        {
            try
            {
                //Update Parent Category
                if (articleCategory.MainCategoryId != 0 && articleCategory.MainCategoryId != null)
                {
                    //Get Item
                    ArticleCategories categories = await _articleRepository.GetArticleCategoriesAsync(articleCategory.Id);

                    //Fill New Value
                    categories.MainArticleId = articleCategory.MainArticleId;
                    categories.NameInUrl = articleCategory.NameInUrl;
                    categories.Title = articleCategory.Title;

                    //Update Item
                    _articleRepository.UpdateArticleCategories(categories);
                    await _articleRepository.SaveAsync();

                }

                //Update Child Category
                else
                {
                    //Get Item
                    MainArticleCategories categories = await _articleRepository.GetMainArticleCategoriesAsync(articleCategory.Id);

                    //Fill New Value
                    categories.NameInUrl = articleCategory.NameInUrl;
                    categories.Title = articleCategory.Title;

                    //Update Item
                    _articleRepository.UpdateMainArticleCategories(categories);
                    await _articleRepository.SaveAsync();

                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }



        }

        public async Task<bool> DeleteArticleCategoryAsync(int id)
        {
            try
            {
                //Get Category
                ArticleCategories categories = await _articleRepository.GetArticleCategoriesAsync(id);

                //Set Deleted Value
                categories.IsActive = false;
                categories.IsDelete = true;

                //Update category
                _articleRepository.UpdateArticleCategories(categories);
                await _articleRepository.SaveAsync();

                return true;
            }
            catch (Exception)
            {
                return false;
            }


        }

        public bool DeleteArticleCategoryAsync(ArticleCategories categories)
        {
            try
            {

                //Set Deleted Value
                categories.IsActive = false;
                categories.IsDelete = true;

                //Update category
                _articleRepository.UpdateArticleCategories(categories);
                _articleRepository.Save();

                return true;
            }
            catch (Exception)
            {
                return false;
            }


        }

        public async Task<bool> DeleteMainArticleCategoryAsync(int id)
        {
            try
            {
                //Get Item
                MainArticleCategories categories = await _articleRepository.GetMainArticleCategoriesAsync(id);

                //Delete Child Categories
                foreach (ArticleCategories item in categories.ArticleCategories)
                {

                    bool deleteResonse = DeleteArticleCategoryAsync(item);
                    if (!deleteResonse)
                    {
                        return false;
                    }
                }

                //Set Deleted Value
                categories.IsActive = false;
                categories.IsDelete = true;

                //Update Category
                _articleRepository.UpdateMainArticleCategories(categories);
                await _articleRepository.SaveAsync();


                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }

        public async Task<bool> CreateAricleAsync(ArticleRequest articleRequest)
        {
            try
            {
                Articles article = new Articles()
                {
                    ShortDescription = FixedText.ConvertNewLineToBr(articleRequest.ShortDescription),
                    CreateDate = DateTime.Now,
                    ArticleText = articleRequest.ArticleText,
                    AuthorId = articleRequest.AuthorId,
                    ArticleTitle = articleRequest.ArticleTitle,
                    CanInsertComment = articleRequest.CanInsertComment,
                    IsActive = true,
                    IsDelete = false,
                    IsVip = articleRequest.IsVip
                };

                await _articleRepository.InsertArticleAsync(article);
                await _articleRepository.SaveAsync();

                article.ArticleImageName = article.ArticleId + "_" + articleRequest.ArticleImage.FileName;

                _articleRepository.UpdateArticle(article);

                await _articleRepository.SaveAsync();

                await AddArticleTagsAsync(articleRequest.Tags, article.ArticleId);

                await SaveArticlePhysicalImageAsync(articleRequest.ArticleImage, article.ArticleId.ToString());

                await AddArticleCategoryAsync(article.ArticleId, articleRequest.SelectedCategories);

                return true;

            }
            catch (Exception)
            {
                return false;
            }

        }

        public async Task<IList<MainArticleCategories>> GetAllArticleCategoriesAsync()
        {
            return await _articleRepository.GetMainArticleCategoriesWithChildAsync();
        }

        public async Task AddArticleTagsAsync(string tag, int articleId)
        {
            string[] tagArray = FixedText.SplitTags(tag);

            foreach (string item in tagArray)
            {
                await _articleRepository.InsertArticleTagAsync(new ArticleTags()
                {
                    ArticleId = articleId,
                    Tag = item
                });
                await _articleRepository.SaveAsync();
            }

        }

        public async Task SaveArticlePhysicalImageAsync(IFormFile file, string imageName)
        {
            imageName = imageName + "_" + file.FileName;
            await file.CopyToAsync(File.Create(SiteDirectories.ArticleImagePhysicalPath() + imageName));
        }

        public async Task AddArticleCategoryAsync(int articleId, List<int> categoryId)
        {
            foreach (int item in categoryId)
            {
                ArticleCategories category = await _articleRepository.GetArticleCategoriesAsync(item);
                await _articleRepository.InsertCategoryArticleAsync(new ArticleCategory()
                {
                    ArticleId = articleId,
                    CategoryId = item,
                    MainCategoryId = category.MainArticleId
                });
                await _articleRepository.SaveAsync();
            }
        }

        public async Task<Articles> GetArticleAsync(int articleId)
        {
            return await _articleRepository.GetArticleAsync(articleId);
        }

        public async Task<bool> UpdateArticleAsync(ArticleRequest request)
        {
            try
            {
                Articles article = await _articleRepository.GetArticleAsync(request.ArticleId);

                article.ArticleText = request.ArticleText;
                article.ArticleTitle = request.ArticleTitle;
                article.AuthorId = request.AuthorId;
                article.CanInsertComment = request.CanInsertComment;
                article.ShortDescription = FixedText.ConvertNewLineToBr(request.ShortDescription);
                article.IsVip = request.IsVip;

                _articleRepository.UpdateArticle(article);
                await _articleRepository.SaveAsync();

                await RemoveArticleCategoryAsync(article.ArticleId);
                await RemoveArticleTagsAsync(article.ArticleId);
                RemoveArticleImage(article.ArticleImageName);

                await AddArticleCategoryAsync(article.ArticleId, request.SelectedCategories);
                await AddArticleTagsAsync(request.Tags, article.ArticleId);
                await SaveArticlePhysicalImageAsync(request.ArticleImage, article.ArticleId.ToString());

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task RemoveArticleCategoryAsync(int articleId)
        {
            IList<ArticleCategory> selectedCategories = await _articleRepository.GetArticleCategoryAsync(x => x.ArticleId == articleId);

            foreach (ArticleCategory item in selectedCategories)
            {
                _articleRepository.RemoveCategoryArticle(item);
                await _articleRepository.SaveAsync();
            }

        }

        public async Task RemoveArticleTagsAsync(int articleId)
        {
            IList<ArticleTags> selectedtags = await _articleRepository.GetArticleTagsAsync(x => x.ArticleId == articleId);
            foreach (ArticleTags item in selectedtags)
            {
                _articleRepository.RemoveArticleTag(item);
                await _articleRepository.SaveAsync();
            }
        }

        public bool RemoveArticleImage(string imageName)
        {
            try
            {
                File.Delete(SiteDirectories.ArticleImagePhysicalPath() + imageName);
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }

        public async Task<bool> RemoveArticleAsync(int id)
        {
            try
            {
                Articles articles = await _articleRepository.GetArticleAsync(id);

                articles.IsActive = false;
                articles.IsDelete = true;

                _articleRepository.UpdateArticle(articles);
                await _articleRepository.SaveAsync();

                await RemoveArticleCategoryAsync(id);

                await RemoveArticleTagsAsync(id);

                RemoveArticleImage(articles.ArticleImageName);

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<bool> ActivateArticleAsync(int id)
        {
            try
            {
                //Get Item
                Articles article = await _articleRepository.GetArticleAsync(id);

                //Set Deleted Value
                article.IsActive = true;
                article.IsDelete = false;

                //Update Item
                _articleRepository.UpdateArticle(article);
                await _articleRepository.SaveAsync();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<bool> DeActivateArticleAsync(int id)
        {
            try
            {
                //Get Item
                Articles article = await _articleRepository.GetArticleAsync(id);

                //Set Deleted Value
                article.IsActive = false;
                article.IsDelete = true;

                //Update Item
                _articleRepository.UpdateArticle(article);
                await _articleRepository.SaveAsync();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<IList<ArticleRelatedViewModel>> GetArticleRelatedsAsync(int articleId)
        {
            IList<ArticleRelateds> relateds = await _articleRepository.GetArticleRelatedsAsync(x => x.ArticleId == articleId);

            return relateds.Select(c => new ArticleRelatedViewModel()
            {
                RelatedID = c.RelatedId,
                Title = c.Article.ArticleTitle
            }).ToList();
        }

        public async Task<bool> AddArticleRelatedAsync(ArticleRelateds relateds)
        {
            try
            {
                await _articleRepository.InsertArticleRelatedAsync(relateds);
                await _articleRepository.SaveAsync();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<bool> DeleteArticleRelatedAsync(int id)
        {
            try
            {
                _articleRepository.RemoveArticleRelated(await _articleRepository.GetArticleRelatedAsync(id));
                return true;
            }
            catch (Exception)
            {
                return false;
            }
            throw new NotImplementedException();
        }

        public async Task<IList<ArticleComments>> GetUnreadsCommentsAsync()
        {
            return await _articleRepository.GetArticleCommentAsync(x => !x.IsRead);
        }

        public async Task<ArticleComments> GetArticleCommentsAsync(int id)
        {
            ArticleComments articleComment = await _articleRepository.GetArticleCommentAsync(id);

            await MarkCommentsAsRead(articleComment);

            return articleComment;
        }

        public async Task MarkCommentsAsRead(ArticleComments articleComments)
        {
            articleComments.IsRead = true;

            _articleRepository.UpdateArticleComments(articleComments);

            await _articleRepository.SaveAsync();
        }

        public async Task<bool> RemoveArticleCommentAsync(int id)
        {
            try
            {
                ArticleComments articleComments = await _articleRepository.GetArticleCommentAsync(id);

                articleComments.IsDelete = true;

                _articleRepository.UpdateArticleComments(articleComments);
                await _articleRepository.SaveAsync();

                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }

        public async Task<IList<ArticleComments>> GetArticleComments(int articleId)
        {
            return await _articleRepository.GetArticleCommentAsync(x => x.ArticleId == articleId);
        }
    }
}