﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using ThemeShop.Application.Interfaces;
using ThemeShop.Domain.Interfaces;
using ThemeShop.Domain.Models;

namespace ThemeShop.Application.Services
{
    public class PermissionService : IPermissionService
    {
        private readonly IPermissionRepository _permissionRepository;

        public PermissionService(IPermissionRepository permissionRepository)
        {
            _permissionRepository = permissionRepository;
        }

        #region Permissions

        public async Task<IList<Permissions>> GetPermissionsAsync()
        {
            IList<Permissions> permission = await _permissionRepository.GetAllPermissionsAsync();
            return permission.OrderBy(permission => permission.DisplayPriority).ToList();
        }

        public async Task<Permissions> GetPermissionByNameAsync(string permissionName)
        {
            return await _permissionRepository.GetPermissionByNameAsync(permissionName);
        }

        #endregion Permissions
    }
}