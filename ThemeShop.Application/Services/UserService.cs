﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using ThemeShop.Application.Convertors;
using ThemeShop.Application.Interfaces;
using ThemeShop.Application.Security;
using ThemeShop.Application.Senders;
using ThemeShop.Application.Utilities;
using ThemeShop.Application.ViewModels.Public;
using ThemeShop.Application.ViewModels.User;
using ThemeShop.Domain.Interfaces;
using ThemeShop.Domain.Models;

namespace ThemeShop.Application.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly IPermissionService _permissionService;
        private readonly IRoleService _roleService;

        public UserService(IUserRepository userRepository, IPermissionService permissionService, IRoleService roleService)
        {
            _userRepository = userRepository;
            _permissionService = permissionService;
            _roleService = roleService;
        }

        #region OnlineUser

        public async Task<IList<OnlineUsers>> GetTodayOnlineUsersAsync(int take = 100)
        {
            IList<OnlineUsers> onlineUser = await _userRepository.GetAllTodayOnlineUsersAsync();
            return onlineUser.OrderByDescending(users => users.StartTime).Take(take).ToList();
        }

        public async Task<OnlineUsers> GetOnlineUserAsync(string userName, string ip)
        {
            if (!string.IsNullOrEmpty(userName) && !string.IsNullOrWhiteSpace(userName) && userName != "کاربر مهمان" && await _userRepository.IsOnlineUserExistByUserNameAsync(userName))
            {
                return await _userRepository.FindOnlineUserByIpAsync(userName);
            }
            else if (!string.IsNullOrEmpty(ip) && !string.IsNullOrWhiteSpace(ip) && await _userRepository.IsOnlineUserExistByIpAsync(ip))
            {
                return await _userRepository.FindOnlineUserByIpAsync(ip);
            }
            else
            {
                OnlineUsers onlineUser = new()
                {
                    Ip = IpAddress.GetUserIPAddress(),
                    OnlineStatus = false,
                    StartTime = DateTime.Now,
                    UserName = userName,
                    VisitPath = "/"
                };
                await _userRepository.InsertOnlineUserAsync(onlineUser);
                await _userRepository.SaveAsync();
                return await _userRepository.FindOnlineUserByIpAsync(ip);
            }
        }

        public void UpdateOnlineUserInformation(OnlineUsers onlineUser)
        {
            _userRepository.UpdateOnlineUser(onlineUser);
        }

        #endregion OnlineUser

        #region Users

        public async Task<IList<Users>> GetAllUsersAsync(int pageId, int take = 30)
        {
            IList<Users> user = await _userRepository.GetAllUsersAsync();
            return user.Skip((pageId - 1) * take).Take(take).ToList();
        }

        public async Task<int> GetAllUsersCountAsync()
        {
            return await _userRepository.GetAllUsersCountAsync();
        }

        public async Task InsertUserAsync(Users user)
        {
            await _userRepository.AddUserAsync(user);
            await _userRepository.SaveAsync();
        }

        public void UpdateUser(Users user)
        {
            _userRepository.UpdateUser(user);
            _userRepository.Save();
        }

        public async Task<IList<Users>> GetUserRegisterToDayAsync()
        {
            DateTime dtNow = new(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
            IList<Users> user = await _userRepository.GetUsersAsync(u => u.RegisterDate >= dtNow);
            return user.OrderByDescending(u => u.RegisterDate).ToList();
        }

        public async Task<Users> GetUserByIdAsync(int userId)
        {
            return await _userRepository.GetUserByIdAsync(userId);
        }

        public async Task<int> GetUserIdByEmailAsync(string email)
        {
            Users user = await _userRepository.GetUserAsync(user => user.Email == FixedText.FixEmail(email));
            return user.UserId;
        }

        public async Task ActiveUserAccountAsync(int userId)
        {
            Users user = await _userRepository.GetUserByIdAsync(userId);
            user.IsActive = true;
            user.ActiveCode = Guid.NewGuid().ToString();
            await _userRepository.SaveAsync();
        }

        public async Task<Users> ActiveUserAccountAsync(string activeCode)
        {
            Users user = await _userRepository.GetUserAsync(user => user.ActiveCode == activeCode && !user.IsDelete);

            if (user == null)
            {
                return null;
            }

            user.IsActive = true;
            user.ActiveCode = Guid.NewGuid().ToString();
            await _userRepository.SaveAsync();
            return user;
        }

        public async Task DeActiveUserAsync(int userId)
        {
            Users user = await _userRepository.GetUserByIdAsync(userId);
            user.IsActive = false;
            await _userRepository.SaveAsync();
        }

        public async Task<bool> IsActiveUserAsync(int userId)
        {
            return await _userRepository.IsUserExistAsync(user => user.UserId == userId && user.IsActive);
        }

        public async Task<bool> IsActiveUserAsync(string email)
        {
            return await _userRepository.IsUserExistAsync(user => user.Email == FixedText.FixEmail(email) && user.IsActive);
        }

        public async Task DeleteUserAsync(int userId)
        {
            await _userRepository.DeleteUserByIdAsync(userId);
            await _userRepository.SaveAsync();
        }

        public void DeleteUser(Users user)
        {
            _userRepository.DeleteUser(user);
            _userRepository.Save();
        }

        public async Task ChangeUserPasswordAsync(int userId, string newPssword)
        {
            Users user = await _userRepository.GetUserByIdAsync(userId);
            user.Password = PasswordHelper.EncodePasswordMd5(newPssword);
            await _userRepository.SaveAsync();
        }

        public async Task<bool> IsOldPasswordCorrectAsync(int userId, string oldPassword)
        {
            return await _userRepository.IsUserExistAsync(user => user.UserId == userId && user.Password == PasswordHelper.EncodePasswordMd5(oldPassword));
        }

        public async Task<bool> IsUserEmailChangeAsync(int userId, string email)
        {
            Users user = await _userRepository.GetUserByIdAsync(userId);
            return user.Email != FixedText.FixEmail(email);
        }

        public async Task<bool> IsUserNameChangeAsync(int userId, string userName)
        {
            Users user = await _userRepository.GetUserByIdAsync(userId);
            return user.UserName != FixedText.FixUserName(userName);
        }

        public async Task<bool> IsExistUserByActiveCodeAsync(string activeCode)
        {
            return await _userRepository.IsUserExistAsync(user => user.ActiveCode == activeCode);
        }

        public async Task RecoveryPasswordUserAsync(string activeCode, string newPasssword)
        {
            Users user = await _userRepository.GetUserAsync(user => user.ActiveCode == activeCode);
            user.ActiveCode = Guid.NewGuid().ToString();
            user.Password = PasswordHelper.EncodePasswordMd5(newPasssword);
            await _userRepository.SaveAsync();
        }

        public async Task ChangeActiveCodeAsync(int userId)
        {
            Users user = await _userRepository.GetUserByIdAsync(userId);
            user.ActiveCode = Guid.NewGuid().ToString();
            await _userRepository.SaveAsync();
        }

        public async Task ActiveUserForSellerAsync(int userId)
        {
            Permissions roleId = await _permissionService.GetPermissionByNameAsync("Seller");
            await _roleService.AddUserRoleAsync(userId, roleId.PermissionId);
            await _userRepository.SaveAsync();
        }

        public async Task InActiveUserForSellerAsync(int userId)
        {
            Permissions roleId = await _permissionService.GetPermissionByNameAsync("Seller");
            await _roleService.RemoveUserRoleAsync(userId, roleId.PermissionId);
            await _userRepository.SaveAsync();
        }

        public async Task<string> GetEmailByUserIdAsync(int userId)
        {
            Users user = await _userRepository.GetUserByIdAsync(userId);
            return user.Email;
        }

        public async Task<IList<Users>> FilterUsersAsync(string query, int userSearchMode = 0, int userStatusEnum = 0)
        {
            return await _userRepository.FilterUsersAsync(query, userSearchMode, userStatusEnum);
        }

        public async Task<int> FilterUsersCountAsync(string query, int userSearchMode = 0, int userStatusEnum = 0)
        {
            return await _userRepository.FilterUsersCountAsync(query, userSearchMode, userStatusEnum);
        }

        #region Seller


        public async Task<SellerIndexViewModel> FilterSellersAsync(byte pageId, byte take, SellerFilterViewModel filter)
        {
            IList<Users> result = await _userRepository.GetAllUsersAsync();

            int skip = (pageId - 1) * take;

            #region Filter
            Tuple<IList<Users>, int> sellers = await _userRepository.FillterSellersAsync(filter.FilterEmail, filter.FilterNameFamily, filter.FilterUserName, (int)filter.Status);

            result = sellers.Item1;

            #endregion Filter

            #region Sellect

            IList<SellerIndexForAdminViewmodel> query = result.Select(u => new SellerIndexForAdminViewmodel
            {
                SellerId = u.UserId,
                UserId = u.UserId,
                Name = u.Name,
                Family = u.Family,
                Email = u.Email,
                CreateDate = u.UserBanks.OrderByDescending(ub => ub.CreateDate).FirstOrDefault().CreateDate,
                IsActive = u.IsActive,
                UserName = u.UserName,
                Persentag = u.ThemeSellerPercentage
            }).ToList();

            #endregion Sellect

            #region Order

            query = query.OrderBy(u => u.CreateDate).ToList();

            #endregion Order

            #region Paging

            int pageCount = 0;

            if ((query.Count() / take) % 2 == 0)
            {
                pageCount = query.Count() / take;
            }
            else
            {
                pageCount = (query.Count() / take) + 1;
            }
            PaggingViewModel paggingViewModel = new()
            {
                ActivePage = pageId,
                PageCount = pageCount,
                ParametrUrlName = "pageId"
            };
            SellerIndexViewModel list = new()
            {
                PaggingViewModel = paggingViewModel,
                Sellers = query.Skip(skip).Take(take).ToList()
            };

            #endregion Paging

            return list;
        }

        public async Task<SellerIndexViewModel> GetAllSellersAsync(byte pageId, byte take)
        {
            IList<Users> result = await _userRepository.GetAllUsersAsync();

            int skip = (pageId - 1) * take;

            #region Filter
            Tuple<IList<Users>, int> sellers = await _userRepository.FillterSellersAsync();

            result = sellers.Item1;

            #endregion Filter

            #region Sellect

            IList<SellerIndexForAdminViewmodel> query = result.Select(u => new SellerIndexForAdminViewmodel
            {
                SellerId = u.UserId,
                UserId = u.UserId,
                Name = u.Name,
                Family = u.Family,
                Email = u.Email,
                CreateDate = u.UserBanks.OrderByDescending(ub => ub.CreateDate).FirstOrDefault().CreateDate,
                IsActive = u.IsActive,
                UserName = u.UserName,
                Persentag = u.ThemeSellerPercentage
            }).ToList();

            #endregion Sellect

            #region Order

            query = query.OrderBy(u => u.CreateDate).ToList();

            #endregion Order

            #region Paging

            int pageCount = 0;

            if ((query.Count() / take) % 2 == 0)
            {
                pageCount = query.Count() / take;
            }
            else
            {
                pageCount = (query.Count() / take) + 1;
            }
            PaggingViewModel paggingViewModel = new()
            {
                ActivePage = pageId,
                PageCount = pageCount,
                ParametrUrlName = "pageId"
            };
            SellerIndexViewModel list = new()
            {
                PaggingViewModel = paggingViewModel,
                Sellers = query.Skip(skip).Take(take).ToList()
            };

            #endregion Paging

            return list;
        }

        public async Task<SellerViewModel> GetSellerByuserIdIdForConfirmationAsync(int userId)
        {
            Users user = await _userRepository.GetUserByIdAsync(userId);
            UserBanks userBank = await _userRepository.GetUserBankAsync(userBank => userBank.UserId == userId);

            return new SellerViewModel()
            {
                Family = user.Family,
                UserId = user.UserId,
                CreateDate = userBank.CreateDate,
                Name = user.Name,
                SellerCardNumber = userBank.SellerCardNumber,
                SellerAccountHolder = userBank.BankAccountHolder,
                BankName = userBank.BankName,
                CodeMeli = userBank.NationalCode,
                SellerAccountNumber = userBank.SellerAccountNumber,
                ImageCardBank = userBank.BankCardImage,
                ImageKartMeli = userBank.NationalCardImage,
                SellerShabaNumber = userBank.SellerShabaNumber,
                UserBankId = userBank.UserBankId,
                Mobile = user.Mobile
            };
        }

        public async Task<bool> IsSellerAsync(int userId)
        {
            Permissions permission = await _permissionService.GetPermissionByNameAsync("Seller");

            return await _roleService.IsUserInPermissionAsync(userId, permission.PermissionId);
        }

        #endregion Seller

        #endregion Users

        #region UserBank

        public async Task ActiveUserBankInfoesAsync(int userBankId)
        {
            UserBanks userBank = await _userRepository.GetUserBankByIdAsync(userBankId);
            userBank.IsActive = true;
            await _userRepository.SaveAsync();
        }

        //public async Task<ShowProfileViewModel> GetUserProfileAsync(string userName, int pageId)
        //{
        //    #region GetUser
        //    var user = await _userRepository.GetUserAsync(u => u.UserName == userName && !u.IsDelete);
        //    if (user == null)
        //    {
        //        throw new Exception("User Not Fount");
        //    }
        //    #endregion

        //    #region GetShowProfileViewModel
        //    ShowProfileViewModel profile = new ShowProfileViewModel();

        //    profile.UserId = user.UserId;
        //    profile.Avatar = user.Avatar;
        //    profile.CommentCount = user.Themes?.Sum(s => s.ThemeComments.Count);
        //    profile.CountMounthActiveUser = ((DateTime.Now - user.RegisterDate).Days / 30);
        //    profile.Description = user.UserDescription;
        //    profile.Rate = user.Themes?.Sum(t => t.ThemeLikes.Count);
        //    profile.SellCount = 0;
        //    profile.SupportCount = 0;
        //    profile.ThemeCount = user.Themes.Count;
        //    profile.UserName = userName;

        //    #region Pagging
        //    int take = 9;
        //    int skip = (pageId - 1) * take;
        //    PaggingViewModel paggingViewModel = new PaggingViewModel();
        //    paggingViewModel.ActivePage = pageId;
        //    paggingViewModel.ParametrUrlName = "pageId";
        //    paggingViewModel.PageCount = (int)Math.Ceiling(((double)profile.ThemeCount / (double)take));
        //    #endregion

        //TODO:ShowThemeListViewModel Impediment Later And Return ShowThemeListViewModel
        //    profile.SellCount = 0;
        //    profile.Themes = new ListThemeViewModel();
        //    profile.Pagging = paggingViewModel;
        //    return profile;
        //    #endregion
        //}

        public async Task AddUserBankInformationAsync(SellerViewModel sellerModel)
        {
            UserBanks userBank = new()
            {
                UserId = sellerModel.UserId,
                BankName = sellerModel.BankName,
                NationalCode = sellerModel.CodeMeli,
                CreateDate = DateTime.Now,
                BankCardImage = sellerModel.ImageCardBank,
                NationalCardImage = sellerModel.ImageKartMeli,
                IsActive = false,
                BankAccountHolder = sellerModel.SellerAccountHolder,
                SellerAccountNumber = sellerModel.SellerAccountNumber,
                SellerCardNumber = sellerModel.SellerCardNumber,
                SellerShabaNumber = sellerModel.SellerShabaNumber
            };
            Users user = await _userRepository.GetUserByIdAsync(sellerModel.UserId);
            user.Name = sellerModel.Name;
            user.Family = sellerModel.Family;
            user.ThemeSellerPercentage = 30;
            await _userRepository.AddUserBankAsync(userBank);
            await _userRepository.SaveAsync();
        }

        public async Task RejectUserBankAsync(int userBankId, string message)
        {
            UserBanks bankInfo = await _userRepository.GetUserBankByIdAsync(userBankId);
            if (bankInfo == null)
            {
                return;
            }
            bankInfo.IsReject = true;
            _userRepository.UpdateUserBank(bankInfo);
            await _userRepository.SaveAsync();
            await _userRepository.InsertUserBankMessageAsync(new UserBankMessages
            {
                Message = message,
                UserBankId = userBankId,
                AnswerTime = DateTime.Now
            });
            await _userRepository.SaveAsync();
        }

        #endregion UserBank


        public void Save()
        {
            _userRepository.Save();
        }

        public async Task SaveAsync()
        {
            await _userRepository.SaveAsync();
        }

        public async Task<bool> ConfirmSellerAsync(int userId, int userBankId)
        {
            try
            {
                //Active Seller User
                await ActiveUserForSellerAsync(userId);
                await ActiveUserBankInfoesAsync(userBankId);

                //Get User Info
                Users user = await _userRepository.GetUserByIdAsync(userId);
                string body = "کاربر گرامی :" + user.Name + "- حساب فروشنده شما تایید شد";

                //Send Email
                await SendEmail.SendAsync(user.Email, "تم شاپ - اطلاع رسانی", body);
                return true;
            }
            catch (Exception)
            {
                return false;
            }


        }

        public async Task<bool> RejectSellerAsync(int userId, int userBankId, string message)
        {
            try
            {
                await RejectUserBankAsync(userBankId, message);
                await InActiveUserForSellerAsync(userId);

                //Get User Info
                Users user = await _userRepository.GetUserByIdAsync(userId);

                //Send Email
                string body = "کاربر گرامی :" + user.Name + message;
                await SendEmail.SendAsync(user.Email, "تم شاپ - اطلاع رسانی", body);
                return true;
            }
            catch (Exception)
            {
                return false;
                throw;
            }
        }
    }
}