﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using ThemeShop.Application.Interfaces;
using ThemeShop.Application.RequestModel;
using ThemeShop.Application.ViewModels.Wallet;
using ThemeShop.Domain.Interfaces;
using ThemeShop.Domain.Models;

namespace ThemeShop.Application.Services
{
    public class WalletService : IWalletService
    {
        private readonly IWalletRepository _walletRepository;

        public WalletService(IWalletRepository walletRepository)
        {
            _walletRepository = walletRepository;
        }

        public async Task<IList<WalletViewModel>> GetAllBuyTransactionAsync(int page, byte take = 20)
        {
            IList<WalletLogs> walletLog = await _walletRepository.GetWalletLogsAsync(x => !x.IsCharge && x.LogDescription == "خرید");
            return walletLog.Skip((page - 1) * take).Take(take).Select(x => new WalletViewModel()
            {
                CreateDate = x.LogTime,
                Description = x.Text,
                IsPay = x.IsCharge,
                Price = x.LogPrice,
                TransactionType = x.LogDescription,
                WalletId = x.WalletId,
                Type = "افزایش اعتبار",
                UserId = x.Wallet.UserId,
                UserName = x.Wallet.User.UserName,
            }).ToList();
        }

        public async Task<int> GetAllBuyTransactionCountAsync()
        {
            return await _walletRepository.GetWalletLogCountAsync(x => !x.IsCharge && x.LogDescription == "خرید");
        }

        public async Task<int> GetAllPaymentCountAsync()
        {
            return await _walletRepository.GetWalletLogCountAsync(x => x.IsCharge);
        }

        public async Task<IList<WalletViewModel>> GetAllPaymentTransactionAsync(int page, byte take = 20)
        {
            IList<WalletLogs> walletLog = await _walletRepository.GetWalletLogsAsync(x => x.IsCharge);
            return walletLog.Skip((page - 1) * take).Take(take).Select(x => new WalletViewModel()
            {
                CreateDate = x.LogTime,
                Description = x.Text,
                IsPay = x.IsCharge,
                Price = x.LogPrice,
                TransactionType = x.LogDescription,
                WalletId = x.WalletId,
                Type = "کسر اعتبار",
                UserId = x.Wallet.UserId,
                UserName = x.Wallet.User.UserName,
            }).ToList();
        }

        public async Task<Tuple<IList<WalletViewModel>, int>> GetTransactionAsync(int pageId, FilterTransaction filter, byte take = 20)
        {
            IList<WalletLogs> transactions = await _walletRepository.FilterWalletLogsAsync(filter.UserName, filter.TransactionType, filter.MinimumPrice, filter.MaximumPrice, filter.StartDate, filter.EndDate, filter.Describtion, pageId, take);

            return new Tuple<IList<WalletViewModel>, int>(transactions.Skip((pageId - 1) * take).Take(take).Select(x => new WalletViewModel()
            {
                CreateDate = x.LogTime,
                Description = x.Text,
                IsPay = x.IsCharge,
                Price = x.LogPrice,
                TransactionType = x.LogDescription,
                WalletId = x.WalletId,
                UserId = x.Wallet.UserId,
                UserName = x.Wallet.User.UserName,
            }).ToList(),
            transactions.Count());
        }
    }
}