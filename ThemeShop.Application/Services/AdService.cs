﻿using Microsoft.AspNetCore.Http;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

using ThemeShop.Application.Interfaces;
using ThemeShop.Application.RequestModel;
using ThemeShop.Application.Utilities;
using ThemeShop.Domain.Interfaces;
using ThemeShop.Domain.Models;

namespace ThemeShop.Application.Services
{
    public class AdService : IAdService
    {
        private readonly IAdRepository _adRepository;

        public AdService(IAdRepository adRepository)
        {
            _adRepository = adRepository;
        }

        #region Ads

        public async Task<bool> CreateAdAsync(AdsRequest ad, IFormFile img)
        {
            try
            {
                //Create Item
                Ads adItem = new Ads()
                {
                    AdImageName = ad.AdImageName,
                    AdTitle = ad.AdTitle,
                    AdUrl = ad.AdUrl,
                    ShowInChild = ad.ShowInChild,
                    StartDate = ad.StartDate,
                    EndDate = ad.EndDate
                };

                //Fill Fixed Value
                adItem.CreateDate = DateTime.Now;
                adItem.IsActive = true;
                adItem.IsDelete = false;

                //Save Ad Img
                string imageURL = ad.AdTitle + Path.GetExtension(img.FileName);
                await img.CopyToAsync(File.Create(SiteDirectories.AdsImagePhysicalPath() + imageURL));
                adItem.AdImageName = imageURL;

                //Add Article - Get Id
                int addId = await _adRepository.InsertAdAsync(adItem);
                await _adRepository.SaveAsync();

                //Insert Selected Ad Categories
                foreach (int categoryId in ad.SelectedCategories)
                {
                    await _adRepository.InsertAdCategoryAsync(new AdThemeCategories() { AdId = ad.AdId, ThemeCategoryId = categoryId });
                }
                await _adRepository.SaveAsync();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<bool> EditAdAsync(AdsRequest ad)
        {
            try
            {
                //Get Item
                Ads adItem = await _adRepository.GetAdAsync(ad.AdId);

                //Change Value
                adItem.AdImageName = ad.AdImageName;
                adItem.AdTitle = ad.AdTitle;
                adItem.AdUrl = ad.AdUrl;
                adItem.EndDate = ad.EndDate;
                adItem.ShowInChild = ad.ShowInChild;
                adItem.StartDate = ad.StartDate;

                //Update Ads
                _adRepository.UpdateAd(adItem);
                await _adRepository.SaveAsync();

                //Delete Previous Ads Category
                await DeleteAdCategoriesByAdId(ad.AdId);

                //Insert New Selected Category
                foreach (int categoryId in ad.SelectedCategories)
                {
                    await _adRepository.InsertAdCategoryAsync(new AdThemeCategories() { AdId = ad.AdId, ThemeCategoryId = categoryId });
                }
                await _adRepository.SaveAsync();

                return true;
            }
            catch (Exception)
            {
                return false;
                throw;
            }
        }

        //-----------------Action By Id-----------------
        public async Task RemoveAd(int adId)
        {
            //Get Item - Remove
            await RemoveAd(await _adRepository.GetAdAsync(adId));
        }

        public async Task DeleteAdCategoriesByAdId(int adId)
        {
            //Get Ad Selected Categories - Remove
            _adRepository.RemoveAdCategories(await _adRepository.GetAdCategoriesAsync(x => x.AdId == adId));
            await _adRepository.SaveAsync();
        }

        public async Task ActivateAd(int adId)
        {
            //Get Ad - Activate
            await ActivateAd(await _adRepository.GetAdAsync(adId));
        }

        public async Task DeActivateAd(int adId)
        {
            //Get ad - DeActivate
            await DeActivateAd(await _adRepository.GetAdAsync(adId));
        }

        //-----------------Action By Id-----------------

        public async Task RemoveAd(Ads ad)
        {
            //Remove Ad Selected Categories
            await DeleteAdCategoriesByAdId(ad.AdId);

            //Remove Value
            ad.IsActive = false;
            ad.IsDelete = true;

            //Update Item
            _adRepository.UpdateAd(ad);
            await _adRepository.SaveAsync();
        }

        public async Task ActivateAd(Ads ad)
        {
            ad.IsActive = true;
            _adRepository.UpdateAd(ad);
            await _adRepository.SaveAsync();
        }

        public async Task DeActivateAd(Ads ad)
        {
            ad.IsActive = false;
            _adRepository.UpdateAd(ad);
            await _adRepository.SaveAsync();
        }

        public async Task<IList<Ads>> GetAdsAsync(byte page = 1, byte take = 20)
        {
            IList<Ads> ads = await _adRepository.GetAdsAsync();
            return ads
                .Skip((page - 1) * take)
                .Take(take).ToList();
        }

        public async Task<Ads> GetAdsAsync(int adId)
        {
            return await _adRepository.GetAdAsync(adId);
        }

        public async Task<int> GetAdsCountAsync()
        {
            return await _adRepository.GetAdsCountAsync();
        }

        //Filter Ads
        public async Task<Tuple<IList<Ads>, int>> GetAdsAsync(FilterAds filter, byte pageId = 1, byte take = 20)
        {
            //Get All Ads
            IList<Ads> ads = await _adRepository.GetAdsAsync();

            //Filter by Ad Url
            if (filter.AddUrl != "" && filter.AddUrl != null)
            {
                ads = ads.Where(x => x.AdUrl.Contains(filter.AddUrl)).ToList();
            }

            //Filter by Ad Title
            if (filter.AdTitle != "" && filter.AdTitle != null)
            {
                ads = ads.Where(x => x.AdTitle.Contains(filter.AdTitle)).ToList();
            }

            //Filter by Ad Time - Begin
            if (filter.EndDate != DateTime.MinValue)
            {
                ads = ads.Where(x => x.EndDate <= filter.EndDate).ToList();
            }

            if (filter.StartTime != DateTime.MinValue)
            {
                ads = ads.Where(x => x.StartDate >= filter.StartTime).ToList();
            }
            //Filter by Ad Time - Done

            //Filter by Ad Status
            if (filter.IsActive != null)
            {
                ads = ads.Where(x => x.IsActive == filter.IsActive).ToList();
            }

            //Filter by Delete Status
            if (filter.IsDeleted != null)
            {
                ads = ads.Where(x => x.IsDelete == filter.IsDeleted).ToList();
            }

            //Return Tuple(20 Ad - All Filtred Ad Count)
            return new Tuple<IList<Ads>, int>(
                ads.Skip((pageId - 1) * take).Take(take).AsEnumerable().ToList(),
                ads.Count()
                );
        }

        public async Task<bool> EditAdImageAsync(int id, IFormFile img)
        {
            try
            {
                Ads ads = await _adRepository.GetAdAsync(id);
                File.Delete(SiteDirectories.AdsImagePhysicalPath() + ads.AdImageName);
                await img.CopyToAsync(File.Create(SiteDirectories.AdsImagePhysicalPath() + ads.AdImageName));
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        #endregion Ads
    }
}