﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using ThemeShop.Application.Interfaces;
using ThemeShop.Domain.Interfaces;
using ThemeShop.Domain.Models;

namespace ThemeShop.Application.Services
{
    public class ContactUsService : IContactUsService
    {
        private readonly IContactUsRepository _contactUsRepository;

        public ContactUsService(IContactUsRepository contactUsRepository)
        {
            _contactUsRepository = contactUsRepository;
        }

        #region getContactus

        public async Task<Tuple<IList<ContactUs>, int>> GetContactUsAsync(string query = "", int pageId = 1)
        {
            if (string.IsNullOrEmpty(query) || string.IsNullOrWhiteSpace(query))
            {
                IList<ContactUs> contactUs = await _contactUsRepository.GetContactUsAsync();
                return new Tuple<IList<ContactUs>, int>
                (
                 // get 50 contactus by page id
                 contactUs.Skip((pageId - 1) * 50).Take(50).ToList(),
                // get contactus count
                 contactUs.Count()
                );
            }
            else
            {
                IList<ContactUs> contactUs = await _contactUsRepository.FilterContactUsAsync(query);
                return new Tuple<IList<ContactUs>, int>
                (
                 // get 50 contactus by page id
                 contactUs.Skip((pageId - 1) * 50).Take(50).ToList(),
                // get contactus count
                 contactUs.Count()
                );
            }
        }

        public async Task<Tuple<IList<ContactUs>, int>> GetContactUsNotReadAsync(string query = "", int pageId = 1)
        {
            IList<ContactUs> contactUs = await _contactUsRepository.GetContactUsAsync(x => !x.IsRead);
            return new Tuple<IList<ContactUs>, int>
                (
                //get 50 not read contactus
                contactUs.Skip((pageId - 1) * 50).ToList(),
                //get not read contactus page cout
                contactUs.Count()
                );
        }

        public async Task<ContactUs> GetContactUsAsync(int id)
        {
            await ReadContactUsAsync(id);

            return await _contactUsRepository.GetContactUsAsync(id);
        }

        #endregion getContactus

        #region read & answer

        public async Task<bool> AnswerContactAsync(int contactId, string answerMessage)
        {
            try
            {
                //get Object
                ContactUs contact = await _contactUsRepository.GetContactUsAsync(contactId);

                //Send Asnwer Email
                //#TODO
                //await SendEmail.SendAsync(contact.Email, "تم شاپ - پاسخ شما", answerMessage);

                //add Answer To Object
                contact.Answer = answerMessage;
                contact.IsAnswered = true;

                //update And save
                _contactUsRepository.Update(contact);
                await _contactUsRepository.SaveAsync();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task ReadContactUsAsync(int id)
        {
            ContactUs contact = await _contactUsRepository.GetContactUsAsync(id);

            contact.IsRead = true;

            _contactUsRepository.Update(contact);

            await _contactUsRepository.SaveAsync();
        }

        public async Task<bool> AddContactUsAsync(ContactUs contact)
        {
            try
            {
                contact.IsRead = false;
                contact.IsAnswered = false;

                await _contactUsRepository.AddContactUs(contact);
                await _contactUsRepository.SaveAsync();

                return true;
            }
            catch (Exception)
            {
                return false;
                throw;
            }
        }

        #endregion read & answer
    }
}