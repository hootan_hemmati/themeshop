﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using ThemeShop.Application.Interfaces;
using ThemeShop.Application.ViewModels.Order;
using ThemeShop.Domain.Interfaces;
using ThemeShop.Domain.Models;

namespace ThemeShop.Application.Services
{
    public class OrderService : IOrderService
    {
        private readonly IOrderRepository _orderRepository;

        public OrderService(IOrderRepository orderRepository)
        {
            _orderRepository = orderRepository;
        }

        /// <summary>
        /// Get List of Website Payment
        /// </summary>
        /// <returns></returns>
        public async Task<IList<UserPaymentViewModel>> GetAllUserPaymentsAsync()
        {
            //Get user buy
            IList<TemplateInfoes> allPayment = await _orderRepository.GetAllUserPaymentsAsync();

            //Fill data for view
            IList<UserPaymentViewModel> payment = allPayment.Select(q => new UserPaymentViewModel()
            {
                CreateBuy = q.CreateDate,
                UsernameSeller = q.Seller.Name + " " + q.Seller.Family + " ( " + q.Seller.UserName + " )",
                Price = q.Price,
                Username = q.User.Name + " " + q.User.Family + " ( " + q.User.UserName + " )",
                ThemeTitle = q.Theme.ThemeTitle,
            }).ToList();

            return payment;
        }

        public async Task<Tuple<List<UserPaymentViewModel>, int>> GetUserPaymentsAsync(int themeId, byte page, byte take)
        {
            IList<OrderDetails> OrderDetails = await _orderRepository.GetOrderDetailsAsync(x => x.ThemeId == themeId, page, take);
            int OrderDetailsCount = await _orderRepository.GetOrderDetailsCountAsync(x => x.ThemeId == themeId);
            int discountUser = 0;

            List<UserPaymentViewModel> userPayments = new List<UserPaymentViewModel>();

            foreach (OrderDetails item in OrderDetails)
            {

                if (item.Dicount != 0)
                {
                    discountUser = (item.Price - (item.Price * item.Dicount / 100));
                }
                else
                {
                    discountUser = item.Price;
                }
                userPayments.Add(new UserPaymentViewModel()
                {
                    Username = item.Order.User.UserName,
                    CreateBuy = item.Order.CreateTime,
                    ThemeTitle = item.Theme.ThemeTitle,
                    Price = discountUser,
                });
            }
            return Tuple.Create(userPayments, OrderDetailsCount);
        }
    }
}