﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using ThemeShop.Application.Interfaces;
using ThemeShop.Domain.Interfaces;
using ThemeShop.Domain.Models;

namespace ThemeShop.Application.Services
{
    public class ReportAbuseService : IReportAbuseService
    {
        private readonly IReportAbuseRepository _ReportAbuseRepository;

        public ReportAbuseService(IReportAbuseRepository ReportAbuseRepository)
        {
            _ReportAbuseRepository = ReportAbuseRepository;
        }

        #region ReportAbuseService
        public async Task<Tuple<IList<ReportAbuse>, int>> GetReportAbuseAsync(string query = "", int pageId = 1)
        {
            if (string.IsNullOrEmpty(query) || string.IsNullOrWhiteSpace(query))
            {
                IList<ReportAbuse> ReportAbuse = await _ReportAbuseRepository.GetAllReportAbuseAsync();
                return new Tuple<IList<ReportAbuse>, int>
                (
                 // get 50 ReportAbuse by page id
                 ReportAbuse.Skip((pageId - 1) * 50).Take(50).ToList(),
                // get ReportAbuse count
                 ReportAbuse.Count()
                );
            }
            else
            {
                IList<ReportAbuse> ReportAbuse = await _ReportAbuseRepository.FilterReportAbuseAsync(query);
                return new Tuple<IList<ReportAbuse>, int>
                (
                 // get 50 ReportAbuse by page id
                 ReportAbuse.Skip((pageId - 1) * 50).Take(50).ToList(),
                // get ReportAbuse count
                 ReportAbuse.Count()
                );
            }
        }

        public async Task<Tuple<IList<ReportAbuse>, int>> GetReportAbuseNotReadAsync(string query = "", int pageId = 1)
        {
            IList<ReportAbuse> ReportAbuse = await _ReportAbuseRepository.GetReportAbuseAsync(x => !x.IsSeen);
            return new Tuple<IList<ReportAbuse>, int>
                (
                //get 50 not read ReportAbuse
                ReportAbuse.Skip((pageId - 1) * 50).ToList(),
                //get not read ReportAbuse page cout
                ReportAbuse.Count()
                );
        }

        public async Task<ReportAbuse> GetReportAbuseByIdAsync(int id)
        {
            await ReadReportAbuseAsync(id);

            return await _ReportAbuseRepository.GetReportAbuseByIdAsync(id);
        }

        public async Task<bool> AnswerReportAbuseAsync(int reportAbuseId, string answerMessage)
        {
            try
            {
                //get Object
                ReportAbuse ReportAbuse = await _ReportAbuseRepository.GetReportAbuseByIdAsync(reportAbuseId);

                //Send Asnwer Email
                //#TODO
                //await SendEmail.SendAsync(ReportAbuse.Email, "تم شاپ - پاسخ شما", answerMessage);

                //add Answer To Object
                ReportAbuse.IsAnswered = true;

                //update And save
                _ReportAbuseRepository.Update(ReportAbuse);
                await _ReportAbuseRepository.SaveAsync();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task ReadReportAbuseAsync(int id)
        {
            ReportAbuse ReportAbuse = await _ReportAbuseRepository.GetReportAbuseByIdAsync(id);

            ReportAbuse.IsSeen = true;

            _ReportAbuseRepository.Update(ReportAbuse);

            await _ReportAbuseRepository.SaveAsync();
        }

        public async Task<bool> AddReportAbuseAsync(ReportAbuse reportAbuse)
        {
            try
            {
                reportAbuse.IsSeen = false;
                reportAbuse.IsAnswered = false;

                await _ReportAbuseRepository.AddReportAbuse(reportAbuse);
                await _ReportAbuseRepository.SaveAsync();

                return true;
            }
            catch (Exception)
            {
                return false;
                throw;
            }
        }

        #endregion
    }
}
