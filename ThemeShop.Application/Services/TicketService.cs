﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using ThemeShop.Application.Interfaces;
using ThemeShop.Domain.Interfaces;
using ThemeShop.Domain.Models;

namespace ThemeShop.Application.Services
{
    public class TicketService : ITicketService
    {
        private readonly ITicketRepository _ticketRepository;

        public TicketService(ITicketRepository ticketRepository)
        {
            _ticketRepository = ticketRepository;
        }

        #region Ticketing

        public async Task<IList<Ticketings>> GetAllTicketsAsync()
        {
            return await _ticketRepository.GetAllTicketsAsync();
        }

        public async Task<IList<Ticketings>> FilterTicketsAsync(string query, int skip, int take = 30)
        {
            IList<Ticketings> ticket = await _ticketRepository.FilterTicketsAsync(query);
            return ticket
                .Distinct()
                .OrderByDescending(ticket => !ticket.IsAnswered && ticket.IsDelete != true)
                .Skip((skip - 1) * take)
                .Take(take).ToList();
        }

        public async Task<int> FilterTicketsCountAsync(string query)
        {
            return await _ticketRepository.FilterTicketsCountAsync(query);
        }

        public async Task<int> GetAllTicketsCountAsync()
        {
            return await _ticketRepository.GetAllTicketsCountAsync();
        }

        public async Task<Ticketings> GetTicketByIdAsync(int ticketId)
        {
            return await _ticketRepository.GetTicketByIdAsync(ticketId);
        }

        public async Task DeleteTicketAsync(int ticketId)
        {
            Ticketings ticket = await _ticketRepository.GetTicketByIdAsync(ticketId);
            ticket.IsDelete = true;
            await _ticketRepository.SaveAsync();
        }

        public async Task DeleteTicketAsync(Ticketings ticket)
        {
            ticket.IsDelete = true;
            await _ticketRepository.SaveAsync();
        }

        public async Task RecoverTicketAsync(int ticketId)
        {
            Ticketings ticket = await _ticketRepository.GetTicketByIdAsync(ticketId);
            ticket.IsDelete = false;
            await _ticketRepository.SaveAsync();
        }

        public async Task RecoverTicketAsync(Ticketings ticket)
        {
            ticket.IsDelete = false;
            _ticketRepository.UpdateTicket(ticket);
            await _ticketRepository.SaveAsync();
        }

        public async Task CloseTicketAsync(int ticketId)
        {
            Ticketings ticket = await _ticketRepository.GetTicketByIdAsync(ticketId);
            ticket.IsClose = true;
            await _ticketRepository.SaveAsync();
        }

        public async Task CloseTicketAsync(Ticketings ticket)
        {
            ticket.IsClose = true;
            _ticketRepository.UpdateTicket(ticket);
            await _ticketRepository.SaveAsync();
        }

        public async Task OpenTicketAsync(int ticketId)
        {
            Ticketings ticket = await _ticketRepository.GetTicketByIdAsync(ticketId);
            ticket.IsClose = false;
            await _ticketRepository.SaveAsync();
        }

        public async Task OpenTicketAsync(Ticketings ticket)
        {
            ticket.IsClose = false;
            _ticketRepository.UpdateTicket(ticket);
            await _ticketRepository.SaveAsync();
        }

        #region NotReadedTickets

        public async Task<IList<Ticketings>> GetNotReadedTicketsAsync()
        {
            return await _ticketRepository.GetTicketsAsync(ticket => ticket.IsAnswered != true && ticket.IsDelete != true && ticket.IsClose != true);
        }

        public async Task<int> GetNotReadedTicketsCountAsync()
        {
            IList<Ticketings> ticket = await _ticketRepository.GetTicketsAsync(ticket => ticket.IsAnswered != true && ticket.IsDelete != true && ticket.IsClose != true);
            return ticket.Count();
        }

        public async Task<IList<Ticketings>> FilterNotReadedTicketsAsync(string query, int skip, int take = 30)
        {
            IList<Ticketings> ticket = await _ticketRepository.FilterTicketsAsync(query);
            return ticket
                .Distinct()
                .Where(ticket => ticket.IsAnswered != true && ticket.IsDelete != true && ticket.IsClose != true)
                .OrderByDescending(ticket => ticket.CreatedOn)
                .Skip((skip - 1) * take)
                .Take(take).ToList();
        }

        public async Task<int> FilterNotReadedTicketsCountAsync(string query)
        {
            IList<Ticketings> ticket = await _ticketRepository.FilterTicketsAsync(query);
            return ticket
                .Distinct()
                .Where(ticket => ticket.IsAnswered != true && ticket.IsDelete != true && ticket.IsClose != true).Count();
        }

        #endregion NotReadedTickets

        #region SentTickets

        public async Task<IList<Ticketings>> GetSentTicketsAsync()
        {
            return await _ticketRepository.GetTicketsAsync(ticket => ticket.IsAnswered == true && ticket.IsDelete != true && ticket.IsClose != true);
        }

        public async Task<int> GetSentTicketsCountAsync()
        {
            IList<Ticketings> ticket = await _ticketRepository.GetTicketsAsync(ticket => ticket.IsAnswered == true && ticket.IsDelete != true && ticket.IsClose != true);
            return ticket.Count();
        }

        public async Task<IList<Ticketings>> FilterSentTicketsAsync(string query, int skip, int take = 30)
        {
            IList<Ticketings> ticket = await _ticketRepository.FilterTicketsAsync(query);
            return ticket
                .Distinct()
                .Where(ticket => ticket.IsAnswered == true && ticket.IsDelete != true && ticket.IsClose != true)
                .OrderByDescending(ticket => ticket.CreatedOn)
                .Skip((skip - 1) * take)
                .Take(take).ToList();
        }

        public async Task<int> FilterSentTicketsCountAsync(string query)
        {
            IList<Ticketings> ticket = await _ticketRepository.FilterTicketsAsync(query);
            return ticket
                .Distinct()
                .Where(ticket => ticket.IsAnswered == true && ticket.IsDelete != true && ticket.IsClose != true).Count();
        }

        #endregion SentTickets

        #region DeletedTickets

        public async Task<IList<Ticketings>> GetDeletedTicketsAsync()
        {
            return await _ticketRepository.GetTicketsAsync(ticket => ticket.IsDelete == true);
        }

        public async Task<int> GetDeletedTicketsCountAsync()
        {
            IList<Ticketings> ticket = await _ticketRepository.GetTicketsAsync(ticket => ticket.IsDelete == true);
            return ticket.Count();
        }

        public async Task<IList<Ticketings>> FilterDeletedTicketsAsync(string query, int skip, int take = 30)
        {
            IList<Ticketings> ticket = await _ticketRepository.FilterTicketsAsync(query);
            return ticket
                .Distinct()
                .Where(ticket => ticket.IsDelete == true)
                .OrderByDescending(ticket => ticket.CreatedOn)
                .Skip((skip - 1) * take)
                .Take(take).ToList();
        }

        public async Task<int> FilterDeletedTicketsCountAsync(string query)
        {
            IList<Ticketings> ticket = await _ticketRepository.FilterTicketsAsync(query);
            return ticket
                .Distinct()
                .Where(ticket => ticket.IsDelete == true).Count();
        }

        #endregion DeletedTickets

        #region ClosedTickets

        public async Task<IList<Ticketings>> GetClosedTicketsAsync()
        {
            return await _ticketRepository.GetTicketsAsync(ticket => ticket.IsClose == true && !ticket.IsDelete);
        }

        public async Task<int> GetClosedTicketsCountAsync()
        {
            IList<Ticketings> ticket = await _ticketRepository.GetTicketsAsync(ticket => ticket.IsClose == true && !ticket.IsDelete);
            return ticket.Count();
        }

        public async Task<IList<Ticketings>> FilterClosedTicketsAsync(string query, int skip, int take = 30)
        {
            IList<Ticketings> ticket = await _ticketRepository.FilterTicketsAsync(query);
            return ticket
                .Distinct()
                .Where(ticket => ticket.IsClose == true && !ticket.IsDelete)
                .OrderByDescending(ticket => ticket.CreatedOn)
                .Skip((skip - 1) * take)
                .Take(take).ToList();
        }

        public async Task<int> FilterClosedTicketsCountAsync(string query)
        {
            IList<Ticketings> ticket = await _ticketRepository.FilterTicketsAsync(query);
            return ticket
                .Distinct()
                .Where(ticket => ticket.IsClose == true && !ticket.IsDelete).Count();
        }

        #endregion ClosedTickets

        #endregion Ticketing

        #region TicketMessage

        public async Task<IList<TicketMessages>> GetTicketMessagesAsync(int ticketId)
        {
            IList<TicketMessages> ticketMessage = await _ticketRepository.GetTicketMessagesAsync(message => message.TicketId == ticketId);
            return ticketMessage.OrderBy(message => message.CreatedOn).ToList();
        }

        public async Task<bool> IsTicketReadOrNotAsync(int ticketId)
        {
            return await _ticketRepository.IsTicketMessageExistAsync(message => message.TicketId == ticketId && message.IsSeen == true);
        }

        public async Task ReadAllTicketMessagesAsync(int ticketId)
        {
            IList<TicketMessages> messages = await GetTicketMessagesAsync(ticketId);
            foreach (TicketMessages message in messages)
            {
                if (message.IsSeen == false)
                {
                    message.IsSeen = true;
                    _ticketRepository.UpdateTicketMessage(message);
                    await _ticketRepository.SaveAsync();
                }
            }
        }

        public async Task SendTicketMessageByAdminAsync(TicketMessages ticketMessage)
        {
            await _ticketRepository.AddTicketMessageAsync(ticketMessage);
            await _ticketRepository.SaveAsync();
            //Get Ticket And Change It
            Ticketings ticket = await _ticketRepository.GetTicketByIdAsync(ticketMessage.TicketId);
            ticket.IsDelete = false;
            ticket.IsClose = false;
            ticket.IsAnswered = true;
            _ticketRepository.UpdateTicket(ticket);
            await _ticketRepository.SaveAsync();
        }

        #endregion TicketMessage
    }
}