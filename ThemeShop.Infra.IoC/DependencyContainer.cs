﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.Extensions.DependencyInjection;

using ThemeShop.Application.Interfaces;
using ThemeShop.Application.Services;
using ThemeShop.Application.ViewModels.Public;
using ThemeShop.Application.ViewModels.Ticket;
using ThemeShop.Domain.Interfaces;
using ThemeShop.Infra.Data.Repository;

namespace ThemeShop.Infra.IoC
{
    public class DependencyContainer
    {
        public static void RegisterServices(IServiceCollection services)
        {
            #region Infra Data Layer

            #region Repositories

            services.AddScoped<IAboutUsRepository, AboutUsRepository>();
            services.AddScoped<IAdRepository, AdRepository>();
            services.AddScoped<IArticleRepository, ArticleRepository>();
            services.AddScoped<ICacheSettingRepository, CacheSettingRepository>();
            services.AddScoped<IContactUsRepository, ContactUsRepository>();
            services.AddScoped<IDownloadTokenRepository, DownloadTokenRepository>();
            services.AddScoped<IEmailSettingRepository, EmailSettingRepository>();
            services.AddScoped<INewsLatterRepository, NewsLatterRepository>();
            services.AddScoped<IOrderRepository, OrderRepository>();
            services.AddScoped<IPageRepository, PageRepository>();
            services.AddScoped<IPermissionRepository, PermissionRepository>();
            services.AddScoped<IRoleRepository, RoleRepository>();
            services.AddScoped<ISiteSettingRepository, SiteSettingRepository>();
            services.AddScoped<ISocialNetworkRepository, SocialNetworkRepository>();
            services.AddScoped<IThemeRepository, ThemeRepository>();
            services.AddScoped<ITicketRepository, TicketRepository>();
            services.AddScoped<IReportAbuseRepository, ReportAbuseRepository>();
            //TODO:Later
            //service.AddScoped<IUserEmailSettingRepository, UserEmailSettingRepository>();
            services.AddScoped<IUserPanelRepository, UserPanelRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IWalletRepository, WalletRepository>();

            #endregion Repositories

            #endregion Infra Data Layer

            #region Application Layer

            #region Services

            services.AddScoped<IAboutUsService, AboutUsService>();
            services.AddScoped<IAdService, AdService>();
            services.AddScoped<IArticelService, ArticelService>();
            services.AddScoped<ICacheSettingService, CacheSettingService>();
            services.AddScoped<IContactUsService, ContactUsService>();
            services.AddScoped<IDownloadTokenService, DownloadTokenService>();
            services.AddScoped<INewsLatterService, NewsLatterService>();
            services.AddScoped<IOrderService, OrderService>();
            services.AddScoped<IPageService, PageService>();
            services.AddScoped<IPermissionService, PermissionService>();
            services.AddScoped<IRoleService, RoleService>();
            services.AddScoped<ISiteSettingService, SiteSettingService>();
            services.AddScoped<ISocialNetworkService, SocialNetworkService>();
            services.AddScoped<IThemeService, ThemeService>();
            services.AddScoped<ITicketService, TicketService>();
            services.AddScoped<IUserEmailSettingService, UserEmailSettingService>();
            services.AddScoped<IUserPanelService, UserPanelService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IWalletService, WalletService>();
            services.AddScoped<IReportAbuseService, ReportAbuseService>();

            #endregion Services

            #region ViewModels

            services.AddScoped<PaggingViewModel, PaggingViewModel>();
            services.AddScoped<TicketCountViewModel, TicketCountViewModel>();

            #endregion ViewModels
            #endregion

            #region Core

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddSingleton<IActionContextAccessor, ActionContextAccessor>();
            #endregion Core
        }
    }
}