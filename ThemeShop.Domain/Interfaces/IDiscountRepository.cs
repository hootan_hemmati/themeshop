﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

using ThemeShop.Domain.Models;

namespace ThemeShop.Domain.Interfaces
{
    public interface IDiscountRepository
    {
        #region Discount

        Task<IList<Discounts>> GetAllDiscountsAsync();

        Task<IList<Discounts>> GetDiscountsAsync();

        Task<Discounts> GetDiscountAsync(int discountId);

        Task<int> InsertDiscountAsync(Discounts discounts);

        Task<int> GetDicountCountAsync();

        void UpdateDiscount(Discounts discounts);

        void RemoveDiscount(Discounts discounts);

        #endregion Discount

        #region DiscountLog

        Task InsertDiscountLogAsync(DiscountLogs logs);

        Task<IList<DiscountLogs>> GetDiscountLogsAsync(Expression<Func<DiscountLogs, bool>> where);

        #endregion DiscountLog

        #region DiscountTheme

        Task InsertDiscountSelectedThemeAsync(DiscountSelectedTheme discountTheme);

        void RemoveDiscountTheme(IList<DiscountSelectedTheme> discountSelectedThemes);

        Task<IList<DiscountSelectedTheme>> GetDiscountSelectedThemesAsync(Expression<Func<DiscountSelectedTheme, bool>> where);

        #endregion DiscountTheme

        #region UserDiscount

        Task InsertUserDiscountAsync(UserDiscounts userDiscounts);

        void RemoveDiscountUsers(IList<UserDiscounts> userDiscounts);

        Task<IList<UserDiscounts>> GetUserDiscountsAsync(Expression<Func<UserDiscounts, bool>> where);

        #endregion UserDiscount

        Task SaveAsync();
    }
}