﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

using ThemeShop.Domain.Models;

namespace ThemeShop.Domain.Interfaces
{
    public interface IReportAbuseRepository
    {
        #region ReportAbuse
        Task<IList<ReportAbuse>> GetAllReportAbuseAsync();

        Task<IList<ReportAbuse>> GetReportAbuseAsync(Expression<Func<ReportAbuse, bool>> where);

        Task<ReportAbuse> GetReportAbuseByIdAsync(int id);

        Task<int> GetReportAbuseCountAsync();

        Task<int> GetReportAbuseCountAsync(Expression<Func<ReportAbuse, bool>> where);

        Task<IList<ReportAbuse>> FilterReportAbuseAsync(string query);

        Task<int> FilterReportAbuseCountAsync(string query);

        public void Update(ReportAbuse contact);

        Task AddReportAbuse(ReportAbuse ReportAbuse);

        #endregion

        Task SaveAsync();
    }
}
