﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

using ThemeShop.Domain.Models;

namespace ThemeShop.Domain.Interfaces
{
    public interface ITicketRepository
    {
        #region Ticketing

        Task<IList<Ticketings>> GetAllTicketsAsync();

        Task<int> GetAllTicketsCountAsync();

        Task<IList<Ticketings>> FilterTicketsAsync(string query);

        Task<int> FilterTicketsCountAsync(string query);

        Task AddTicketAsync(Ticketings ticketing);

        Task<Ticketings> GetTicketByIdAsync(int ticketId);

        Task<IList<Ticketings>> GetTicketsAsync(Expression<Func<Ticketings, bool>> where);

        Task<Ticketings> GetTicketAsync(Expression<Func<Ticketings, bool>> where);

        Task<bool> IsTicketExistAsync(Expression<Func<Ticketings, bool>> where);

        void UpdateTicket(Ticketings ticket);

        #endregion Ticketing

        #region TicketMessage

        Task<IList<TicketMessages>> GetAllTicketMessagesByTicketIdAsync(int ticketId);

        Task<IList<TicketMessages>> GetTicketMessagesAsync(Expression<Func<TicketMessages, bool>> where);

        Task<TicketMessages> GetTicketMessageAsync(Expression<Func<TicketMessages, bool>> where);

        Task<bool> IsTicketMessageExistAsync(Expression<Func<TicketMessages, bool>> where);

        Task AddTicketMessageAsync(TicketMessages ticketMessage);

        void UpdateTicketMessage(TicketMessages ticketMessages);

        #endregion TicketMessage

        Task SaveAsync();

        void Save();
    }
}