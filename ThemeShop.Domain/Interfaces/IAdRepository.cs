﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

using ThemeShop.Domain.Models;

namespace ThemeShop.Domain.Interfaces
{
    public interface IAdRepository
    {
        #region Ads

        Task<IList<Ads>> GetAdsAsync();

        Task<int> GetAdsCountAsync();

        Task<int> InsertAdAsync(Ads ad);

        Task<Ads> GetAdAsync(int adId);

        void UpdateAd(Ads ad);

        void RemoveAd(Ads ad);

        Task<IList<Ads>> GetAdsAsync(Expression<Func<Ads, bool>> where);

        #endregion Ads

        #region Ad Theme Categories

        Task InsertAdCategoryAsync(AdThemeCategories adThemeCategories);

        void RemoveAdCategories(IList<AdThemeCategories> categories);

        Task<IList<AdThemeCategories>> GetAdCategoriesAsync(Expression<Func<AdThemeCategories, bool>> where);

        Task SaveAsync();

        void Save();

        #endregion Ad Theme Categories
    }
}