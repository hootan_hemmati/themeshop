﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

using ThemeShop.Domain.Models;

namespace ThemeShop.Domain.Interfaces
{
    public interface IRoleRepository
    {
        #region Roles

        Task<IList<Roles>> GetAllRolesAsync();

        Task<IList<Roles>> FilterRolesAsync(string filterRoles = "");

        Task<int> FilterRolesCountAsync(string filterRoles = "");

        Task<Roles> GetRoleByIdAsync(int roleId);

        Task<Roles> GetRoleByTitleAsync(string roleTitle);

        Task<Roles> GetDefualtRoleAsync();

        void UpdateRole(Roles role);

        Task InsertRoleAsync(Roles role);

        void RemoveRole(Roles role);

        Task RemoveRoleAsync(int roleId);

        Task<bool> IsRoleExistByRoleTitleAsync(string roleTitle);

        Task<bool> IsRoleExistByRoleIdAsync(int roleId);

        #endregion Roles

        #region UserRoles

        Task InsertUserRoleAsync(UserRoles userRole);

        Task<IList<UserRoles>> GetUserRolesAsync(Expression<Func<UserRoles, bool>> where);

        Task<UserRoles> GetUserRoleAsync(Expression<Func<UserRoles, bool>> where);

        Task<bool> IsUserRoleExistAsync(Expression<Func<UserRoles, bool>> where);

        void DeleteUserRole(UserRoles userRole);

        Task DeleteUserRoleByIdAsync(int userRoleId);

        Task<UserRoles> GetUserRoleByIdAsync(int userRoleId);

        #endregion UserRoles

        #region RolesPermissions

        Task<IList<RolePremissions>> GetRolePremissionsAsync(Expression<Func<RolePremissions, bool>> where);

        Task<RolePremissions> GetRolePremissionAsync(Expression<Func<RolePremissions, bool>> where);

        Task<bool> IsRolePremissionAsync(Expression<Func<RolePremissions, bool>> where);

        Task<IList<RolePremissions>> GetAllRolePremissionsAsync();

        Task<IList<RolePremissions>> GetAllRolePremissionsByRoleIdAsync(int roleId);

        Task InsertRolePremissionAsync(RolePremissions rolePremission);

        Task<RolePremissions> GetRolePremissionByIdAsync(int rolePermissionId);

        void DeleteRolePremission(RolePremissions rolePremission);

        Task DeleteRolePremissionByIdAsync(int rolePermissionId);

        #endregion RolesPermissions

        void Save();

        Task SaveAsync();
    }
}