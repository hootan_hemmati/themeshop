﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

using ThemeShop.Domain.Models;

namespace ThemeShop.Domain.Interfaces
{
    public interface IArticleRepository
    {
        #region Article

        Task<Articles> GetArticleAsync(int id);
        Task<IList<Articles>> GetArticlesAsync();


        Task InsertArticleAsync(Articles articles);

        void RemoveArticle(Articles article);

        void UpdateArticle(Articles articles);

        Task<int> GetArticleCountAsync();

        #endregion Article

        #region ArticleCategories

        Task InsertArticleCategoriesAsync(IList<ArticleCategories> articleCategories);
        Task<ArticleCategories> GetArticleCategoriesAsync(int id);

        Task InsertArticleCategoryAsync(ArticleCategories articleCategories);

        void UpdateArticleCategories(ArticleCategories categories);

        void RemoveArticleCategories(IList<ArticleCategories> articleCvategories);


        #endregion ArticleCategories

        #region ArticleCategory
        Task InsertCategoryArticleAsync(ArticleCategory category);

        void UpdateCategoryArticle(ArticleCategory category);

        void RemoveCategoryArticle(ArticleCategory category);

        Task<IList<ArticleCategory>> GetArticleCategoryAsync(Expression<Func<ArticleCategory, bool>> where);
        #endregion

        #region Main Category
        Task InsertMainArticleCategoryAsync(MainArticleCategories mainArticleCategories);

        Task<IList<MainArticleCategories>> GetAllMainArticleCategoriesAsync(int articleId);

        Task<IList<MainArticleCategories>> GetMainArticleCategoriesAsync();

        Task<IList<MainArticleCategories>> GetMainArticleCategoriesWithChildAsync();


        Task<MainArticleCategories> GetMainArticleCategoriesAsync(int categoryId);

        void UpdateMainArticleCategories(MainArticleCategories categories);

        #endregion Main Category

        #region Related

        Task InsertArticleRelatedAsync(IList<ArticleRelateds> articleRelateds);

        Task InsertArticleRelatedAsync(ArticleRelateds articleRelated);

        Task<ArticleRelateds> GetArticleRelatedAsync(int id);

        void RemoveArticleRelated(IList<ArticleRelateds> articleRelateds);

        void RemoveArticleRelated(ArticleRelateds articleRelated);

        Task<IList<ArticleRelateds>> GetArticleRelatedsAsync(Expression<Func<ArticleRelateds, bool>> where);

        #endregion Related

        #region Article Tag
        Task InsertArticleTagAsync(ArticleTags tags);

        Task<IList<ArticleTags>> GetArticleTagsAsync(Expression<Func<ArticleTags, bool>> where);

        void RemoveArticleTag(ArticleTags tags);

        Task<ArticleTags> GetArticleTagsAsync(int id);
        #endregion

        #region Article Comments
        Task<IList<ArticleComments>> GetArticleCommentAsync(Expression<Func<ArticleComments, bool>> where);

        Task<ArticleComments> GetArticleCommentAsync(int id);

        void RemoveArticleComments(ArticleComments articleComments);

        Task InsertArticleCommentsAsync(ArticleComments articleComments);

        void UpdateArticleComments(ArticleComments articleComments);

        #endregion
        Task SaveAsync();
        void Save();
    }
}