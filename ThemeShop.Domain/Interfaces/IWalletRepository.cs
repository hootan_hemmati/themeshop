﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

using ThemeShop.Domain.Models;

namespace ThemeShop.Domain.Interfaces
{
    public interface IWalletRepository
    {
        #region Wallet

        Task InsertWalletAsync(Wallets wallets);

        Task<Wallets> GetWalletsAsync(int id);

        void UpdateWallet(Wallets wallets);

        #endregion Wallet

        #region Wallet Log

        Task InsertWalletLogAsync(WalletLogs walletLogs);

        Task<int> GetWalletLogCountAsync(Expression<Func<WalletLogs, bool>> where);

        Task<IList<WalletLogs>> GetWalletLogsAsync(Expression<Func<WalletLogs, bool>> where);

        Task<WalletLogs> GetWalletLogsAsync(int id);

        Task<IList<WalletLogs>> GetAllWalletLogsAsync();
        Task<IList<WalletLogs>> FilterWalletLogsAsync(string userName, string transactionType, long? minimumPrice, long? maximumPrice, DateTime startDate, DateTime endDate, string description, int pageId = 1, byte take = 20);

        #endregion Wallet Log

        #region Wallet Transactions

        Task<long> CalculateAccouuntBallanceAsync(int walletId);

        Task<IList<long>> CalculateAccouuntChargesAsync(int walletId);

        Task<IList<long>> CalculateAccouuntPaymentsAsync(int walletId);

        #endregion Wallet Transactions

        Task SaveAsync();
    }
}