﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

using ThemeShop.Domain.Models;

namespace ThemeShop.Domain.Interfaces
{
    public interface IPermissionRepository
    {
        #region Permissions

        Task<IList<Permissions>> GetAllPermissionsAsync();

        Task<Permissions> GetPermissionByIdAsync(int permissionId);

        Task<Permissions> GetPermissionByNameAsync(string permissionName);

        Task<IList<Permissions>> GetPermissionsAsync(Expression<Func<Permissions, bool>> where);

        Task<Permissions> GetPermissionAsync(Expression<Func<Permissions, bool>> where);

        Task<bool> IsPermissionExist(Expression<Func<Permissions, bool>> where);

        #endregion Permissions
    }
}