﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

using ThemeShop.Domain.Models;

namespace ThemeShop.Domain.Interfaces
{
    public interface IThemeRepository
    {
        void Save();
        Task<IList<Themes>> GetThemesAsync();

        Task<ThemeCategories> GetCategoryByIdAsync(int categoryId);
        void RemoveThemeSelectedCategories(IList<ThemeSelectedCategories> selectedCategories);

        Task<IList<int>> GetCategoriesIdBythemeIdAsync(int themeId);

        Task AddCategoryThemeAsync(int themeId, int CategoryId);

        Task AddCategoryAsync(ThemeCategories categories);

        void RemoveCategory(ThemeCategories categories);

        void UpdateCategory(ThemeCategories categories);

        void RemoveCategoryFromTheme(IList<ThemeSelectedCategories> selectedCategories);

        Task<IList<ThemeCategories>> GetThemeCategoriesAsync(Expression<Func<ThemeCategories, bool>> where);
        Task<IList<ThemeSelectedCategories>> GetThemeSelectedCategoriesAsync(Expression<Func<ThemeSelectedCategories, bool>> where);

        Task<ThemeCategoriesChilds> GetThemeCategoryParentAsync(int categoryId);

        Task<IList<ThemeCategories>> GetThemeCategoryChildAsync(int parentId);

        Task AddThemeChangeStatusAsync(int themeId, int newStatus, string message);

        Task<Themes> GetThemeByIdAsync(int themeId);

        void UpdateTheme(Themes themes);

        Task<IList<ThemeChangeStatusMessages>> GetThemeChangeStatusMessagesAsync(int themeId);

        Task<IList<ThemeCategories>> GetAllThemeCategoriesAsync();

        Task<IList<Users>> GetSellersAsync();

        Task RemoveTagsFromThemeAsync(int themeId);

        void RemoveThemeTag(ThemeTags tags);

        Task AddTagToThemeAsync(int themeId, string[] tags);

        Task AddThemeAsync(Themes theme);

        Task<bool> CheckRelatedExist(int themeId, int themeRelatedId);

        Task AddRelatedThemeAsync(ThemeRelateds themeRelateds);

        Task SaveAsync();

        Task<IList<ThemeRelateds>> GetRelatedsAsync(Expression<Func<ThemeRelateds, bool>> where, byte page, byte take);

        void DeleteRelatedTheme(ThemeRelateds themeRelateds);

        Task<ThemeRelateds> GetThemeRelated(int themeRelatedId);

        Task InsertThemeGalleryAsync(ThemeGalleries themeGallery);

        void UpdateThemeGallery(ThemeGalleries themeGalleriey);

        Task<IList<ThemeGalleries>> GetThemeGalleriesAsync(Expression<Func<ThemeGalleries, bool>> where);

        void DeleteThemeGallery(ThemeGalleries themeGalleries);
        Task<ThemeGalleries> GetThemeGalleriesAsync(int id);

        Task<IList<ThemeAttachments>> GetThemeAttachmentsAsync(int themeId);

        Task AddThemeAttachmentAsync(ThemeAttachments themeAttachments);

        Task<ThemeAttachments> GetThemeAttachmentByIdAsync(int themeAttachmentsId);

        void DeleteThemeAttachment(ThemeAttachments themeAttachments);

        void UpdateThemeAttachment(ThemeAttachments themeAttachments);

        Task<IList<ThemeOfferTodays>> GetAllOfferAsync(int page);
        Task<IList<ThemeOfferTodays>> GetOffersAsync(Expression<Func<ThemeOfferTodays, bool>> where);

        Task<int> GetOfferCountAsync();
        Task<int> GetOfferCountAsync(Expression<Func<ThemeOfferTodays, bool>> where);

        Task<bool> IsThemeExistAsync(int themeId);

        Task AddOfferAsync(ThemeOfferTodays themeOffer);

        void DeleteThemeOffer(ThemeOfferTodays themeOffer);

        Task<ThemeOfferTodays> GetThemeOfferAsync(int themeOfferId);

        void UpdateThemeOffer(ThemeOfferTodays themeOffer);

        Task<IList<ThemePreviews>> GetThemePreviewsAsync(Expression<Func<ThemePreviews, bool>> where, byte page, byte take);

        Task AddThemePreviewAsync(ThemePreviews previews);

        Task<ThemePreviews> GetThemePreviewsAsync(int themePreviewId);

        void UpdateThemePreview(ThemePreviews previews);

        void DeleteThemePreview(ThemePreviews themePreviews);

        Task<IList<ThemeComments>> GetUnreadCommentsAsync(int page, byte take);
        Task<int> GetUnreadCommentsCountAsync();

        void UpdateComment(ThemeComments comments);

        void UpdateComment(IList<ThemeComments> comments);

        Task<ThemeComments> GetThemeCommentsAsync(int themeCommentId);

        Task<IList<ThemeComments>> GetCommentsbyThemeIdAsync(int themeId, bool showDeleteComments);
        Task<int> GetThemePreviewsCountAsync(Expression<Func<ThemePreviews, bool>> where);
    }
}