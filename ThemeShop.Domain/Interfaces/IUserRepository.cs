﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

using ThemeShop.Domain.Models;

namespace ThemeShop.Domain.Interfaces
{
    public interface IUserRepository
    {
        #region OnlineUser

        Task<IList<OnlineUsers>> GetAllOnlineUsersAsync();

        Task<IList<OnlineUsers>> GetAllTodayOnlineUsersAsync();

        Task InsertOnlineUserAsync(OnlineUsers onlineUser);

        void UpdateOnlineUser(OnlineUsers onlineUser);

        Task<OnlineUsers> FindOnlineUserAsync(int id);

        Task<bool> IsOnlineUserExistByUserNameAsync(string username);

        Task<bool> IsOnlineUserExistByIpAsync(string ip);

        Task<OnlineUsers> FindOnlineUserByUserNameAsync(string userName);

        Task<OnlineUsers> FindOnlineUserByIpAsync(string ip);

        #endregion OnlineUser

        #region Users

        Task<IList<Users>> GetAllUsersAsync();

        Task<int> GetAllUsersCountAsync();

        Task<IList<Users>> FilterUsersAsync(string query, int userSearchMode = 0, int userStatusEnum = 0);

        Task<int> FilterUsersCountAsync(string query, int userSearchMode = 0, int userStatusEnum = 0);

        Task AddUserAsync(Users user);

        void UpdateUser(Users user);

        Task<Users> GetUserByIdAsync(int userId);

        Task<IList<Users>> GetUsersAsync(Expression<Func<Users, bool>> where);

        Task<Users> GetUserAsync(Expression<Func<Users, bool>> where);

        Task<bool> IsUserExistAsync(Expression<Func<Users, bool>> where);

        void DeleteUser(Users user);

        Task DeleteUserByIdAsync(int userId);

        #region Seller

        Task<Tuple<IList<Users>, int>> FillterSellersAsync(string filterEmail = "", string filterFamily = "", string filterUserName = "", int userStatusEnum = 0);

        #endregion Seller

        #endregion Users

        #region UserBank

        Task AddUserBankAsync(UserBanks userBank);

        void UpdateUserBank(UserBanks userBank);

        Task<UserBanks> GetUserBankByIdAsync(int userBankId);

        Task<IList<UserBanks>> GetUsersBanksAsync(Expression<Func<UserBanks, bool>> where);

        Task<UserBanks> GetUserBankAsync(Expression<Func<UserBanks, bool>> where);

        Task<bool> IsUserBankExistAsync(Expression<Func<UserBanks, bool>> where);

        Task InsertUserBankMessageAsync(UserBankMessages bankRejectMessage);

        #endregion UserBank

        void Save();

        Task SaveAsync();
    }
}