﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

using ThemeShop.Domain.Models;

namespace ThemeShop.Domain.Interfaces
{
    public interface IContactUsRepository
    {
        #region GetContactUs

        Task<IList<ContactUs>> GetContactUsAsync();

        Task<IList<ContactUs>> GetContactUsAsync(Expression<Func<ContactUs, bool>> where);

        Task<int> GetContactUsCountAsync();

        Task<int> GetContactUsCountAsync(Expression<Func<ContactUs, bool>> where);

        Task<ContactUs> GetContactUsAsync(int id);

        Task<IList<ContactUs>> FilterContactUsAsync(string query);

        Task<int> FilterContactUsCountAsync(string query);

        #endregion GetContactUs

        Task AddContactUs(ContactUs contactUs);

        void Update(ContactUs contact);

        Task SaveAsync();
    }
}