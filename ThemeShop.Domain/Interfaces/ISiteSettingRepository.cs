﻿using System;
using System.Threading.Tasks;

namespace ThemeShop.Domain.Interfaces
{
    public interface ISiteSettingRepository
    {
        #region GetOrderCount

        Task<int> OrderCountOndayAsync(DateTime time);

        Task<int> OrderCountOnMonthAsync(DateTime time);

        Task<int> OrderCountOnYearsAsync(DateTime time);

        #endregion GetOrderCount
    }
}