﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

using ThemeShop.Domain.Models;

namespace ThemeShop.Domain.Interfaces
{
    public interface IOrderRepository
    {
        Task<IList<TemplateInfoes>> GetAllUserPaymentsAsync();
        Task<IList<OrderDetails>> GetOrderDetailsAsync(Expression<Func<OrderDetails, bool>> where, byte page, byte take);
        Task<int> GetOrderDetailsCountAsync(Expression<Func<OrderDetails, bool>> where);
    }
}