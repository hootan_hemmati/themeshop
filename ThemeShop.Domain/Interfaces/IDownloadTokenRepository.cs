﻿using System.Collections.Generic;
using System.Threading.Tasks;

using ThemeShop.Domain.Models;

namespace ThemeShop.Domain.Interfaces
{
    public interface IDownloadTokenRepository
    {
        Task<IList<DownloadTokens>> GetDownloadTokensAsync();

        Task InsertDownloadTokenAsync(DownloadTokens downloadToken);

        Task<bool> IsExpireAsync(int themeId, int userId);

        Task<string> GetSingleTokenAsync(int themeId, int userId);

        void Save();

        Task SaveAsync();
    }
}