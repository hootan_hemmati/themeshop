﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ThemeShop.Domain.Models
{
    // جدول واسط
    public partial class RolePremissions
    {
        public RolePremissions()
        {
                
        }

        [Key]
        public int RolePermissionsId { get; set; }

        [Display(Name = "نقش")]
        public int RoleId { get; set; }

        [Display(Name = "دسترسی")]
        public int PermissionId { get; set; }

        public virtual Permissions Permission { get; set; }
        public virtual Roles Role { get; set; }
    }
}