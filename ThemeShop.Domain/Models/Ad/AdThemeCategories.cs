﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ThemeShop.Domain.Models
{
    public partial class AdThemeCategories
    {
        public AdThemeCategories()
        {

        }

        [Key]
        public int AdThemeCategoryId { get; set; }

        [Required]
        public int AdId { get; set; }

        [Required]
        public int ThemeCategoryId { get; set; }

        public virtual Ads Ad { get; set; }
        public virtual ThemeCategories ThemeCategory { get; set; }
    }
}