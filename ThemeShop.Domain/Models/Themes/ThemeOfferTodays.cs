﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ThemeShop.Domain.Models
{
    public partial class ThemeOfferTodays
    {
        public ThemeOfferTodays()
        {

        }

        [Key]
        public int OfferId { get; set; }

        [Display(Name = "کد قالب")]
        [Required(ErrorMessage = "لطفا {0} را وارد کنید")]
        public int ThemeId { get; set; }

        [Display(Name = "تاریخ شروع نمایش")]
        [Required(ErrorMessage = "لطفا {0} را وارد کنید")]
        [DisplayFormat(DataFormatString = "{0: yyyy/MM/dd}")]
        public DateTime StartDate { get; set; }

        [Display(Name = "تاریخ پایان نمایش")]
        [Required(ErrorMessage = "لطفا {0} را وارد کنید")]
        [DisplayFormat(DataFormatString = "{0: yyyy/MM/dd}")]
        public DateTime EndDate { get; set; }

        [Display(Name ="تعداد نمایش")]
        public int VisitCount { get; set; }

        [Display(Name ="فعال")]
        public bool IsActive { get; set; }

        public virtual Themes Theme { get; set; }
    }
}