﻿using Microsoft.EntityFrameworkCore;

using ThemeShop.Infra.Data.Context;

namespace ThemeShop.Test.Infra.Data
{
    public class ThemeShopTestContext
    {
        public static ThemeShop_DBContext GetContext()
        {
            DbContextOptions<ThemeShop_DBContext> options = new DbContextOptionsBuilder<ThemeShop_DBContext>().UseSqlServer("Data Source=46.4.37.226\\MSSQLSERVER2019,51019;Initial Catalog=Themeshop_DB;User ID=themeshopcore;Password=8jJnt00?;MultipleActiveResultSets=true").Options;
            ThemeShop_DBContext _context = new ThemeShop_DBContext(options);
            return _context;
        }
    }
}
