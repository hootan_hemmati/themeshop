﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

using ThemeShop.Application.Interfaces;
using ThemeShop.Application.Services;
using ThemeShop.Domain.Interfaces;
using ThemeShop.Infra.Data.Context;
using ThemeShop.Infra.Data.Repository;

namespace ThemeShop.Test.IoC
{
    public class DependencyContainerTest
    {

        public T Service<T>()
        {
            ServiceCollection services = new ServiceCollection();
            #region Application Layer
            services.AddScoped<IAboutUsService, AboutUsService>();
            services.AddScoped<IAdService, AdService>();
            services.AddScoped<IArticelService, ArticelService>();
            services.AddScoped<ICacheSettingService, CacheSettingService>();
            services.AddScoped<IContactUsService, ContactUsService>();
            services.AddScoped<IDownloadTokenService, DownloadTokenService>();
            services.AddScoped<INewsLatterService, NewsLatterService>();
            services.AddScoped<IOrderService, OrderService>();
            services.AddScoped<IPageService, PageService>();
            services.AddScoped<IPermissionService, PermissionService>();
            services.AddScoped<IRoleService, RoleService>();
            services.AddScoped<ISiteSettingService, SiteSettingService>();
            services.AddScoped<ISocialNetworkService, SocialNetworkService>();
            services.AddScoped<IThemeService, ThemeService>();
            services.AddScoped<ITicketService, TicketService>();
            services.AddScoped<IUserEmailSettingService, UserEmailSettingService>();
            services.AddScoped<IUserPanelService, UserPanelService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IWalletService, WalletService>();
            #endregion


            #region Infra Data Layer
            services.AddScoped<IAboutUsRepository, AboutUsRepository>();
            services.AddScoped<IAdRepository, AdRepository>();
            services.AddScoped<IArticleRepository, ArticleRepository>();
            services.AddScoped<ICacheSettingRepository, CacheSettingRepository>();
            services.AddScoped<IContactUsRepository, ContactUsRepository>();
            services.AddScoped<IDownloadTokenRepository, DownloadTokenRepository>();
            services.AddScoped<IEmailSettingRepository, EmailSettingRepository>();
            services.AddScoped<INewsLatterRepository, NewsLatterRepository>();
            services.AddScoped<IOrderRepository, OrderRepository>();
            services.AddScoped<IPageRepository, PageRepository>();
            services.AddScoped<IPermissionRepository, PermissionRepository>();
            services.AddScoped<IRoleRepository, RoleRepository>();
            services.AddScoped<ISiteSettingRepository, SiteSettingRepository>();
            services.AddScoped<ISocialNetworkRepository, SocialNetworkRepository>();
            services.AddScoped<IThemeRepository, ThemeRepository>();
            services.AddScoped<ITicketRepository, TicketRepository>();
            //TODO:Later
            //service.AddScoped<IUserEmailSettingRepository, UserEmailSettingRepository>();
            services.AddScoped<IUserPanelRepository, UserPanelRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IWalletRepository, WalletRepository>();
            #endregion

            #region Database
            services.AddDbContext<ThemeShop_DBContext>(option =>
            {
                option.UseSqlServer("Data Source=.;Initial Catalog=ThemeShop_DB;User ID=sa;Password=hootanHT1376;MultipleActiveResultSets=true");
            });
            #endregion

            ServiceProvider s = services.BuildServiceProvider();
            return services.BuildServiceProvider().GetService<T>();

        }
    }
}
